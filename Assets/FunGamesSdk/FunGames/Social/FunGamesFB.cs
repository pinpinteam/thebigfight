using System;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using FunGamesSdk.FunGames.Analytics.Helpers;
using UnityEngine;

public class FunGamesFB
{
    // Start is called on the start of FunGamesSocial
    internal static void Start()
    {
        Debug.Log("Initialize FaceBook");
        FacebookHelpers.Initialize();
    }

    public static void ShareOnFB(FacebookDelegate<IShareResult> ShareCallback)
    {
        FB.ShareLink(
            new Uri("https://developers.facebook.com/"),
            callback: ShareCallback
        );

    }

    public static void LogInFB(FacebookDelegate<ILoginResult> AuthCallback)
    {
        var perms = new List<string>() { "public_profile", "email" };
        FB.LogInWithReadPermissions(perms, AuthCallback);
    }
}
