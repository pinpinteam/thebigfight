/*
using System.Collections;
using FunGames.Sdk;
using UnityEngine;

namespace FunGamesSdk.FunGames.Gdpr
{
    public class FunGamesGdpr : MonoBehaviour
    {
        private const string AdsPersonalizationConsent = "Ads";
        void Awake()
        {
            var settings = FunGamesSettings.settings;

            if (settings.useGdpr == false)
            {
                return;
            }
            
            StartCoroutine(ShowGDPRConsentDialogAndWait());
            
            MaxSdkCallbacks.OnSdkInitializedEvent += sdkConfiguration => {
                if (sdkConfiguration.ConsentDialogState == MaxSdkBase.ConsentDialogState.Applies)
                {
                    StartCoroutine(ShowGDPRConsentDialogAndWait());
                }
                else if (sdkConfiguration.ConsentDialogState == MaxSdkBase.ConsentDialogState.DoesNotApply)
                {
                    MaxSdk.SetHasUserConsent(false);
                }
                else
                {
                    MaxSdk.SetHasUserConsent(false);
                }
            };
        }

        private IEnumerator ShowGDPRConsentDialogAndWait()
        {
            var settings = FunGamesSettings.settings;

            yield return SimpleGDPR.WaitForDialog( new GDPRConsentDialog().
                AddSectionWithToggle( AdsPersonalizationConsent, "Ads Personalization", "When enabled, you'll see ads that are more relevant to you. Otherwise, you will still receive ads, but they will no longer be tailored toward you." ).
                AddPrivacyPolicies( "https://policies.google.com/privacy",  settings.gdprUrl) );

            if( SimpleGDPR.GetConsentState( AdsPersonalizationConsent ) == SimpleGDPR.ConsentState.Yes )
            {
                MaxSdk.SetHasUserConsent(true);
            }
            else
            {
                MaxSdk.SetHasUserConsent(false);
            }
        }
    }
}
*/
