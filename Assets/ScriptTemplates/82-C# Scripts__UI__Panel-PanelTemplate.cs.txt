﻿using Pinpin.UI;
using UnityEngine;
using UnityEngine.UI;
using Pinpin.Helpers;

namespace Pinpin.Scene.MainScene.UI
{

	public sealed class #SCRIPTNAME# : AUIPanel
	{
		
		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}

	}
}