﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin.UI
{
	public class UIGaugeObject : MonoBehaviour
	{

		public delegate float UICompassObjectTracker();

		protected UIGauge m_compass;
		protected RectTransform m_rect;
		protected float m_ratio;
		protected int m_order;
		protected UICompassObjectTracker m_getter;


		public UIGauge compass { get { return m_compass; } }
		public RectTransform rect { get { return m_rect; } }
		public float ratio { get { return m_ratio; } }

		public int order
		{
			get { return m_order; }
			set
			{
				if (m_order != value)
				{
					if (value == -1)
						transform.SetAsLastSibling();
					else
						transform.SetSiblingIndex(value);
					m_order = value;
				}
			}
		}

		public bool isTracked { get { return m_getter != null; } }

		protected virtual void OnEnable()
		{
			Init();
		}

		protected virtual void OnDestroy()
		{
			m_compass.Unregister(this);
		}

		public virtual void Init()
		{
			if (m_compass == null)
				m_compass = GetComponentInParent<UIGauge>();

			if (m_rect == null)
				m_rect = GetComponent<RectTransform>();

			if (m_compass == null || m_rect == null)
				return;

			if (!m_compass.Register(this))
				return;

			m_order = transform.GetSiblingIndex();

		}

		public virtual void Setup(int sibilingIndex = -1, UICompassObjectTracker getter = null)
		{
			order = sibilingIndex;
			m_getter = getter;
		}

		protected virtual void Update()
		{

		}

		protected virtual void LateUpdate()
		{
#if UNITY_EDITOR
			if ((m_compass == null || m_rect == null) && runInEditMode)
			{
				OnEnable();
				Update();
			}
#endif
		}
	}
}
