﻿using UnityEngine;
using UnityEngine.UI;

public class AutoAnimateTextColor : MonoBehaviour
{
	[SerializeField] private Text text;
	[SerializeField] private Color[] colors;
	[SerializeField] private AnimationCurve curve;
	[SerializeField] private float speed;

	void Update()
	{
		Color c = Color.Lerp(colors[0], colors[1], curve.Evaluate(Time.time * speed));
		text.color = c;
	}
}
