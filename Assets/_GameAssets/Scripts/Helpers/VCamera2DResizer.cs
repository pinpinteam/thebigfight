using UnityEngine;
using Cinemachine;

namespace Pinpin
{
	[ExecuteInEditMode]
	public class VCamera2DResizer : MonoBehaviour
	{
		public enum ResizeMode
		{
			None,
			Zoom
		}

		[SerializeField] private float targetOrthographicSize = 5f;
		[SerializeField] private Vector2 targetAspectRatio = new Vector2(16, 9);
		[SerializeField] private ResizeMode largestResizeMode;
		[SerializeField] private ResizeMode narrowResizeMode;
		[SerializeField] private bool inAnimation;
		[SerializeField] private new CinemachineVirtualCamera camera;
		private bool ortographicSizeChanged;

		private Vector2Int screenSize { get; set; }
		private int ScreenWidth
		{
			get
			{
#if UNITY_EDITOR
				return (int)UnityEditor.Handles.GetMainGameViewSize().x;
#else
				return Screen.width;
#endif
			}
		}
		private int ScreenHeight
		{
			get
			{
#if UNITY_EDITOR
				return (int)UnityEditor.Handles.GetMainGameViewSize().y;
#else
				return Screen.height;
#endif
			}
		}

		private void Awake ()
		{
			OnScreenSizeChanged();
		}

		private void OnScreenSizeChanged ()
		{
			ortographicSizeChanged = false;
			if (Screen.height == 0)
				return;

			float currentRatio = Screen.width / (float)Screen.height;
			float targetRatio = targetAspectRatio.x / targetAspectRatio.y;

			this.DefaultSize();
			if (Mathf.Approximately(currentRatio, targetRatio))
				return;

			if (currentRatio > targetRatio)
				this.LargestResize(currentRatio, targetRatio);
			else
				this.NarrowResize(currentRatio, targetRatio);

			screenSize = new Vector2Int(ScreenWidth, ScreenHeight);
		}

		private void NarrowResize ( float currentRatio, float targetRatio )
		{
			float delta = currentRatio / targetRatio;
			switch (this.narrowResizeMode)
			{
				case ResizeMode.Zoom:
					this.camera.m_Lens.OrthographicSize = targetOrthographicSize / delta;
					break;
			}
		}

		private void LargestResize ( float currentRatio, float targetRatio )
		{
			float delta = targetRatio / currentRatio;
			switch (this.largestResizeMode)
			{
				case ResizeMode.Zoom:
					this.camera.m_Lens.OrthographicSize = targetOrthographicSize * delta;
					break;
			}
		}

		public void SetOrtographicSize ( float ortographicSize )
		{
			targetOrthographicSize = ortographicSize;
			ortographicSizeChanged = true;
		}

		public float GetOrtographicSize ()
		{
			return targetOrthographicSize;
		}

		private void DefaultSize ()
		{
			this.camera.m_Lens.OrthographicSize = targetOrthographicSize;
		}

		private void Reset ()
		{
			if (camera == null)
			{
				camera = GetComponent<CinemachineVirtualCamera>();
				targetOrthographicSize = camera.m_Lens.OrthographicSize;
			}
		}


		private void OnValidate ()
		{
			if (!ApplicationManager.IsGameObjectPartOfCurrentPrefabOrScene(gameObject))
				return;

			this.OnScreenSizeChanged();
		}

		private void OnDestroy ()
		{
			if (camera != null)
			{
				camera.m_Lens.OrthographicSize = targetOrthographicSize;
			}
		}

		private void Update ()
		{
			if (screenSize.x != ScreenWidth || screenSize.y != ScreenHeight || inAnimation || ortographicSizeChanged)
				this.OnScreenSizeChanged();
		}
	}
}