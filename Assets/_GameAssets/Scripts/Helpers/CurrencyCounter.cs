﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using Pinpin.Helpers;

namespace Pinpin
{
	public class CurrencyCounter : MonoBehaviour
	{
		[SerializeField] private float m_minCurrencyToBurstUpdate = 3f;
		[SerializeField] private RectTransform m_iconRect;
		[SerializeField] private Text m_counterText;

		[Header("SetVisible")]
		[SerializeField] private CanvasGroup m_canvasGroup;
		[SerializeField] private RectTransform m_rectTransformParent;
		[SerializeField] private Vector2 m_hiddenOffset;
		[SerializeField] private float m_moveDuration = 0.5f;
		[SerializeField] private bool m_useFade = false;
		[SerializeField] private bool m_useEndBurst = false;
		[SerializeField] private bool m_useDynamicOffset = false;

		[Header("Burst")]
		[SerializeField] private ParticleSystem m_burstParticleSystem;
		[SerializeField] private Vector3 m_burstNewScale;
		[SerializeField] private float m_burstDuration;

		[Header("Loop")]
		[SerializeField] private ParticleSystem m_loopParticleSystem;
		[SerializeField] private Vector3 m_loopNewScale;
		[SerializeField] private float m_loopDuration;

		[Header("Shake")]
		[SerializeField] private RectTransform m_shakeParent;
		[SerializeField] private float m_shakeForceX = 20f;
		[SerializeField] private AnimationCurve m_shakeCurveX;
		[SerializeField] private float m_shakeDuration = 1f;
		[SerializeField] private Color m_shakeColor;

		private Vector3[] m_anchorPosArray;
		private Vector3[] AnchorPosArray
		{
			get
			{
				if (m_anchorPosArray == null)
				{
					Init();
				}
				return m_anchorPosArray;
			}
		}

		private Tweener m_fadeTween;
		private Tweener m_parentVisibleTween;
		private Tweener m_textLoopTween;
		private Tweener m_IconLoopTween;
		private Tweener m_shakeTween;
		private Tweener m_shakeColorTween;
		private Tweener m_burstTextTween;
		private Tweener m_burstIconTween;

		private float m_currencyInc;
		private float m_currencyStart;
		private float m_currencyFinal;
		private float m_currentCurrency;
		private float m_t = 99f;
		private float m_timer;

		private void Awake ()
		{
			Init();
		}

		public void Init ()
		{
			m_anchorPosArray = new Vector3[2];
			m_anchorPosArray[0] = m_rectTransformParent.anchoredPosition;
			m_anchorPosArray[1] = m_rectTransformParent.anchoredPosition + m_hiddenOffset;
			m_t = 99f;
		}

		void ResetAll ()
		{
			if (m_loopParticleSystem != null)
				m_loopParticleSystem.Stop();

			m_burstTextTween.Pause();
			m_burstTextTween.Kill();
			m_burstIconTween.Pause();
			m_burstIconTween.Kill();

			m_textLoopTween.Pause();
			m_textLoopTween.Kill();
			m_IconLoopTween.Pause();
			m_IconLoopTween.Kill();
			m_shakeTween.Pause();
			m_shakeTween.Kill();
			m_shakeColorTween.Pause();
			m_shakeColorTween.Kill();
			m_iconRect.localScale = Vector3.one;
			m_counterText.rectTransform.localScale = Vector3.one;
			m_counterText.color = Color.white;
		}

		public void SetCurrencyInstant ( ulong currency )
		{
			m_currentCurrency = (float)currency;
			m_counterText.text = MathHelper.ConvertToEngineeringNotation(currency, 3);
		}

		public void SetCurrencyInstant ( string prefix, float currency, string stringFormat )
		{
			m_currentCurrency = currency;
			m_counterText.text = prefix + currency.ToString(stringFormat);
		}

		public void ResetToHiddenPos ()
		{
			m_rectTransformParent.anchoredPosition = AnchorPosArray[0];
			if (m_useFade)
				m_canvasGroup.alpha = 0f;
		}

		public void SetVisible ( bool visible )
		{
			m_parentVisibleTween.Pause();
			m_parentVisibleTween.Kill();

			if (!m_useDynamicOffset || visible)
			{
				m_parentVisibleTween = m_rectTransformParent.DOAnchorPos(visible ? AnchorPosArray[1] : AnchorPosArray[0], m_moveDuration);
			}
			else
			{
				Vector3 newPos = AnchorPosArray[1] - (Vector3.right * m_shakeParent.sizeDelta.x * (m_hiddenOffset.x > 0 ? 1 : -1));
				m_parentVisibleTween = m_rectTransformParent.DOAnchorPos(newPos, m_moveDuration);
			}

			if (m_useFade)
			{
				m_fadeTween.Pause();
				m_fadeTween.Kill();
				m_fadeTween = m_canvasGroup.DOFade(visible ? 1f : 0f, m_moveDuration);
			}
		}

		public void BurstFx ( bool withPS )
		{
			ResetAll();

			
			m_burstIconTween = m_iconRect.DOPunchScale(m_burstNewScale, m_burstDuration);
			m_burstTextTween = m_counterText.rectTransform.DOPunchScale(m_burstNewScale, m_burstDuration);

			m_burstParticleSystem.Stop();
			if (withPS)
				m_burstParticleSystem.Play();
		}

		public void StartLoopFx ( bool withPS )
		{
			if (m_loopParticleSystem != null)
			{
				m_loopParticleSystem.Stop();
				if (withPS)
					m_loopParticleSystem.Play();
			}

			m_textLoopTween.Pause();
			m_textLoopTween.Kill();
			m_IconLoopTween.Pause();
			m_IconLoopTween.Kill();

			m_textLoopTween = m_counterText.rectTransform.DOScale(m_loopNewScale, m_loopDuration).SetLoops(-1, LoopType.Yoyo);
			m_IconLoopTween = m_iconRect.DOScale(m_loopNewScale, m_loopDuration).SetLoops(-1, LoopType.Yoyo);
		}

		public void EndLoopFx ()
		{
			ResetAll();
		}

		public void ShakeFx () //when not enough money
		{
			EndLoopFx();
			m_shakeTween = m_shakeParent.DOAnchorPosX(m_shakeForceX, m_shakeDuration).SetEase(m_shakeCurveX);
			m_counterText.color = m_shakeColor;
			m_shakeColorTween = m_counterText.DOColor(Color.white, m_shakeDuration);
		}

		#region TextAnimation
		public void UpdateDisplay ( ulong oldCoin, ulong currentCoin )
		{
			ResetAll();

			m_currencyStart = (float)oldCoin;
			m_currencyFinal = (float)currentCoin;
			m_currencyInc = (float)currentCoin - (float)oldCoin;
			m_timer = 1.75f;

			//is Instant Burst ?
			if (Mathf.Abs(m_currencyInc) <= m_minCurrencyToBurstUpdate)
			{
				if (m_t < m_timer) //a coroutine is already running
				{
					//Stop the current coroutine and set the text to the final currencyState
					m_t = 99f;
				}
				else
				{
					m_counterText.text = MathHelper.ConvertToEngineeringNotation((ulong)m_currencyFinal, 3);
				}
				BurstFx(m_currencyInc > 0);
			}
			else
			{
				if (m_t < m_timer) //a coroutine is already running
				{
					m_t = 0f;

					if (m_currencyInc > 0)
					{
						StartLoopFx(true);
					}
					else //lose currency
					{
						StartLoopFx(false);
						m_timer = 1f; //anim faster 
					}
				}
				else
				{
					m_t = 0f;

					if (m_currencyInc > 0)
					{
						StartLoopFx(true);
					}
					else //lose currency
					{
						StartLoopFx(false);
						m_timer = 1f; //anim faster 
					}

					StartCoroutine(UpdateUlongCounterCoroutine());
				}
			}
		}
		IEnumerator UpdateUlongCounterCoroutine ()
		{
			while (m_t < m_timer)
			{
				m_currentCurrency = (ulong)(m_currencyStart + (m_currencyInc * (m_t / m_timer)));
				m_counterText.text = MathHelper.ConvertToEngineeringNotation((ulong)m_currentCurrency, 3);

				yield return null;
				m_t += Time.deltaTime;
			}

			m_t = 99f;
			m_counterText.text = MathHelper.ConvertToEngineeringNotation((ulong)m_currencyFinal, 3);
			EndLoopFx();

			if (m_useEndBurst)
				BurstFx(true);
		}

		public void UpdateDisplay ( string prefix, float oldCoin, float currentCoin, string stringFormat )
		{
			ResetAll();

			m_currencyStart = oldCoin;
			m_currencyFinal = currentCoin;
			m_currencyInc = currentCoin - oldCoin;
			m_timer = 1.75f;

			//is Instant Burst ?
			if (Mathf.Abs(m_currencyInc) <= m_minCurrencyToBurstUpdate)
			{
				if (m_t < m_timer) //a coroutine is already running
				{
					//Stop the current coroutine and set the text to the final currencyState
					m_t = 99f;
				}
				else
				{
					m_counterText.text = prefix + m_currencyFinal.ToString(stringFormat);
				}
				BurstFx(m_currencyInc > 0);
			}
			else
			{
				if (m_t < m_timer) //a coroutine is already running
				{
					m_t = 0f;

					if (m_currencyInc > 0)
					{
						StartLoopFx(true);
					}
					else //lose currency
					{
						StartLoopFx(false);
						m_timer = 1f; //anim faster 
					}
				}
				else
				{
					m_t = 0f;

					if (m_currencyInc > 0)
					{
						StartLoopFx(true);
					}
					else //lose currency
					{
						StartLoopFx(false);
						m_timer = 1f; //anim faster 
					}

					StartCoroutine(UpdateFloatCounterCoroutine(prefix, stringFormat));
				}
			}
		}

		IEnumerator UpdateFloatCounterCoroutine ( string prefix, string stringFormat )
		{
			while (m_t < m_timer)
			{
				m_currentCurrency = (m_currencyStart + (m_currencyInc * (m_t / m_timer)));
				m_counterText.text = prefix + m_currentCurrency.ToString(stringFormat);

				yield return null;
				m_t += Time.deltaTime;
			}

			m_t = 99f;
			m_counterText.text = prefix + m_currencyFinal.ToString(stringFormat);
			EndLoopFx();

			if (m_useEndBurst)
				BurstFx(true);
		}


		#endregion
	}
}
