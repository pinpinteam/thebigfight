﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RectTransformPositionsLerp : MonoBehaviour
{
	[SerializeField] private RectTransform m_rect;
	[SerializeField] private Vector2[] m_poses;
	[SerializeField] private float m_damp = 3f;
	[SerializeField] private float m_minOffsetToValidate = 0.05f;

	private float m_currentOffset = 999f;
	private int m_currentIndex = -1;

	private bool m_isInLerp;

	public void SetPos(int index,bool instant)
	{
		if(instant)
		{
			StopAllCoroutines();
			m_isInLerp = false;
			m_currentIndex = index;
			m_currentOffset = 999f;
			m_rect.anchoredPosition = m_poses[index];
		}
		else if (m_currentIndex != index)
		{
			if (!gameObject.activeInHierarchy)
				return;

			m_currentIndex = index;
			m_currentOffset = 999f;

			if (!m_isInLerp)
			{
				StartCoroutine(MoveCoroutine());
			}
		}
	}

	IEnumerator MoveCoroutine()
	{
		m_isInLerp = true;

		while (m_currentOffset > m_minOffsetToValidate)
		{
			m_currentOffset = Vector2.Distance(m_rect.anchoredPosition, m_poses[m_currentIndex]);
			m_rect.anchoredPosition = Vector2.Lerp(m_rect.anchoredPosition, m_poses[m_currentIndex], m_damp * Time.deltaTime);
			yield return null;
		}

		m_isInLerp = false;

		m_rect.anchoredPosition = m_poses[m_currentIndex];
	}

	private void OnDisable ()
	{
		StopAllCoroutines();
		if(m_isInLerp)
		{
			m_isInLerp = false;
			m_rect.anchoredPosition = m_poses[m_currentIndex];
		}
	}
}
