﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using UICompassMarkerTracker = Pinpin.UI.UIGaugeObject.UICompassObjectTracker;

namespace Pinpin.UI
{

	[ExecuteInEditMode]
	public class UIGauge : MonoBehaviour
	{
		public enum EAxis
		{
			Horizontal,
			vertical
		}

		public enum EDirection
		{
			Default,
			Invert
		}

		[SerializeField] private RectTransform m_container;

		public EAxis axis;
		public EDirection direction;
		public float min;
		public float max;

		private float m_rectMax;
		private float m_size = -1;

		public float size { get { return m_size; } }

		protected List<UIGaugeObject> m_objects = new List<UIGaugeObject>();

		protected virtual void OnEnable()
		{
			Init();
		}

		public virtual void Init()
		{
			if (m_container == null)
				return;

			m_size = m_container.rect.size[(int)axis];

			if (m_objects == null || m_objects.Count <= 0)
				return;

			foreach (UIGaugeObject marker in m_objects)
				marker.Init();
		}



		public virtual UIGaugeObject AddMarker(UIGaugeObject marker, int order = -1, UICompassMarkerTracker getter = null)
		{
			UIGaugeObject markerInstance;

			markerInstance = Instantiate<UIGaugeObject>(marker, m_container);
			markerInstance.Setup(order, getter);

			m_objects.Add(markerInstance);

			return markerInstance;
		}

		public virtual bool Register(UIGaugeObject marker)
		{
			if (ReferenceEquals(marker.compass, this) &&
				!m_objects.Contains(marker))
			{
				m_objects.Add(marker);
				return true;
			}

			return false;
		}

		public virtual void Remove(UIGaugeObject marker)
		{
			if (m_objects.Contains(marker))
			{
				Destroy(marker.gameObject);
				m_objects.Remove(marker);
			}
		}

		public virtual void Unregister(UIGaugeObject marker)
		{
			if (m_objects.Contains(marker))
				m_objects.Remove(marker);
		}

		public virtual void Clear(Type markerType)
		{
			for (int i = m_objects.Count - 1; i >= 0; i--)
			{
				if (m_objects[i].GetType() == markerType)
				{
					Destroy(m_objects[i].gameObject);
					m_objects.RemoveAt(i);
				}
			}
		}

		public virtual void Clear()
		{
			for (int i = m_objects.Count - 1; i >= 0; i--)
			{
				Destroy(m_objects[i].gameObject);
				m_objects.RemoveAt(i);
			}
		}
	}
}
