﻿using UnityEngine;
namespace Pinpin
{
	[ExecuteInEditMode]
	public class Camera2DResizer : MonoBehaviour
	{

		public enum ResizeMode
		{
			None,
			Crop,
			Zoom
		}

		[SerializeField] private float targetOrthographicSize = 5f;
		[SerializeField] private Vector2 targetAspectRatio = new Vector2(9, 16);
		[SerializeField] private ResizeMode largestResizeMode;
		[SerializeField] private ResizeMode narrowResizeMode;
		[SerializeField] private bool inAnimation;
		[SerializeField] private new Camera camera;
		private bool ortographicSizeChanged;

		private Vector2Int screenSize { get; set; }
		private int ScreenWidth
		{
			get
			{
#if UNITY_EDITOR
				return (int)UnityEditor.Handles.GetMainGameViewSize().x;
#else
				return Screen.width;
#endif
			}
		}
		private int ScreenHeight
		{
			get
			{
#if UNITY_EDITOR
				return (int)UnityEditor.Handles.GetMainGameViewSize().y;
#else
				return Screen.height;
#endif
			}
		}

		private void Awake ()
		{
			OnScreenSizeChanged();
		}

		private void OnScreenSizeChanged ()
		{
			ortographicSizeChanged = false;
			if (Screen.height == 0)
				return;

			float currentRatio = ScreenWidth / (float)ScreenHeight;
			float targetRatio = targetAspectRatio.x / targetAspectRatio.y;

			DefaultSize();
			if (Mathf.Approximately(currentRatio, targetRatio))
				return;

			if (currentRatio > targetRatio)
				LargestResize(currentRatio, targetRatio);
			else
				NarrowResize(currentRatio, targetRatio);

			screenSize = new Vector2Int(ScreenWidth, ScreenHeight);
		}

		private void NarrowResize ( float currentRatio, float targetRatio )
		{
			float delta = currentRatio / targetRatio;
			switch (narrowResizeMode)
			{

				case ResizeMode.Crop:
					float yOffset = (1f - delta) / 2f;
					camera.rect = new Rect(0, yOffset, 1, delta);
					break;

				case ResizeMode.Zoom:
					camera.orthographicSize = targetOrthographicSize / delta;
					break;
			}
		}

		private void LargestResize ( float currentRatio, float targetRatio )
		{
			float delta = targetRatio / currentRatio;
			switch (largestResizeMode)
			{

				case ResizeMode.Crop:
					float xOffset = (1f - delta) / 2f;
					camera.rect = new Rect(xOffset, 0, delta, 1);
					break;

				case ResizeMode.Zoom:
					camera.orthographicSize = targetOrthographicSize * delta;
					break;
			}
		}

		public void SetOrtographicSize ( float ortographicSize )
		{
			targetOrthographicSize = ortographicSize;
			ortographicSizeChanged = true;
		}

		public float GetOrtographicSize ()
		{
			return targetOrthographicSize;
		}

		private void DefaultSize ()
		{
			camera.rect = new Rect(0, 0, 1, 1);
			camera.orthographicSize = targetOrthographicSize;
		}

		private void Reset ()
		{
			if (camera == null)
			{
				camera = GetComponent<UnityEngine.Camera>();
				targetOrthographicSize = camera.orthographicSize;
			}
		}

		private void OnValidate ()
		{
			if (!ApplicationManager.IsGameObjectPartOfCurrentPrefabOrScene(gameObject))
				return;

			OnScreenSizeChanged();
		}

		private void OnDestroy ()
		{
			if (camera != null)
			{
				camera.orthographicSize = targetOrthographicSize;
			}
		}

		private void Update ()
		{
			if (screenSize.x != ScreenWidth || screenSize.y != ScreenHeight || inAnimation || ortographicSizeChanged)
				OnScreenSizeChanged();
		}
	}
}