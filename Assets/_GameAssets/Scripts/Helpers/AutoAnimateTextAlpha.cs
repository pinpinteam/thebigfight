﻿using UnityEngine;
using UnityEngine.UI;

public class AutoAnimateTextAlpha : MonoBehaviour
{
	[SerializeField] private Text text;
	[SerializeField] private float[] alphas;
	[SerializeField] private AnimationCurve curve;
	[SerializeField] private float speed;
	private Color currentColor;

	private void Start()
	{
		currentColor = text.color;
	}

	void Update()
	{
		currentColor.a = Mathf.Lerp(alphas[0], alphas[1], curve.Evaluate(Time.time * speed));
		text.color = currentColor;
	}
}
