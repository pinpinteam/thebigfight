﻿using System;
using UnityEngine;

namespace Pinpin
{

	public class OnDestroyEvent : MonoBehaviour
	{
		public Action<GameObject> onDestroy;

		private void OnDestroy ()
		{
			onDestroy?.Invoke(gameObject);
		}
	}

}