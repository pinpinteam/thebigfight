﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
	public class SpawnParticlesAnimationTrigger : MonoBehaviour
	{
		[Serializable]
		private class ParticleSystemInfo
		{
			[Tooltip("Setup with play on awake = true, and StopAction = Destroy")]
			public ParticleSystem particlesPrefab;
			public Transform positionTransform;
		}
		[Header("Particles Prefabs")]
		[SerializeField] private ParticleSystemInfo[] m_particles;

		public void PlayParticles ( int index )
		{
			ParticleSystem newFx = Instantiate(m_particles[index].particlesPrefab, m_particles[index].positionTransform.position, m_particles[index].positionTransform.rotation);
		}
	}
}
