﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class VolumeSpawner : MonoBehaviour
{
	[SerializeField] private GameObject prefab;
	[SerializeField] private Transform m_parentTransform;

	[Header("Size")]
	[SerializeField] private float m_capsuleHeight = 2f;
	[SerializeField] private float m_capsuleRadius = 4.5f;
	[SerializeField, Min(0.09f)] private float m_fillingObjectRadius = 0.2f;
	[SerializeField] private float m_spacing = 0f;
	[SerializeField] private float m_angleVariationFactor = 0f;

	[Header("DEBUG")]
	[SerializeField] private bool m_drawGizmo;
	[SerializeField] private Mesh m_cylinderMesh;
	[SerializeField] private Color m_capsuleColor;
	[SerializeField] private Color m_sphereColor;

	private float m_indexHeight;
	private float m_indexRadius;
	private float m_indexAngle;
	private float m_nextHalfIndexAngle;

	public void Awake ()
	{
		if (m_parentTransform == null)
			m_parentTransform = transform;
	}

	public void Spawn ()
	{
#if UNITY_EDITOR
		List<GameObject> m_spawnedObjects = new List<GameObject>();

		m_indexHeight = 0f;
		Vector3 position;
		float m_angleVariation = 0f;
		while (m_indexHeight < m_capsuleHeight * 2f)
		{
			m_indexRadius = 0f;
			while (m_indexRadius < m_capsuleRadius / 2f)
			{
				m_angleVariation += m_angleVariationFactor;
				m_indexAngle = m_angleVariation;
				m_nextHalfIndexAngle = m_angleVariation;
				if (m_indexRadius > 0f && m_fillingObjectRadius > 0f)
				{
					while (m_nextHalfIndexAngle < 360f + m_angleVariation)
					{
						position.x = transform.position.x;
						position.y = transform.position.y - m_capsuleHeight + m_fillingObjectRadius;
						position.z = transform.position.z;

						position += new Vector3(m_indexRadius * Mathf.Cos(m_indexAngle * Mathf.Deg2Rad), m_indexHeight, m_indexRadius * Mathf.Sin(m_indexAngle * Mathf.Deg2Rad));

						GameObject fillingObject = PrefabUtility.InstantiatePrefab(prefab, m_parentTransform) as GameObject;

						//InstanciatedObject.Add(fillingObject);
						fillingObject.transform.position = position;

						m_spawnedObjects.Add(fillingObject);
						m_indexAngle += (360f + m_spacing) / ((2f * Mathf.PI * m_indexRadius) / (2f * m_fillingObjectRadius * 1f));
						m_nextHalfIndexAngle = m_indexAngle + ((360f + m_spacing) / ((2f * Mathf.PI * m_indexRadius) / (2f * m_fillingObjectRadius * 1f))) / 2f;
					}
				}
				else
				{
					GameObject fillingObject = PrefabUtility.InstantiatePrefab(prefab, m_parentTransform) as GameObject;
					fillingObject.transform.position = new Vector3(transform.position.x, transform.position.y - m_capsuleHeight + m_fillingObjectRadius + m_indexHeight, transform.position.z);
					m_spawnedObjects.Add(fillingObject);
				}
				m_indexRadius += m_fillingObjectRadius * 2f;
			}
			m_indexHeight += m_fillingObjectRadius * 2f;
		}
#endif
	}
	/*
	public void DestroyAll()
	{
		foreach(GameObject obj in InstanciatedObject)
		{
			Destroy(obj.gameObject);
		}
	}
	*/

#if UNITY_EDITOR
	private void OnDrawGizmos ()
	{
		if (m_drawGizmo)
		{
			Gizmos.color = m_capsuleColor;
			Gizmos.DrawMesh(m_cylinderMesh, transform.position, transform.rotation, new Vector3(m_capsuleRadius, m_capsuleHeight, m_capsuleRadius));

			Gizmos.color = m_sphereColor;
			m_indexHeight = 0f;
			Vector3 position;
			float m_angleVariation = 0f;
			if (m_fillingObjectRadius > 0.1f)
			{
				while (m_indexHeight < m_capsuleHeight * 2f)
				{
					m_indexRadius = 0f;
					while (m_indexRadius < m_capsuleRadius / 2f)
					{
						m_angleVariation += m_angleVariationFactor;
						m_indexAngle = m_angleVariation;
						m_nextHalfIndexAngle = m_angleVariation;
						if (m_indexRadius > 0.1f)
						{
							while (m_nextHalfIndexAngle < 360f + m_angleVariation)
							{
								position.x = transform.position.x;
								position.y = transform.position.y - m_capsuleHeight + m_fillingObjectRadius;
								position.z = transform.position.z;

								position += new Vector3(m_indexRadius * Mathf.Cos(m_indexAngle * Mathf.Deg2Rad), m_indexHeight, m_indexRadius * Mathf.Sin(m_indexAngle * Mathf.Deg2Rad));

								Gizmos.DrawSphere(position, m_fillingObjectRadius);
								m_indexAngle += (360f + m_spacing) / ((2f * Mathf.PI * m_indexRadius) / (2f * m_fillingObjectRadius * 1f));
								m_nextHalfIndexAngle = m_indexAngle + ((360f + m_spacing) / ((2f * Mathf.PI * m_indexRadius) / (2f * m_fillingObjectRadius * 1f))) / 2f;
							}
						}
						else
						{
							Gizmos.DrawSphere(new Vector3(transform.position.x, transform.position.y - m_capsuleHeight + m_fillingObjectRadius + m_indexHeight, transform.position.z), m_fillingObjectRadius);
						}
						m_indexRadius += m_fillingObjectRadius * 2f;
					}
					m_indexHeight += m_fillingObjectRadius * 2f;
				}
			}
		}
	}
#endif

}
