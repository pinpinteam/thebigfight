﻿namespace Pinpin.Utils
{
	[System.Serializable]
	public class Counter
	{
		/// <summary>
		/// Value at which Counter is mark as 'ended'
		/// </summary>
		public float threshold;

		/// <summary>
		/// Remaining value to reach Counter's Threshold
		/// </summary>
		public float rest { get; private set; }

		public bool isStarted { get; private set; }
		public bool isEnd { get; private set; }

		/// <summary>
		/// Counter's completing ratio from 0 (just started)
		/// to 1 (just ended)
		/// </summary>
		public float completion
		{
			get
			{
				return isEnd ? 1 : 1 - (rest / threshold);
			}
		}

		public Counter(float threshold)
		{
			this.threshold = threshold;
			rest = this.threshold;
			isEnd = false;
			isStarted = false;
		}

		/// <summary>
		/// Decrement Counter by given amount
		/// </summary>
		/// <param name="amount"></param>
		public float Tick(float amount)
		{
			if (isEnd)
				isEnd = false;

			if (!isStarted)
				isStarted = true;

			rest -= amount;

			if (rest <= 0.0f)
			{
				Stop();
			}

			return rest;
		}

		/// <summary>
		/// Mark Counter as 'ended' and reset 'Rest' to 'Threshold' value
		/// </summary>
		public void Stop()
		{
			isEnd = true;
			rest = threshold;
		}

		/// <summary>
		/// Reset 'Rest' to 'Threshold' value without marking Counter as ended
		/// Also mark Timer has 'not-started'
		/// </summary>
		public void Reset()
		{
			isStarted = false;
			isEnd = false;
			rest = threshold;
		}

		/// <summary>
		/// Set Counter threshold
		/// </summary>
		/// <param name="value"></param>
		public static implicit operator Counter(float value)
		{
			return new Counter(value);
		}

		public static implicit operator float(Counter timer)
		{
			return timer.threshold;
		}

		/// <summary>
		/// Same as Tick(-amount)
		/// </summary>
		/// <param name="counter"></param>
		/// <param name="amount"></param>
		/// <returns></returns>
		public static float operator +(Counter counter, float amount)
		{
			return counter.Tick(amount);
		}

		/// <summary>
		/// Same as Tick(amount)
		/// </summary>
		/// <param name="counter"></param>
		/// <param name="amount"></param>
		/// <returns></returns>
		public static float operator -(Counter counter, float amount)
		{
			return counter.Tick(amount);
		}


	}
}
