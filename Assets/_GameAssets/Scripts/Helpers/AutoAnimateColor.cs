﻿using UnityEngine;
using UnityEngine.UI;

public class AutoAnimateColor : MonoBehaviour
{
	[SerializeField] private Image img;
	[SerializeField] private Color[] colors;
	[SerializeField] private AnimationCurve curve;
	[SerializeField] private float speed;

	void Update()
	{
		img.color = Color.Lerp(colors[0], colors[1], curve.Evaluate(Time.time * speed));
	}

	public void SetValue(int index)
	{
		img.color = colors[index];
	}
}
