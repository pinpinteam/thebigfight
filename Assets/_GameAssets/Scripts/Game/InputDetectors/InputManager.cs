using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin.InputControllers
{

	public class InputManager : MonoBehaviour
	{
		private static InputManager singleton { get; set; }

		[SerializeField] private SlideArea m_slideArea;
		[SerializeField] private float m_sensitivity = 1f;
		public static float sensitivity
		{
			get { return (singleton.m_sensitivity); }
			set { singleton.m_sensitivity = value; }
		}

		public static Vector2 moveDelta
		{
			get { return (InputManager.singleton.m_slideArea.delta * InputManager.singleton.m_sensitivity); }
		}

		public static Vector2 moveDeltaV2
		{
			get { return (InputManager.singleton.m_slideArea.deltaV2 * InputManager.singleton.m_sensitivity); }
		}

		public static bool isMoving
		{
			get { return (InputManager.singleton.m_slideArea.isMoving); }
		}

		public static Vector2 touchPosRatio
		{
			get { return (InputManager.singleton.m_slideArea.touchPosRatio); }
		}

		public static bool touchDown
		{
			get
			{
#if UNITY_EDITOR
				return Input.GetMouseButtonDown(0);
#endif
				if (Input.touchCount > 0)
				{
					return Input.GetTouch(0).phase == TouchPhase.Began;
				}

				return false;
			}
		}

		public static bool isTouching
		{
			get
			{
#if UNITY_EDITOR
				return Input.GetMouseButton(0);
#endif
				if (Input.touchCount > 0)
				{
					return Input.GetTouch(0).phase == TouchPhase.Moved ||
						   Input.GetTouch(0).phase == TouchPhase.Stationary;
				}

				return false;
			}
		}


		public static bool touchUp
		{
			get
			{
#if UNITY_EDITOR
				return Input.GetMouseButtonUp(0);
#endif
				if (Input.touchCount > 0)
				{
					return /*Input.GetTouch(0).phase == TouchPhase.Began || */Input.GetTouch(0).phase == TouchPhase.Ended;
				}
				return false;
			}
		}

		public static Vector3 touchPosition
		{
			get
			{
#if UNITY_EDITOR
				return Input.mousePosition;
#endif
				if (Input.touchCount > 0)
				{
					return Input.GetTouch(0).position;
				}

				return Vector3.zero;
			}
		}

		public static int touchId
		{
			get
			{
#if UNITY_EDITOR
				return -1;
#endif
				if (Input.touchCount > 0)
				{
					return Input.GetTouch(0).fingerId;
				}

				return -1;
			}
		}

		private void Awake ()
		{
			if (InputManager.singleton != null)
			{
				GameObject.Destroy(this.gameObject);
				return;
			}
			InputManager.singleton = this;
		}

		public static void Reset ()
		{
			InputManager.singleton.m_slideArea.ResetTouch();
		}

		/*private void Start ()
		{
			sensitivity = GameManager.datas.sensitivity;
		}*/

	}

}