using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Pinpin.InputControllers
{
	[DisallowMultipleComponent,
	RequireComponent(typeof(CanvasRenderer))]
	public class SlideArea : Graphic, IPointerDownHandler, IDragHandler, IPointerUpHandler
	{
		[SerializeField] private bool horizontalOnly = true;
		[SerializeField] private RectTransform m_rectTransform;
		public Vector2 pointerDelta { get; private set; }
		public Vector2 pointerDeltaV2 { get; private set; }
		public Vector2 deltaV2 { get; private set; }
		public Vector2 delta { get; private set; }


		private Vector2 m_touchPos;
		public Vector2 touchPosRatio;

		private int m_touchCount = 0;
		Camera m_camera;

		protected override void OnEnable ()
		{
			base.OnEnable();
			m_touchCount = 0;
		}

		public bool moved
		{
			get { return (delta != Vector2.zero); }
		}

		public bool isMoving
		{
			get { return (m_touchCount > 0); }
		}


		protected override void Start ()
		{
			pointerDelta = Vector2.zero;
			pointerDeltaV2 = Vector2.zero;
			delta = Vector2.zero;
			deltaV2 = Vector2.zero;
			m_camera = Camera.main;
		}

		private void OnApplicationPause ( bool pause )
		{
			m_touchCount = 0;
			pointerDelta = Vector2.zero;
			pointerDeltaV2 = Vector2.zero;
		}

		void Update ()
		{
			if (Input.GetAxisRaw("Vertical") != 0f || Input.GetAxisRaw("Horizontal") != 0f)
			{
				delta = new Vector2(Input.GetAxisRaw("Horizontal"), horizontalOnly ? 0f : Input.GetAxisRaw("Vertical")) * Time.deltaTime * 2f;
				deltaV2 = delta;
			}
			else if (pointerDelta != Vector2.zero)
			{
				delta = pointerDelta;
				deltaV2 = pointerDeltaV2;
				if (horizontalOnly)
					delta = new Vector2(delta.x, 0f);
				pointerDelta = Vector2.zero;
				pointerDeltaV2 = Vector2.zero;
			}
			else
			{
				deltaV2 = Vector2.zero;
				delta = Vector2.zero;
			}
		}

		public void OnPointerDown ( PointerEventData ped )
		{
			m_touchCount++;
			pointerDelta = Vector2.zero;
			pointerDeltaV2 = Vector2.zero; 
			if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(m_rectTransform, ped.position, ped.pressEventCamera, out m_touchPos))
				return;
			touchPosRatio = new Vector2(m_touchPos.x / Screen.width * 2, m_touchPos.y / Screen.height * 2);
		}

		public void OnDrag ( PointerEventData ped )
		{
			pointerDelta = ped.delta / canvas.transform.localScale.x;
			pointerDeltaV2 = ped.delta / Screen.height * 2f * Camera.main.orthographicSize;
			if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(m_rectTransform, ped.position, ped.pressEventCamera, out m_touchPos))
				return;
			touchPosRatio = new Vector2(m_touchPos.x / Screen.width * 2, m_touchPos.y / Screen.height * 2);
		}

		public void OnPointerUp ( PointerEventData ped )
		{
			if (m_touchCount > 0)
				m_touchCount--;
			pointerDelta = Vector2.zero;
			pointerDeltaV2 = Vector2.zero;
			m_touchPos = Vector2.zero;
			touchPosRatio = Vector2.zero;
		}

		public void ResetTouch ()
		{
			m_touchCount = 0;
		}
	}
}