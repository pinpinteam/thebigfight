﻿using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

namespace Pinpin
{
    [CreateAssetMenu(fileName = "LevelInfo001", menuName = "Level Info", order = 0)]
    public class LevelInfo : ScriptableObject
    {
        public enum RoadShape { Straight, RampUP, RampDown, TurnRight, TurnLeft, RampUpTurn, RampDownTurn }

        [ReadOnly] public float totalLength;
        public int charactersQuantityAtStart = 1;
        public int ammoQuantityAtStart = 50;
        public int enemyQuantityFinisher = 100;
        public ColorPreset[] colorPalette = new ColorPreset[3] { ColorPreset.Red, ColorPreset.Yellow, ColorPreset.Green };
        [Space]
        public List<LevelPattern> patterns = new List<LevelPattern>();
        public List<RoadShape> roadShapes = new List<RoadShape>();
        public List<int> roadShapeAngles = new List<int>();
        
        public void AddPattern ( LevelPattern pattern, RoadShape roadShape = RoadShape.Straight, int roadShapeAngle = 0 )
        {
            patterns.Add(pattern);
            roadShapes.Add(roadShape);
            roadShapeAngles.Add(roadShapeAngle);
        }
		
        private void OnValidate ()
        {
            totalLength = 0f;

            for (int i = 0; i < patterns.Count; ++i)
            {
                totalLength += patterns[i].length;
            }
            for (int i = roadShapes.Count; i < patterns.Count; i++)
            {
                roadShapes.Add(RoadShape.Straight);
            }
            for (int i = roadShapeAngles.Count; i < patterns.Count; i++)
            {
                roadShapeAngles.Add(0);
            }
            if (patterns.Count < roadShapes.Count)
            {
                roadShapes.RemoveRange(patterns.Count, roadShapes.Count - patterns.Count);
            }
            if (patterns.Count < roadShapeAngles.Count)
            {
                roadShapeAngles.RemoveRange(patterns.Count, roadShapeAngles.Count - patterns.Count);
            }
        }
    }
}