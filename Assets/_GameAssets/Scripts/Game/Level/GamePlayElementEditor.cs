﻿﻿using System;
#if UNITY_EDITOR
#endif
using UnityEngine;

namespace Pinpin
{
#if UNITY_EDITOR
	[ExecuteInEditMode]
	public class GamePlayElementEditor : MonoBehaviour
	{
		public GamePlayElement element;

		public Action<GamePlayElement> onDestroy;
		public Action onModified;
		public BezierSpline spline;
		private Vector3 m_lastPosition;
		
		private void OnDestroy ()
		{
			onDestroy?.Invoke(element);
		}

		private void Update ()
		{
			if (transform.position != m_lastPosition)
			{
				OnValidate();
			}
		}

		private void OnValidate ()
		{
			if (element == null) return;

			if (transform.position != m_lastPosition)
			{
				float limitX = spline.GetWidthMultiplierFromdist(element.positionZ) * spline.baseWidth;
				element.positionX = Mathf.Clamp(transform.localPosition.x, -limitX, limitX);
				element.height = Mathf.Max(transform.localPosition.y, 0f);
				element.positionZ = Mathf.Clamp(transform.localPosition.z, 0f, spline.TotalLength);
			}
			
			switch (element.type)
			{
				case GamePlayElement.GamePlayElementType.Wall:
					transform.localPosition = Vector3.zero;
					GetComponent<Wall>().Configure(element, spline);
					break;
				case GamePlayElement.GamePlayElementType.Slope:
					transform.localPosition = Vector3.zero;
					GetComponent<Slope>().Configure(element, spline);
					break;
				case GamePlayElement.GamePlayElementType.RotatingPlatform:
					GetComponent<RotatingPlatform>().Configure(element, spline);
					break;
				case GamePlayElement.GamePlayElementType.CollectibleGem:
					GetComponent<Collectible>().Configure(element, spline);
					break;
				case GamePlayElement.GamePlayElementType.CollectibleBullet:
					GetComponent<CollectibleBullet>().Configure(element, spline);
					break;
				case GamePlayElement.GamePlayElementType.BulletStack:
					GetComponent<BulletStack>().ConfigureFake(element, spline);
					break;
				default:
					break;
			}
			m_lastPosition = transform.position;
			onModified?.Invoke();
		}
	}
#endif
}