﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
    [CreateAssetMenu(fileName = "NewPattern", menuName = "Game/LevelPattern", order = 1)]
    public class LevelPattern : ScriptableObject
    {
        // [Serializable]
        // public class GamePlayElement : IComparable<GamePlayElement>
        // {
        //     public enum GamePlayElementType
        //     {
        //         BorderWall
        //     }
        //
        //     public GamePlayElementType type;
        //     [Range(0,2)]public int colorIndex;
        //     [Range(-3.25f, 3.25f)] public float positionX;
        //     [Min(0f)] public float dist;
        //     public float length;
        //     [Min(0.01f)] public float width;
        //     [Min(0.01f)] public float height;
        //     public int value;
        //     public float[] blockHeights = new float[3];
        //
        //     public int CompareTo ( GamePlayElement other )
        //     {
        //         return dist.CompareTo(other.dist);
        //     }
        //
        //     public GamePlayElement Clone ()
        //     {
        //         return new GamePlayElement
        //         {
        //             type = type,
        //             colorIndex = colorIndex,
        //             positionX = positionX,
        //             dist = dist,
        //             length = length,
        //             width = width,
        //             height = height,
        //             value = value,
        //             blockHeights = blockHeights
        //         };
        //     }
        // }

        public float length = 20f;
        public List<GamePlayElement> elements = new List<GamePlayElement>();
		public AnimationCurve widthCurve;
    }

}