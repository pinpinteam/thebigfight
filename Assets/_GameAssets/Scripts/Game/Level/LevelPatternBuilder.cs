﻿﻿#if UNITY_EDITOR
 using UnityEditor;
#endif
using UnityEngine;
using System.Collections.Generic;

namespace Pinpin
{
#if UNITY_EDITOR
	[ExecuteInEditMode]
	public class LevelPatternBuilder : MonoBehaviour
	{
		[HideInInspector] public int selectedDirectoryIndex;
		[SerializeField] private BezierSpline m_spline;
		[SerializeField] private RoadMesh m_roadMesh;
		[SerializeField] private GameAssets assets;

		public bool selectCreatedItem = true;
		[ShowOnly] public LevelPattern editingLD = null;
		[SerializeField, ShowOnly] private string loadedLD;
		[SerializeField] private float m_newElemPosZ;
		[SerializeField, Min(0.01f)] private float m_newElemHeight;
		[SerializeField, Range(-4f, 4f)] private float m_newElemPosX;
		[Header("LineCreation")]
		[SerializeField, Range(0,2)] private int m_colorIndex;
		[SerializeField] private int m_lineCount;
		[SerializeField] private float m_lineDistBetweenElements;

		private float m_lastLength = 0f;

		private void OnDestroy ()
		{
			editingLD = null;
			for (int i = transform.childCount - 1; i >= 0; i--)
			{
				transform.GetChild(i).GetComponent<GamePlayElementEditor>().onDestroy = null;
			}
		}

		public void ClearLD ()
		{
			editingLD = null;
			for (int i = transform.childCount - 1; i >= 0; i--)
			{
				if (transform.GetChild(i).TryGetComponent<GamePlayElementEditor>(out GamePlayElementEditor ldElement))
				{
					ldElement.onDestroy = null;
				}
				DestroyImmediate(transform.GetChild(i).gameObject);
			}
		}

		public void LoadLD ( LevelPattern ld )
		{
			editingLD = null;
			for (int i = transform.childCount - 1; i >= 0; i--)
			{
				Transform child = transform.GetChild(i);
				GamePlayElementEditor gamePlayElementEditor = child.GetComponent<GamePlayElementEditor>();
				if (gamePlayElementEditor != null)
					gamePlayElementEditor.onDestroy = null;
				DestroyImmediate(child.gameObject);
			}

			editingLD = ld;

			if (editingLD != null)
			{
				loadedLD = editingLD.name;
				RecomputeLength();
				LoadElements(editingLD);
				GamePlayElement borderWallElem = new GamePlayElement { height = 0.2f, length = ld.length, positionZ = 0f, width = 0.5f, positionX = -4.5f };
				AddBorderWall(borderWallElem);
				borderWallElem.positionX = 4.5f;
				AddBorderWall(borderWallElem);
			}
		}

		private void LoadElements ( LevelPattern pattern )
		{
			for (int i = 0; i < pattern.elements.Count; i++)
			{
				GamePlayElement element = pattern.elements[i];
				switch (element.type)
				{
					case GamePlayElement.GamePlayElementType.Wall:
						AddWall(element);
						break;
					case GamePlayElement.GamePlayElementType.Slope:
						AddSlope(element);
						break;
					case GamePlayElement.GamePlayElementType.CrowdArea:
						AddCrowdArea(element);
						break;
					case GamePlayElement.GamePlayElementType.CollectibleGem:
						AddCollectible(element);
						break;
					case GamePlayElement.GamePlayElementType.Saw:
						AddSawTrap(element);
						break;
					case GamePlayElement.GamePlayElementType.Pendulum:
						AddPendulumTrap(element);
						break;
					case GamePlayElement.GamePlayElementType.RotatingCylinder:
						AddRotatingCylinderTrap(element);
						break;
					case GamePlayElement.GamePlayElementType.SpikyCylinder:
						AddSpikyCylinderTrap(element);
						break;
					case GamePlayElement.GamePlayElementType.Swatter:
						AddSwatter(element);
						break;
					case GamePlayElement.GamePlayElementType.RotatingPlatform:
						AddRotatingPlatform(element);
						break;
					case GamePlayElement.GamePlayElementType.BigCrowdArea:
						AddBigCrowdArea(element);
						break;
					case GamePlayElement.GamePlayElementType.CollectibleBullet:
						AddCollectibleBullet(element);
						break;
					case GamePlayElement.GamePlayElementType.BulletStack:
						AddBulletStack(element);
						break;
					case GamePlayElement.GamePlayElementType.RotorTrap:
						AddRotorTrap(element);
						break;
					default:
						break;
				}
			}
		}

		#region Create functions

		public void CreateWall ()
		{
			GamePlayElement element = new GamePlayElement();
			element.type = GamePlayElement.GamePlayElementType.Wall;
			element.positionZ = m_newElemPosZ;
			element.positionX = m_newElemPosX;
			element.length = 1f;
			element.width = 0.2f;
			element.height = 0.5f;

			editingLD.elements.Add(element);
			Wall newWall = AddWall(element);
			if (selectCreatedItem)
				Selection.activeObject = newWall;
			EditorUtility.SetDirty(editingLD);
		}

		public void CreateSlope ()
		{
			GamePlayElement element = new GamePlayElement();
			element.type = GamePlayElement.GamePlayElementType.Slope;
			element.positionZ = m_newElemPosZ;
			element.positionX = m_newElemPosX;
			element.length = 1f;
			element.width = 0.2f;
			element.height = 0.5f;

			editingLD.elements.Add(element);
			Slope newSlope = AddSlope(element);
			if (selectCreatedItem)
				Selection.activeObject = newSlope;
			EditorUtility.SetDirty(editingLD);
		}

		public void CreateCrowdArea()
		{
			GamePlayElement element = new GamePlayElement();
			element.type = GamePlayElement.GamePlayElementType.CrowdArea;
			element.positionZ = m_newElemPosZ;
			editingLD.elements.Add(element);
			CrowdArea newCrowdArea = AddCrowdArea(element);
			if (selectCreatedItem)
				Selection.activeObject = newCrowdArea;
			EditorUtility.SetDirty(editingLD);
		}

		public void CreateBigCrowdArea()
		{
			GamePlayElement element = new GamePlayElement();
			element.type = GamePlayElement.GamePlayElementType.BigCrowdArea;
			element.positionZ = m_newElemPosZ;
			editingLD.elements.Add(element);
			CrowdArea newCrowdArea = AddBigCrowdArea(element);
			if (selectCreatedItem)
				Selection.activeObject = newCrowdArea;

			EditorUtility.SetDirty(editingLD);
		}

		public void CreateBulletStack()
		{
			GamePlayElement element = new GamePlayElement();
			element.type = GamePlayElement.GamePlayElementType.BulletStack;
			element.positionZ = m_newElemPosZ;
			element.positionX = m_newElemPosX;
			editingLD.elements.Add(element);
			BulletStack newCrowdArea = AddBulletStack(element);
			if (selectCreatedItem)
				Selection.activeObject = newCrowdArea;

			EditorUtility.SetDirty(editingLD);
		}

		public void CreateCollectibleLine (  )
		{
			for (int i = 0; i < m_lineCount; i++)
			{
				CreateCollectible(m_newElemPosZ + i * m_lineDistBetweenElements);
			}
		}

		public void CreateCollectible( float posZ = -1f)
		{
			GamePlayElement element = new GamePlayElement();
			element.type = GamePlayElement.GamePlayElementType.CollectibleGem;
			element.height = m_newElemHeight;
			element.positionZ = posZ >= 0f ? posZ : m_newElemPosZ;
			element.positionX = m_newElemPosX;
			editingLD.elements.Add(element);
			Collectible newCollectible = AddCollectible(element);
			if (selectCreatedItem)
				Selection.activeObject = newCollectible;
			EditorUtility.SetDirty(editingLD);
		}

		public void CreateCollectibleBulletLine ()
		{
			for (int i = 0; i < m_lineCount; i++)
			{
				CreateCollectibleBullet(m_newElemPosZ + i * m_lineDistBetweenElements);
			}
		}

		public void CreateCollectibleBullet ( float posZ = -1f )
		{
			GamePlayElement element = new GamePlayElement();
			element.type = GamePlayElement.GamePlayElementType.CollectibleBullet;
			element.height = m_newElemHeight;
			element.positionZ = posZ >= 0f ? posZ : m_newElemPosZ;
			element.positionX = m_newElemPosX;
			editingLD.elements.Add(element);

			CollectibleBullet newCollectibleBullet = AddCollectibleBullet(element);
			if (selectCreatedItem)
				Selection.activeObject = newCollectibleBullet;

			EditorUtility.SetDirty(editingLD);
		}

		public void CreateTrap(GamePlayElement.GamePlayElementType type)
		{
			GamePlayElement element = new GamePlayElement();

			element.type = type;
			element.positionZ = m_newElemPosZ;
			editingLD.elements.Add(element);
			Trap newTrap = AddTrap(element);

			if (selectCreatedItem)
				Selection.activeObject = newTrap;
			EditorUtility.SetDirty(editingLD);
		}

		public void CreateRotatingPlatform()
		{
			GamePlayElement element = new GamePlayElement();
			element.type = GamePlayElement.GamePlayElementType.RotatingPlatform;
			element.positionZ = m_newElemPosZ;
			editingLD.elements.Add(element);

			RotatingPlatform newRotatingPlatform = AddRotatingPlatform(element);

			if (selectCreatedItem)
				Selection.activeObject = newRotatingPlatform;

			EditorUtility.SetDirty(editingLD);
		}
		
		private GamePlayElementEditor CreateNewGamePlayElementEditor( GameObject go, GamePlayElement element )
		{
			GamePlayElementEditor newGamePlayElementEditor = go.AddComponent<GamePlayElementEditor>();
			newGamePlayElementEditor.spline = m_spline;
			newGamePlayElementEditor.element = element;
			newGamePlayElementEditor.onDestroy += ElementDestroyed;
			newGamePlayElementEditor.onModified += SetDirty;

			return newGamePlayElementEditor;
		}
		
		#endregion

		private Trap AddTrap(GamePlayElement element)
		{
			Trap newTrap = null;
			switch (element.type)
			{
				case GamePlayElement.GamePlayElementType.Saw:
					newTrap = Instantiate(assets.sawTrapPrefab, transform);
					break;
				case GamePlayElement.GamePlayElementType.Pendulum:
					newTrap = Instantiate(assets.pendulumTrapPrefab, transform);
					break;
				case GamePlayElement.GamePlayElementType.RotatingCylinder:
					newTrap = Instantiate(assets.rotatingCylinderTrapPrefab, transform);
					break;
				case GamePlayElement.GamePlayElementType.Swatter:
					newTrap = Instantiate(assets.swatterTrapPrefab, transform);
					break;
			}
			newTrap.transform.localPosition = Vector3.forward * element.positionZ;
			CreateNewGamePlayElementEditor(newTrap.gameObject, element);
			return newTrap;
		}
		
		private Wall AddWall ( GamePlayElement element )
		{
			Wall newWall = Instantiate(assets.wallPrefab, transform);
			newWall.Configure(element, m_spline);

			CreateNewGamePlayElementEditor(newWall.gameObject, element);
			return newWall;
		}

		private BorderWall AddBorderWall ( GamePlayElement element )
		{
			BorderWall newWall = Instantiate(assets.borderWallPrefab, transform);
			newWall.Configure(element, m_spline);
			return newWall;
		}

		private Slope AddSlope ( GamePlayElement element )
		{
			Slope newSlope = Instantiate(assets.slopePrefab, transform);
			newSlope.Configure(element, m_spline);

			CreateNewGamePlayElementEditor(newSlope.gameObject, element);
			return newSlope;
		}

		private RotatingPlatform AddRotatingPlatform( GamePlayElement element )
		{
			RotatingPlatform platform = Instantiate(assets.rotatingPlatformPrefab, transform);
			platform.Configure(element, m_spline);
			
			CreateNewGamePlayElementEditor(platform.gameObject, element);
			return platform;
		}
		
		private CrowdArea AddCrowdArea( GamePlayElement element )
		{
			CrowdArea area = Instantiate(assets.colorCrowdAreaPrefab, transform);
			area.ConfigureFake(element, m_spline);

			CreateNewGamePlayElementEditor(area.gameObject, element);
			return area;
		}

		private CrowdArea AddBigCrowdArea( GamePlayElement element )
		{
			CrowdArea area = Instantiate(assets.bigColorCrowdAreaPrefab, transform);
			area.ConfigureFake(element, m_spline);

			CreateNewGamePlayElementEditor(area.gameObject, element);
			return area;
		}

		private BulletStack AddBulletStack ( GamePlayElement element )
		{
			BulletStack bulletStack = Instantiate(assets.bulletStackPrefab, transform);
			bulletStack.ConfigureFake(element, m_spline, assets.bulletPrefab);

			CreateNewGamePlayElementEditor(bulletStack.gameObject, element);
			return bulletStack;
		}

		private Collectible AddCollectible( GamePlayElement element )
		{
			Collectible collectible = Instantiate(assets.collectiblePrefab, transform);
			collectible.Configure(element, m_spline);

			CreateNewGamePlayElementEditor(collectible.gameObject, element);
			return collectible;
		}

		private CollectibleBullet AddCollectibleBullet ( GamePlayElement element )
		{
			CollectibleBullet collectible = Instantiate(assets.collectibleBulletPrefab, transform);
			collectible.Configure(element, m_spline);

			CreateNewGamePlayElementEditor(collectible.gameObject, element);
			return collectible;
		}

		private void AddSawTrap( GamePlayElement element )
		{
			SawTrap trap = Instantiate(assets.sawTrapPrefab, transform);
			trap.Configure(element, m_spline);

			CreateNewGamePlayElementEditor(trap.gameObject, element);
		}

		private void AddPendulumTrap( GamePlayElement element )
		{
			PendulumTrap trap = Instantiate(assets.pendulumTrapPrefab, transform);
			trap.Configure(element, m_spline);

			CreateNewGamePlayElementEditor(trap.gameObject, element);
		}

		private void AddRotatingCylinderTrap( GamePlayElement element )
		{
			RotatingCylinderTrap trap = Instantiate(assets.rotatingCylinderTrapPrefab, transform);
			trap.Configure(element, m_spline);

			CreateNewGamePlayElementEditor(trap.gameObject, element);
		}

		private void AddSpikyCylinderTrap( GamePlayElement element )
		{
			SpikyCylinderTrap trap = Instantiate(assets.spikyCylinderTrapPrefab, transform);
			trap.Configure(element, m_spline);

			CreateNewGamePlayElementEditor(trap.gameObject, element);
		}

		private void AddSwatter( GamePlayElement element )
		{
			SwatterTrap swatter = Instantiate(assets.swatterTrapPrefab, transform);
			swatter.Configure(element, m_spline);

			CreateNewGamePlayElementEditor(swatter.gameObject, element);
		}

		private void AddRotorTrap(GamePlayElement element)
		{
			RotorTrap trap = Instantiate(assets.rotorTrapPrefab, transform);
			trap.Configure(element, m_spline);

			CreateNewGamePlayElementEditor(trap.gameObject, element);
		}
		
		public void ElementDestroyed ( GamePlayElement element )
		{
			if (editingLD != null)
				editingLD.elements.Remove(element);
		}

		private void SetDirty ()
		{
			editingLD.elements.Sort();
			EditorUtility.SetDirty(editingLD);
		}

		public void RecomputeLength ()
		{
			if (editingLD != null)
			{
				//if (m_lastLength != editingLD.length)
				//{
					m_spline.Reset(editingLD.length);
					m_spline.SetWidthCurve(0, editingLD.widthCurve);
					m_lastLength = editingLD.length;
					m_roadMesh.GenerateMesh();
				//}
				editingLD.elements.Sort();
				EditorUtility.SetDirty(editingLD);
			}
		}

	}
#endif
}