﻿using UnityEngine;

namespace Pinpin
{
    public class RotatingCylinderTrap : Trap
    {
        public static RotatingCylinderTrap Instantiate( GamePlayElement element, BezierSpline spline, Transform parent )
        {
            RotatingCylinderTrap trap = Instantiate(ApplicationManager.assets.rotatingCylinderTrapPrefab, parent);
            trap.Configure(element, spline);
            return trap;
        }

        public override void Configure(GamePlayElement element, BezierSpline spline)
        {
            Vector3 position = spline.GetPointInWorldFromdist(element.positionZ);
            Vector3 direction = spline.GetDirectionFromDist(element.positionZ);
            float angle = spline.GetAngleFromdist(element.positionZ);
            Vector3 right = spline.GetNormalFromdist(element.positionZ);
            right = Quaternion.AngleAxis(angle, direction) * right;
            Vector3 up = Vector3.Cross(right, direction);
            Quaternion orientation = Quaternion.LookRotation(direction, up);

            Transform tfm = transform;

            tfm.position = position;
            tfm.rotation = orientation;
        }
    }
}