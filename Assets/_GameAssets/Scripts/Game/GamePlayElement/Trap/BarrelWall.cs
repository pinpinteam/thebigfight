﻿using UnityEngine;

namespace Pinpin
{
	public class BarrelWall : Trap
	{
		[SerializeField] private ShootableBarrelWall m_shootableBarrelWall;

		public static BarrelWall Instantiate ( GamePlayElement element, BezierSpline spline, Transform parent )
		{
			BarrelWall trap = Instantiate(ApplicationManager.assets.barrelWallTrapPrefab, parent);
			trap.Configure(element, spline);
			return trap;
		}

		public override void Configure ( GamePlayElement element, BezierSpline spline )
		{
			//position X set the Barrel pos and value set Barrel HP
			m_shootableBarrelWall.Configure(element.positionX, element.value);

			Vector3 position = spline.GetPointInWorldFromdist(element.positionZ);
			position.x = 0;
			Vector3 direction = spline.GetDirectionFromDist(element.positionZ);
			float angle = spline.GetAngleFromdist(element.positionZ);
			Vector3 right = spline.GetNormalFromdist(element.positionZ);
			right = Quaternion.AngleAxis(angle, direction) * right;
			Vector3 up = Vector3.Cross(right, direction);
			Quaternion orientation = Quaternion.LookRotation(direction, up);

			Transform tfm = transform;

			tfm.position = position;
			tfm.rotation = orientation;
		}
	}
}