﻿using UnityEngine;

namespace Pinpin
{
    public class SpikeCarpetTrap : Trap
    {
        [SerializeField] public float m_damage;

        public static SpikeCarpetTrap Instantiate(GamePlayElement element, BezierSpline spline, Transform parent)
        {
            SpikeCarpetTrap trap = Instantiate(ApplicationManager.assets.spikeCarpetTrapPrefab, parent);
            trap.Configure(element, spline);
            return trap;
        }
        
        public override void Configure(GamePlayElement element, BezierSpline spline)
        {
            Vector3 position = spline.GetPointInWorldFromdist(element.positionZ);
            position.x += element.positionX;
            Vector3 direction = spline.GetDirectionFromDist(element.positionZ);
            float angle = spline.GetAngleFromdist(element.positionZ);
            Vector3 right = spline.GetNormalFromdist(element.positionZ);
            right = Quaternion.AngleAxis(angle, direction) * right;
            Vector3 up = Vector3.Cross(right, direction);
            Quaternion orientation = Quaternion.LookRotation(direction, up);

            Transform tfm = transform;

            tfm.position = position;
            tfm.rotation = orientation;
        }

        public override void ApplyOnPlayer(PlayerGunnerController player)
        {
            player.ActivateDamageOnTime(m_damage);
        }

        public override void ClearOnPlayer(PlayerGunnerController player)
        {
            player.DeactivateDamageOnTime();
        }
    }
}