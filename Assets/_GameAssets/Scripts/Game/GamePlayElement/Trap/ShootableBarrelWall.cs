﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
	public class ShootableBarrelWall : ShootableElement
	{
		[SerializeField] private BarrelWall m_barrelWall;
		[SerializeField] private SphereCollider m_sphereCollider;
		[SerializeField] private ParticleSystem m_explosionPS;
		[SerializeField] private Rigidbody[] m_brickArray;

		private int m_healthPoint;

		public void Configure ( float barrelPosX, int healthPoint )
		{
			transform.localPosition = new Vector3(barrelPosX, transform.position.y, transform.position.z);
			m_sphereCollider.center = new Vector3(-barrelPosX, m_sphereCollider.center.y, m_sphereCollider.center.z);

			m_healthPoint = healthPoint;
			UpdateDisplayCount(m_healthPoint);
			splinePosX = barrelPosX;
		}

		public override void GetShot ()
		{
			--m_healthPoint;
			UpdateDisplayCount(m_healthPoint);

			if (m_healthPoint <= 0)
			{
				isInteractable = false;
				StartCoroutine(DelayExplode());
			}
		}
		private IEnumerator DelayExplode ()
		{
			yield return new WaitForSeconds(.075f);
			Explode();
			yield return new WaitForSeconds(4f);
			foreach (Rigidbody rb in m_brickArray)
				Destroy(rb.gameObject);
		}
		private void Explode ()
		{
			m_barrelWall.isInteractable = false;
			m_explosionPS.Play();

			foreach (Rigidbody rb in m_brickArray)
			{
				rb.isKinematic = false;
				rb.useGravity = true;
				rb.AddExplosionForce(1500, transform.position, 100f);
			}
		}
	}
}
