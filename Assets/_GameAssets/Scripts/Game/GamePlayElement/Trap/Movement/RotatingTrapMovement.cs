﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingTrapMovement : MonoBehaviour
{
	[SerializeField] private float m_rotationDuration = 6f;
	[SerializeField] private Transform m_body;

	Sequence m_sequence;
	private float m_yRotation = 0f;

    void Start()
    {
		m_yRotation = Random.Range(0f, 270f);

		m_body.localRotation = Quaternion.Euler(0f, m_yRotation, 0f);

		m_sequence = DOTween.Sequence();

		m_sequence.Append(m_body.DOLocalRotate(new Vector3(0f, m_yRotation  - 360f, 0f), m_rotationDuration, RotateMode.FastBeyond360).SetEase(Ease.Linear));
		//m_sequence.Append(m_body.DOLocalRotate(new Vector3(0f, m_yRotation - 360f, 0f), m_rotationDuration / 2f).SetEase(Ease.Linear));
		//m_sequence.Append(m_body.DOLocalRotate(new Vector3(0f, m_yRotation, 0f), 0f));

		m_sequence.SetLoops(-1);

		m_sequence.Play();
	}
}
