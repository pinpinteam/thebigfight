﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PendulumMovement : MonoBehaviour
{
	[SerializeField] private float m_minRotation = -45f;
	[SerializeField] private float m_maxRotation = 45f;
	[SerializeField] private float m_rotationDuration = 5f;
	[SerializeField] private Transform m_body;

	Sequence m_sequence;


    void Start()
    {
		bool startingLeft = Random.Range(0, 100f) < 50f;

		if (startingLeft)
		{
			m_body.localRotation = Quaternion.Euler(0f, 0f, m_minRotation);
		}
		else
		{
			m_body.localRotation = Quaternion.Euler(0f, 0f, m_maxRotation);
		}

		StartCoroutine(StartDelay(Random.Range(0f, 2f), startingLeft));
	}

	private IEnumerator StartDelay( float delay, bool startingLeft )
	{
		yield return new WaitForSeconds(delay);
		m_sequence = DOTween.Sequence();

		if (startingLeft)
		{
			m_sequence.Append(m_body.DOLocalRotate(new Vector3(0f, 0f, m_maxRotation), m_rotationDuration / 2f).SetEase(Ease.InOutQuad));
			m_sequence.Append(m_body.DOLocalRotate(new Vector3(0f, 0f, m_minRotation), m_rotationDuration / 2f).SetEase(Ease.InOutQuad));
		}
		else
		{
			m_sequence.Append(m_body.DOLocalRotate(new Vector3(0f, 0f, m_minRotation), m_rotationDuration / 2f).SetEase(Ease.InOutQuad));
			m_sequence.Append(m_body.DOLocalRotate(new Vector3(0f, 0f, m_maxRotation), m_rotationDuration / 2f).SetEase(Ease.InOutQuad));
		}
		m_sequence.SetLoops(-1);
		m_sequence.Play();
	}
}
