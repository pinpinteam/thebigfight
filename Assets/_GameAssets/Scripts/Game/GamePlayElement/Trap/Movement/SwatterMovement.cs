﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace Pinpin
{
    public class SwatterMovement : MonoBehaviour
    {
        [SerializeField] Transform m_swatterTransformPivot;
        [SerializeField] float m_swipeDuration = 0.4f;
        [SerializeField] float m_timeBetweenSwipes = 1f;
        [SerializeField] Ease m_swipeEasing = Ease.InQuad;

        float m_startDelay = 0f;
        Tween m_slapTween;

        private void Start()
        {
            m_swatterTransformPivot.localRotation = Quaternion.Euler(new Vector3(0, 0, -90f));
            if (m_startDelay == 0f)
                SlapRight();
            else
                StartCoroutine(ExecuteAfterTime(m_startDelay));
        }

        IEnumerator ExecuteAfterTime( float time )
        {
            yield return new WaitForSeconds(time);

            SlapRight();
        }

        private void SlapRight()
        {
            m_slapTween = m_swatterTransformPivot.DOLocalRotate(new Vector3(0, 0, 180f), m_swipeDuration, RotateMode.LocalAxisAdd);
            m_slapTween.SetDelay(m_timeBetweenSwipes);
            m_slapTween.SetEase(m_swipeEasing);
            m_slapTween.OnComplete(SlapLeft);
        }

        private void SlapLeft()
        {
            m_slapTween = m_swatterTransformPivot.DOLocalRotate(new Vector3(0, 0, -180f), m_swipeDuration, RotateMode.LocalAxisAdd);
            m_slapTween.SetDelay(m_timeBetweenSwipes);
            m_slapTween.SetEase(m_swipeEasing);
            m_slapTween.OnComplete(SlapRight);
        }

        private void OnDestroy()
        {
            m_slapTween?.Kill();
        }
    }
}