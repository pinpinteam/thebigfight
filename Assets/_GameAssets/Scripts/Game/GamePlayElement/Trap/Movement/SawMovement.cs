﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawMovement : MonoBehaviour
{
	[SerializeField] private float m_minXPosition = -2f;
	[SerializeField] private float m_maxXPosition = 2f;
	[SerializeField] private float m_duration = 8f;
	[SerializeField] private float m_rotationSpeed = 45f;
	[SerializeField] private Transform m_body;
	[SerializeField] private Transform m_visual;

	private float zRotation = 0f;
	Sequence m_sequence;

    void Start()
    {

		bool startingLeft = Random.Range(0, 100f) < 50f;

		if (startingLeft)
		{
			m_body.localPosition += Vector3.right * m_minXPosition;
		}
		else
		{
			m_body.localPosition += Vector3.right * m_maxXPosition;
		}

		StartCoroutine(StartDelay(Random.Range(0f, 2f), startingLeft));
	}

	private IEnumerator StartDelay (float delay, bool startingLeft)
	{
		yield return new WaitForSeconds(delay);
		m_sequence = DOTween.Sequence();

		if (startingLeft)
		{
			m_sequence.Append(m_body.DOLocalMoveX(m_maxXPosition, m_duration / 2f).SetEase(Ease.InOutSine));
			m_sequence.Append(m_body.DOLocalMoveX(m_minXPosition, m_duration / 2f).SetEase(Ease.InOutSine));
		}
		else
		{
			m_sequence.Append(m_body.DOLocalMoveX(m_minXPosition, m_duration / 2f).SetEase(Ease.InOutSine));
			m_sequence.Append(m_body.DOLocalMoveX(m_maxXPosition, m_duration / 2f).SetEase(Ease.InOutSine));
		}
		m_sequence.SetLoops(-1);
		m_sequence.Play();
	}

	private void Update ()
	{
		zRotation = (zRotation + Time.deltaTime * m_rotationSpeed) % 360;
		m_visual.localRotation = Quaternion.Euler(0f, 0f, zRotation);
	}
}
