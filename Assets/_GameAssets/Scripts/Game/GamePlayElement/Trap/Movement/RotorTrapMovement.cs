﻿using DG.Tweening;
using UnityEngine;

namespace Pinpin
{
    public class RotorTrapMovement : MonoBehaviour
    {
        [SerializeField] private float m_rotorDuration = 1.3f;
        [SerializeField] private Transform m_topRotor;
        [SerializeField] private Transform m_bottomRotor;

        private void Start()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(m_topRotor
                .DOLocalRotate(new Vector3(0, 360, 0), m_rotorDuration, RotateMode.FastBeyond360)
                .SetEase(Ease.Linear));
            sequence.Insert(0, m_bottomRotor
                .DOLocalRotate(new Vector3(0, -360, 0), m_rotorDuration, RotateMode.FastBeyond360)
                .SetEase(Ease.Linear));
            sequence.SetLoops(-1);
        }
    }
}