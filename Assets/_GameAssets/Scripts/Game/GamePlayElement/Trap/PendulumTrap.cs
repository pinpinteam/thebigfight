﻿using UnityEngine;

namespace Pinpin
{
    public class PendulumTrap : Trap
    {
        public static PendulumTrap Instantiate( GamePlayElement element, BezierSpline spline, Transform parent )
        {
            PendulumTrap trap = Instantiate(ApplicationManager.assets.pendulumTrapPrefab, parent);
            trap.Configure(element, spline);
            return trap;
        }

        public override void Configure(GamePlayElement element, BezierSpline spline)
        {
            Vector3 position = spline.GetPointInWorldFromdist(element.positionZ);
            Vector3 direction = spline.GetDirectionFromDist(element.positionZ);
            float angle = spline.GetAngleFromdist(element.positionZ);
            Vector3 right = spline.GetNormalFromdist(element.positionZ);
            right = Quaternion.AngleAxis(angle, direction) * right;
            Vector3 up = Vector3.Cross(right, direction);
            Quaternion orientation = Quaternion.LookRotation(direction, up);

            Transform tfm = transform;

            tfm.position = position + right * -element.positionX + up * element.height;
            tfm.rotation = orientation;
        }
    }
}