﻿using UnityEngine;
using System;

namespace Pinpin
{
	public class TrapPhysics : MonoBehaviour
	{
		[SerializeField] private Collider m_collider;
		[SerializeField] private Rigidbody m_rigidBody;
		[SerializeField] private ParticleSystem m_hitFx;

		[Space]
		[SerializeField] private float m_expulsionForce = 25f;
		[SerializeField] private float m_shakeForce = 1f;

		private void Awake ()
		{
			//if(ApplicationManager.datas.level % 4 != 3)
			//{
			//	m_collider.enabled = false;
			//}
		}

		private void OnTriggerEnter ( Collider other )
		{
			if (!other.CompareTag("Player")) return;
		}
	}
}
