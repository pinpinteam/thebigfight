﻿using System;
using UnityEngine;

namespace Pinpin
{
	public abstract class Trap : MonoBehaviour
	{
		// TriggerEnter (true) or TriggerExit - Damage (true) or Buff
		public static Action<bool, Trap> OnCollide;
		public bool isInteractable = true;

		public void OnTriggerEnter ( Collider other )
		{
			if (GameManager.Instance.GameState != GameManager.EGameState.InGame || !isInteractable) return;

			if (other.CompareTag("Player"))
				OnCollide?.Invoke(true, this);
		}

		public void OnTriggerExit(Collider other)
		{
			if (GameManager.Instance.GameState != GameManager.EGameState.InGame || !isInteractable) return;

			if (other.CompareTag("Player"))
				OnCollide?.Invoke(false, this);
		}

		public virtual void ApplyOnPlayer(PlayerGunnerController player)
		{
			player.Kill();
		}

		public virtual void ClearOnPlayer(PlayerGunnerController player)
		{
			return;
		}
		
		public abstract void Configure ( GamePlayElement element, BezierSpline spline );
	}
}