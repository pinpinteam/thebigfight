﻿using UnityEngine;

namespace Pinpin
{
	public class SwatterTrap : Trap
	{
		public override void Configure( GamePlayElement element, BezierSpline spline )
        {
			Vector3 position = spline.GetPointInWorldFromdist(element.positionZ);
			position.x += element.positionX;
			Vector3 direction = spline.GetDirectionFromDist(element.positionZ);
			float angle = spline.GetAngleFromdist(element.positionZ);
			Vector3 right = spline.GetNormalFromdist(element.positionZ);
			right = Quaternion.AngleAxis(angle, direction) * right;
			Vector3 up = Vector3.Cross(right, direction);
			Quaternion orientation = Quaternion.LookRotation(direction, up);

			Transform tfm = transform;

			tfm.position = position;
			tfm.rotation = orientation;
		}

        public static SwatterTrap Instantiate( GamePlayElement element, BezierSpline spline, Transform parent )
		{
			SwatterTrap swatter = Instantiate(ApplicationManager.assets.swatterTrapPrefab, parent);
			swatter.Configure(element, spline);
			return swatter;
		}
	}
}