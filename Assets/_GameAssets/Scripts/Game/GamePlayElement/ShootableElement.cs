﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
	public class ShootableElement : MonoBehaviour
	{
		[Header("Counter")]
		[SerializeField] protected CrowdCounter m_crowdCounter;
		[HideInInspector] public CrowdArea crowdArea = null;

		[HideInInspector] public bool isInteractable = true;
		protected bool m_playerEnterTrigger = false;
		public float splinePosX;

		private void OnTriggerEnter ( Collider other )
		{
			if (GameManager.Instance.GameState != GameManager.EGameState.InGame || !isInteractable) return;
			if (!other.CompareTag("Player") || m_playerEnterTrigger) return;

			GameManager.level.player.OnEnterCrowdAreaRange(this);
			m_playerEnterTrigger = true;
		}

		private void OnTriggerExit ( Collider other )
		{
			if (GameManager.Instance.GameState != GameManager.EGameState.InGame || !isInteractable) return;
			if (!other.CompareTag("Player") || !m_playerEnterTrigger) return;

			GameManager.level.player.OnExitCrowdAreaRange(this);
			m_playerEnterTrigger = false;
		}

		public virtual void GetShot () { }


		protected void UpdateDisplayCount ( int newCount )
		{
			m_crowdCounter.UpdateCounter(newCount);
		}
	}
}
