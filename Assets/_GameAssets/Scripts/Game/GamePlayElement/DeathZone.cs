﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
	public class DeathZone : MonoBehaviour
	{
		public bool isInteractable = true;

		private void OnTriggerEnter ( Collider other )
		{
			if (GameManager.Instance.GameState != GameManager.EGameState.InGame || !isInteractable) return;

			if (other.CompareTag("Player"))
			{
				GameManager.level.player.Kill();
			}
		}
	}
}
