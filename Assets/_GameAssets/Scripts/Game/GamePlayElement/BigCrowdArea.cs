﻿using UnityEngine;

namespace Pinpin
{
	public class BigCrowdArea : CrowdArea
	{
		public override void Configure ( GamePlayElement element, BezierSpline spline )
		{
			ColorIndex = 0;
			float positionZ = element.positionZ;
			Vector3 position = spline.GetPointInWorldFromdist(positionZ);
			position.x += element.positionX;
			Vector3 direction = spline.GetDirectionFromDist(positionZ);
			float angle = spline.GetAngleFromdist(positionZ);
			Vector3 right = spline.GetNormalFromdist(positionZ);
			right = Quaternion.AngleAxis(angle, direction) * right;
			Vector3 up = Vector3.Cross(right, direction);
			Quaternion orientation = Quaternion.LookRotation(direction, up);

			TargetCharacterCount = Mathf.RoundToInt(element.length);

			Transform tfm = transform;
			tfm.position = position;
			tfm.rotation = orientation;
			m_circle.Play();
		}

		public override void ConfigureFake ( GamePlayElement element, BezierSpline spline )
		{
			float positionZ = element.positionZ;
			Vector3 position = spline.GetPointInWorldFromdist(positionZ);
			position.x += element.positionX;
			Vector3 direction = spline.GetDirectionFromDist(positionZ);
			float angle = spline.GetAngleFromdist(positionZ);
			Vector3 right = spline.GetNormalFromdist(positionZ);
			right = Quaternion.AngleAxis(angle, direction) * right;
			Vector3 up = Vector3.Cross(right, direction);
			Quaternion orientation = Quaternion.LookRotation(direction, up);

			TargetCharacterCount = Mathf.RoundToInt(element.length);

			Transform tfm = transform;
			tfm.position = position;
			tfm.rotation = orientation;
			m_circle.Play();
		}

		public static BigCrowdArea Instantiate ( GamePlayElement gamePlayElement, BezierSpline spline, Transform parent )
		{
			BigCrowdArea area = Instantiate(ApplicationManager.assets.bigColorCrowdAreaPrefab, parent);
			area.Configure(gamePlayElement, spline);
			return area;
		}
	}
}