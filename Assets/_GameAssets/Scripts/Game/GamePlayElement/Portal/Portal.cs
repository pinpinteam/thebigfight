﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
    //[ExecuteInEditMode]
    public abstract class Portal : MonoBehaviour
    {
        private float m_positionX;
        private float m_positionZ;

        private bool m_used = false;
        private int m_patternIndex;

        [SerializeField] protected AnimationCurve m_vanishingAnimationCurve;
        
        public bool Used
        {
            get => m_used;
            set
            {
                m_used = value;
                //SetUsed();
            }
        }

        protected void Awake()
        {
            if (PortalLinker.Instance) return;

            PortalLinker.Instance = gameObject.AddComponent<PortalLinker>();
        }

        private void OnTriggerEnter( Collider other )
        {
            if (!other.CompareTag("Player") || m_used) return;

            m_used = true;
            SetUsed();
            
            List<Portal> neighbors = PortalLinker.Instance ?
                PortalLinker.Instance.GetNeighbors(m_patternIndex) : new List<Portal>();
            if (neighbors.Count != 0) {
                foreach (Portal portal in neighbors)
                    portal.Used = true;
            }
            TriggerEnter();
        }

        public void Configure(GamePlayElement element, BezierSpline spline)
        {
            m_patternIndex = (int) element.positionZ;
            if (PortalLinker.Instance)
                PortalLinker.Instance.AddPortal(this, m_patternIndex);
            
            m_positionX = element.positionX;
            m_positionZ = element.positionZ;

            Vector3 position = spline.GetPointInWorldFromdist(m_positionZ);
            position.x += element.positionX;
            Vector3 direction = spline.GetDirectionFromDist(m_positionZ);
            float angle = spline.GetAngleFromdist(m_positionZ);
            Vector3 right = spline.GetNormalFromdist(m_positionZ);
            right = Quaternion.AngleAxis(angle, direction) * right;
            Vector3 up = Vector3.Cross(right, direction);
            Quaternion orientation = Quaternion.LookRotation(direction, up);
            
            transform.position = position;
            transform.rotation = orientation;
        }

        protected abstract void TriggerEnter();
        protected abstract void SetUsed();
    }
}