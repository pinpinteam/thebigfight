﻿using System;
using DG.Tweening;
using UnityEngine;

namespace Pinpin
{
	public class ColorPortal : Portal
	{
		public static Action<float> OnEnterPortal;

		[ShowOnly] private int m_colorValue = 0;
		[SerializeField] private ParticleSystem[] m_particleSystems;
		[SerializeField] private MeshRenderer[] m_renderer;
		[SerializeField] private GameObject[] m_objectsToHide;

		public int ColorValue
		{
			get => m_colorValue;
			set
			{
				Color color = ColorCollection.Instance.ReturnColor(GameManager.level.currentLevelInfo.colorPalette[value]);

				foreach (ParticleSystem particleSytem in m_particleSystems)
				{
					ParticleSystem.MainModule main = particleSytem.main;
					main.startColor = color;
				}

				foreach (MeshRenderer meshRenderer in m_renderer)
				{
					meshRenderer.material.color = color;
				}

				m_colorValue = value;
			}
		}

		protected override void TriggerEnter ()
		{
			if (GameManager.Instance.GameState == GameManager.EGameState.InGame)
				OnEnterPortal?.Invoke(m_colorValue);
		}

		protected override void SetUsed ()
		{
			// foreach (GameObject go in m_objectsToHide)
			// {
			// 	go.transform
			// 		.DOLocalMoveZ(-1.5f, .3f)
			// 		.SetEase(m_vanishingAnimationCurve)
			// 		.OnComplete(() =>
			// 		{
			// 			go.SetActive(false);
			// 		});
			// }
		}

		public static ColorPortal Instantiate ( GamePlayElement element, BezierSpline spline, Transform parent )
		{
			ColorPortal portal = Instantiate(ApplicationManager.assets.colorPortalPrefab, parent);
			portal.ColorValue = element.value;
			portal.Configure(element, spline);
			return portal;
		}
	}
}