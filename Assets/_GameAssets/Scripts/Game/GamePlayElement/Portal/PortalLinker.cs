﻿using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
    public class PortalLinker : MonoBehaviour
    {
        public static PortalLinker Instance;

        private readonly Dictionary<int, List<Portal>> m_portals = new Dictionary<int, List<Portal>>();

        public void AddPortal(Portal portal, int index)
        {
            if (!m_portals.ContainsKey(index))
                m_portals.Add(index, new List<Portal>());
            
            m_portals[index].Add(portal);
        }

        public List<Portal> GetNeighbors(int index)
        {
            return m_portals.ContainsKey(index) ? m_portals[index] : new List<Portal>();
        }
    }
}