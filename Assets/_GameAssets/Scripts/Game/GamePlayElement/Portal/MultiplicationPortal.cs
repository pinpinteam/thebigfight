﻿using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pinpin
{
    public class MultiplicationPortal : Portal
    {
        public static Action<float, MultiplicationPortal> OnEnterPortal;

        private const string m_multiplicationPrefix = "X";
        private const string m_multiplicationUsedSymbol = "X";
        
        [SerializeField] private TextMeshPro m_multiplicationText;
        [SerializeField] private MeshRenderer m_portalWindow;
        [SerializeField] private ParticleSystem m_portalWindowParticleSystem;
        [SerializeField] private int m_multiplicationValue = 2;

        [SerializeField] private TextMeshPro m_characterGenerated;
        
        public int MultiplicationValue
        {
            get => m_multiplicationValue;
            set
            {
                m_multiplicationText.text = $"{value}<size=80%>{m_multiplicationPrefix}</size>";
                m_multiplicationValue = value;
            }
        }

        public int CharacterGenerated
        {
            set
            {
//                 m_characterGenerated.text = $"+{value}";
//                 m_characterGenerated.transform.DOLocalMove(Vector3.up * 100f, 0.3f)
//                     .SetEase(Ease.OutQuad);
//                 m_characterGenerated.DOFade(0f, 0.3f).SetDelay(2f);
// //                    .OnComplete(() => m_characterGenerated.transform.DOScale(Vector3.zero, 0.3f));
//                 m_characterGenerated.transform.DOScale(Vector3.one, .3f);
            }
        }
        
        private new void Awake()
        {
            base.Awake();
            MultiplicationValue = m_multiplicationValue;
        }
        
        protected override void TriggerEnter()
        {
            if (GameManager.Instance.GameState == GameManager.EGameState.InGame)
            {
                OnEnterPortal?.Invoke(m_multiplicationValue, this);
                SetUsed();
            }
        }

        protected override void SetUsed()
        {
            m_portalWindow.enabled = false;
            m_portalWindowParticleSystem.Play();
            m_multiplicationText.transform.DOScale(0f, 0.3f);
        }

        public static MultiplicationPortal Instantiate( GamePlayElement element, BezierSpline spline, Transform parent )
        {
            MultiplicationPortal portal = Instantiate(ApplicationManager.assets.multiplicationPortalPrefab, parent);
            portal.MultiplicationValue = element.value;
            portal.Configure(element, spline);
            return portal;
        }
    }
}