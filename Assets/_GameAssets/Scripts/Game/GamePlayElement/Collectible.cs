﻿using DG.Tweening;
using MoreMountains.NiceVibrations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
	public class Collectible : MonoBehaviour
	{
		static public Action onCollectibleCollected;

		[SerializeField] private Renderer m_renderer;
		[SerializeField] private ParticleSystem m_collisionPS;
		[SerializeField] private ParticleSystem m_endPS;

		bool m_collected = false;

		[Space] [SerializeField]
		private AnimationCurve m_deSpawnAnimationCurve;

		private void OnTriggerEnter ( Collider other )
		{
			if (m_collected || !other.CompareTag("Player"))
				return;

			// if (!m_isWallGem)
			// {
			// 	FXManager.Instance.PlayEmoji(2);
			// }

			m_collisionPS.Play();
			m_collected = true;
			MMVibrationManager.Haptic(HapticTypes.LightImpact);

			transform
				.DOScale(Vector3.zero, .3f)
				.SetEase(m_deSpawnAnimationCurve)
				.OnComplete(() =>
				{
					onCollectibleCollected?.Invoke();
					m_renderer.enabled = false;
					m_endPS.Play();
					Destroy(gameObject);
				});
		}

		public void Configure(GamePlayElement element, BezierSpline spline)
		{
			Vector3 position = spline.GetPointInWorldFromdist(element.positionZ);
			float limitX = spline.GetWidthMultiplierFromdist(element.positionZ) * spline.baseWidth;

		   Vector3 direction = spline.GetDirectionFromDist(element.positionZ);
			float angle = spline.GetAngleFromdist(element.positionZ);
			Vector3 right = spline.GetNormalFromdist(element.positionZ);
			right = Quaternion.AngleAxis(angle, direction) * right;
			Vector3 up = Vector3.Cross(right, direction);
			Quaternion orientation = Quaternion.LookRotation(direction, up);

			position += right * -Mathf.Clamp(element.positionX, -limitX, limitX) + up * element.height;

			Transform tfm = transform;

			tfm.position = position;
			tfm.rotation = orientation;
		}

		public static Collectible Instantiate( GamePlayElement element, BezierSpline spline, Transform parent )
		{
			Collectible collectible = Instantiate(ApplicationManager.assets.collectiblePrefab, parent);
			collectible.Configure(element, spline);
			return collectible;
		}

		// public void SetBodyScale(float scaleFactor)
		// {
		// 	m_renderer.transform.localScale *= scaleFactor;
		// }
	}
}