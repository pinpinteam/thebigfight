﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{

	[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter)), ExecuteInEditMode]
	public class Slope : MonoBehaviour
	{
		[SerializeField, Range(0.01f, 1f)] private float precision = 1f;
		private float m_positionX;
		private float m_positionZ;
		private float m_length;
		private float m_height;
		private float m_width;
		private MeshFilter m_meshFilter;
		public float splineStartDist { get { return m_positionZ; } }
		public float splineEndDist { get { return m_positionZ + m_length; } }
		

		private void Awake ()
		{
			m_meshFilter = GetComponent<MeshFilter>();
		}

		public void Configure ( GamePlayElement element, BezierSpline spline )
		{
			m_positionZ = element.positionZ;
			m_length = Mathf.Abs(element.length);
			m_height = element.height;
			m_width = element.width;
			m_positionX = element.positionX;
			GenerateMesh(spline);
		}

		public static Slope Instantiate ( GamePlayElement slopeElement, BezierSpline spline, Transform parent )
		{
			Slope newSlope = Instantiate(ApplicationManager.assets.slopePrefab, parent);
			newSlope.Configure(slopeElement, spline);
			return newSlope;
		}

		public Vector3 GetLimmits (float fieldwidth, float posX, float posY, float playerDist)
		{
			float x1 = -fieldwidth, x2 = fieldwidth;
			float y = 0f;
			float ratio = (playerDist - m_positionZ) / m_length;
			float slopeHeight = ratio * m_height;
			if (posX < m_positionX - m_width / 2f && posY < slopeHeight)
				x2 = m_positionX - m_width / 2f;
			else if (posX > m_positionX + m_width / 2f && posY < slopeHeight)
				x1 = m_positionX + m_width / 2f;
			else
				y = slopeHeight;
			return new Vector3(x1, x2, y);
		}

		public void GenerateMesh (BezierSpline spline )
		{
			int definition = 4;
			int pointCount = (int)(m_length / precision) + 1;
			int vertexCount = (pointCount + 1) * definition;
			int triangleCount = pointCount * 6 * definition;
			Vector3[] vertices = new Vector3[vertexCount];
			Vector2[] uvs = new Vector2[vertexCount];
			int[] triangles = new int[triangleCount];
			float roadWidth = 4.5f;
			int baseIndex = 0;
			for (int i = 0; i <= pointCount; i++)
			{
				float dist = Mathf.Min(m_positionZ + i * precision, m_positionZ + m_length);
				Vector3 position = spline.GetPointFromDist(dist);
				Vector3 direction = spline.GetDirectionFromDist(dist);
				float widthMultiplier = spline.GetWidthMultiplierFromdist(dist);
				float angle = spline.GetAngleFromdist(dist);
				Vector3 right = spline.GetNormalFromdist(dist);
				right = Quaternion.AngleAxis(angle, direction) * right;
				Vector3 up = Vector3.Cross(right, direction);

				float height = m_height * Mathf.Min(i * precision, m_length) / m_length;
				for (int k = 0; k < definition; k++)
				{
					float pointHeight = (k == 0 || k == 3) ? 0f : height;
					float pointRight = (k == 0 || k == 1) ? -m_width / 2f : m_width / 2f;

					vertices[i * definition + k] = position + up * pointHeight + right * Mathf.Clamp(pointRight - m_positionX, -roadWidth * widthMultiplier + 0.5f, roadWidth * widthMultiplier - 0.5f);
					uvs[i * definition + k] = new Vector2(1f / k, dist);
					if (i < pointCount)
					{
						baseIndex = (i * definition + k) * 6;
						if (k < definition - 1)
						{
							triangles[baseIndex] = i * definition + k;
							triangles[baseIndex + 1] = i * definition + k + 1;
							triangles[baseIndex + 2] = (i + 1) * definition + k;

							triangles[baseIndex + 3] = i * definition + k + 1;
							triangles[baseIndex + 4] = (i + 1) * definition + k + 1;
							triangles[baseIndex + 5] = (i + 1) * definition + k;
						}
						else if (definition > 2)
						{
							triangles[baseIndex] = i * definition + k;
							triangles[baseIndex + 1] = i * definition;
							triangles[baseIndex + 2] = (i + 1) * definition + k;

							triangles[baseIndex + 3] = i * definition;
							triangles[baseIndex + 4] = (i + 1) * definition;
							triangles[baseIndex + 5] = (i + 1) * definition + k;
						}
					}
				}
			}

			Mesh mesh = new Mesh();
			mesh.name = gameObject.name;
			mesh.vertices = vertices;
			mesh.triangles = triangles;
			mesh.uv = uvs;
			mesh.RecalculateNormals();
			mesh.RecalculateBounds();
			m_meshFilter.mesh = mesh;
		}
	}

}