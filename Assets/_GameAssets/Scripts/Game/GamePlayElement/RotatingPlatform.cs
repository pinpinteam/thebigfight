﻿using System.Collections;
using UnityEngine;

namespace Pinpin
{
    public class RotatingPlatform : MonoBehaviour
    {
		[SerializeField] GameObject[] patterns;
		GameObject pattern = null;
		public void Configure( GamePlayElement element, BezierSpline spline )
        {
            float positionZ = element.positionZ;
            Vector3 position = spline.GetPointInWorldFromdist(positionZ);
            position.x += element.positionX;
            Vector3 direction = spline.GetDirectionFromDist(positionZ);
            float angle = spline.GetAngleFromdist(positionZ);
            Vector3 right = spline.GetNormalFromdist(positionZ);
            right = Quaternion.AngleAxis(angle, direction) * right;
            Vector3 up = Vector3.Cross(right, direction);
            Quaternion orientation = Quaternion.LookRotation(direction, up);
            
            Transform tfm = transform;
            tfm.position = position;
            tfm.rotation = orientation;
			if (pattern != null)
				StartCoroutine(Destroy(pattern));
			pattern = Instantiate(patterns[Mathf.Clamp(element.value, 0, patterns.Length - 1)], transform);
		}

		public static RotatingPlatform Instantiate( GamePlayElement element, BezierSpline spline, Transform parent )
        {
            RotatingPlatform platform = Instantiate(ApplicationManager.assets.rotatingPlatformPrefab, parent);
            platform.Configure(element, spline);
            return platform;
		}

		IEnumerator Destroy ( GameObject go )
		{
			yield return new WaitForEndOfFrame();
			DestroyImmediate(go);
		}
	}
}