﻿using System;
using UnityEngine;

namespace Pinpin
{
    [Serializable]
    public class GamePlayElement : IComparable<GamePlayElement>
    {
        public enum GamePlayElementType
        {
            Wall,
			Slope,
            CrowdArea = 3,
            CollectibleGem,
            Saw,
            Pendulum,
            RotatingCylinder,
            SpikyCylinder,
            Swatter,
            RotatingPlatform,
            BigCrowdArea,
            CollectibleBullet,
            BarrelWall,
            BulletStack,
            RotorTrap,
        }

        public GamePlayElementType type;

        [Header("Based on spline")]
        [Range(-4.75f, 4.75f)] public float positionX;
        [Min(0f)] public float positionZ;
        
        [Header("Only if volume")]
        public float length;
        [Min(0.01f)] public float width;
        [Min(0.01f)] public float height;
        public int value;

        public int CompareTo ( GamePlayElement other )
        {
            return positionZ.CompareTo(other.positionZ);
        }

        public GamePlayElement Clone()
        {
            return new GamePlayElement
            {
                type = type,
                positionX = positionX,
                positionZ = positionZ,
                length = length,
                width = width,
                height = height,
                value = value
            };
        }
    }
}