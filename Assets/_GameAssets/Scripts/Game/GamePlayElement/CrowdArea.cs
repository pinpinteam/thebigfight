﻿using System;
using DG.Tweening;
using UnityEngine;

namespace Pinpin
{
	public class CrowdArea : CrowdController
	{
		[Space]
		[Header("Particles")]
		[SerializeField] protected ParticleSystem m_circle;
		[SerializeField] private ParticleSystem m_innerCircle;
		[SerializeField] private ParticleSystem m_dust;
		[SerializeField] private ParticleSystem m_aura;
		[SerializeField] private ParticleSystem m_pulse;

		[Space]
		[Header("Items to disable on use")]
		[SerializeField] private DeathZone m_deathZone;
		[SerializeField] private GameObject[] m_gameObjectsToHide;
		[SerializeField] private AnimationCurve m_vanishingAnimationCurve;

		public static Action<float> stopAnimRunAction;

		private float m_currentSpeed = 0f;
		private bool m_setAnimationSpeed = false;

		private Quaternion m_offsetRotation;
		private Vector3 m_offsetWhithPlayer;

		private bool m_initDone;

		private void Awake ()
		{
			stopAnimRunAction += SetAnimationRun;
		}

		private void OnDestroy ()
		{
			stopAnimRunAction -= SetAnimationRun;
		}

		protected new void Start ()
		{
			base.Start();
			crowdArea = this;
			UpdateColor();
			UpdateDisplayCount(TargetCharacterCount);
			m_circle.Play();
		}

		private void FixedUpdate ()
		{
			if (m_playerEnterTrigger && !GameManager.level.player.stopMoving)
			{
				MoveCrowdFollowPlayer();
			}
		}

		protected override void UpdateColor ()
		{
			base.UpdateColor();
			m_color = ColorCollection.Instance.ReturnColor(m_currentColorPreset);

			// Particles update
			ParticleSystem.MainModule main = m_dust.main;
			m_color.a = main.startColor.color.a;
			main.startColor = m_color;
			main = m_circle.main;
			m_color.a = main.startColor.color.a;
			main.startColor = m_color;
			main = m_innerCircle.main;
			m_color.a = main.startColor.color.a;
			main.startColor = m_color;
			main = m_aura.main;
			m_color.a = main.startColor.color.a;
			main.startColor = m_color;
			main = m_pulse.main;
			m_color.a = main.startColor.color.a;
			main.startColor = m_color;
		}

		public virtual void Configure ( GamePlayElement element, BezierSpline spline )
		{
			ColorIndex = 0;// element.value;
			float positionZ = element.positionZ;
			Vector3 position = spline.GetPointInWorldFromdist(positionZ);
			position.x += element.positionX;
			Vector3 direction = spline.GetDirectionFromDist(positionZ);
			float angle = spline.GetAngleFromdist(positionZ);
			Vector3 right = spline.GetNormalFromdist(positionZ);
			right = Quaternion.AngleAxis(angle, direction) * right;
			Vector3 up = Vector3.Cross(right, direction);
			Quaternion orientation = Quaternion.LookRotation(direction, up);

			TargetCharacterCount = Mathf.RoundToInt(element.length);

			Transform tfm = transform;
			tfm.position = position;
			tfm.rotation = orientation;
			m_circle.Play();
		}

		public virtual void ConfigureFake ( GamePlayElement element, BezierSpline spline )
		{
			float positionZ = element.positionZ;
			Vector3 position = spline.GetPointInWorldFromdist(positionZ);
			position.x += element.positionX;
			Vector3 direction = spline.GetDirectionFromDist(positionZ);
			float angle = spline.GetAngleFromdist(positionZ);
			Vector3 right = spline.GetNormalFromdist(positionZ);
			right = Quaternion.AngleAxis(angle, direction) * right;
			Vector3 up = Vector3.Cross(right, direction);
			Quaternion orientation = Quaternion.LookRotation(direction, up);

			TargetCharacterCount = Mathf.RoundToInt(element.length);

			Transform tfm = transform;
			tfm.position = position;
			tfm.rotation = orientation;
			m_circle.Play();
		}

		public static CrowdArea Instantiate ( GamePlayElement gamePlayElement, BezierSpline spline, Transform parent )
		{
			CrowdArea area = Instantiate(ApplicationManager.assets.colorCrowdAreaPrefab, parent);
			area.ColorIndex = gamePlayElement.value;
			area.Configure(gamePlayElement, spline);
			area.splinePosX = gamePlayElement.positionX;
			return area;
		}

		public override void GetShot ()
		{
			PickCharacter().DelayKill();
		}

		public void ShotCharacter (Character character)
		{
			GameObject gem = Instantiate(ApplicationManager.assets.gemPrefab, character.transform.position + new Vector3(0f, 0.2f, 0f), Quaternion.identity, GameManager.level.transform);
			gem.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
			character.DelayKill();
		}

		protected override void SetEmpty ()
		{
			isInteractable = false;
			m_deathZone.isInteractable = false;
			foreach (GameObject go in m_gameObjectsToHide)
			{
				go.transform
					.DOScale(Vector3.zero, .3f)
					.SetEase(m_vanishingAnimationCurve)
					.OnComplete(() =>
					{
						go.SetActive(false);
					});
			}
		}

		#region CROWD MANAGEMENT

		private void MoveCrowdFollowPlayer ()
		{
			if (!m_setAnimationSpeed)
			{
				SetAnimationRun(1f);
				m_setAnimationSpeed = true;
			}

			m_currentSpeed += ApplicationManager.config.game.speedMoveTowardPlayer * Time.fixedDeltaTime;
			if (m_currentSpeed > ApplicationManager.config.game.speedMoveTowardPlayer)
				m_currentSpeed = ApplicationManager.config.game.speedMoveTowardPlayer;

			foreach (Character character in m_charactersList)
			{
				if (GameManager.level.player.characterTransform.position.z > character.transform.position.z)
				{
					if (character.isRunning)
					{
						character.SetAnimatorRunValue(0f);
						character.isRunning = false;
					}
					continue;
				}
				m_offsetWhithPlayer = GameManager.level.player.characterTransform.position - character.transform.position;
				m_offsetRotation = Quaternion.LookRotation(m_offsetWhithPlayer);
				character.transform.rotation = Quaternion.Lerp(character.transform.rotation, m_offsetRotation, 5f * Time.fixedDeltaTime);
				character.transform.position = character.transform.forward * m_currentSpeed * Time.fixedDeltaTime + character.transform.position;
				//character.transform.localPosition = new Vector3(0f, GameManager.level.player.bezierSpline.GetPointInWorldFromdist(character.transform.position.z).y, 0f);
				if (m_offsetWhithPlayer.sqrMagnitude < 1f)
					GameManager.level.player.Kill();
			}
		}

		private void SetAnimationRun(float value )
		{
			foreach (Character character in m_charactersList)
			{
				character.SetAnimatorRunValue(value);
				character.isRunning = true;
			}
		}

		public Character PickCharacter ()
		{
			int rndIndex = UnityEngine.Random.Range(0, m_charactersList.Count);
			Character characterPicked = m_charactersList[rndIndex];
			RemoveCharacter(characterPicked);
			--TargetCharacterCount;

			BalanceCrowdSize();

			characterPicked.transform.SetParent(GameManager.level.transform);

			return characterPicked;
		}

		public void FollowPlayer ()
		{

		}
		#endregion
	}
}