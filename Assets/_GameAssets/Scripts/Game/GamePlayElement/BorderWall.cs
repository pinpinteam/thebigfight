﻿using UnityEngine;

namespace Pinpin
{

	[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter)), ExecuteInEditMode]
	public class BorderWall : MonoBehaviour
	{
		[SerializeField, Range(0.01f, 1f)] private float precision = 1f;
		private float m_positionX;
		private float m_positionZ;
		private float m_length;
		private float m_height;
		private float m_width;
		[SerializeField] private MeshRenderer m_meshRenderer;
		private MeshFilter m_meshFilter;
		public float splineStartDist { get { return m_positionZ; } }
		public float splineEndDist { get { return m_positionZ + m_length; } }

		static private Vector3[] predefinedNormals = { Vector3.left, new Vector3(-1f, 1f, 0f).normalized, new Vector3(1f, 1f, 0f).normalized, Vector3.right };

		private void Awake ()
		{
			m_meshFilter = GetComponent<MeshFilter>();
		}

		public void Configure (GamePlayElement element, BezierSpline spline)
		{
			m_positionZ = element.positionZ;
			m_length = Mathf.Abs(element.length);
			m_height = element.height;
			m_width = element.width;
			m_positionX = element.positionX;
			GenerateMesh(spline);
		}

		public static BorderWall Instantiate( GamePlayElement wallElement, BezierSpline spline, Transform parent )
		{
			BorderWall newWall = Instantiate(ApplicationManager.assets.borderWallPrefab, parent);
			newWall.Configure(wallElement, spline);
			return newWall;
		}

		#region Mesh

		private void GenerateMesh ( BezierSpline spline )
		{
			int definition = 4;
			//int doubleDefinition = definition * 2;
			int pointCount = (int)(m_length / precision) + 1;
			int vertexCount = (pointCount + 1) * definition * 2/* + 4*/;
			int triangleCount = pointCount * 6 * (definition - 1)/* + 6*/;
			Vector3[] vertices = new Vector3[vertexCount];
			Vector3[] normals = new Vector3[vertexCount];
			Vector2[] uvs = new Vector2[vertexCount];
			int[] triangles = new int[triangleCount];
			float roadWidth = 4.5f;

			int baseIndex = 0;
			for (int i = 0; i <= pointCount; i++)
			{
				float dist = Mathf.Min(m_positionZ + i * precision, m_positionZ + m_length);
				Vector3 position = spline.GetPointFromDist(dist);
				Vector3 direction = spline.GetDirectionFromDist(dist);
				float widthMultiplier = spline.GetWidthMultiplierFromdist(dist);
				float angle = spline.GetAngleFromdist(dist);
				Vector3 right = spline.GetNormalFromdist(dist);
				right = Quaternion.AngleAxis(angle, direction) * right;
				Vector3 up = Vector3.Cross(right, direction);
				float cumulativeUVx = 0.25f;
				
				float positionX = m_positionX;
				float width = m_width;
				float height = m_height;
				for (int k = 0; k < definition; k++)
				{
					float pointHeight = (k == 0 || k == 3) ? 0f : height;
					float pointRight = m_positionX > 0f ? (k == 0 || k == 1) ? 0f : width : (k == 0 || k == 1) ? -width : 0f;

					vertices[i * definition + k] = position + up * pointHeight + right * Mathf.Clamp(pointRight - m_positionX * widthMultiplier, -roadWidth * widthMultiplier, roadWidth * widthMultiplier);
					normals[i * definition + k] = predefinedNormals[k];
					uvs[i * definition + k] = new Vector2(cumulativeUVx, dist);
					cumulativeUVx += (k == 0 || k == 2) ? height : width;
					if (i < pointCount)
					{
						baseIndex = (i * (definition - 1) + k) * 6;
						if (k < definition - 1)
						{
							triangles[baseIndex] = i * definition + k;
							triangles[baseIndex + 1] = i * definition + k + 1;
							triangles[baseIndex + 2] = (i + 1) * definition + k;

							triangles[baseIndex + 3] = i * definition + k + 1;
							triangles[baseIndex + 4] = (i + 1) * definition + k + 1;
							triangles[baseIndex + 5] = (i + 1) * definition + k;
						}
						/*else
						{
							triangles[baseIndex] = i * definition;
							triangles[baseIndex + 1] = (i + 1) * definition;
							triangles[baseIndex + 2] = i * definition + k * 2 + 1;

							triangles[baseIndex + 3] = i * definition + k * 2 + 1;
							triangles[baseIndex + 4] = (i + 1) * definition;
							triangles[baseIndex + 5] = (i + 1) * definition + k * 2 + 1;
						}*/
					}
				}
				/*for (int k = 0; k < definition; k++)
				{
					float pointHeight = (k == 0 || k == 3) ? 0f : height;
					float pointRight = (k == 0 || k == 1) ? -width / 2f : width / 2f;

					vertices[i * doubleDefinition + k * 2] = position + up * pointHeight + right * (pointRight - positionX);
					vertices[i * doubleDefinition + k * 2 + 1] = vertices[i * doubleDefinition + k * 2];
					uvs[i * doubleDefinition + k * 2] = new Vector2(cumulativeUVx, dist);
					uvs[i * doubleDefinition + k * 2 + 1] = uvs[i * doubleDefinition + k * 2];
					cumulativeUVx += (k == 0 || k == 2) ? height : width;
					if (i < pointCount)
					{
						baseIndex = (i * definition + k) * 6;
						if (k < definition - 1)
						{
							triangles[baseIndex] = i * doubleDefinition + k * 2 + 1;
							triangles[baseIndex + 1] = i * doubleDefinition + k * 2 + 2;
							triangles[baseIndex + 2] = (i + 1) * doubleDefinition + k * 2 + 1;

							triangles[baseIndex + 3] = i * doubleDefinition + k * 2 + 2;
							triangles[baseIndex + 4] = (i + 1) * doubleDefinition + k * 2 + 2;
							triangles[baseIndex + 5] = (i + 1) * doubleDefinition + k * 2 + 1;
						}
						else
						{
							triangles[baseIndex] = i * doubleDefinition;
							triangles[baseIndex + 1] = (i + 1) * doubleDefinition;
							triangles[baseIndex + 2] = i * doubleDefinition + k * 2 + 1;

							triangles[baseIndex + 3] = i * doubleDefinition + k * 2 + 1;
							triangles[baseIndex + 4] = (i + 1) * doubleDefinition;
							triangles[baseIndex + 5] = (i + 1) * doubleDefinition + k * 2 + 1;
						}
					}
				}*/
			}
			/*// front side Face
			vertices[vertexCount - 4] = vertices[0];
			uvs[vertexCount - 4] = new Vector2(0f, 0f);
			vertices[vertexCount - 3] = vertices[1];
			uvs[vertexCount - 3] = new Vector2(0f, 1f);
			vertices[vertexCount - 2] = vertices[2];
			uvs[vertexCount - 2] = new Vector2(1f, 1f);
			vertices[vertexCount - 1] = vertices[3];
			uvs[vertexCount - 1] = new Vector2(1f, 0f);
			baseIndex = triangleCount - 6;
			triangles[baseIndex] = vertexCount - 4;
			triangles[baseIndex + 1] = vertexCount - 2;
			triangles[baseIndex + 2] = vertexCount - 3;

			triangles[baseIndex + 3] = vertexCount - 2;
			triangles[baseIndex + 4] = vertexCount - 4;
			triangles[baseIndex + 5] = vertexCount - 1;
			
			// back side Face
			vertices[vertexCount - 4] = vertices[vertexCount - 16];
			uvs[vertexCount - 4] = new Vector2(1f, 0f);
			vertices[vertexCount - 3] = vertices[vertexCount - 14];
			uvs[vertexCount - 3] = new Vector2(1f, 1f);
			vertices[vertexCount - 2] = vertices[vertexCount - 12];
			uvs[vertexCount - 2] = new Vector2(0f, 1f);
			vertices[vertexCount - 1] = vertices[vertexCount - 10];
			uvs[vertexCount - 1] = new Vector2(0f, 0f);
			baseIndex = triangleCount - 6;
			triangles[baseIndex] = vertexCount - 4;
			triangles[baseIndex + 1] = vertexCount - 3;
			triangles[baseIndex + 2] = vertexCount - 2;

			triangles[baseIndex + 3] = vertexCount - 4;
			triangles[baseIndex + 4] = vertexCount - 2;
			triangles[baseIndex + 5] = vertexCount - 1;*/

			Mesh mesh = new Mesh();
			mesh.name = gameObject.name;
			mesh.vertices = vertices;
			mesh.triangles = triangles;
			mesh.normals = normals;
			mesh.uv = uvs;
			//mesh.RecalculateNormals();
			mesh.RecalculateBounds();
			m_meshFilter.mesh = mesh;
		}

		#endregion
	}
}