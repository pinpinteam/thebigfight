﻿using System.Collections;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using System;
using MoreMountains.NiceVibrations;

namespace Pinpin
{
	public class CollectibleBullet : MonoBehaviour
	{
		static public Action<int> onBulletCollectedAction;

		[SerializeField] private ParticleSystem m_collisionPS;
		[SerializeField] private ParticleSystem m_endPS;
		[SerializeField] private AnimationCurve m_deSpawnAnimationCurve;

		bool m_collected = false;

		private void OnTriggerEnter ( Collider other )
		{
			if (m_collected || !other.CompareTag("Player"))
				return;

			MMVibrationManager.Haptic(HapticTypes.LightImpact);
			if (m_collisionPS != null) m_collisionPS.Play();
			m_collected = true;

			transform.DOScale(Vector3.zero, .3f).SetEase(m_deSpawnAnimationCurve).OnComplete(() =>
			{
				onBulletCollectedAction?.Invoke(1);
				if (m_endPS != null) m_endPS.Play();
				Destroy(gameObject);
			});
		}

		public void Configure ( GamePlayElement element, BezierSpline spline )
		{
			Vector3 position = spline.GetPointInWorldFromdist(element.positionZ);
			float limitX = spline.GetWidthMultiplierFromdist(element.positionZ) * spline.baseWidth;

			Vector3 direction = spline.GetDirectionFromDist(element.positionZ);
			float angle = spline.GetAngleFromdist(element.positionZ);
			Vector3 right = spline.GetNormalFromdist(element.positionZ);
			right = Quaternion.AngleAxis(angle, direction) * right;
			Vector3 up = Vector3.Cross(right, direction);
			Quaternion orientation = Quaternion.LookRotation(direction, up);

			position += right * -Mathf.Clamp(element.positionX, -limitX, limitX) + up * element.height;

			transform.position = position;
			transform.rotation = orientation;
		}

		public static CollectibleBullet Instantiate ( GamePlayElement element, BezierSpline spline, Transform parent )
		{
			CollectibleBullet collectible = Instantiate(ApplicationManager.assets.collectibleBulletPrefab, parent);
			collectible.Configure(element, spline);
			return collectible;
		}
	}
}
