﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Pinpin.Helpers;
using UnityEngine;

namespace Pinpin
{
	public class BulletStack : MonoBehaviour
	{
		public static Action<Character, BulletStack> OnEnterArea;

		public bool isInteractable = true;
		[Space]
		[Header("Particles")]
		[SerializeField] protected ParticleSystem m_circle;
		[SerializeField] private ParticleSystem m_pulse;
		
		private float m_currentSpeed = 0f;
		
		
		private GameObject m_bulletPrefab;
		private List<GameObject> m_bulletList = new List<GameObject>();
		[SerializeField] private Transform bulletParent;

		private void Start ()
		{
			m_circle.Play();
		}
		

		private void OnTriggerEnter ( Collider other )
		{
			if (GameManager.Instance.GameState != GameManager.EGameState.InGame || !isInteractable) return;

			if (!other.CompareTag("Player")) return;

			Character character = other.GetComponent<Character>();
			

			m_pulse.Play();
			OnEnterArea?.Invoke(character, this);
		}

		public virtual void Configure ( GamePlayElement element, BezierSpline spline )
		{
			float positionZ = element.positionZ;
			Vector3 position = spline.GetPointInWorldFromdist(positionZ);
			Vector3 direction = spline.GetDirectionFromDist(positionZ);
			float angle = spline.GetAngleFromdist(positionZ);
			Vector3 right = spline.GetNormalFromdist(positionZ);
			right = Quaternion.AngleAxis(angle, direction) * right;
			Vector3 up = Vector3.Cross(right, direction);
			Quaternion orientation = Quaternion.LookRotation(direction, up);
			position -= right * element.positionX;

			List<Vector3> positions = MathHelper.GetPositionListCircle(Vector3.zero, 0.5f, element.value);
			for (int i = 0; i < positions.Count; i++)
			{
				AddBullet(positions[i], ApplicationManager.assets.bulletPrefab);
			}

			Transform tfm = transform;
			tfm.position = position;
			tfm.rotation = orientation;
			m_circle.Play();
		}

		public virtual void ConfigureFake ( GamePlayElement element, BezierSpline spline, GameObject bulletPrefab = null )
		{
			float positionZ = element.positionZ;
			Vector3 position = spline.GetPointInWorldFromdist(positionZ);
			Vector3 direction = spline.GetDirectionFromDist(positionZ);
			float angle = spline.GetAngleFromdist(positionZ);
			Vector3 right = spline.GetNormalFromdist(positionZ);
			right = Quaternion.AngleAxis(angle, direction) * right;
			Vector3 up = Vector3.Cross(right, direction);
			Quaternion orientation = Quaternion.LookRotation(direction, up);
			position -= right * element.positionX;

			if (bulletPrefab != null)
				m_bulletPrefab = bulletPrefab;
			if (m_bulletList.Count != element.value)
			{
				for (int i = 0; i < m_bulletList.Count; i++)
				{
					StartCoroutine(Destroy(m_bulletList[i]));
				}
				m_bulletList.Clear();

				List<Vector3> positions = MathHelper.GetPositionListCircle(Vector3.zero, 0.5f, element.value);
				for (int i = 0; i < positions.Count; i++)
				{
					AddBullet(positions[i], m_bulletPrefab);
				}
			}

			Transform tfm = transform;
			tfm.position = position;
			tfm.rotation = orientation;
			m_circle.Play();
		}

		protected void AddBullet ( Vector3 offset, GameObject bulletPrefab)
		{
			Vector3 basePosition = transform.position;
			Vector3 position = basePosition + offset;
			GameObject newBullet = Instantiate(bulletPrefab, position, transform.rotation, bulletParent);

			m_bulletList.Add(newBullet);

		}

		public static BulletStack Instantiate ( GamePlayElement gamePlayElement, BezierSpline spline, Transform parent )
		{
			BulletStack bulletStack = Instantiate(ApplicationManager.assets.bulletStackPrefab, parent);
			bulletStack.Configure(gamePlayElement, spline);
			return bulletStack;
		}
		

		IEnumerator Destroy ( GameObject go )
		{
			yield return new WaitForEndOfFrame();
			DestroyImmediate(go);
		}

		#region CROWD MANAGEMENT

		#endregion
	}
}