﻿using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pinpin
{
    public class CrowdCounter : MonoBehaviour
    {
        [SerializeField] protected Text m_counterText;
        [SerializeField] protected TextMeshPro m_popCounterText;
        [SerializeField] protected float m_YOffSet = 80f;
        // [SerializeField] protected Image[] m_backgroundImages;

        private Tweener m_countBounceTween;

        private Vector3 defaultScale;
        private Vector3 defaultCharacterGainedPosition;
        
        private Vector3 m_defaultPopCounterPosition;
        
        private void Awake()
        {
            defaultScale = transform.localScale;
            m_defaultPopCounterPosition = m_popCounterText.transform.localPosition;
        }

        public void UpdateCounter(int value)
        {
            //UpdateCharacterGenerated(value - m_formerCount);
            
            m_countBounceTween.Pause();
            m_countBounceTween.Kill();
            m_counterText.rectTransform.localScale = Vector3.one;
            m_countBounceTween = m_counterText.rectTransform.DOPunchScale(Vector3.one * 0.15f, 0.3f);

            m_counterText.text = $"{value}";
            
            if (value <= 0)
            {
                Hide();
            }
            else
            {
                Show();
            }
        }

        private void ResetPopCounter()
        {
            m_popCounterText.transform.localPosition = m_defaultPopCounterPosition;
            m_popCounterText.transform.localScale = Vector3.zero;
            m_popCounterText.DOFade(1f, 0f);
        }
        
        public void UpdateCharacterGenerated( int count )
        {
            if (count <= 0) return;

            m_popCounterText.DOKill();
            ResetPopCounter();
            
            m_popCounterText.text = $"+{count}";
            m_popCounterText.transform
                .DOLocalMove(Vector3.up * m_YOffSet, 0.5f)
                .SetEase(Ease.OutQuad);
            m_popCounterText.transform.DOScale(Vector3.one, .5f);
            m_popCounterText
                .DOFade(0f, 0.5f)
                .SetDelay(2f)
                .OnComplete(ResetPopCounter);
        }
        
        private void Hide()
        {
            transform.DOScale(Vector3.zero, 0.3f);
        }

        private void Show()
        {
            transform.DOScale(defaultScale, 0.3f);
        }
    }
}