﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{

	public class ColorCollection : Singleton<ColorCollection>
	{
		public Gradient[] colors;

		public Gradient ReturnGradient ( ColorPreset preset )
		{
			return colors[(int)preset];
		}

		public Color ReturnColor ( ColorPreset preset, int index )
		{
			return colors[(int)preset].colorKeys[index].color;
		}

		public Color ReturnColor ( ColorPreset preset )
		{
			return ReturnColor(preset, 0);
		}

		// public Color ReturnWaterColor ( ColorPreset preset )
		// {
		// 	return ReturnColor(preset, UI.CheatCanvas.enviroIndex == 0? 1 : 2);
		// }
	}

	public enum ColorPreset
	{
		Red,
		Yellow,
		Blue,
		Pink,
		Purple,
		Cyan,
		Green
	}
}
