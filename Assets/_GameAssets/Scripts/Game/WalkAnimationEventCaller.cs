﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkAnimationEventCaller : MonoBehaviour
{
	public static Action<bool> onWalk;

	public void LeftStep ()
	{
		onWalk?.Invoke(true);
	}

	public void RightStep ()
	{
		onWalk?.Invoke(false);
	}
}
