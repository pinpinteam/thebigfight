﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


namespace Pinpin
{
	public class DetectionToFinisher : MonoBehaviour
	{

		public static Action JumpToFinisherAction;

		private void OnTriggerEnter ( Collider other )
		{
			if (other.CompareTag("Player") && GameManager.Instance.GameState != GameManager.EGameState.TransitionToFinisher)
			{
				GameManager.Instance.GameState = GameManager.EGameState.TransitionToFinisher;
			}
		}

	}
}