﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Pinpin.InputControllers;
using Cinemachine;
using MoreMountains.NiceVibrations;

namespace Pinpin
{
	public class Finisher : MonoBehaviour
	{
		public static Action JumpToFinisherAction;

		[SerializeField] private Transform m_posPlayerToPlatform = null;
		[SerializeField] private Transform m_posEnemyPlatform = null;

		private List<CharacterFinisher> m_charactersFinisher = new List<CharacterFinisher>();
		private List<Transform> m_bullets = new List<Transform>();
		private bool m_playerIsDetect = false;
		private PlayerGunnerController m_playerGunnerController;
		private bool m_scopeIsActive = false;
		private Vector2 m_scopeWantedPos = Vector2.zero;
		private GameObject m_scopeInstance;
		private Vector3 m_scopeStartPos;
		private Quaternion m_startBodyRotation;
		private int m_enemyInLifeCount = 0;

		private bool m_isShootCorout = false;
		private bool m_canMoveScope = true;

		private GameConfig.GameSettings gameConfig
		{
			get { return ApplicationManager.config.game; }
		}

		private void Awake ()
		{
		}

		private void OnDestroy ()
		{
		}

		private void Start ()
		{
			InitEnemyPlatform();
		}

		private void Update ()
		{
			if (GameManager.Instance.GameState == GameManager.EGameState.Finisher)
			{
				Shoot();
			}
		}

		private void FixedUpdate ()
		{
			if (GameManager.Instance.GameState == GameManager.EGameState.Finisher && m_isShootCorout)
			{
			}
		}

		private void OnTriggerEnter ( Collider other )
		{
			if(other.CompareTag("Player") && !m_playerIsDetect)
			{
				m_playerIsDetect = true;
				m_playerGunnerController = other.attachedRigidbody.transform.parent.GetComponent<PlayerGunnerController>();
				GameManager.level.ammoCountOnFinisher = (int)m_playerGunnerController.ammoCount;
				JumpToFinisherAction?.Invoke();
				StartCoroutine(MovePlayerToPlatform(other.attachedRigidbody.transform.parent));
				m_enemyInLifeCount = GameManager.level.currentLevelInfo.enemyQuantityFinisher;
			}
		}

		private void Shoot ()
		{
			if (InputManager.isTouching && m_canMoveScope)
			{
				if (!m_scopeIsActive)
				{
					m_scopeIsActive = true;
					//m_scopeWantedPos = new Vector2(InputManager.touchPosRatio.x * 5f, 0);
					m_scopeInstance.gameObject.SetActive(true);
				}
				m_scopeWantedPos.x += Mathf.Clamp(InputManager.moveDeltaV2.x * gameConfig.shootMoveSensitivity, -5.6f, 5.6f);

				if (m_scopeWantedPos.x > 5.6f) m_scopeWantedPos.x = 5.6f;
				else if (m_scopeWantedPos.x < -5.6f) m_scopeWantedPos.x = -5.6f;

				m_scopeInstance.transform.position = new Vector3(m_scopeStartPos.x + m_scopeWantedPos.x, m_scopeStartPos.y, m_scopeStartPos.z);

				m_playerGunnerController.body.rotation = Quaternion.LookRotation(m_scopeInstance.transform.position - m_playerGunnerController.body.position) * m_startBodyRotation;
			}
		}

		private IEnumerator MovePlayerToPlatform (Transform player)
		{
			Vector3 startPos = player.transform.position;
			float posY;
			float percentValue;
			float startTime = Time.time;
			m_playerGunnerController.finisherCamera.Priority = 100;
			while (Time.time < startTime + gameConfig.durationJumpFinisher)
			{
				percentValue = (Time.time - startTime) / gameConfig.durationJumpFinisher;
				posY = (m_posPlayerToPlatform.position.y - startPos.y) * gameConfig.animCurveJump.Evaluate(percentValue);
				player.transform.position = new Vector3(Mathf.Lerp(startPos.x, m_posPlayerToPlatform.position.x, percentValue), posY, Mathf.Lerp(startPos.z, m_posPlayerToPlatform.position.z, percentValue));
				yield return null;
			}
			player.transform.position = m_posPlayerToPlatform.position;
			m_playerGunnerController.body.localEulerAngles = new Vector3(0f, 135f, 0f);
			m_startBodyRotation = m_playerGunnerController.body.rotation;

			Vector3 scopePos = m_posPlayerToPlatform.position + new Vector3(0f, 1.5f, 19f);
			m_scopeInstance = Instantiate(ApplicationManager.assets.scopePrefab, scopePos, Quaternion.identity, transform.parent);
			m_scopeStartPos = m_scopeInstance.transform.position;
			GameManager.Instance.GameState = GameManager.EGameState.Finisher;
			m_playerGunnerController.stopMoving = true;
		}

		private IEnumerator MoveRocketAnimation (Transform rocket)
		{
			float startTime = Time.time;
			Vector3 startPos = rocket.position;
			while(Time.time < startTime + 1f)
			{
				rocket.position = Vector3.Lerp(startPos, m_posEnemyPlatform.position, (Time.time - startTime) / 1f);
				yield return null;
			}
			MMVibrationManager.Haptic(HapticTypes.HeavyImpact);
			FXManager.Instance.PlayBombExplosion(m_posEnemyPlatform.position);
			FXManager.Instance.PlayBombExplosion(m_posEnemyPlatform.position + Vector3.right);
			FXManager.Instance.PlayBombExplosion(m_posEnemyPlatform.position + Vector3.left);
			foreach (CharacterFinisher enemy in m_charactersFinisher)
			{
				enemy.rigidbody.useGravity = true;
				enemy.rigidbody.AddExplosionForce(1500, m_posEnemyPlatform.position + Vector3.down, 100f);
			}
			rocket.gameObject.SetActive(false);
		}

		private void InitEnemyPlatform ()
		{
			float sizeCircle = Mathf.Clamp((float)GameManager.level.currentLevelInfo.enemyQuantityFinisher / 100f * 4.4f, 1f,  4.4f);
			for (int i = 0; i < GameManager.level.currentLevelInfo.enemyQuantityFinisher; i++)
			{
				Vector2 randomInCircle = UnityEngine.Random.insideUnitCircle * sizeCircle;
				Vector3 enemyPosition = new Vector3(randomInCircle.x, 0f, randomInCircle.y) + m_posEnemyPlatform.position;
				CharacterFinisher enemy = Instantiate(ApplicationManager.assets.characterFinisher, enemyPosition, Quaternion.identity, transform.parent);
				m_charactersFinisher.Add(enemy);
			}
		}

		private void Endlevel ()
		{
			GameManager.Instance.LevelWon();
		}
	}
}
