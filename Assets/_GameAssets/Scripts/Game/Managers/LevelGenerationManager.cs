﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
	public class LevelGenerationManager : MonoBehaviour
	{
		[SerializeField] private PlayerGunnerController m_player;
		[SerializeField] private BezierSpline m_spline;

		private float m_lastPositionZ = 0f;
		private bool m_playing;
		private Vector3 m_limits;
		private List<Wall> m_walls = new List<Wall>();
		private List<Slope> m_slopes = new List<Slope>();

		private Dictionary<GamePlayElement.GamePlayElementType, Delegate> InstantiateDictionnary =
			new Dictionary<GamePlayElement.GamePlayElementType, Delegate>
			{
				{GamePlayElement.GamePlayElementType.Wall, new Func<GamePlayElement, BezierSpline, Transform, Wall>(Wall.Instantiate)},
				{GamePlayElement.GamePlayElementType.Slope, new Func<GamePlayElement, BezierSpline, Transform, Slope>(Slope.Instantiate)},
				{GamePlayElement.GamePlayElementType.CrowdArea, new Func<GamePlayElement, BezierSpline, Transform, CrowdArea>(CrowdArea.Instantiate)},
				{GamePlayElement.GamePlayElementType.CollectibleGem, new Func<GamePlayElement, BezierSpline, Transform, Collectible>(Collectible.Instantiate)},
				{GamePlayElement.GamePlayElementType.Saw, new Func<GamePlayElement, BezierSpline, Transform, SawTrap>(SawTrap.Instantiate)},
				{GamePlayElement.GamePlayElementType.Pendulum, new Func<GamePlayElement, BezierSpline, Transform, PendulumTrap>(PendulumTrap.Instantiate)},
				{GamePlayElement.GamePlayElementType.RotatingCylinder, new Func<GamePlayElement, BezierSpline, Transform, RotatingCylinderTrap>(RotatingCylinderTrap.Instantiate)},
				{GamePlayElement.GamePlayElementType.SpikyCylinder, new Func<GamePlayElement, BezierSpline, Transform, SpikyCylinderTrap>(SpikyCylinderTrap.Instantiate)},
				{GamePlayElement.GamePlayElementType.Swatter, new Func<GamePlayElement, BezierSpline, Transform, SwatterTrap>(SwatterTrap.Instantiate)},
				{GamePlayElement.GamePlayElementType.RotatingPlatform, new Func<GamePlayElement, BezierSpline, Transform, RotatingPlatform>(RotatingPlatform.Instantiate)},
				{GamePlayElement.GamePlayElementType.BigCrowdArea, new Func<GamePlayElement, BezierSpline, Transform, BigCrowdArea>(BigCrowdArea.Instantiate)},
				{GamePlayElement.GamePlayElementType.CollectibleBullet, new Func<GamePlayElement, BezierSpline, Transform, CollectibleBullet>(CollectibleBullet.Instantiate)  },
				{GamePlayElement.GamePlayElementType.BarrelWall, new Func<GamePlayElement, BezierSpline, Transform, BarrelWall>(BarrelWall.Instantiate)  },
				{GamePlayElement.GamePlayElementType.BulletStack, new Func<GamePlayElement, BezierSpline, Transform, BulletStack>(BulletStack.Instantiate)  },
				{GamePlayElement.GamePlayElementType.RotorTrap, new Func<GamePlayElement, BezierSpline, Transform, RotorTrap>(RotorTrap.Instantiate)  },

			};

		[SerializeField] private Transform m_endLevelTrigger = null;

		public void Setup ( LevelInfo p_info )
		{
			m_lastPositionZ = 0f;

			GamePlayElement wallElementLeft = new GamePlayElement
			{
				positionZ = 0f,
				positionX = -3.25f,
				length = 20f,
				height = 0.2f,
				width = 0.5f
			};

			GamePlayElement wallElementRight = new GamePlayElement
			{
				positionZ = 0f,
				positionX = 3.25f,
				length = 20f,
				height = 0.2f,
				width = 0.5f
			};
			

			m_lastPositionZ = 20f;

			LevelPattern levelPattern;

			for (int i = 0; i < p_info.patterns.Count; i++)
			{
				levelPattern = p_info.patterns[i];


				GenerateNextSpline(levelPattern.length, p_info.roadShapes[i], p_info.roadShapeAngles[i], levelPattern.widthCurve);
				//GenerateNextSpline(levelPattern.length, LevelInfo.RoadShape.Straight, 0f); 
				AddPattern(m_lastPositionZ, levelPattern);
				m_lastPositionZ = m_spline.TotalLength;
			}
			GamePlayElement borderWallElem = new GamePlayElement { height = 0.2f, length = m_lastPositionZ, positionZ = 0f, width = 0.5f, positionX = -4.5f};
			BorderWall.Instantiate(borderWallElem, m_spline, transform);
			borderWallElem.positionX = 4.5f;
			BorderWall.Instantiate(borderWallElem, m_spline, transform);

			m_endLevelTrigger.position = m_spline.GetPointInWorldFromdist(m_spline.TotalLength) + new Vector3(0f, 0f, -1f);

			Stop();
		}

		public void Play ()
		{
			m_playing = true;
		}

		public void Pause ()
		{
			m_playing = false;
		}

		public void Stop ()
		{
			m_playing = false;
		}

		#region Spline Generation

		private void GenerateNextSpline ( float length, LevelInfo.RoadShape roadShape, float angle, AnimationCurve widthCurve)
		{
			Vector3 p1, p2, p3;
			float lastLength = m_spline.TotalLength;
			Vector3 lastPosition = m_spline.GetPointFromDist(m_spline.TotalLength);
			Vector3 direction = m_spline.GetDirectionFromDist(m_spline.TotalLength);

			// for turn Only
			Vector3 rotationPivot = lastPosition + direction * length * 1.2f / 2f;
			Vector3 newDirection;
			Vector3 heightVector;
			Vector3 downVector;
			switch (roadShape)
			{
				case LevelInfo.RoadShape.Straight:
					p1 = lastPosition + direction * (length / 3f);
					p2 = lastPosition + direction * (2f * length / 3f);
					p3 = lastPosition + direction * length;
					break;
				case LevelInfo.RoadShape.RampUP:
					heightVector = Vector3.up * (Mathf.Sin(Mathf.Deg2Rad * angle) * length);
					p1 = lastPosition + direction * (length / 3f);
					p2 = lastPosition + direction * (2f * length / 3f) + heightVector;
					p3 = lastPosition + direction * length + heightVector;
					break;
				case LevelInfo.RoadShape.RampDown:
					downVector = Vector3.down * (Mathf.Sin(Mathf.Deg2Rad * angle) * length);
					p1 = lastPosition + direction * (length / 3f);
					p2 = lastPosition + direction * (2f * length / 3f) + downVector;
					p3 = lastPosition + direction * length + downVector;
					break;
				case LevelInfo.RoadShape.TurnRight:
					p1 = lastPosition + direction * length * 1.2f / 3f;
					newDirection = Quaternion.Euler(0f, angle, 0f) * direction;
					p2 = rotationPivot + newDirection * length * (1.2f / 2f - 1 / 3f);
					p3 = rotationPivot + newDirection * length * 1.2f / 2f;
					break;
				case LevelInfo.RoadShape.TurnLeft:
					p1 = lastPosition + direction * length * 1.2f / 3f;
					newDirection = Quaternion.Euler(0f, -angle, 0f) * direction;
					p2 = rotationPivot + newDirection * length * (1.2f / 2f - 1 / 3f);
					p3 = rotationPivot + newDirection * length * 1.2f / 2f;
					break;
				case LevelInfo.RoadShape.RampUpTurn:
					heightVector = Vector3.up * (Mathf.Sin(Mathf.Deg2Rad * 15f) * length);
					p1 = lastPosition + direction * length * 1.2f / 3f;
					newDirection = Quaternion.Euler(0f, angle, 0f) * direction;
					p2 = rotationPivot + newDirection * length * (1.2f / 2f - 1 / 3f) + heightVector;
					p3 = rotationPivot + newDirection * length * 1.2f / 2f + heightVector;
					break;
				case LevelInfo.RoadShape.RampDownTurn:
					downVector = Vector3.down * (Mathf.Sin(Mathf.Deg2Rad * 15f) * length);
					p1 = lastPosition + direction * length * 1.2f / 3f;
					newDirection = Quaternion.Euler(0f, angle, 0f) * direction;
					p2 = rotationPivot + newDirection * length * (1.2f / 2f - 1 / 3f) + downVector;
					p3 = rotationPivot + newDirection * length * 1.2f / 2f + downVector;
					break;
				default:
					p1 = lastPosition + direction * (length / 3f);
					p2 = lastPosition + direction * (2f * length / 3f);
					p3 = lastPosition + direction * length;
					break;
			}

			m_spline.AddCurveAtEnd(p1, p2, p3, 0f, widthCurve);
		}

		#endregion
		
		private void CheckWalls ( float fieldwidth )
		{
			int wallIndex = 0;
			while (wallIndex < m_walls.Count && m_player.splineDist > m_walls[wallIndex].splineStartDist)
			{
				if (m_player.splineDist <= m_walls[wallIndex].splineEndDist + m_player.radius)
				{
					Vector3 limits = m_walls[wallIndex].GetLimits(fieldwidth, m_player.posX, m_player.posY, m_player.splineDist);
					if (limits.x > m_limits.x)
						m_limits.x = limits.x;
					if (limits.y < m_limits.y)
						m_limits.y = limits.y;
					if (limits.z > m_limits.z)
						m_limits.z = limits.z;
					wallIndex++;
				}
				else if (m_walls[wallIndex].splineEndDist < m_player.splineDist - 10f)
				{
					Destroy(m_walls[wallIndex].gameObject);
					m_walls.RemoveAt(wallIndex);
				}
				else
					wallIndex++;
			}
		}

		private void CheckSlopes ( float fieldwidth )
		{
			int slopeIndex = 0;
			while (slopeIndex < m_slopes.Count && m_player.splineDist > m_slopes[slopeIndex].splineStartDist)
			{
				if (m_player.splineDist <= m_slopes[slopeIndex].splineEndDist)
				{
					Vector3 limits = m_slopes[slopeIndex].GetLimmits(fieldwidth, m_player.posX, m_player.posY, m_player.splineDist);
					if (limits.x > m_limits.x)
						m_limits.x = limits.x;
					if (limits.y < m_limits.y)
						m_limits.y = limits.y;
					if (limits.z > m_limits.z)
						m_limits.z = limits.z;
					slopeIndex++;
				}
				else if (m_slopes[slopeIndex].splineEndDist < m_player.splineDist - 10f)
				{
					Destroy(m_slopes[slopeIndex].gameObject);
					m_slopes.RemoveAt(slopeIndex);
				}
				else
					slopeIndex++;
			}
		}

		// Update is called once per frame
		void FixedUpdate ()
		{
			if (m_playing && !m_player.isDead)
			{
				float fieldWidth = m_spline.GetWidthMultiplierFromdist(m_player.splineDist) * 4f;
				m_limits.x = -fieldWidth;
				m_limits.y = fieldWidth;
				m_limits.z = 0f;

				CheckWalls(fieldWidth);
				CheckSlopes(fieldWidth);
				m_player.SetLimits(m_limits.x, m_limits.y, m_limits.z);
			}
		}

		private void AddPattern ( float currentPositionZ, LevelPattern pattern )
		{
			Transform container = new GameObject(pattern.name).transform;
			container.parent = transform;
			

			foreach (GamePlayElement gamePlayElement in pattern.elements)
			{
				GamePlayElement element = gamePlayElement.Clone();
				element.positionZ += currentPositionZ;
				switch (element.type)
				{
					case GamePlayElement.GamePlayElementType.Wall:
						Wall wall = Wall.Instantiate(element, m_spline, container);
						m_walls.Add(wall);
						break;

					case GamePlayElement.GamePlayElementType.Slope:
						Slope slope = Slope.Instantiate(element, m_spline, container);
						m_slopes.Add(slope);
						break;

					default:
						InstantiateDictionnary[element.type].DynamicInvoke(element, m_spline, container);
						break;
				}
			}
		}
	}
}