﻿using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
	public class TouchManager : Singleton<TouchManager>
	{
		private LayerMask m_backgroundMask;
		// Use this for initialization
		void Start()
		{
			m_backgroundMask = LayerMask.GetMask("Background");
		}

		// Update is called once per frame
		void Update()
		{
			int touchCount = Input.touchCount;
			Vector2 points;
#if UNITY_EDITOR
			if (Input.GetMouseButton(0))
			{
				//touchCount++;
				points = Input.mousePosition;
                //m_target.position = Camera.main.ScreenToWorldPoint( points )+Vector3.forward;

            }
#endif
            if (touchCount > 0)
			{
				points = Input.touches[0].position;
            }
        }

	}

}
