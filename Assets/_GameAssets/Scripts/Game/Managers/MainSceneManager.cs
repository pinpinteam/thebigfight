﻿using Pinpin.Scene.MainScene.UI;
using PaperPlaneTools;
using System;

namespace Pinpin.Scene.MainScene
{

	public sealed class MainSceneManager : ASceneManager
	{
		public GameManager gameManager;
		private new MainSceneUIManager UI
		{
			get { return (base.UI as MainSceneUIManager); }
		}

#region MonoBehaviour

		protected override void Awake ()
		{
			base.Awake();
			if (!ApplicationManager.isInitialized)
				return;
			
		}

		protected override void Start ()
		{
			base.Start();
			this.UI.OpenPanel<MainPanel>();

			if (UI.GetPopup<RatingPopup>().GetComponent<IAlertPlatformAdapter>() != null)
			{
				RateBox.Instance.AlertAdapter = UI.GetPopup<RatingPopup>().GetComponent<IAlertPlatformAdapter>();
			}

			base.SetSceneReady();
		}
		#endregion

		#region Ads

#if TAPNATION
		public bool ShowInterstitial ()
		{
			if (ApplicationManager.canWatchInterstitial)
			{
				this.UI.OpenProcessingPopup("Loading ad");
				FunGamesMax.ShowInterstitial(( status, p2, p3 ) =>
				{
					this.UI.CloseProcessingPopup();
				});
				return true;
			}
			return false;
		}


		public bool ShowRewardedVideo ( Action<AdsManagement.RewardedVideoEvent> adEvent, string location )
		{
#if CAPTURE
			adEvent?.Invoke(AdsManagement.RewardedVideoEvent.Rewarded);
			return true;
#else
			if (ApplicationManager.canWatchRewardedVideo)
			{
				this.UI.OpenProcessingPopup("Loading ad");
				FunGamesMax.ShowRewarded(( status, p2, p3 ) =>
				{
					if (status == "success")
						adEvent?.Invoke(AdsManagement.RewardedVideoEvent.Rewarded);
					else
						adEvent?.Invoke(AdsManagement.RewardedVideoEvent.Failed);

					this.UI.CloseProcessingPopup();

				});
				return true;
			}
			return false;
#endif
		}
#endif

		public void CloseActivePopup(bool force = false)
		{
			UI.CloseActivePopup(force);
		}

#endregion
#region ASceneManager

		public override void OnQuit ()
		{
			ApplicationManager.Quit();
		}

		public bool rewardedShown { get; internal set; }


#endregion


	}

}