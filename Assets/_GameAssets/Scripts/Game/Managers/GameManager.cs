﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;
using URandom = UnityEngine.Random;
#if TAPNATION
using FunGames.Sdk.Analytics;
#endif

namespace Pinpin
{
	public class GameManager : Singleton<GameManager>
	{
		#region members
		public static Action OnLevelLoaded;
		public static Action<bool> OnLevelEnd;
		public static Action<EGameState> OnStateChange;

		public bool canInput;

		public enum EGameState
		{
			InMenu,
			InGame,
			TransitionToFinisher,
			Finisher,
			EndGame,
		}

		private EGameState m_gameState;

		public EGameState GameState
		{
			get
			{
				return m_gameState;
			}
			set
			{
				m_gameState = value;
				OnStateChange?.Invoke(m_gameState);
			}
		}

		//[SerializeField] private LightsPlayerFollow m_directionalLight;

		private int m_currentLevelIndex;
		public int currentLevelIndex => m_currentLevelIndex;
		private Level m_currentLevel;
		private List<LevelInfo> m_lastLevels = new List<LevelInfo>();
		private LevelInfo m_randomLevelInfo;

		private bool m_levelIsLoading = false;
		public bool levelIsLoading => m_levelIsLoading;

		static public Level level { get { return Instance.m_currentLevel; } }
		//public bool newHighscore { get => level.bossZone.boss.highscoreTaken; }

		#endregion

		#region MonoBehavior
		private new void Awake ()
		{
			//ApplicationManager.onRewardedVideoAvailabilityChange += OnRewardedVideoAvailabilityChange;
			base.Awake();
		}

		// Use this for initialization
		void Start ()
		{
			LoadNextLevel();
		}

		private void OnDestroy ()
		{
			//ApplicationManager.onRewardedVideoAvailabilityChange -= OnRewardedVideoAvailabilityChange;
		}
		private void Update ()
		{
#if UNITY_EDITOR
			if (Input.GetKeyDown(KeyCode.T))
				ApplicationManager.datas.haveCompleteTutorial = false;

			if (Input.GetKeyDown(KeyCode.W))
				LevelWon();

			if (Input.GetKeyDown(KeyCode.L))
				LevelFailed();
#endif
		}

		#endregion

		private void OnRewardedVideoAvailabilityChange ( bool value )
		{

		}

		#region Level Callbacks
		public void LevelWon ()
		{
			OnLevelEnd.Invoke(true);

#if TAPNATION
			FunGamesAnalytics.NewProgressionEvent("Complete", ApplicationManager.datas.totalLevelDoneCount.ToString());
#endif
			if (ApplicationManager.datas.levelProgression == m_currentLevelIndex)
			{
				ApplicationManager.datas.levelProgression++;
				ApplicationManager.datas.postGameLevelDoneCount = 0;
			}
			else
			{
				ApplicationManager.datas.postGameLevelDoneCount++;
			}
			ApplicationManager.datas.totalLevelDoneCount++;

			MMVibrationManager.Haptic(HapticTypes.Success);

			m_randomLevelInfo = null;
			GameState = EGameState.EndGame;
		}

		public void LevelFailed ()
		{
			OnLevelEnd.Invoke(false);

#if TAPNATION
			FunGamesAnalytics.NewProgressionEvent("Fail", ApplicationManager.datas.totalLevelDoneCount.ToString());
#endif
			MMVibrationManager.Haptic(HapticTypes.Failure);
			GameState = EGameState.EndGame;
		}
		#endregion

		#region GAME
		public IEnumerator DelayCR ( Action method, float timer )
		{
			yield return new WaitForSeconds(timer);
			method();
		}

		public void StartGame ()
		{
			GameState = EGameState.InGame;
			m_currentLevel.StartGame();
			//m_directionalLight.OnLevelStart();
#if TAPNATION
			FunGamesAnalytics.NewProgressionEvent("Start", ApplicationManager.datas.totalLevelDoneCount.ToString());
#endif
		}
		public void Revive ()
		{
			GameState = EGameState.InGame;
			m_currentLevel.player.Revive();
		}

		public void LoadNextLevel ()
		{
			RemoveCurrentLevel();

			int targetLevelIndex;

#if CAPTURE
			if (ApplicationManager.datas.levelProgression < ApplicationManager.assets.captureLevelInfos.Length) //first loop
				targetLevelIndex = ApplicationManager.datas.levelProgression;
			else // Second loop skip onBoardingLevelToSkipCount
			{
				int levelToSkipCount = ApplicationManager.config.game.onBoardingLevelToSkipCount < ApplicationManager.assets.captureLevelInfos.Length ? ApplicationManager.config.game.onBoardingLevelToSkipCount : 0; //security
				targetLevelIndex = levelToSkipCount + (ApplicationManager.datas.postGameLevelDoneCount % (ApplicationManager.assets.captureLevelInfos.Length - levelToSkipCount));
			}
#else
			if (ApplicationManager.datas.levelProgression < ApplicationManager.assets.levelInfos.Length) //first loop
				targetLevelIndex = ApplicationManager.datas.levelProgression;
			else // Second loop skip onBoardingLevelToSkipCount
			{
				int levelToSkipCount = ApplicationManager.config.game.onBoardingLevelToSkipCount < ApplicationManager.assets.levelInfos.Length ? ApplicationManager.config.game.onBoardingLevelToSkipCount : 0; //security
				targetLevelIndex = levelToSkipCount + (ApplicationManager.datas.postGameLevelDoneCount % (ApplicationManager.assets.levelInfos.Length - levelToSkipCount));
			}
#endif

			StartCoroutine(LoadLevelCoroutine(targetLevelIndex));
		}
		public void RemoveCurrentLevel ()
		{
			if (m_currentLevel != null)
				Destroy(m_currentLevel.gameObject);
		}

		private IEnumerator LoadLevelCoroutine ( int levelIndex )
		{
			m_levelIsLoading = true;
			RemoveCurrentLevel();

			yield return null;

			m_currentLevelIndex = levelIndex;
			m_currentLevel = Instantiate(ApplicationManager.assets.baseLevel);
#if CAPTURE
			m_currentLevel.currentLevelInfo = ApplicationManager.assets.captureLevelInfos[levelIndex % ApplicationManager.assets.captureLevelInfos.Length];
#else
			m_currentLevel.currentLevelInfo = ApplicationManager.assets.levelInfos[levelIndex % ApplicationManager.assets.levelInfos.Length];
#endif

			OnLevelLoaded?.Invoke();
			m_levelIsLoading = false;
		}

		/*private LevelInfo SelectRandomLevel ()
		{
			LevelInfo randomLevelInfo;
			do
			{
				randomLevelInfo = ApplicationManager.assets.levelInfos[UnityEngine.Random.Range(12, ApplicationManager.assets.levelInfos.Length)];
			}
			while (m_lastLevels.Contains(randomLevelInfo));

			m_lastLevels.Add(randomLevelInfo);
			if (m_lastLevels.Count > 4)
				m_lastLevels.RemoveAt(0);

			return randomLevelInfo;
		}

		private LevelPattern GetRandomElementFromList ( List<LevelPattern> elementList, List<LevelPattern> excludeElementList, int max = 3 )
		{
			LevelPattern selectedElement = null;
			do
			{
				selectedElement = elementList[URandom.Range(0, elementList.Count)];
			} while (excludeElementList.Contains(selectedElement));

			excludeElementList.Add(selectedElement);
			if (excludeElementList.Count > Mathf.Min(max, elementList.Count - 1))
				excludeElementList.RemoveAt(0);

			return selectedElement;
		}*/

		// private LevelInfo GenerateRandomLevel ( bool isBonus )
		// {
		// 	if (!m_randomLevelInfo)
		// 	{
		// 		List<LevelPattern> alreadyUsedTraps = new List<LevelPattern>();
		// 		List<LevelPattern> alreadyUsedGems = new List<LevelPattern>();
		// 		List<LevelPattern> alreadyUsedUnits = new List<LevelPattern>();
		// 		List<LevelPattern> alreadyUsedBonusUnits = new List<LevelPattern>();
		// 		List<LevelPattern> alreadyUsedSlope = new List<LevelPattern>();
		// 		List<LevelPattern> alreadyUsedWalls = new List<LevelPattern>();
		// 		List<LevelPattern> alreadyUsedSlicer = new List<LevelPattern>();
		//
		// 		m_randomLevelInfo = new LevelInfo();
		// 		//float playerLife = ApplicationManager.config.game.baseLife;
		// 		int currentColor = m_randomLevelInfo.startColorIndex = 2;
		// 		int lastColor = currentColor;
		// 		bool starUsed = false;
		// 		int blockCount = 3;
		// 		int roadHeight = 0;
		// 		//m_randomLevelInfo.bossLevel = isBonus;
		//
		// 		for (int i = 0; i < blockCount; i++)
		// 		{
		// 			int patternPerBlock = URandom.Range(3, 6);
		// 			// generate the block
		// 			for (int j = 0; j < patternPerBlock; j++)
		// 			{
		// 				bool isWall = false;
		// 				int patternRandom = URandom.Range(0, 100);
		// 				LevelPattern randomLevelDesign = null;
		// 				 if (patternRandom < 50)
		// 				 {
		// 				 	if (!isBonus)
		// 				 	{
		// 				 		List<LevelPattern> selectedUnityList;
		// 				 		int selectedColor = currentColor;
		// 				 		if (selectedColor == 3)
		// 				 			selectedColor = URandom.Range(0, 3);
		// 				 		switch (selectedColor)
		// 				 		{
		// 				 			case 0:
		// 				 				selectedUnityList = ApplicationManager.assets.units0Patterns;
		// 				 				break;
		// 				 			case 1:
		// 				 				selectedUnityList = ApplicationManager.assets.units1Patterns;
		// 				 				break;
		// 				 			case 2:
		// 				 				selectedUnityList = ApplicationManager.assets.units2Patterns;
		// 				 				break;
		// 				 			default:
		// 				 				selectedUnityList = ApplicationManager.assets.units0Patterns;
		// 				 				break;
		// 				 		}
		// 				 		randomLevelDesign = GetRandomElementFromList(selectedUnityList, alreadyUsedUnits);
		// 				 		
		// 				 	}
		// 				 	else
		// 				 	{
		// 				 		randomLevelDesign = GetRandomElementFromList(ApplicationManager.assets.bonusUnitsPatterns, alreadyUsedBonusUnits);
		// 				 		//playerLife += 10 * ApplicationManager.config.game.baseUnitValue;
		// 				 	}
		// 				
		// 				 }
		// 				 else if (patternRandom < 75)
		// 				 {
		// 				 	randomLevelDesign = GetRandomElementFromList(ApplicationManager.assets.gemsPatterns, alreadyUsedGems);
		// 				 }
		// 				 else if (patternRandom < 100)
		// 				 {
		// 				 	if (i > 0 && URandom.Range(0, 3) == 0)
		// 				 		randomLevelDesign = ApplicationManager.assets.wallPatterns[URandom.Range(0, 2) + (Mathf.Min(i, 2) - 1)];
		// 				 	else
		// 				 		randomLevelDesign = GetRandomElementFromList(ApplicationManager.assets.trapPatterns, alreadyUsedTraps);
		// 				
		// 				 	//playerLife -= 1000f * ApplicationManager.config.game.wallHitHealthLostRatio;
		// 				 	//isWall = true;
		// 				 }
		// 				playerLife += randomLevelDesign.elements.FindAll(x => x.type == LevelDesign.Element.ElementType.ColoredUnit && x.colorIndex == currentColor).Count * ApplicationManager.config.game.baseUnitValue;
		//
		// 				LevelInfo.RoadShape shape = LevelInfo.RoadShape.Straight;
		// 				int angle = 0;
		// 				if (!isWall && randomLevelDesign.haveLongSpace && URandom.Range(0, 100) < 30)
		// 				{
		// 					shape = (LevelInfo.RoadShape)URandom.Range(1, 5);
		// 					switch (shape)
		// 					{
		// 						case LevelInfo.RoadShape.RampUP:
		// 							int height = URandom.Range(1, 3);
		// 							roadHeight += height;
		// 							angle = height * 10;
		// 							break;
		// 						case LevelInfo.RoadShape.RampDown:
		// 							if (roadHeight > 0)
		// 							{
		// 								height = URandom.Range(1, roadHeight + 1);
		// 								roadHeight -= height;
		// 								angle = height * 10;
		// 							}
		// 							else
		// 							{
		// 								shape = LevelInfo.RoadShape.RampUP;
		// 								height = URandom.Range(1, 3);
		// 								roadHeight += height;
		// 								angle = height * 10;
		// 							}
		//
		// 							break;
		// 					}
		// 				}
		//
		// 				m_randomLevelInfo.AddPattern(randomLevelDesign, shape, angle);
		// 			}
		// 			if (!isBonus)
		// 			{
		// 				if (currentColor == 3)
		// 				{
		// 					currentColor = lastColor;
		// 				}
		// 				// select new Color
		// 				if (i < blockCount - 1)
		// 				{
		// 					currentColor = (currentColor + URandom.Range(1, 3)) % 3; m_randomLevelInfo.AddPattern(ApplicationManager.assets.colorChangePatterns[currentColor]);
		// 				}
		// 			}
		// 		}
		// 		//m_randomLevelInfo.bossHealth = playerLife * 0.85f;
		// 	}
		// 	return m_randomLevelInfo;
		// }
		#endregion
	}
}