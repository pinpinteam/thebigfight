﻿﻿using Pinpin.Scene.MainScene.UI;
using Pinpin.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
	public class WaterPool : MonoBehaviour
	{
		[SerializeField] private GameObject[] m_waterElements;
		[SerializeField] private float m_elementsLength = 200f;
		[SerializeField] private float m_leftOffset = 500f;

		[SerializeField] private int m_environmentTypeIndex = 0;

		private bool m_setup = false;
		private Transform m_currentPlayerTransform;
		private int m_currentIndex;

		private float previousZ;
		private float previousX;

		private void Awake ()
		{
			GameManager.OnLevelLoaded += OnLevelLoaded;
			GameManager.OnLevelEnd += OnLevelEnd;
			//LosePopup.onRetry += OnRetry;
		}

		private void OnDestroy ()
		{
			GameManager.OnLevelLoaded -= OnLevelLoaded;
			GameManager.OnLevelEnd -= OnLevelEnd;
			//LosePopup.onRetry -= OnRetry;
		}

		private void OnRetry ()
		{
			m_setup = true;
		}

		private void OnLevelEnd ( bool unused )
		{
			m_setup = false;
		}

		private void OnLevelLoaded ()
		{
			m_currentPlayerTransform = GameManager.level.player.transform;

			previousX = m_currentPlayerTransform.position.x;
			previousZ = m_currentPlayerTransform.position.z;

			m_setup = true;

			for (int i = 0; i < m_waterElements.Length; i++)
			{
				m_waterElements[i].transform.localPosition = new Vector3(0f, 0f, i * m_elementsLength);
			}
		}

		private void Update ()
		{
			if (!m_setup) return;

			if (m_currentPlayerTransform.position.x < transform.position.x - m_elementsLength / 2f)
			{
				transform.position -= transform.right * m_elementsLength;
			}
			else if (m_currentPlayerTransform.position.x > transform.position.x + m_elementsLength / 2f)
			{
				transform.position += transform.right * m_elementsLength;
			}

			if (m_currentPlayerTransform.position.z < transform.position.z - m_elementsLength / 2f)
			{
				transform.position -= transform.forward * m_elementsLength;
			}
			else if (m_currentPlayerTransform.position.z > transform.position.z + m_elementsLength / 2f)
			{
				transform.position += transform.forward * m_elementsLength;
			}
		}
	}
}