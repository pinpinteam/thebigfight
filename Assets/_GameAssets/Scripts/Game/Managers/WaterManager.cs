﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using DG.Tweening;
using Pinpin.UI;
using UnityEngine;

namespace Pinpin
{
	public class WaterManager : MonoBehaviour
	{
		public static Action onWaterColoured;

		[SerializeField] private Color m_mainColor;
		[SerializeField] private Color m_bossWaterColor;
		[SerializeField] private Color m_secondaryColor;
		[SerializeField] private float m_duration;
		[SerializeField] private Color m_startFogColor;
		private static readonly int MainColor = Shader.PropertyToID("MainColor");
		private static readonly int SecondaryColor = Shader.PropertyToID("SecondaryColor");
		private static readonly int ColouriseOrigin = Shader.PropertyToID("ColoriseOrigin");
		private static readonly int ColouriseDistance = Shader.PropertyToID("ColoriseDistance");

		private void OnEnable ()
		{
			m_startFogColor = RenderSettings.fogColor;
			Shader.SetGlobalColor(MainColor, m_mainColor);
			Shader.SetGlobalColor(SecondaryColor, m_secondaryColor);
			Shader.SetGlobalFloat(ColouriseDistance, 0);
		}

		private void Update ()
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				//ColouriseWater(m_secondaryColor, Vector3.zero);
				ColouriseWater(0, Vector3.zero);
				(m_mainColor, m_secondaryColor) = (m_secondaryColor, m_mainColor);
			}
		}

		public void ColouriseWater ( int colorIndex, Vector3 origin )
		{
			// if (ApplicationManager.config.game.waterColorationActive == 0)
			// 	return;
			// Color secondaryColor = ColorCollection.Instance.ReturnWaterColor(GameManager.level.currentLevelInfo.colorPalette[colorIndex] + 3 * ApplicationManager.config.game.colorPaletteIndex);

			// ColouriseWaterRough(secondaryColor, origin, m_duration);
		}

		public void ColouriseWaterRough ( Color secondaryColor, Vector3 origin, float duration )
		{
			Shader.SetGlobalFloat(ColouriseDistance, 0);
			Shader.SetGlobalColor(SecondaryColor, secondaryColor);
			Shader.SetGlobalVector(ColouriseOrigin, origin);

			float currentExpansion = 0;
			DOTween.To(() => currentExpansion, x => currentExpansion = x, Mathf.Pow(RenderSettings.fogEndDistance * 5f / 4, 2), duration)
				.SetEase(Ease.InCubic).OnUpdate(() =>
				{
					Shader.SetGlobalFloat(ColouriseDistance, currentExpansion);
				})
				.OnComplete(() =>
				{
					Shader.SetGlobalColor(MainColor, secondaryColor);
					OnWaterColoured();
				});

			Color currentFogColor = RenderSettings.fogColor;
			Color.RGBToHSV(currentFogColor, out _, out float s, out float v);
			Color.RGBToHSV(secondaryColor, out float targetHue, out _, out _);
			Color targetColor = Color.HSVToRGB(targetHue, s, v);

			DOTween.To(() => currentFogColor, x => currentFogColor = x, targetColor, duration / 2)
				.SetEase(Ease.InOutSine).SetDelay(duration / 2).OnUpdate(
					() =>
					{
						RenderSettings.fogColor = currentFogColor;
					});
		}

		private void ColoriseWaterInstant ( Color color )
		{
			Shader.SetGlobalColor(MainColor, color);
		}

		private void ResetWaterColor ( Vector3 pos, float duration )
		{
#if !CAPTURE
			//ColouriseWaterRough(m_mainColor, pos, duration);
			Shader.SetGlobalFloat(ColouriseDistance, 0);
			Color currentColor = Shader.GetGlobalColor(MainColor);

			DOTween.To(() => currentColor, x => currentColor = x, m_bossWaterColor, 2f).SetEase(Ease.InOutSine).OnUpdate(
				() =>
				{
					Shader.SetGlobalColor(MainColor, currentColor);
				});

			Color currentFogColor = RenderSettings.fogColor;

			DOTween.To(() => currentFogColor, x => currentFogColor = x, m_startFogColor, 2f)
				.SetEase(Ease.InOutSine).OnUpdate(
					() =>
					{
						RenderSettings.fogColor = currentFogColor;
					});
#endif
		}

		private void OnWaterColoured ()
		{
			onWaterColoured?.Invoke();
		}

		private void MoveWaterVertically ( Vector3 pos, float duration )
		{
			transform.DOMoveY(pos.y - 2, duration).SetEase(Ease.OutQuad);
		}

		private void OnWaterColorToggle ( bool flag )
		{
			// m_startFogColor = ApplicationManager.config.game.fogColor[flag ? 0 : 1];
		}

	}
}
