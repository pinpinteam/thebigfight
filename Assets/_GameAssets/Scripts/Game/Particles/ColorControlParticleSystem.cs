﻿using UnityEngine;

namespace Pinpin
{

	public class ColorControlParticleSystem : MonoBehaviour
	{
		[SerializeField] private ParticleSystem[] particles;

		private Color m_color;
		public Color Color
		{
			get => m_color;
			set
			{
				m_color = value;

				foreach (ParticleSystem p in particles)
				{
					ParticleSystem.ColorOverLifetimeModule main = p.colorOverLifetime;
					main.color = new ParticleSystem.MinMaxGradient(Color.white, value);
				}
			}
		}

		public void Play ()
		{
			particles[0].Play();
		}
	}
}
