﻿using System;
using System.Collections.Generic;
using Cinemachine;
using DG.Tweening;
using Pinpin.Scene.MainScene.UI;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace Pinpin
{
	public class Level : MonoBehaviour
	{
		public static Action<ulong, ulong> onCollectibleCollectAction;
		public static Action<float> onTimerUpdateAction;

		public string levelName;
		public LevelInfo currentLevelInfo { get; set; }

		[SerializeField] LevelGenerationManager m_levelGenerationManager;

		[SerializeField] private PlayerGunnerController m_player;
		public PlayerGunnerController player => m_player;

		[SerializeField] private CinemachineVirtualCamera m_startVCamera;
		[SerializeField] private CinemachineVirtualCamera[] m_levelVCameras;

		private ulong m_gemCollectedCount;

#if CAPTURE
		public CinemachineVirtualCamera[] captureLevelVCameras => m_levelVCameras;
#endif
		public int ammoCountOnFinisher;

		private void Awake ()
		{
			Collectible.onCollectibleCollected += OnCollectibleCollected;
		}
		private void OnDestroy ()
		{
			Collectible.onCollectibleCollected -= OnCollectibleCollected;
		}

		private void Start ()
		{
			m_levelGenerationManager.Setup(currentLevelInfo);

#if CAPTURE
			for (int index = 0; index < m_levelVCameras.Length; index++)
			{
				m_levelVCameras[index].gameObject.SetActive(index == CheatMenu.Instance.m_cameraIndex);
				m_levelVCameras[index].enabled = index == CheatMenu.Instance.m_cameraIndex;
			}
#endif
		}

		private void OnCollectibleCollected ()
		{
			ulong newGemCount = m_gemCollectedCount + (ulong)ApplicationManager.config.game.gemValue;

			onCollectibleCollectAction?.Invoke(m_gemCollectedCount, (ulong)newGemCount);
			m_gemCollectedCount = newGemCount;
		}

		public void StartGame ()
		{
			m_startVCamera.Priority = 0;
			m_levelGenerationManager.Play();
			GameManager.Instance.GameState = GameManager.EGameState.InGame;
			m_player.StartGame();
		}
	}
}
