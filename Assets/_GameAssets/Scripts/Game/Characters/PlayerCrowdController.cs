﻿using DG.Tweening;
using Pinpin.InputControllers;
using System;
using System.Collections;
using UnityEngine;
using MoreMountains.NiceVibrations;
using Pinpin.Scene.MainScene.UI;

namespace Pinpin
{
	public class PlayerCrowdController : CrowdController
	{
		public static Action<float> onPlayerCrowdProgressAction;
		public Action onPlayerCrowdDeathAction;
		public Action<int> onScoreChangeAction;

		private bool m_playing = false;
		private bool m_destroyed = false;
		private bool m_isInvincible = false;

		public float radius;
		private float m_posX;
		private float m_posY;
		//private float m_speedY = 0f;
		private float m_xLimitMin = -1f;
		private float m_xLimitMax = 1f;
		private float m_yLimitMin = 1f;
		private Vector3 m_counterOffset;

		private float m_currentDamageStopTimerDelay = 0f;
		private float m_currentBattleSlowTimerDelay = 0f;

		[SerializeField] BezierSpline m_splineToFollow;
		[SerializeField] private Transform m_camTarget;

		private Vector3[] m_leaderPosHistory;
		[HideInInspector] public float speedMultiplier = 1f;

		#region GETTER SETTER
		public float splineDist { get; private set; }
		public BezierSpline splineToFollow { get { return m_splineToFollow; } }
		public float currentSpeed { get; private set; }
		public float speed { get; private set; }
		public bool isDead { get { return (m_destroyed); } }

		private Vector3 m_camTargetOffset;


		#endregion

		#region MONOBEHAVIOR

		private void Awake ()
		{
			ColorPortal.OnEnterPortal += OnEnterColorPortal;
			MultiplicationPortal.OnEnterPortal += OnEnterMultiplicationPortal;
			MainPanel.MoveCrowdDuringSlideToStart += MoveDuringSlideToStart;
			// m_camTargetOffset = m_crowdZTargetTfm.position - m_camTarget.position;
			m_camTargetOffset = Vector3.forward * 4f;
			m_counterOffset = m_crowdCounter.transform.position - m_crowdXTargetTfm.position;
#if CAPTURE
			CheatMenu.ResetPlayerAction += ResetPlayer;
#endif
		}

		protected override void Start ()
		{
			TargetCharacterCount = GameManager.level.currentLevelInfo.charactersQuantityAtStart;
			base.Start();
		}

		private void OnDestroy ()
		{
			ColorPortal.OnEnterPortal -= OnEnterColorPortal;
			MultiplicationPortal.OnEnterPortal -= OnEnterMultiplicationPortal;
			MainPanel.MoveCrowdDuringSlideToStart -= MoveDuringSlideToStart;

#if CAPTURE
			CheatMenu.ResetPlayerAction -= ResetPlayer;
#endif
		}

		private void FixedUpdate ()
		{
			if (m_destroyed || !m_playing || GameManager.Instance.GameState == GameManager.EGameState.Finisher || GameManager.Instance.GameState == GameManager.EGameState.EndGame)
				return;

			//X Movement
			Vector3 newXPosition;
			if (GameManager.Instance.GameState == GameManager.EGameState.InGame)
			{
				m_posX = Mathf.Clamp(m_posX + InputManager.moveDelta.x * ApplicationManager.config.game.sensibility, m_xLimitMin, m_xLimitMax);
			}
			else
			{
				m_posX = 0f;
			}

			Transform tfm = transform;
			newXPosition = tfm.position + tfm.up * m_posY + tfm.right * m_posX;
			m_crowdXTargetTfm.transform.localPosition = newXPosition;

			//TAKE DAMAGE
			if (m_currentDamageStopTimerDelay > 0f)
			{
				m_currentDamageStopTimerDelay -= Time.fixedDeltaTime;
			}
			else
			{
				m_currentDamageStopTimerDelay = 0f;

				float distDelta;

				// if (m_currentBattleSlowTimerDelay > 0f)
				// {
				// 	m_currentBattleSlowTimerDelay -= Time.fixedDeltaTime;
				// 	distDelta = Mathf.Min(currentSpeed, ApplicationManager.datas.levelProgression % 4 == 3 ? 0.70f : 0.85f) * speed * Time.fixedDeltaTime *ApplicationManager.config.game.battleSlowMultiplier;
				// }
				// else
				// {
				// 	m_currentBattleSlowTimerDelay = 0f;
				// 	distDelta = Mathf.Min(currentSpeed, ApplicationManager.datas.levelProgression % 4 == 3 ? 0.70f : 0.85f) * speed * Time.fixedDeltaTime;
				// }

				distDelta = currentSpeed * Time.fixedDeltaTime;

				splineDist += distDelta;
			}

			//Player Movement
			Move();

			//m_animator.SetFloat("Speed", currentSpeed); // parametre entre 0 et 1
		}

		private void Update ()
		{
			if (m_currentLeader)
			{
				Vector3 newPos = m_currentLeader.transform.position + m_counterOffset;
				newPos.z = m_crowdXTargetTfm.position.z;
				m_crowdCounter.transform.position = Vector3.Lerp(m_crowdCounter.transform.position, newPos, 30f * Time.deltaTime);
			}
			else
			{
				if (m_charactersList.Count <= 0) return;

				//m_charactersList[0].isLeader = true;
				m_currentLeader = m_charactersList[0];
			}
		}

		private void LateUpdate ()
		{
			m_camTarget.position = Vector3.Lerp(m_camTarget.position, m_crowdZTargetTfm.position + m_camTargetOffset, ApplicationManager.config.game.camTargetSmoothDamp * Time.deltaTime);
		}

		#endregion

		#region MOVEMENTS

		private void MoveDuringSlideToStart ()
		{
			Vector3 newXPosition;
			m_posX = Mathf.Clamp(m_posX + InputManager.moveDelta.x * ApplicationManager.config.game.sensibility, m_xLimitMin, m_xLimitMax);
			Transform tfm = transform;
			newXPosition = tfm.position + tfm.up * m_posY + tfm.right * m_posX;
			m_crowdXTargetTfm.transform.localPosition = newXPosition;
			Move();
		}

		private void Move ()
		{
			Vector3 destination = m_splineToFollow.GetPointInWorldFromdist(splineDist);
			m_crowdZTargetTfm.position = destination;
			Vector3 direction = m_splineToFollow.GetDirectionFromDist(splineDist);
			float angle = m_splineToFollow.GetAngleFromdist(splineDist);
			Vector3 right = m_splineToFollow.GetNormalFromdist(splineDist);
			right = Quaternion.AngleAxis(angle, direction) * right;
			Vector3 up = Vector3.Cross(right, direction);
			m_crowdZTargetTfm.rotation = Quaternion.LookRotation(direction, up);

			//m_charactersList[0].MoveToTarget(m_crowdXTargetTfm.position, m_splineToFollow, splineDist);
			for (int i = 1; i < m_charactersList.Count; i++)
			{
				//m_charactersList[i].MoveToTarget(GetHistoryPos(ApplicationManager.config.game.crowdOffsetWithLeader + i * ApplicationManager.config.game.crowdOffsetPerIndex), m_splineToFollow, splineDist);
			}
			onPlayerCrowdProgressAction?.Invoke(splineDist / m_splineToFollow.TotalLength);
		}

		public void SetLimits ( float min, float max, float height )
		{
			m_xLimitMin = min + radius;
			m_xLimitMax = max - radius;
			m_yLimitMin = height;
			if (m_posY < m_yLimitMin)
			{
				//m_speedY = (m_yLimitMin - m_posY) / Time.deltaTime;
				m_posY = m_yLimitMin;
			}
		}
		#endregion

		#region LEADER HISTORY
		/*private void InitLeaderPosHistory ()
		{
			m_leaderPosHistory = new Vector3[ApplicationManager.config.game.leaderHistoryPrecision];
			m_leaderPosHistory[0] = Vector3.zero;
			for (int i = 1; i < m_leaderPosHistory.Length; i++)
			{
				m_leaderPosHistory[i] = m_leaderPosHistory[i - 1] + new Vector3(0, 0, -ApplicationManager.config.game.distanceBetweenHistoryPos);
			}
		}
		private void FixedUpdateLeaderPosHistory ()
		{
			for (int i = m_leaderPosHistory.Length - 1; i > 0; i--)
			{
				m_leaderPosHistory[i] = m_leaderPosHistory[i - 1];
			}
			m_leaderPosHistory[0] = m_crowdXTargetTfm.transform.position;*/

		/*m_leaderPosHistory[0] = m_crowdXTargetTfm.transform.position;
		for (int i = m_leaderPosHistory.Length - 1; i > 0; i--)
		{
			Vector3 vectorToNextPoint = (m_leaderPosHistory[i - 1] - m_leaderPosHistory[i]);
			if (vectorToNextPoint.magnitude > ApplicationManager.config.game.distanceBetweenHistoryPos)
			{
				m_leaderPosHistory[i] += vectorToNextPoint.normalized * (vectorToNextPoint.magnitude - ApplicationManager.config.game.distanceBetweenHistoryPos);
			}
		}
	}
	private Vector3 GetHistoryPos ( float ratio )
	{
		float point;
		int targetPoint;

		point = m_leaderPosHistory.Length * ratio;
		point = Mathf.Min(point, m_leaderPosHistory.Length - 1);
		targetPoint = Mathf.CeilToInt(point);
		Vector3 pos = m_leaderPosHistory[targetPoint];
		pos += (m_leaderPosHistory[targetPoint - 1] - m_leaderPosHistory[targetPoint]).normalized * (point - targetPoint);

		return pos;
	}*/
		#endregion

		#region CROWD MANAGEMENT

		/*protected override void ActionAtSpawn ( Character character, int index, bool wasNew )
		{
			if (index == 0)
			{
				//character.isLeader = true;
				m_currentLeader = character;
			}

			if (wasNew)
			{
				//character.PlaySpawnFx();
			}
			//character.isControlledCharacter = true;

			if (GameManager.Instance.GameState != GameManager.EGameState.InMenu)
				character.SetAnimatorRunValue(1f);
		}*/

		protected override Vector3 GetCharacterOffSetVector ( int characterIndex )
		{
			if (characterIndex == 0) return Vector3.zero;

			//SPAWN in capsule behind leader
			Vector3 offSet = Vector3.zero;

			float line = 1f + Mathf.Floor(characterIndex / ApplicationManager.config.game.playerCrowdCharacterPerLine);
			float column = characterIndex % ApplicationManager.config.game.playerCrowdCharacterPerLine;

			offSet -= m_crowdXTargetTfm.forward * (ApplicationManager.config.game.playerCrowdZOffsetPerLine * line);

			Vector3 value = m_crowdXTargetTfm.right *
						  (ApplicationManager.config.game.playerCrowdXOffsetMultiplier *
						   Mathf.Floor((column + 1f) / 2f));

			if (Math.Abs(column) > 0.01f)
			{
				if (Math.Abs(column % 2) < 0.01f)
					offSet += value;
				else
					offSet -= value;
			}

			Vector3 randomPos = UnityEngine.Random.insideUnitSphere * ApplicationManager.config.game.crowdRandomOffSet;
			randomPos.y = 0f;

			offSet += randomPos;

			return offSet;
		}

		public void ResetPlayer ()
		{
			speed = ApplicationManager.config.game.playerSpeed;
#if CAPTURE
			speed = ApplicationManager.config.game.playerSpeed * speedMultiplier;
#endif
			splineDist = 20f;
			m_destroyed = false;
			m_playing = false;
			m_posX = 0f;
			m_posY = 0f;
			//m_speedY = 0f;

			ResetPosition();
		}

		private void ResetPosition ()
		{
			Vector3 destination = m_splineToFollow.GetPointInWorldFromdist(splineDist);
			transform.position = destination;
			Vector3 direction = m_splineToFollow.GetDirectionFromDist(splineDist);
			float angle = m_splineToFollow.GetAngleFromdist(splineDist);
			Vector3 right = m_splineToFollow.GetNormalFromdist(splineDist);
			right = Quaternion.AngleAxis(angle, direction) * right;
			Vector3 up = Vector3.Cross(right, direction);
			transform.rotation = Quaternion.LookRotation(direction, up);

			m_crowdXTargetTfm.transform.position = transform.position + transform.right * m_posX;

			foreach (Character character in m_charactersList)
			{
				//character.ResetPosition();
			}
		}

		#endregion

		#region GAME
		public void Play ()
		{
			m_playing = true;
			currentSpeed = 0;
			DOTween.To(() => currentSpeed, x => currentSpeed = x, 0.5f, 1).SetEase(Ease.InSine);

			for (int i = 0; i < m_charactersList.Count; i++)
			{
				m_charactersList[i].SetAnimatorRunValue(1f);
			}

			//m_speedLine.Play();
			//m_speedLineEmission.rateOverTime = 0;
		}

		public void SetInvincible ( float time )
		{
			m_isInvincible = true;
			StartCoroutine(SetInvincibleCR(time));
		}
		private IEnumerator SetInvincibleCR ( float time )
		{
			m_isInvincible = true;
			yield return new WaitForSeconds(time);
			m_isInvincible = false;
		}

		protected override void SetEmpty ()
		{
			m_destroyed = true;
			GameManager.Instance.LevelFailed();
		}

		#endregion

		#region TRIGGER CALLBACKS
		private void OnEnterColorPortal ( float colorIndex )
		{
			MMVibrationManager.Haptic(HapticTypes.LightImpact);
			//Debug.Log("OnEnterColorPortal");
			ColorIndex = (int)colorIndex;
		}

		public void Revive ()
		{
			MMVibrationManager.Haptic(HapticTypes.HeavyImpact);

			TargetCharacterCount = ApplicationManager.config.game.retryCharacterCount;
			m_crowdCounter.UpdateCharacterGenerated(ApplicationManager.config.game.retryCharacterCount);
			BalanceCrowdSize();
			m_destroyed = false;

			SetInvincible(ApplicationManager.config.game.retryInvincibleTime);
		}

		private void OnEnterMultiplicationPortal ( float multiplicationValue, MultiplicationPortal triggeredPortal )
		{
			MMVibrationManager.Haptic(HapticTypes.SoftImpact);
			int formerValue = TargetCharacterCount;
			TargetCharacterCount *= (int)multiplicationValue;
			m_crowdCounter.UpdateCharacterGenerated(TargetCharacterCount - formerValue);
			// triggeredPortal.CharacterGenerated = TargetCharacterCount - formerSize;
			BalanceCrowdSize();
		}

		private void OnEnterColorCrowdArea ( int colorIndex, Character character, CrowdArea area )
		{
			//Debug.Log("OnEnterColorCrowdArea");
			if (m_targetCharacterCount > 0 && area.isInteractable)
			{
				if (colorIndex == m_currentColorIndex && area.TargetCharacterCount > 0) // Characters assimilation
				{
					MMVibrationManager.Haptic(HapticTypes.LightImpact);
					StartCoroutine(CollectAllCharacterInColoredAreaCR(area));
					return;
				}

				if (colorIndex != m_currentColorIndex /*&& !character.IsCulled*/) // Remove Character from our list
				{
					if (m_isInvincible)
						return;

					m_currentBattleSlowTimerDelay += Time.fixedDeltaTime * 6f;

					MMVibrationManager.Haptic(HapticTypes.LightImpact);
					character.transform.SetParent(GameManager.level.transform);
					/*character.characterTarget = area.PickCharacter(); //remove character from area
																	  //they will rush to each other
					character.characterTarget.SetAnimatorRunValue(1f);
					character.characterTarget.characterTarget = character;*/
					RemoveCharacter(character);
					--TargetCharacterCount;
					BalanceCrowdSize();
				}
			}
		}

		private void OnHitTrap ( Character character )
		{
			if (m_isInvincible)
				return;

			// if (character.isLeader)
			// 	m_currentDamageStopTimerDelay += Time.fixedDeltaTime * 5;

			MMVibrationManager.Haptic(HapticTypes.SoftImpact);
			character.transform.SetParent(GameManager.level.transform);
			RemoveCharacter(character);
			--TargetCharacterCount;
			BalanceCrowdSize();

			character.Kill();
			// if (character.isLeader)
			// 	DistributeTargetOffSetsAndLeader();
		}

		private IEnumerator CollectAllCharacterInColoredAreaCR ( CrowdArea area )
		{
			area.isInteractable = false;

			WaitForSeconds delay = new WaitForSeconds(ApplicationManager.config.game.collectCharacterFrq);

			m_crowdCounter.UpdateCharacterGenerated(area.TargetCharacterCount);

			while (area.TargetCharacterCount > 0)
			{
				Character newCharacter = area.PickCharacter();
				//newCharacter.isColorCrowd = false;
				//newCharacter.playerEffect.ChangeColor(ColorCollection.Instance.ReturnColor(m_currentColorPreset));
				AddCharacter(newCharacter);
				++TargetCharacterCount;
				yield return delay;
			}
			BalanceCrowdSize();
		}

		private void OnEnterEndLevel ( Transform posCannon )
		{
			m_crowdCounter.transform.DOScale(0f, 0.5f).onComplete += () => { m_crowdCounter.gameObject.SetActive(false); };

			for (int i = 0; i < m_charactersList.Count; i++)
			{
				m_charactersList[i].SetAnimatorRunValue(0f);
			}
		}

		#endregion

	}
}
