﻿using DG.Tweening;
using MoreMountains.NiceVibrations;
using Pinpin.InputControllers;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using Cinemachine;
using System.Collections.Generic;
using Pinpin.UI;
using Pinpin.Helpers;

namespace Pinpin
{
	public class PlayerBackUpScript : MonoBehaviour
	{
		public static Action<float> onPlayerProgress;
		public Action onDied;
		public Action<int> onScoreChange;

		#region MEMBERS
		[Header("References")]
		[Space, Tooltip("Physics")]
		[SerializeField] private Transform m_body;
		[SerializeField] private Transform m_model;
		[SerializeField] private Rigidbody m_rigidbody;
		[SerializeField] BezierSpline m_splineToFollow;
		[SerializeField] float m_gravity = 20f;

		[Space, Tooltip("Visual")]
		[SerializeField] private Animator m_animator;
		[SerializeField] private Renderer m_baseRenderer;
		[SerializeField] private Transform m_baseGravityCenter;

		[Space]
		[SerializeField] private GameObject m_baseModel;

		[SerializeField] private BoxCollider m_collider;

		//HADRIEN
		public float radius;
		private bool m_playing = false;
		private bool m_destroyed = false;

		private Vector3 m_headPosition;
		private float m_posX;
		private float m_posY;
		private float m_speedY = 0f;
		private float m_xLimitMin = -1f;
		private float m_xLimitMax = 1f;
		private float m_yLimitMin = 1f;
		private float refVelocityRotation = 0;

		public float posX { get { return (m_posX); } }
		public float posY { get { return (m_posY); } }
		public float speed { get; private set; }
		public float currentSpeed { get; private set; }
		public bool isDead { get { return (m_destroyed); } }

		public float splineDist { get; private set; }
		public BezierSpline splineToFollow { get { return m_splineToFollow; } }
		#endregion

		#region MONOBEHAVIOR

		// Update is called once per frame
		void FixedUpdate ()
		{
			if (m_destroyed || !m_playing)
				return;

			//Head Movement
			Vector3 newHeadPosition = m_headPosition;

			m_posX = Mathf.Clamp(m_posX + InputManager.moveDelta.x * ApplicationManager.config.game.sensibility, m_xLimitMin, m_xLimitMax);

			if (m_yLimitMin < m_posY)
			{
				if (m_yLimitMin == 0f)
				{
					m_speedY += -m_gravity * Time.fixedDeltaTime;
					m_posY += m_speedY * Time.fixedDeltaTime;
					if (m_posY < m_yLimitMin && m_yLimitMin > 0f)
					{
						m_speedY = 0f;
						m_posY = m_yLimitMin;
					}
				}
				else
				{
					m_posY = m_yLimitMin;
				}
			}

			newHeadPosition = transform.position + transform.up * m_posY + transform.right * m_posX;

			m_headPosition = newHeadPosition;
			m_body.transform.position = m_headPosition;

			float distDelta = Mathf.Min(currentSpeed, ApplicationManager.datas.levelProgression % 4 == 3 ? 0.70f : 0.85f) * speed * Time.fixedDeltaTime;

			splineDist += distDelta;

			//Player Movement
			Move();

			m_animator.SetFloat("Speed", currentSpeed); // parametre entre 0 et 1
		}

		private void LateUpdate ()
		{
			if (m_playing)
			{
				float angle = Mathf.Atan2(InputManager.moveDelta.x * ApplicationManager.config.game.moveRotMultiplier, speed * Time.deltaTime) * Mathf.Rad2Deg;
				angle = Mathf.SmoothDampAngle(m_body.localEulerAngles.y, angle, ref refVelocityRotation, 0.2f);
				Vector3 rotation = new Vector3(0f, angle, 0f);
				m_body.localEulerAngles = rotation;
			}
		}
		#endregion

		#region GAME
		public void Play ()
		{
			m_playing = true;
			currentSpeed = 0;
			DOTween.To(() => currentSpeed, x => currentSpeed = x, 0.5f, 1).SetEase(Ease.InSine);
			//m_speedLine.Play();
			//m_speedLineEmission.rateOverTime = 0;
		}


		#endregion

		#region MOVEMENTS
		private void Move ()
		{
			Vector3 destination = m_splineToFollow.GetPointInWorldFromdist(splineDist);
			m_rigidbody.MovePosition(destination);
			Vector3 direction = m_splineToFollow.GetDirectionFromDist(splineDist);
			float angle = m_splineToFollow.GetAngleFromdist(splineDist);
			Vector3 right = m_splineToFollow.GetNormalFromdist(splineDist);
			right = Quaternion.AngleAxis(angle, direction) * right;
			Vector3 up = Vector3.Cross(right, direction);
			m_rigidbody.MoveRotation(Quaternion.LookRotation(direction, up));
			onPlayerProgress?.Invoke(splineDist / m_splineToFollow.TotalLength);
		}

		public void SetLimits ( float min, float max, float height )
		{
			m_xLimitMin = min + radius;
			m_xLimitMax = max - radius;
			m_yLimitMin = height;
			if (m_posY < m_yLimitMin)
			{
				//m_speedY = (m_yLimitMin - m_posY) / Time.deltaTime;
				m_posY = m_yLimitMin;
			}
		}

		public void ResetPosition ()
		{
			Vector3 destination = m_splineToFollow.GetPointInWorldFromdist(splineDist);
			transform.position = destination;
			Vector3 direction = m_splineToFollow.GetDirectionFromDist(splineDist);
			float angle = m_splineToFollow.GetAngleFromdist(splineDist);
			Vector3 right = m_splineToFollow.GetNormalFromdist(splineDist);
			right = Quaternion.AngleAxis(angle, direction) * right;
			Vector3 up = Vector3.Cross(right, direction);
			transform.rotation = Quaternion.LookRotation(direction, up);

			m_headPosition = transform.position + transform.right * m_posX;
			m_body.transform.position = m_headPosition;
		}

		public void ResetPlayer ()
		{
			speed = ApplicationManager.config.game.playerSpeed;
			splineDist = 20f;
			m_destroyed = false;
			m_playing = false;
			m_posX = 0f;
			m_posY = 0f;
			m_speedY = 0f;

			ResetPosition();
		}
		#endregion

#if UNITY_EDITOR
		private void OnDrawGizmos ()
		{
			Gizmos.color = Color.red;
			Gizmos.DrawLine(transform.position + transform.right * m_xLimitMin + transform.up * m_yLimitMin - transform.forward * 0.25f, transform.position + transform.right * m_xLimitMin + transform.up * m_yLimitMin + transform.forward * 0.25f);
			Gizmos.DrawLine(transform.position + transform.right * m_xLimitMax + transform.up * m_yLimitMin - transform.forward * 0.25f, transform.position + transform.right * m_xLimitMax + transform.up * m_yLimitMin + transform.forward * 0.25f);
			Gizmos.DrawLine(transform.position + transform.right * m_xLimitMin + transform.up * m_yLimitMin, transform.position + transform.right * m_xLimitMax + transform.up * m_yLimitMin);
		}
#endif
	}
}
