﻿using DG.Tweening;
using MoreMountains.NiceVibrations;
using Pinpin.InputControllers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Cinemachine;

namespace Pinpin
{
	public class PlayerGunnerController : MonoBehaviour
	{
		public static Action<ulong, ulong> updateGameAmmo;
		public static Action OnReviveChangeMatAction;

		[SerializeField] Character m_character;
		[SerializeField] private Transform m_camTarget;
		[SerializeField] BezierSpline m_splineToFollow;
		[SerializeField] float m_gravity = 15f;

		[SerializeField] private Transform m_armRight;
		[SerializeField] private Transform m_armLeft;
		[SerializeField] private ParticleSystem m_muzzleFlashRightFx = null;
		[SerializeField] private ParticleSystem m_muzzleFlashLeftFx = null;

		public CinemachineVirtualCamera finisherCamera;

		private bool m_playing = false;
		private bool m_isDead = false;
		public bool isDead { get { return (m_isDead); } }
		private bool m_isInvincible = false;

		public float radius = 0.6f;
		private float m_posX;
		private float m_targetPosX;
		private float m_posY;
		private float m_speedY = 0f;
		private float m_xLimitMin;
		private float m_xLimitMax;
		private float m_yLimitMin = 1f;

		[HideInInspector] public bool stopMoving = true;

		private bool m_isTakingDamangeOnTime = false;
		private float m_damageOnTime = 0f;
		
		public float splineDist { get; private set; }
		public float currentSpeed { get; private set; }
#if !CAPTURE
		public float targetSpeed { get { return gameConfig.playerSpeed; } }
		public Transform body { get { return m_character.body; } }
		public Transform characterTransform { get { return m_character.transform; } }
		public BezierSpline bezierSpline { get { return m_splineToFollow; } }
#else
		public float targetSpeed { get { return gameConfig.playerSpeed * speedMultiplier; } }
		[HideInInspector] public float speedMultiplier = 1f;
#endif

		private Vector3 m_camTargetOffset;

		public ulong ammoCount { get; private set; }
		private List<ShootableElement> m_shootableElementInRangeList = new List<ShootableElement>();

		public float posX { get { return (m_posX); } }
		public float posY { get { return (m_posY); } }

		private GameConfig.GameSettings gameConfig
		{
			get { return ApplicationManager.config.game; }
		}

		private void Awake ()
		{
			Finisher.JumpToFinisherAction += JumpToFinisher;
			Trap.OnCollide += OnTrapCollide;
			
			m_camTargetOffset = Vector3.forward * 4f;

			m_xLimitMin = -4 + radius;
			m_xLimitMax = 4 - radius;
		}


		private void OnDestroy ()
		{
			Finisher.JumpToFinisherAction -= JumpToFinisher;
		}

		public void StartGame ()
		{
			ammoCount = (ulong)(GameManager.level.currentLevelInfo.ammoQuantityAtStart * ApplicationManager.config.game.GetBulletUpgradeValue(ApplicationManager.datas.bulletUpgradeLevel));
			m_playing = true;
			stopMoving = false;
			currentSpeed = 0;
			splineDist = 20;
			m_character.SetAnimatorRunValue(1f);
		}

		public void SetLimits ( float min, float max, float height )
		{
			m_xLimitMin = min + radius;
			m_xLimitMax = max - radius;
			m_yLimitMin = height;
			if (m_posY < m_yLimitMin)
			{
				//m_speedY = (m_yLimitMin - m_posY) / Time.deltaTime;
				m_posY = m_yLimitMin;
			}
		}

		public void ActivateDamageOnTime(float damage)
		{
			m_isTakingDamangeOnTime = true;
			m_damageOnTime = damage;
		}

		public void DeactivateDamageOnTime()
		{
			m_isTakingDamangeOnTime = false;
			m_damageOnTime = 0f;
		}

		private float m_timerDamageOnTime = 0f;
		private float m_life = 10f;
		
		private void FixedUpdate ()
		{

			if (m_isDead || !m_playing || GameManager.Instance.GameState == GameManager.EGameState.Finisher || GameManager.Instance.GameState == GameManager.EGameState.EndGame)
				return;

			if (m_isTakingDamangeOnTime)
			{
				m_timerDamageOnTime -= Time.fixedDeltaTime;
				if (m_timerDamageOnTime <= 0f)
				{
					m_timerDamageOnTime += ApplicationManager.config.game.timerDamageOnTime;
					TakeDamageOverTime();
				}
			}
			
			//X Movement
			if (GameManager.Instance.GameState == GameManager.EGameState.InGame)
			{
				m_targetPosX = Mathf.Clamp(m_targetPosX + InputManager.moveDelta.x * gameConfig.sensibility, m_xLimitMin, m_xLimitMax);
			}
			else
			{
				m_posX = 0f; // pos to finisher 
			}
			m_posX = Mathf.Lerp(m_posX, m_targetPosX, 0.05f);


			if (m_yLimitMin < m_posY)
			{
				if (m_yLimitMin == 0f)
				{
					m_speedY += -m_gravity * Time.fixedDeltaTime;
					m_posY += m_speedY * Time.fixedDeltaTime;
					if (m_posY < m_yLimitMin && m_yLimitMin > 0f)
					{
						m_speedY = 0f;
						m_posY = m_yLimitMin;
					}
				}
				else
				{
					m_posY = m_yLimitMin;
				}
			}

			m_character.transform.localPosition = transform.right * m_posX + transform.up * m_posY;
			

			if (currentSpeed < targetSpeed)
			{
				currentSpeed += targetSpeed * Time.fixedDeltaTime;
				if (currentSpeed > targetSpeed)
					currentSpeed = targetSpeed;
			}

			float distDelta = currentSpeed * Time.fixedDeltaTime;
			splineDist += distDelta;

			Move();
		}
		private void LateUpdate ()
		{
			m_camTarget.position = Vector3.Lerp(m_camTarget.position, transform.position + m_camTargetOffset, gameConfig.camTargetSmoothDamp * Time.deltaTime);
		}

		private void Move ()
		{
			//pos on spline
			Vector3 destination = m_splineToFollow.GetPointInWorldFromdist(splineDist);
			transform.position = destination;
			//rot on spline move
			Vector3 direction = m_splineToFollow.GetDirectionFromDist(splineDist);
			float angle = m_splineToFollow.GetAngleFromdist(splineDist);
			Vector3 right = m_splineToFollow.GetNormalFromdist(splineDist);
			right = Quaternion.AngleAxis(angle, direction) * right;
			Vector3 up = Vector3.Cross(right, direction);
			transform.rotation = Quaternion.LookRotation(direction, up);
		}



		internal void OnEnterCrowdAreaRange ( ShootableElement shootableElement )
		{
			m_shootableElementInRangeList.Add(shootableElement);
		}

		internal void OnExitCrowdAreaRange ( ShootableElement shootableElement )
		{
			if (m_shootableElementInRangeList.Contains(shootableElement))
				m_shootableElementInRangeList.Remove(shootableElement);
		}

		public void Kill ()
		{
			if (m_isInvincible) return;
			m_isDead = true;
			stopMoving = true;
			CrowdArea.stopAnimRunAction(0f);
			m_character.Kill();
			GameManager.Instance.LevelFailed();
		}
		public void Revive ()
		{
			m_character.Revive();
			MMVibrationManager.Haptic(HapticTypes.HeavyImpact);
			m_isDead = false;
			stopMoving = false;
			m_character.SetAnimatorRunValue(1f);
			SetInvincible(gameConfig.retryInvincibleTime);
		}
		public void SetInvincible ( float time )
		{
			m_isInvincible = true;
			StartCoroutine(SetInvincibleCR(time));
		}
		private IEnumerator SetInvincibleCR ( float time )
		{
			m_isInvincible = true;
			OnReviveChangeMatAction?.Invoke();
			yield return new WaitForSeconds(time);
			m_isInvincible = false;
		}
		
		private void TakeDamageOverTime()
		{
			m_life -= m_damageOnTime;
			if (m_life <= 0)
				Kill();
		}
		
		private void OnTrapCollide(bool isTriggerEnter, Trap trap)
		{
			if (isTriggerEnter)
				trap.ApplyOnPlayer(this);
			else
				trap.ClearOnPlayer(this);
		}
		
#region Finisher
		private void JumpToFinisher ()
		{
			m_playing = false;
			m_character.SetAnimatorFrontFlip();
			//m_character.SetAnimatorRunValue(0f);
		}
#endregion


		private void OnDrawGizmos ()
		{
			Gizmos.color = Color.red;
			Gizmos.DrawLine(transform.position + transform.right * m_xLimitMin + transform.up * m_yLimitMin - transform.forward * 0.25f, transform.position + transform.right * m_xLimitMin + transform.up * m_yLimitMin + transform.forward * 0.25f);
			Gizmos.DrawLine(transform.position + transform.right * m_xLimitMax + transform.up * m_yLimitMin - transform.forward * 0.25f, transform.position + transform.right * m_xLimitMax + transform.up * m_yLimitMin + transform.forward * 0.25f);
			Gizmos.DrawLine(transform.position + transform.right * m_xLimitMin + transform.up * m_yLimitMin, transform.position + transform.right * m_xLimitMax + transform.up * m_yLimitMin);
		}
	}
}
