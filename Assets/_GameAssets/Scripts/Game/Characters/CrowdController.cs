﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Pinpin.Helpers;
using UnityEngine;
using UnityEngine.UI;

namespace Pinpin
{
	public abstract class CrowdController : ShootableElement
	{
		#region MEMBERS
		[SerializeField] protected Transform m_crowdZTargetTfm;
		[SerializeField] protected Transform m_crowdXTargetTfm;
		public List<Character> m_charactersList;

		protected int m_characterLeftToSpawn = 0;
		[SerializeField] protected int m_targetCharacterCount = 0;

		public int TargetCharacterCount
		{
			get => m_targetCharacterCount;
			set
			{
				m_targetCharacterCount = value;
				UpdateDisplayCount(m_targetCharacterCount);
			}
		}

		public int GetCharacterLeftToSpawn
		{
			get { return m_characterLeftToSpawn; }
			set { m_characterLeftToSpawn = value; }
		}

		public int GetTargetCharacterCount
		{
			get { return m_targetCharacterCount; }
			set { m_targetCharacterCount = value; }
		}

		private List<Vector3> m_positionList;

		[Range(0, 2)]
		[SerializeField]
		protected int m_currentColorIndex;
		protected ColorPreset m_currentColorPreset;
		protected Color m_color;

		private Tweener m_countBounceTween;
		protected Character m_currentLeader;

		public Vector3 TargetPos
		{
			get { return m_crowdXTargetTfm.position; }
		}
		#endregion

		#region COLOR
		public virtual int ColorIndex
		{
			get => m_currentColorIndex;
			set
			{
				if (value < 0) return;
				//if (value == m_currentColorIndex || value < 0) return;

				m_currentColorIndex = value;
				m_currentColorPreset = GameManager.level.currentLevelInfo.colorPalette[value];

				UpdateColor();
			}
		}

		protected virtual void UpdateColor ()
		{
			m_charactersList.ForEach(character => character.ChangeColor(ColorCollection.Instance.ReturnColor(m_currentColorPreset)));
		}

		public Color GetColor ()
		{
			return ColorCollection.Instance.ReturnColor(m_currentColorPreset);
		}

		#endregion

		protected virtual void Start ()
		{
			ColorIndex = m_currentColorIndex;
			BalanceCrowdSize();
			StartCoroutine(AddCharacterCoroutine());
		}

		#region CROWD MANAGEMENT

		private IEnumerator AddCharacterCoroutine ()
		{
			WaitForSeconds wait = new WaitForSeconds(.01f);

			while (gameObject.activeSelf)
			{
				int characterToSpawn = Mathf.Min(m_characterLeftToSpawn,
					ApplicationManager.config.game.crowMaxCharacterToSpawnPerFrame);

				for (int i = 0; i < characterToSpawn; ++i)
				{
					AddCharacter();
				}

				m_characterLeftToSpawn -= characterToSpawn;
				yield return wait;
			}
		}

		protected void BalanceCrowdSize ()
		{
			// 10 - 20
			if (m_charactersList.Count < m_targetCharacterCount) // Le nombre de personnages en jeu est < au nombre que l'on doit avoir
			{
				m_characterLeftToSpawn = Mathf.Min(
					ApplicationManager.config.game.crowdMaxSpawnSize - m_charactersList.Count,
					m_targetCharacterCount - m_charactersList.Count);
			}
			else
			{
				m_characterLeftToSpawn = 0;
			}

			if (m_targetCharacterCount == 0 && m_charactersList.Count == 0 && m_characterLeftToSpawn == 0)
			{
				SetEmpty();
			}
		}

		protected void AddCharacter ( Character p_character = null )
		{
			Vector3 basePosition = m_currentLeader ? m_currentLeader.transform.position : m_crowdXTargetTfm.position;
			Vector3 offset = GetCharacterOffSetVector(m_charactersList.Count);
			Vector3 position = basePosition + offset + Vector3.up * 0.1f;
			Character newCharacter = p_character != null ? p_character : Instantiate(ApplicationManager.assets.characterPrefab, position, Quaternion.identity, transform);

			m_charactersList.Add(newCharacter);
			newCharacter.SetTargetAndOffset(m_crowdXTargetTfm, offset);
			StartCoroutine(ChangeColorWait(newCharacter));

			//newCharacter.gameObject.AddComponent<DeathZone>();
			//newCharacter.body.GetComponent<Collider>().isTrigger = true;
			//newCharacter.body.gameObject.tag = "Untagged";
		}

		private IEnumerator ChangeColorWait ( Character newCharacter )
		{
			newCharacter.skinCharacter.SetActive(false);
			newCharacter.ChangeColor(ColorCollection.Instance.ReturnColor(m_currentColorPreset));
			yield return new WaitForEndOfFrame();
			newCharacter.skinCharacter.SetActive(true);
		}

		protected void RemoveCharacter ( Character p_character = null )
		{
			if (p_character) // Le character a collide dans un trap on le supprime de notre liste
			{
				m_charactersList.Remove(p_character);
			}
			else // Le character n'existe pas et on rebalance la size de la liste
			{
				Character c = m_charactersList.Last();
				Destroy(c.gameObject);
				m_charactersList.Remove(c);
			}
		}

		protected virtual Vector3 GetCharacterOffSetVector ( int characterIndex )
		{
			//SPAWN In Circle
			if (m_positionList == null || m_positionList.Count != m_targetCharacterCount)
				m_positionList = MathHelper.GetPositionListCircle(m_crowdXTargetTfm.transform.position, ApplicationManager.config.game.colorCrowdSpawndistanceStep, m_targetCharacterCount);

			Vector3 newPos = m_positionList[characterIndex] - m_crowdXTargetTfm.transform.position;

			Vector3 randomPos = Random.insideUnitSphere * ApplicationManager.config.game.crowdRandomOffSet;
			randomPos.y = 0f;

			newPos += randomPos;

			return newPos;
		}

		protected void DistributeTargetOffSetsAndLeader ()
		{
			for (int i = 0; i < m_charactersList.Count; i++)
			{
				m_charactersList[i].SetTargetAndOffset(m_crowdXTargetTfm, GetCharacterOffSetVector(i));
			}
		}

		protected abstract void SetEmpty ();

		#endregion
	}
}
