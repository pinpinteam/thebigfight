﻿using Pinpin;
using System.Collections;
using DG.Tweening;
using Unity.Collections;
using UnityEngine;

public class Character : MonoBehaviour
{
	public Transform body;
	[SerializeField] private Animator m_animator;
	[SerializeField] private PlayerEffect m_basePlayerEffect;
	public GameObject skinCharacter;

	[Header("FX Spawn")]
	[SerializeField] private ParticleSystem m_spawnParticleSystem;
	[Header("FX Death")]
	[SerializeField] private ParticleSystem m_deathParticleSystem;
	[SerializeField] private MeshRenderer m_groundDeathMeshRenderer;
	[Space]
	[SerializeField] private bool m_hasGuns = false;
	[HideInInspector] public bool isRunning = false;

	public bool IsAlive => m_isAlive;
	private bool m_isAlive = true;

	public void SetTargetAndOffset ( Transform p_tfm, Vector3 p_offset )
	{
		transform.position = p_tfm.position + p_offset;
		transform.localEulerAngles = new Vector3(0, 180, 0);
	}

	public void ChangeColor ( Color newColor )
	{
		ParticleSystem.MainModule main = m_deathParticleSystem.main;
		main.startColor = newColor;

		main = m_spawnParticleSystem.main;
		main.startColor = newColor;

		Material material = m_groundDeathMeshRenderer.material;
		Vector4 hdrColor = material.color;

		hdrColor.x = newColor.r;
		hdrColor.y = newColor.g;
		hdrColor.z = newColor.b;

		material.color = hdrColor;

		m_basePlayerEffect.ChangeColor(newColor);
	}

	public void SetAnimatorRunValue ( float value )
	{
		m_animator.SetFloat("Speed", value);
		m_animator.SetLayerWeight(1, 1);

	}
	public void SetAnimatorFrontFlip ()
	{
		m_animator.SetTrigger("FrontFlip");
	}

	public void DelayKill ()
	{
		StartCoroutine(WaitToDeath());
	}
	private IEnumerator WaitToDeath ()
	{
		yield return new WaitForSeconds(.075f);
		Kill();
	}
	public void Kill ()
	{
		if (!m_isAlive) return;

		body.gameObject.SetActive(false);
		m_deathParticleSystem.Play();
		m_groundDeathMeshRenderer.enabled = true;
		m_isAlive = false;
	}
	public void Revive ()
	{
		if (m_isAlive) return;

		body.gameObject.SetActive(true);
		m_groundDeathMeshRenderer.enabled = false;
		m_isAlive = true;
	}
}
