﻿using DG.Tweening;
using UnityEngine;

namespace Pinpin
{
	public class PlayerEffect : MonoBehaviour
	{
		private static readonly string COLOR = "_Color";
		private static readonly string AMOUNT = "_Amount";
		private static readonly string HIGHLIGHT_COLOR = "_HighlightColor";

		[SerializeField] private Renderer m_renderer;
		[Header("Highlight")]
		[SerializeField] private AnimationCurve m_highlightCurve;
		[SerializeField] private float m_highlightDuration;

		private Tweener m_highlightTween;

		public void Highlight ( bool positiv, Color color, float power = 1, bool overrideColor = false )
		{
			m_highlightTween?.Pause();

			foreach (Material m in m_renderer.materials)
			{
				//m.SetFloat(AMOUNT, 0);
				m.SetColor(HIGHLIGHT_COLOR, positiv ? GetLighterColor(0.22f, color, overrideColor) : GetDarkerColor(0.25f, color, overrideColor));
			}

			float currentAmount = 1f;
			if (m_renderer.material.HasProperty(AMOUNT))
				currentAmount = m_renderer.material.GetFloat(AMOUNT);

			m_highlightTween = DOTween.To(() => currentAmount, x => currentAmount = x, power, m_highlightDuration * 0.25f).SetEase(Ease.InOutSine).OnUpdate(() =>
			{
				foreach (Material m in m_renderer.materials)
				{
					m.SetFloat(AMOUNT, currentAmount);
				}
			}).OnComplete(() =>
			{
				HighlightBack(power);
			});
		}

		private Color GetDarkerColor ( float newValue, Color newColor, bool overrideColor )
		{
			Color color = m_renderer.material.GetColor(COLOR);
			if (overrideColor)
			{
				color = newColor;
			}
			Color.RGBToHSV(color, out float h, out float s, out _);
			color = Color.HSVToRGB(h, s, newValue);

			return color;
		}

		private Color GetLighterColor ( float newSaturation, Color newColor, bool overrideColor )
		{
			if (!m_renderer.material.HasProperty(COLOR)) return Color.clear;

			Color color = m_renderer.material.GetColor(COLOR);
			if (overrideColor)
			{
				color = newColor;
			}
			Color.RGBToHSV(color, out float h, out _, out _);
			color = Color.HSVToRGB(h, newSaturation, 1);

			return color;
		}

		private void HighlightBack ( float power )
		{
			float currentAmount = power;
			m_highlightTween = DOTween.To(() => currentAmount, x => currentAmount = x, 0, m_highlightDuration * 0.75f).SetEase(Ease.InOutSine).OnUpdate(() =>
			{
				foreach (Material m in m_renderer.materials)
				{
					m.SetFloat(AMOUNT, currentAmount);
				}
			});
		}

		private Color m_currentColor;

		public void ChangeColor ( Color color )
		{
			if (m_renderer.material.HasProperty(COLOR))
				m_currentColor = m_renderer.material.GetColor(COLOR);
			Highlight(true, color, 1, true);

			DOTween.To(() => m_currentColor, x => m_currentColor = x, color, 0.5f).SetEase(Ease.OutSine).OnUpdate(() =>
			{
				foreach (Material m in m_renderer.materials)
				{
					if (m.HasProperty(COLOR))
						m.SetColor(COLOR, m_currentColor);
				}
			});
		}
	}
}
