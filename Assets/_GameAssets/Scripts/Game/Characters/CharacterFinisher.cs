﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Pinpin
{
    public class CharacterFinisher : MonoBehaviour
	{
		public static Action<Transform, CharacterFinisher> OnShootByBulletAction;

		private bool m_isShot = false;

		[SerializeField] private ParticleSystem m_deathParticleSystem = null;
		[SerializeField] private MeshRenderer m_groundDeathMeshRenderer = null;
		[SerializeField] private PlayerEffect m_basePlayerEffect = null;
		[SerializeField] private GameObject m_body = null;
		[SerializeField] private Collider m_collider = null;
		[SerializeField] private Animator m_animator = null;

		public Rigidbody rigidbody;

		private void Awake ()
		{
			ChangeColor(ColorCollection.Instance.ReturnColor(ColorPreset.Red));
			m_animator.SetBool("BossReady", true);
		}


		private void OnTriggerEnter ( Collider other )
		{
			if (other.CompareTag("FinisherBullet") && !m_isShot)
			{
				m_isShot = true;
				OnShootByBulletAction?.Invoke(other.transform, this);
				m_deathParticleSystem.Play();
				m_groundDeathMeshRenderer.enabled = true;
				m_body.SetActive(false);
				m_collider.enabled = false;
			}
		}

		public void ChangeColor ( Color newColor )
		{
			ParticleSystem.MainModule main = m_deathParticleSystem.main;
			main.startColor = newColor;

			Material material = m_groundDeathMeshRenderer.material;
			Vector4 hdrColor = material.color;

			hdrColor.x = newColor.r;
			hdrColor.y = newColor.g;
			hdrColor.z = newColor.b;

			material.color = hdrColor;

			m_basePlayerEffect.ChangeColor(newColor);
		}
	}
}

