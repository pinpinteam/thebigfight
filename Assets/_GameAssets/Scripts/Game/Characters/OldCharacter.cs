﻿using Pinpin;
using System.Collections;
using DG.Tweening;
using Unity.Collections;
using UnityEngine;

public class OldCharacter : MonoBehaviour
{
	private static OldCharacter Leader;

	[Pinpin.ReadOnly] public OldCharacter characterTarget;
	[SerializeField] private Rigidbody m_rb;
	[SerializeField] private Collider m_characterCollider;
	public Rigidbody Rigidbody => m_rb;
	[SerializeField] private Transform m_body;
	[SerializeField] private Animator m_animator;
	[SerializeField] private PlayerEffect m_basePlayerEffect;
	public GameObject skinCharacter;

	[Header("FX Spawn")]
	[SerializeField] private ParticleSystem m_spawnParticleSystem;

	[Header("FX Death")]
	[SerializeField] private ParticleSystem m_deathParticleSystem;
	[SerializeField] private MeshRenderer m_groundDeathMeshRenderer;

	[SerializeField] private GameObject m_helmet;

	[Space()]
	[SerializeField] private GameObject[] m_culledGOArray;

	private Transform m_crowdCenterTfm;
	private Vector3 m_offset;
	private Vector3 m_targetPos;
	private Vector3 m_delayedTargetPos;

	private Vector3 m_dir;

	private bool m_isLeader;
	public bool isLeader
	{
		get => m_isLeader;
		set
		{
			if (m_isLeader != value)
			{
				m_isLeader = value;
				Leader = this;
				m_helmet.SetActive(m_isLeader);
				transform.DOScale(Vector3.one * 0.8f, 0.3f);
			}
		}
	}
	public bool isRunning;

	[HideInInspector]
	public bool isControlledCharacter;

	private float m_damp;
	private bool m_isCulled;
	public bool IsCulled => m_isCulled;
	public bool IsAlive => m_isAlive;
	private bool m_isAlive = true;

	private bool m_isColorCrowd = false;
	private bool m_kickAnimationTriggered = false;
	public bool isColorCrowd
	{
		get => m_isColorCrowd;
		set
		{
			m_isColorCrowd = value;
			if (m_isColorCrowd) transform.forward = -Vector3.forward;
		}
	}

	public PlayerEffect playerEffect => m_basePlayerEffect;

	public float offsetZ { get { return m_offset.z; } }

	public void SetTargetAndOffset ( Transform p_tfm, Vector3 p_offset )
	{
		m_crowdCenterTfm = p_tfm;
		m_targetPos = m_crowdCenterTfm.position + p_offset;
		m_delayedTargetPos = m_crowdCenterTfm.position + p_offset;

		m_offset = p_offset;
		if (isControlledCharacter && !isLeader)
		{
			//transform.position += Vector3.up * 0.3f;
			m_offset += new Vector3(0, 0, -0.3f); //To split a bit the leader from the crowd
												  //m_offset.x = m_crowdCenterTfm.position.x; //only z offset ?
		}

		//m_damp = ApplicationManager.config.game.crowdBaseFollowDamp * (ApplicationManager.config.game.crowdFollowDampMultiplier / m_offset.magnitude);
		m_offset = p_offset;
	}

	public void MoveToTarget ( Vector3 pos, BezierSpline spline, float splineDist )
	{
		//AdjustTargetPosY();
		m_targetPos = m_crowdCenterTfm.position + m_offset;

		m_delayedTargetPos = pos;
		//m_delayedTargetPos = Vector3.Lerp(m_delayedTargetPos, m_targetPos, m_damp * Time.fixedDeltaTime);
		Rigidbody.MovePosition(new Vector3(Rigidbody.position.x, spline.GetPointInWorldFromdist(Rigidbody.position.z + 20).y, Rigidbody.position.z));

		m_dir = (m_delayedTargetPos - m_rb.position);

		// parametre entre 0 et 1 //overkill
		//m_animator.SetFloat("Speed", m_dir.magnitude * ApplicationManager.config.game.characterAnimatorSpeedMultiplier * Time.fixedDeltaTime);
		//Debug.Log(m_dir.magnitude * ApplicationManager.config.game.characterAnimatorSpeedMultiplier * Time.fixedDeltaTime);

		//MovePos
		//m_rb.MovePosition(Vector3.MoveTowards(m_rb.position, m_delayedTargetPos, ApplicationManager.config.game.characterFollowSpeed * Time.fixedDeltaTime));

		//AddForce
		//m_rb.AddForce(m_dir * ApplicationManager.config.game.characterFollowSpeed * ApplicationManager.config.game.addForceMultiplier * Time.fixedDeltaTime);
		m_rb.rotation = Quaternion.identity;

		/*if (isLeader)
			m_rb.rotation = Quaternion.identity;
		else
			m_rb.rotation = Quaternion.LookRotation(m_dir);*/
	}

	private void FixedUpdate ()
	{
		//Move to character Target
		if ((GameManager.Instance.GameState == GameManager.EGameState.InGame && !m_isCulled) || characterTarget != null)
		{
			if (characterTarget != null && m_isAlive)
			{
				m_dir = characterTarget.transform.position - m_rb.position;

				transform.rotation = Quaternion.identity;

				//move to enemy and explode
				if (m_dir.magnitude > ApplicationManager.config.game.minDistToCollisionBetweenCharacter)
				{
					//if (m_isColorCrowd) return;
					m_dir.Normalize();
					m_rb.MovePosition(m_rb.position + m_dir * (ApplicationManager.config.game.freeCharacterSpeed * Time.fixedDeltaTime));
					//m_animator.SetFloat("Speed", m_dir.magnitude * ApplicationManager.config.game.freeCharacterSpeed * ApplicationManager.config.game.characterAnimatorSpeedMultiplier * Time.fixedDeltaTime);
				}
				else
				{
					if (!m_kickAnimationTriggered)
					{
						m_kickAnimationTriggered = true;
						m_animator.SetTrigger("KO");
						m_rb.isKinematic = true;
						m_characterCollider.enabled = false;
					}

					//m_isAlive = false;
					SetAnimatorRunValue(0f);
					StartCoroutine(WaitToDeath());
				}
			}
		}

		m_rb.velocity = Vector3.zero;
		m_rb.angularVelocity = Vector3.zero;
	}

	public void SetAnimatorRunValue ( float value )
	{
		m_animator.SetFloat("Speed", value);
	}

	private IEnumerator WaitToDeath ()
	{
		yield return new WaitForSeconds(.7f);
		Kill();
	}

	void AdjustTargetPosY ()
	{
		//TODO set correct Y pos from spline
	}

	public void ResetPosition ()
	{
		m_rb.position = m_crowdCenterTfm.transform.position;
		m_rb.velocity = Vector3.zero;
	}

	public void Kill ()
	{
		if (!m_isAlive) return;

		m_body.gameObject.SetActive(false);
		m_deathParticleSystem.Play();
		m_groundDeathMeshRenderer.enabled = true;
		m_isAlive = false;
	}

	public void ChangeColor ( Color newColor )
	{
		ParticleSystem.MainModule main = m_deathParticleSystem.main;
		main.startColor = newColor;

		main = m_spawnParticleSystem.main;
		main.startColor = newColor;

		Material material = m_groundDeathMeshRenderer.material;
		Vector4 hdrColor = material.color;

		hdrColor.x = newColor.r;
		hdrColor.y = newColor.g;
		hdrColor.z = newColor.b;

		material.color = hdrColor;

		m_basePlayerEffect.ChangeColor(newColor);
	}

	public void PlaySpawnFx ()
	{
		m_spawnParticleSystem.Play();
	}
}
