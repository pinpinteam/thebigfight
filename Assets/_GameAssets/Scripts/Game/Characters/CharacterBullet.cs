﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
	public class CharacterBullet : MonoBehaviour
	{
		[SerializeField] private Renderer[] m_renderers;
		[SerializeField] private AnimationCurve m_animCurve;
		private void Awake ()
		{
			PlayerGunnerController.OnReviveChangeMatAction += SetMaterialsRevive;
		}

		private void OnDestroy ()
		{
			PlayerGunnerController.OnReviveChangeMatAction -= SetMaterialsRevive;
		}

		private void SetMaterialsRevive ()
		{
			StartCoroutine(AnimBullet());
		}

		private IEnumerator AnimBullet ()
		{
			
			float percentTime;
			for (int i = 0; i < 3; ++i)
			{
				float startTime = Time.time; 
				while (Time.time < startTime + ApplicationManager.config.game.retryInvincibleTime / 3f)
				{
					percentTime = m_animCurve.Evaluate((Time.time - startTime) / (ApplicationManager.config.game.retryInvincibleTime / 3f));
					foreach (Renderer renderer in m_renderers)
						renderer.material.SetFloat("_SpecularDistance", Mathf.Lerp(0.03f, -1f, percentTime));
					yield return null;
				}
				foreach (Renderer renderer in m_renderers)
					renderer.material.SetFloat("_SpecularDistance", 0.03f);
			}
		
		}
	}

}