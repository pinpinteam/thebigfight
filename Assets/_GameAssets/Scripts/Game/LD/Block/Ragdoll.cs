﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
	public class Ragdoll : MonoBehaviour
	{
		[SerializeField] private Collider[] m_colliders;
		[SerializeField] private Rigidbody m_mainRigidbody;
		[SerializeField] private Collider m_collider;
		[SerializeField] private Renderer m_renderer = null;

		private void Start ()
		{
			Ragdollize();
			transform.Rotate(transform.eulerAngles.x, transform.eulerAngles.y, Random.Range(0f, 360f));
		}

		private void Ragdollize ()
		{
			m_collider.attachedRigidbody.isKinematic = true;
			m_collider.enabled = false;
			float massMultiply = m_mainRigidbody.mass;

			foreach (Collider subCol in m_colliders)
			{
				subCol.enabled = true;
				subCol.isTrigger = false;
				subCol.attachedRigidbody.mass *= massMultiply;
				subCol.attachedRigidbody.useGravity = true;
				subCol.attachedRigidbody.isKinematic = false;
				//subCol.attachedRigidbody.AddForce(transform.forward * ApplicationManager.config.game.shootSpeed * CurrentPowerMultiplier * massMultiply, ForceMode.Impulse);
			}
		}
	}
}
