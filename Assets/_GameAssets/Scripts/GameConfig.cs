﻿using Pinpin.Helpers;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{

	[CreateAssetMenu(fileName = "GameConfig", menuName = "Game/GameConfig", order = 1)]
	public class GameConfig : ScriptableObject
	{

		[Serializable]
		public class ApplicationConfig
		{
			public string version = "1.0";
			public int targetFrameRate = 60;
			public int splashScreenDuration;
			public bool enableRemoteSettings = false;
			public bool enablePurchasing = false;
			public bool enableAds = false;
			public bool enableOfflineEarning;
			public string amplitudeAPIKey;
		}

		[Serializable]
		public sealed class AdsSettings
		{
			public enum Banner
			{
				None,
				Bottom,
				Top
			}

			public int lifetimeCollectBeforeInterstitial = 1;
			[Min(0f)] public float delayBetweenInterstitials = 0f;
			[Min(0f)] public float delayFirstInterstitial = 0f;
			public Banner banner;
			public bool useSmartBanner = false;

			public string interstitialTestAdUnit;
			public string rewardedVideoTestAdUnit;

			public bool isFirstInterstitial { get; internal set; }
		}

		[Serializable]
		public sealed class GameSettings
		{
			[Header("Level")]
			public int onBoardingLevelToSkipCount = 4;
			public float timeBeforeToBlinkWarning = 10f;
			public float timeBeforeGameOverScreen = .6f;

			[Header("GAME")]
			public float playerSpeed = 23f;
			public float camTargetSmoothDamp = 9f;
			public float moveRotMultiplier = 3.5f;
			public float sensibility = 1f;
			public float minDistToCollisionBetweenCharacter = 0.2f;
			public float freeCharacterSpeed = 10f;
			public int retryCharacterCount = 3;
			public float retryInvincibleTime = 3f;
			public float timerDamageOnTime = 1f;
			
			[Header("Player CROWD - OLD")]
			public float playerCrowdCharacterPerLine = 3f;
			public float playerCrowdZOffsetPerLine = -0.3f;
			public float playerCrowdXOffsetMultiplier = 0.3f;

			[Header("Enemy CROWD")]
			public float colorCrowdSpawndistanceStep = 1.5f;
			public float collectCharacterFrq = 0.1f;
			public float crowdRandomOffSet = 0.12f;
			public int crowdMaxSpawnSize = 30;
			public int crowMaxCharacterToSpawnPerFrame = 1;
			public float speedMoveTowardPlayer = 5f;

			[Header("Finisher Jump")]
			public float durationJumpFinisher = 1.5f;
			public AnimationCurve animCurveJump;

			[Header("Finisher Shoot")]
			public float shootMoveSensitivity = 3f;
			public float cooldownShootFinisher = 0.1f;
			public float shootSpeedTravelFinisher = 60f;
			public float endLevelWaitTime = 2f;

			[Header("Economy")]
			public int gemValue = 10;
			public int ammoValue = 10;
			public ulong[] finisherRewardStepArray;

			[Header("Upgrades")]
			public float gemUpgradeIncrease = 0.1f;
			public int gemUpgradeMaxLevel = 30;
			public float bulletUpgradeIncrease = 0.1f;
			public int bulletUpgradeMaxLevel = 30;
			public ulong gemPriceUpgradeIncrease = 100;
			public ulong gemPriceUpgradeBase = 100;
			public ulong bulletPriceUpgradeIncrease = 100;
			public ulong bulletPriceUpgradeBase = 100;

			[Header("UI")]
			public float loseItBtnDelay = 3.5f;
			public float fadeTransitionDuration = 0.5f;
			public float timeWithoutInputToTutorial = 3f;
			public float distanceSlideToStart = 2f;

			[Header("Vibration")]
			public float hapticUpdate = 0.1f;

			//GETTER
			public float GetGemUpgradeValue ( int levelIndex )
			{
				return 1 + (levelIndex * gemUpgradeIncrease);
			}
			public float GetBulletUpgradeValue ( int levelIndex )
			{
				return 1 + (levelIndex * bulletUpgradeIncrease);
			}
			public ulong GetGemUpgradePrice ( int levelIndex )
			{
				return gemPriceUpgradeBase + ((ulong)levelIndex * gemPriceUpgradeIncrease);
			}
			public ulong GetBulletUpgradePrice ( int levelIndex )
			{
				return gemPriceUpgradeBase + ((ulong)levelIndex * gemPriceUpgradeIncrease);
			}

			//[Header("AB Tests")]
		}

		public ApplicationConfig application;
		public AdsSettings ads;
		public GameSettings game;

		public bool isInitialized { get; private set; }
		public Action onInitialized;

		public void Inititialize ()
		{
			isInitialized = false;
			Dictionary<string, object> remoteConfigs = new Dictionary<string, object>();
			//remoteConfigs.Add("GameplaySpeed", 0);               // 0 = 23 - 1 = 20
			FunGames.Sdk.RemoteConfig.FunGamesRemoteConfig.SetDefaultValues(remoteConfigs);
			FunGames.Sdk.RemoteConfig.FunGamesRemoteConfig.FetchValues(OnRemoteSettingsUpdate);

			if (this.application.enableRemoteSettings)
				RemoteSettings.Updated += new RemoteSettings.UpdatedEventHandler(this.OnRemoteSettingsUpdate);
		}

		private void OnDisable ()
		{
			RemoteSettings.Updated -= new RemoteSettings.UpdatedEventHandler(this.OnRemoteSettingsUpdate);
		}

		private void OnRemoteSettingsUpdate ()
		{
			if (isInitialized)
				return;

			isInitialized = true;
#if !CAPTURE
			/*if (int.TryParse(FunGames.Sdk.RemoteConfig.FunGamesRemoteConfig.GetValueByKey("GameplaySpeed").ToString(), out game.cohortGameplaySpeed) == false)
			{
				game.cohortGameplaySpeed = 0;
			}*/
#endif

			Debug.Log("GameConfig - OnRemoteSettingsUpdate()");
		}
	}
}