﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pinpin
{
	public class HideUI : MonoBehaviour
	{
		[SerializeField] private Button m_button;
		[SerializeField] private GameObject m_ui;
		private bool m_isHidden = true;

		// Use this for initialization
		void Start()
		{
			m_button.onClick.AddListener(HideUIAction);
		}

		private void HideUIAction()
		{
			m_isHidden = !m_isHidden;
			Debug.Log(m_isHidden);
			m_ui.SetActive(m_isHidden);
		}
	}
}
