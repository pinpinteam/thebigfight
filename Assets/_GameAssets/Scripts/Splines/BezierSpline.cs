﻿using UnityEngine;
using System;
using System.Collections.Generic;


public class BezierSpline : MonoBehaviour
{

	[SerializeField]
	private List<Vector3> points = new List<Vector3>();

	[SerializeField]
	private List<BezierControlPointMode> modes;
	[SerializeField]
	private List<float> angles;
	[SerializeField]
	private List<float> segmentLengths;
	[SerializeField]
	private List<float> segmentEndDistance = new List<float>();
	[SerializeField]
	private List<AnimationCurve> widthCurves = new List<AnimationCurve>();

	public Action<bool/*, float, float*/> onChange;
	[SerializeField] AnimationCurve easeInOutCurve;
	public float baseWidth = 4.5f;

	public int ControlPointCount
	{
		get
		{
			return points.Count;
		}
	}

	public float TotalLength
	{
		get
		{
			return segmentEndDistance[segmentEndDistance.Count - 1];
		}
	}

	public Vector3 GetControlPoint ( int index )
	{
		return points[index];
	}

	public void SetControlPoint ( int index, Vector3 point )
	{
		if (index % 3 == 0)
		{
			Vector3 delta = point - points[index];
			if (index > 0)
			{
				points[index - 1] += delta;
			}
			if (index + 1 < points.Count)
			{
				points[index + 1] += delta;
			}
		}
		points[index] = point;
		EnforceMode(index);
		ComputeLenghts();
		if (onChange != null)
			onChange.Invoke(true/*, 0f, 0f*/);
	}

	public BezierControlPointMode GetControlPointMode ( int index )
	{
		return modes[(index + 1) / 3];
	}

	public float GetControlPointAngle ( int index )
	{
		return angles[(index + 1) / 3];
	}

	public void SetControlPointMode ( int index, BezierControlPointMode mode )
	{
		int modeIndex = (index + 1) / 3;
		modes[modeIndex] = mode;
		EnforceMode(index);
		ComputeLenghts();
		if (onChange != null)
			onChange.Invoke(true/*, 0f, 0f*/);
	}

	public void SetControlPointAngle ( int index, float angle )
	{
		angles[(index + 1) / 3] = angle;
		if (onChange != null)
			onChange.Invoke(true/*, 0f, 0f*/);
	}

	private void EnforceMode ( int index )
	{
		int modeIndex = (index + 1) / 3;
		BezierControlPointMode mode = modes[modeIndex];
		if (mode == BezierControlPointMode.Free)
		{
			return;
		}

		int middleIndex = modeIndex * 3;
		int fixedIndex, enforcedIndex;
		if (index <= middleIndex)
		{
			fixedIndex = middleIndex - 1;
			if (fixedIndex < 0)
			{
				fixedIndex = points.Count - 2;
			}
			enforcedIndex = middleIndex + 1;
			if (enforcedIndex >= points.Count)
			{
				return;
			}
		}
		else
		{
			fixedIndex = middleIndex + 1;
			if (fixedIndex >= points.Count)
			{
				fixedIndex = 1;
			}
			enforcedIndex = middleIndex - 1;
			if (enforcedIndex < 0)
			{
				return;
			}
		}

		Vector3 middle = points[middleIndex];
		Vector3 enforcedTangent = middle - points[fixedIndex];
		if (mode == BezierControlPointMode.Aligned)
		{
			enforcedTangent = enforcedTangent.normalized * Vector3.Distance(middle, points[enforcedIndex]);
		}
		points[enforcedIndex] = middle + enforcedTangent;
	}

	public int CurveCount
	{
		get
		{
			return (points.Count - 1) / 3;
		}
	}

	public float FirstCurveEndDist
	{
		get
		{
			if (segmentEndDistance.Count > 0)
				return segmentEndDistance[0];
			else
				return 0f;
		}
	}

	public float FirstCurveStartDist
	{
		get
		{
			if (segmentEndDistance.Count > 0)
				return segmentEndDistance[0] - segmentLengths[0];
			else
				return 0f;
		}
	}
	

	struct SplinePosition
	{
		public int p; // start point index
		public int i;
		public float t;
	}
	
	#region Time


	private SplinePosition GetSplineFromTime ( float t )
	{
		SplinePosition position = new SplinePosition();

		int i;
		int p;
		if (t >= 1f)
		{
			t = 1f;
			p = points.Count - 4;
			i = CurveCount - 1;
		}
		else
		{
			t = Mathf.Clamp01(t) * CurveCount;
			p = (int)t;
			i = (int)t;
			t -= p;
			p *= 3;
		}
		position.i = i;
		position.t = t;
		position.p = p;
		return position;
	}

	public Vector3 GetPoint ( float t )
	{
		SplinePosition splinePosition = GetSplineFromTime(t);
		return Bezier.GetPoint(points[splinePosition.p], points[splinePosition.p + 1], points[splinePosition.p + 2], points[splinePosition.p + 3], splinePosition.t);
	}

	public Vector3 GetPointInWorld ( float t )
	{
		return transform.TransformPoint(GetPoint(t));
	}

	public Vector3 GetVelocity ( float t )
	{
		SplinePosition splinePosition = GetSplineFromTime(t);
		return transform.TransformPoint(Bezier.GetFirstDerivative(points[splinePosition.p], points[splinePosition.p + 1], points[splinePosition.p + 2], points[splinePosition.p + 3], splinePosition.t)) - transform.position;
	}

	public Vector3 GetDirection ( float t )
	{
		return GetVelocity(t).normalized;
	}

	public float GetAngle ( float t )
	{
		SplinePosition splinePosition = GetSplineFromTime(t);
		return Mathf.Lerp(angles[splinePosition.i], angles[splinePosition.i + 1], splinePosition.t);
	}

	public Vector3 GetNormal ( float t )
	{
		return Vector3.Cross(GetVelocity(t), Vector3.up).normalized;
	}
	#endregion

	#region Dist

	private SplinePosition GetSplineFromDist ( float dist )
	{
		SplinePosition position = new SplinePosition();

		if (dist > segmentEndDistance[segmentEndDistance.Count - 1])
		{
			dist = segmentEndDistance[segmentEndDistance.Count - 1];
		}
		int splineId = 0;
		while (dist > segmentEndDistance[splineId])
		{
			splineId++;
		}
		position.i = splineId;
		position.t = (dist - (splineId == 0 ? FirstCurveStartDist : segmentEndDistance[splineId - 1])) / segmentLengths[splineId];
		position.p = splineId * 3;
		return position;
	}

	public Vector3 GetPointFromDist ( float dist )
	{
		SplinePosition splinePosition = GetSplineFromDist(dist);
		return Bezier.GetPoint(points[splinePosition.p], points[splinePosition.p + 1], points[splinePosition.p + 2], points[splinePosition.p + 3], splinePosition.t);
	}

	public Vector3 GetPointInWorldFromdist ( float dist )
	{
		return transform.TransformPoint(GetPointFromDist(dist));
	}

	public Vector3 GetVelocityFromdist ( float dist )
	{
		SplinePosition splinePosition = GetSplineFromDist(dist);
		return transform.TransformPoint(Bezier.GetFirstDerivative(points[splinePosition.p], points[splinePosition.p + 1], points[splinePosition.p + 2], points[splinePosition.p + 3], splinePosition.t)) - transform.position;
	}

	public Vector3 GetDirectionFromDist ( float dist )
	{
		return GetVelocityFromdist(dist).normalized;
	}

	public float GetAngleFromdist ( float dist )
	{
		SplinePosition splinePosition = GetSplineFromDist(dist);
		return Mathf.Lerp(angles[splinePosition.i], angles[splinePosition.i + 1], easeInOutCurve.Evaluate(splinePosition.t));
	}

	public Vector3 GetNormalFromdist ( float dist )
	{
		return Vector3.Cross(GetVelocityFromdist(dist), Vector3.up).normalized;
	}

	public float GetWidthMultiplierFromdist ( float dist )
	{
		SplinePosition splinePosition = GetSplineFromDist(dist);
		if (splinePosition.i < widthCurves.Count && widthCurves[splinePosition.i] != null)
		{
			return widthCurves[splinePosition.i].Evaluate(splinePosition.t);
		}
		return 1f;
	}

	#endregion

	public void AddCurve ( int index )
	{
		int curveIndex = CurveCount;
		if (index >= 0)
			curveIndex = (index + 1) / 3;

		Vector3 point = points[curveIndex * 3];
		point.x += 1f;
		points.Insert(curveIndex * 3 + 1, point);
		point.x += 1f;
		points.Insert(curveIndex * 3 + 2, point);
		point.x += 1f;
		points.Insert(curveIndex * 3 + 3, point);

		modes.Insert(curveIndex, BezierControlPointMode.Aligned);
		EnforceMode(points.Count - 4);

		angles.Insert(curveIndex, 0f);

		segmentLengths.Insert(curveIndex, 1f);
		segmentEndDistance.Insert(curveIndex, 1f);
		widthCurves.Insert(curveIndex, null);
		this.ComputeLenghts();
		if (onChange != null)
			onChange.Invoke(true/*, 0f, 0f*/);
	}

	public void RemoveCurve ( int index )
	{
		int curveIndex = CurveCount;
		if (index >= 0)
			curveIndex = (index + 1) / 3;
		int indexDelta = -1;
		if (curveIndex == 0)
			indexDelta = 0;
		else if(curveIndex == CurveCount)
			indexDelta = -2;
		points.RemoveAt(curveIndex * 3 + indexDelta);
		points.RemoveAt(curveIndex * 3 + indexDelta);
		points.RemoveAt(curveIndex * 3 + indexDelta);

		modes.RemoveAt(curveIndex);
		angles.RemoveAt(curveIndex);
		segmentLengths.RemoveAt(curveIndex);
		segmentEndDistance.RemoveAt(curveIndex);
		widthCurves.RemoveAt(curveIndex);
		this.ComputeLenghts();

		if (onChange != null)
			onChange.Invoke(true/*, 0f, 0f*/);
	}

	public void AddCurveAtEnd (Vector3 point1, Vector3 point2, Vector3 point3, float angleDiff, AnimationCurve widthCurve = null)
	{
		points.Add(point1);
		points.Add(point2);
		points.Add(point3);

		modes.Add(BezierControlPointMode.Aligned);
		EnforceMode(points.Count - 4);
		
		angles.Add(angles[angles.Count - 1] + angleDiff);
		float distance = Bezier.GetApproximateLenght(points[points.Count - 4], points[points.Count - 3], points[points.Count - 2], points[points.Count - 1]);

		segmentLengths.Add(distance);
		float start = segmentEndDistance[segmentEndDistance.Count - 1];
		float end = start + distance;
		segmentEndDistance.Add(end);
		widthCurves.Add(widthCurve);

		if (onChange != null)
			onChange.Invoke(false/*, start, end*/);
	}

	public void RemoveFirstCurve ()
	{
		float start = segmentEndDistance[0] - segmentLengths[0];
		float end = segmentEndDistance[0];
		points.RemoveAt(0);
		points.RemoveAt(0);
		points.RemoveAt(0);

		modes.RemoveAt(0);
		angles.RemoveAt(0);
		segmentLengths.RemoveAt(0);
		segmentEndDistance.RemoveAt(0);
		widthCurves.RemoveAt(0);

		if (onChange != null)
			onChange.Invoke(false/*, start, end*/);
	}

	public void Reset ()
	{
		Reset(5f);
	}

	public void Reset ( float length )
	{
		float totalLenght = length;
		points = new List<Vector3> {
			new Vector3(0f, 0f, 0f),
			new Vector3(0f, 0f, 0f + totalLenght / 3),
			new Vector3(0f, 0f, 0f + 2f * totalLenght / 3),
			new Vector3(0f, 0f, length)
		};
		modes = new List<BezierControlPointMode> {
			BezierControlPointMode.Free,
			BezierControlPointMode.Aligned
		};
		angles = new List<float> {
			0f,
			0f
		};
		segmentLengths = new List<float> {
			0f
		};

		segmentEndDistance = new List<float> {
			0f
		};
		widthCurves = new List<AnimationCurve>
		{
			null
		};
		this.ComputeLenghts();
	}

	public void ComputeLenghts ()
	{
		int curveCount = CurveCount;
		float total = 0f;
		for (int i = 0; i < curveCount; i++)
		{
			int p = i * 3;
			float distance = Bezier.GetApproximateLenght(points[p], points[p + 1], points[p + 2], points[p + 3]);
			segmentLengths[i] = distance;
			total += distance;
			segmentEndDistance[i] = total;
		}
	}

	public void SetWidthCurve ( int index, AnimationCurve widthCurve )
	{
		widthCurves[index] = widthCurve;
	}
}