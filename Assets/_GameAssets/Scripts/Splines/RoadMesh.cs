﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter)), ExecuteInEditMode]
public class RoadMesh : MonoBehaviour
{
	[SerializeField, Range(0.01f, 1f)] private float precision = 1f;
	[SerializeField] private BezierSpline m_spline;
	[SerializeField] private MeshFilter m_meshFilter;
	/*public float startDist = 0f;
	public float drawDist = 10f;*/
	[SerializeField] private Vector2[] m_shape;
	[SerializeField] private bool m_inverted;
	private bool isDirty = false;
	private float[] m_UVXs;

	private void Awake ()
	{
		m_UVXs = new float[m_shape.Length];
		m_UVXs[0] = 0f;
		for (int i = 1; i < m_shape.Length; i++)
		{
			m_UVXs[i] = m_UVXs[i - 1] + Vector2.Distance(m_shape[i - 1], m_shape[i]);
		}
		m_spline = GetComponent<BezierSpline>();
		m_meshFilter = GetComponent<MeshFilter>();
		m_spline.onChange -= GenerateMesh;
		m_spline.onChange += GenerateMesh;
	}

	// Use this for initialization
	void Start ()
	{
	}

	private void Reset ()
	{
	}

	private void OnDestroy ()
	{
		m_spline.onChange -= GenerateMesh;
	}

	private void OnValidate ()
	{
		m_UVXs = new float[m_shape.Length];
		m_UVXs[0] = 0f;
		for (int i = 1; i < m_shape.Length; i++)
		{
			m_UVXs[i] = m_UVXs[i - 1] + Vector2.Distance(m_shape[i - 1], m_shape[i]);
		}
		GenerateMesh();
	}
	private void SetDirty ()
	{
		isDirty = true;
	}

	private void LateUpdate ()
	{
		if (isDirty)
		{
			this.GenerateMesh();
			isDirty = false;
		}
	}
	private void GenerateMesh ( bool regenerate )
	{
		if (regenerate)
			GenerateMesh();
		else
			SetDirty();
	}
	public void GenerateMesh (/*bool regenerate, float start, float end*/)
	{
		int definition = m_shape.Length;
		/*if (regenerate)
		{*/
		/*float totalLenght = Mathf.Min(Mathf.Max(drawDist, 0f), m_spline.TotalLength - startDist) + 1;
		int pointCount = (int)(totalLenght / precision);*/
		float startDist = m_spline.FirstCurveStartDist;
		int pointCount = (int)((m_spline.TotalLength - startDist) / precision);
		int vertexCount = (pointCount + 1) * definition;
		int triangleCount = pointCount * 6 * definition;
		Vector3[] vertices = new Vector3[vertexCount];
		Vector2[] uvs = new Vector2[vertexCount];
		Vector3[] normals = new Vector3[vertexCount];
		int[] triangles = new int[triangleCount];

		for (int i = 0; i <= pointCount; i++)
		{
			float dist = startDist + i * precision;
			Vector3 position = m_spline.GetPointFromDist(dist);
			Vector3 direction = m_spline.GetDirectionFromDist(dist);
			float widthMultiplier = m_spline.GetWidthMultiplierFromdist(dist);
			float angle = m_spline.GetAngleFromdist(dist);
			Vector3 right = m_spline.GetNormalFromdist(dist);
			right = Quaternion.AngleAxis(angle, direction) * right;
			Vector3 up = Vector3.Cross(right, direction);
			for (int j = 0; j < definition; j++)
			{
				vertices[i * definition + j] = position + up * m_shape[j].y + right * m_shape[j].x * widthMultiplier;
				normals[i * definition + j] = up;
				uvs[i * definition + j] = new Vector2(m_UVXs[j], dist);
				if (i < pointCount)
				{
					int baseIndex = (i * definition + j) * 6;
					if (j < definition - 1)
					{
						triangles[baseIndex] = i * definition + j;
						triangles[baseIndex + (m_inverted ? 2 : 1)] = i * definition + j + 1;
						triangles[baseIndex + (m_inverted ? 1 : 2)] = (i + 1) * definition + j;

						triangles[baseIndex + 3] = i * definition + j + 1;
						triangles[baseIndex + (m_inverted ? 5 : 4)] = (i + 1) * definition + j + 1;
						triangles[baseIndex + (m_inverted ? 4 : 5)] = (i + 1) * definition + j;
					}
					/*else if (definition > 2)
					{
						triangles[baseIndex] = i * definition + j;
						triangles[baseIndex + (m_inverted ? 2 : 1)] = i * definition;
						triangles[baseIndex + (m_inverted ? 1 : 2)] = (i + 1) * definition + j;

						triangles[baseIndex + 3] = i * definition;
						triangles[baseIndex + (m_inverted ? 5 : 4)] = (i + 1) * definition;
						triangles[baseIndex + (m_inverted ? 4 : 5)] = (i + 1) * definition + j;
					}*/
				}
			}

		}
		Mesh mesh = new Mesh();
		mesh.name = gameObject.name;
		mesh.vertices = vertices;
		mesh.normals = normals;
		mesh.triangles = triangles;
		mesh.uv = uvs;
		//mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		if (m_meshFilter.sharedMesh != null)
			DestroyImmediate(m_meshFilter.sharedMesh);
		m_meshFilter.sharedMesh = mesh;
		/*}
		else
		{
		//TODO remove or add points instead of regenerate all
		}*/
	}

}
