﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
public class TubeMesh : MonoBehaviour
{
	[SerializeField, Range(0.01f, 1f)] private float precision = 1f;
	[SerializeField, Range(2,100)] private int m_definition = 8;
	[SerializeField] private float m_radius = 1f;
	[SerializeField] private BezierSpline m_spline;
	[SerializeField] private MeshFilter m_meshFilter;
	[SerializeField] private bool m_inverted;

	// Use this for initialization
	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{

	}

	private void Reset ()
	{
		m_spline = GetComponent<BezierSpline>();
		m_meshFilter = GetComponent<MeshFilter>();
	}

	private void OnValidate ()
	{
		GenerateMesh();
	}

	public void GenerateMesh()
	{
		float totalLenght = m_spline.TotalLength + 1;
		int pointCount = (int)(totalLenght / precision);
		int vertexCount = (pointCount + 1) * m_definition;
		int triangleCount = pointCount * 6 * m_definition;
		Vector3[] vertices = new Vector3[vertexCount];
		Vector2[] uvs = new Vector2[vertexCount];
		//Vector3[] normals = new Vector3[vertexCount];
		Color[] colors = new Color[vertexCount];
		int[] triangles = new int[triangleCount];
		Vector3 lastNormal = Vector3.up;

		for (int i = 0; i <= pointCount; i++)
		{
			float dist = i * precision;
			Vector3 position = m_spline.GetPointFromDist(dist);
			Vector3 direction = m_spline.GetDirectionFromDist(dist);
			float angle = m_spline.GetAngleFromdist(dist);
			Vector3 normal = -Vector3.Cross(Vector3.Cross(lastNormal, direction), direction);
			lastNormal = normal;
			for (int j = 0; j < m_definition; j++)
			{
				vertices[i * m_definition + j] = position + (Quaternion.AngleAxis(angle - 90f + 360f / m_definition * j, direction) * normal) * m_radius;
				uvs[i * m_definition + j] = new Vector2(j / m_definition, dist);
				colors[i * m_definition + j] = Color.red;
				if (i < pointCount)
				{
					int baseIndex = (i * m_definition + j) * 6;
					if (j < m_definition - 1)
					{
						triangles[baseIndex] = i * m_definition + j;
						triangles[baseIndex + (m_inverted ? 2 : 1)] = i * m_definition + j + 1;
						triangles[baseIndex + (m_inverted ? 1 : 2)] = (i + 1) * m_definition + j;

						triangles[baseIndex + 3] = i * m_definition + j + 1;
						triangles[baseIndex + (m_inverted ? 5 : 4)] = (i + 1) * m_definition + j + 1;
						triangles[baseIndex + (m_inverted ? 4 : 5)] = (i + 1) * m_definition + j;
					}
					else if (m_definition > 2)
					{
						triangles[baseIndex] = i * m_definition + j;
						triangles[baseIndex + (m_inverted ? 2 : 1)] = i * m_definition;
						triangles[baseIndex + (m_inverted ? 1 : 2)] = (i + 1) * m_definition + j;

						triangles[baseIndex + 3] = i * m_definition;
						triangles[baseIndex + (m_inverted ? 5 : 4)] = (i + 1) * m_definition;
						triangles[baseIndex + (m_inverted ? 4 : 5)] = (i + 1) * m_definition + j;
					}
				}
			}

		}
		Mesh mesh = new Mesh();
		mesh.name = gameObject.name;
		mesh.vertices = vertices;
		//m_meshFilter.mesh.normals = normals;
		mesh.colors = colors;
		mesh.triangles = triangles;
		mesh.uv = uvs;
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		if (m_meshFilter.sharedMesh != null)
			DestroyImmediate(m_meshFilter.sharedMesh);
		m_meshFilter.sharedMesh = mesh;

	}

}
