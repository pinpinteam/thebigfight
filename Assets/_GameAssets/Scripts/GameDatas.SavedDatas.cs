﻿using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using System.IO;
using System;
using System.Collections.Generic;
#if UNITY_WEBGL && !UNITY_EDITOR
	using System.Runtime.InteropServices;
#endif

namespace Pinpin
{

	public partial class GameDatas : ScriptableObject
	{

		[System.Serializable]
		public class SavedDatas
		{
			public const string FILENAME = "product.sav";
			public static string path { get { return (Application.persistentDataPath + "/" + FILENAME); } }

			public uint lastApplicationLaunchTime = 0;
			public uint consecutiveDaysLaunches = 0;
			public uint lastApplicationQuitTime = 0;
			public uint firstApplicationLaunchTime = 0;
			public uint sessionCount = 0;
			public bool noAds = false;
			public bool subscribed = false;
			public uint lifetimeCollect = 0;
			public int rateboxShown = 0;
			//player datas
			public ulong softCurrency;
			public ulong hardCurrency;
			//game progression
			public int levelProgression = 0;
			public int postGameLevelDoneCount = 0;
			public int totalLevelDoneCount = 0;
			//public int levelGiftProgression = 0;
			public int gemUpgradeLevel = 0;
			public int bulletUpgradeLevel = 0;
			//Finisher
			public int currentStructureIndex = 0;
			public int currentStructureDestroyedBlockCount = 0;

			public bool haveCompleteTutorial = false;




#if UNITY_WEBGL && !UNITY_EDITOR
			[DllImport("__Internal")]
			private static extern void SyncFiles();
#endif
			public static SavedDatas LoadDatas ()
			{
				SavedDatas datas = new SavedDatas();

				if (File.Exists(SavedDatas.path))
				{
					BinaryFormatter bf = new BinaryFormatter();
					FileStream file = File.Open(SavedDatas.path, FileMode.Open);
					try
					{
						string jsonDatas = (string)bf.Deserialize(file);
						file.Close();
						JsonUtility.FromJsonOverwrite(jsonDatas, datas);
						SaveDatas(datas);
					}
					catch
					{
						file.Close();
						File.Delete(SavedDatas.path);
					}
				}

				return (datas);
			}


			public void SaveDatas ()
			{

			}

			public static void SaveDatas ( SavedDatas data )
			{
				BinaryFormatter bf = new BinaryFormatter();
				FileStream file = File.Create(SavedDatas.path);
				bf.Serialize(file, JsonUtility.ToJson(data));
				file.Close();
#if UNITY_WEBGL && !UNITY_EDITOR
					SyncFiles();
#endif
			}

			public static void LoadCloudDatas ( Action<SavedDatas> callback )
			{
#if DEBUG
				Debug.Log("GameDatas.CloudDatas - LoadCloudDatas()");
#endif

#if UNITY_EDITOR || !MOBILE || !PINPIN_SOCIAL
#if DEBUG
				Debug.Log("GameDatas.CloudDatas - LoadCloudDatas() Not Available in editor.");
#endif
				callback.Invoke(null);
				return;
#else

					if (SocialManager.isInitilized)
					{
						SocialManager.LoadFromCloud(( byte[] datas, bool success ) =>
						{
							if (success)
							{
								try
								{
									SavedDatas cloudDatas;
									BinaryFormatter bf = new BinaryFormatter();
									MemoryStream memoryStream = new MemoryStream(datas);
									cloudDatas = (SavedDatas)bf.Deserialize(memoryStream);
#if DEBUG
										Debug.Log("GameDatas.CloudDatas - LoadCloudDatas() Success." + datas.Length + "Bytes loaded");
#endif
									callback.Invoke(cloudDatas);
								}
								catch
								{
#if DEBUG
										Debug.LogError("GameDatas.CloudDatas - LoadCloudDatas() Exception");
#endif
									callback.Invoke(null);
								}
							}
							else
							{
#if DEBUG
									Debug.LogError("GameDatas.CloudDatas - LoadCloudDatas() Failed.");
#endif
								callback.Invoke(null);
							}
						});
					}
					else
					{
#if DEBUG
							Debug.LogError("GameDatas.CloudDatas - LoadCloudDatas() SocialManager Not Initialized.");
#endif
						callback.Invoke(null);
					}
#endif
			}

			public static void SaveCloudDatas ( SavedDatas data )
			{
#if DEBUG
				Debug.Log("GameDatas.CloudDatas - SaveCloudDatas()");
#endif

#if UNITY_EDITOR || !MOBILE || !PINPIN_SOCIAL
#if DEBUG
				Debug.Log("GameDatas.CloudDatas - SaveCloudDatas() Not Available in editor.");
#endif
				return;
#else
					BinaryFormatter bf = new BinaryFormatter();
					MemoryStream memoryStream = new MemoryStream();
					bf.Serialize(memoryStream, data);
					SocialManager.SaveGameDatas(memoryStream.GetBuffer());
#endif
			}

		}
	}
}