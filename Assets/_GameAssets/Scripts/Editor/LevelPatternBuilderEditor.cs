﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using Pinpin;

[CustomEditor(typeof(LevelPatternBuilder))]
public class LevelPatternBuilderEditor : Editor
{
	int selectedIndex = 0;
	string newLDName;
	public override void OnInspectorGUI ()
	{
		Pinpin.LevelPatternBuilder ldBuilder = ((Pinpin.LevelPatternBuilder)target);
		List<Pinpin.LevelPattern> lds = new List<Pinpin.LevelPattern>();
		DirectoryInfo parentDir = new DirectoryInfo(Application.dataPath + "/_GameAssets/Objects/LevelPatterns");
		DirectoryInfo[] directoryInfos = parentDir.GetDirectories();
		string patternSubfolder = "";
		
		string[] folderNames = new string[directoryInfos.Length + 1];
		folderNames[0] = "None";
		for (int i = 0; i < directoryInfos.Length; i++)
		{
			folderNames[i + 1] = directoryInfos[i].Name;
		}
		ldBuilder.selectedDirectoryIndex = EditorGUILayout.Popup("LevelPattern", ldBuilder.selectedDirectoryIndex, folderNames);

		if (ldBuilder.selectedDirectoryIndex > 0)
			patternSubfolder += folderNames[ldBuilder.selectedDirectoryIndex] + "/";

		DirectoryInfo dir = new DirectoryInfo(Application.dataPath + "/_GameAssets/Objects/LevelPatterns/" + patternSubfolder);
		if (dir.Exists)
		{
			FileInfo[] fileInfos = dir.GetFiles("*.asset");

			foreach (FileInfo f in fileInfos)
			{
				Pinpin.LevelPattern ld = AssetDatabase.LoadAssetAtPath<Pinpin.LevelPattern>("Assets/_GameAssets/Objects/LevelPatterns/" + patternSubfolder + f.Name);
				if (ld != null)
					lds.Add(ld);
			}
		}
	   

		string[] ldNames = new string[lds.Count];
		for (int i = 0; i < ldNames.Length; i++)
		{
			ldNames[i] = lds[i].name;
		}
		selectedIndex = EditorGUILayout.Popup("LevelPattern", selectedIndex, ldNames);


		if (GUILayout.Button("Load LD"))
		{
			ldBuilder.LoadLD(lds[selectedIndex]);
		}

		if (GUILayout.Button("Clear LD"))
		{
			ldBuilder.ClearLD();
		}

		if (GUILayout.Button("Delete LD"))
		{
			if (EditorUtility.DisplayDialog("Delete LD ?", "Are you sure you want to delete " + ldNames[selectedIndex] + " LD", "yes", "no"))
			{
				lds.RemoveAt(selectedIndex);
				AssetDatabase.DeleteAsset("Assets/_GameAssets/Objects/LevelPatterns/" + patternSubfolder + ldNames[selectedIndex] + ".asset");
				if (selectedIndex >= lds.Count)
					selectedIndex = lds.Count - 1;
			}
		}

		newLDName = EditorGUILayout.TextField("NewLDName", newLDName);
		if (GUILayout.Button("Create LD"))
		{
			Pinpin.LevelPattern existingLd = lds.Find(x => x.name == newLDName);
			if (existingLd == null)
			{
				Pinpin.LevelPattern newLD = CreateInstance<Pinpin.LevelPattern>();
				newLD.name = newLDName;
				newLD.length = 20f;
				AssetDatabase.CreateAsset(newLD, "Assets/_GameAssets/Objects/LevelPatterns/" + patternSubfolder + newLDName + ".asset");
				AssetDatabase.SaveAssets();
				
				lds.Add(newLD);
				selectedIndex = lds.Count - 1;
			}
			else
				selectedIndex = lds.IndexOf(existingLd);

			ldBuilder.LoadLD(lds[selectedIndex]);
		}

		GUILayout.Space(20f);

		base.OnInspectorGUI();

		if (ldBuilder.editingLD != null)
		{

			EditorGUI.BeginChangeCheck();
			EditorGUILayout.Space();
			EditorGUILayout.LabelField("Pattern Infos");
			ldBuilder.editingLD.length = EditorGUILayout.FloatField("Length : ", ldBuilder.editingLD.length);
			ldBuilder.editingLD.widthCurve = EditorGUILayout.CurveField("Width Curve", ldBuilder.editingLD.widthCurve);
			if (EditorGUI.EndChangeCheck())
			{
				ldBuilder.RecomputeLength();
			}
			
			if (GUILayout.Button("Create Wall"))
			{
				ldBuilder.CreateWall();
			}

			if (GUILayout.Button("Create Slope"))
			{
				ldBuilder.CreateSlope();
			}

			if (GUILayout.Button("Create Crowd Area"))
			{
				ldBuilder.CreateCrowdArea();
			}

			if (GUILayout.Button("Create Saw"))
			{
				ldBuilder.CreateTrap(GamePlayElement.GamePlayElementType.Saw);
			}
			
			if (GUILayout.Button("Create Pendulum"))
			{
				ldBuilder.CreateTrap(GamePlayElement.GamePlayElementType.Pendulum);
			}
			
			if (GUILayout.Button("Create Rotating Cylinder"))
			{
				ldBuilder.CreateTrap(GamePlayElement.GamePlayElementType.RotatingCylinder);
			}
			
			if (GUILayout.Button("Create Spiky Cylinder"))
			{
				ldBuilder.CreateTrap(GamePlayElement.GamePlayElementType.SpikyCylinder);
			}
			
			if (GUILayout.Button("Create Swatter"))
			{
				ldBuilder.CreateTrap(GamePlayElement.GamePlayElementType.Swatter);
			}

			if (GUILayout.Button("Create Rotating Platform"))
			{
				ldBuilder.CreateRotatingPlatform();
			}

			if (GUILayout.Button("Create Big Colored Crowd Area (CAPTURE)"))
			{
				ldBuilder.CreateBigCrowdArea();
			}

			if (GUILayout.Button("Create Bullet Stack"))
			{
				ldBuilder.CreateBulletStack();
			}

			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Create Gems"))
			{
				ldBuilder.CreateCollectible();
			}

			if (GUILayout.Button("Create Bullet Collectible"))
			{
				ldBuilder.CreateCollectibleBullet();
			}

			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Create Gems Line"))
			{
				ldBuilder.CreateCollectibleLine();
			}

			if (GUILayout.Button("Create Bullet Collectible Line"))
			{
				ldBuilder.CreateCollectibleBulletLine();
			}

			GUILayout.EndHorizontal();
		}
	}
}
