﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Pinpin.UI
{

	public class LoadingScreen: MonoBehaviour
	{

		private static LoadingScreen	singleton { get; set; }

		//[SerializeField] private Image			m_loadingScreenBackground;
		[SerializeField] private GameObject     m_loader;
		[SerializeField] private Text			m_loadingText;
		[SerializeField] private Sprite			m_splashScreen;
		[SerializeField] private Image			m_viewPortFront;
		[SerializeField] private Image			m_viewPortBack;
		[SerializeField] private float			m_swapDelay;
		[SerializeField] private float			m_fadeSpeed;

		private int screensCount { get; set; }
		private int currenFront { get; set; }
		private int currenBack { get; set; }
		private YieldInstruction waitEndFrame { get; set; }
		private YieldInstruction waitDelay { get; set; }
		private float m_splashScreenApparitionTime;
		private bool m_splashScreenActivated;

		private void Awake ()
		{
			if (singleton != null)
			{
				GameObject.Destroy(this.gameObject);
				return ;
			}

			singleton = this;


			this.waitEndFrame = new WaitForEndOfFrame();
			this.waitDelay = new WaitForSeconds(m_swapDelay);
			GameObject.DontDestroyOnLoad(this.gameObject);
			Hide();
		}

		public static void ShowSplashScreen ()
		{
			#if  DEBUG
						Debug.Log("LoadingScreen - ShowSplashScreen()");
			#endif

			singleton.m_viewPortFront.sprite = singleton.m_splashScreen;
			//singleton.m_viewPortFront.color = Color.white;
			singleton.m_splashScreenApparitionTime = Time.time;
			singleton.m_viewPortFront.gameObject.SetActive(true);
			singleton.m_viewPortBack.gameObject.SetActive(true);
			singleton.gameObject.SetActive(true);
			singleton.m_splashScreenActivated = true;
		}

		public static void HideSplashScreen ()
		{
#if  DEBUG
			Debug.Log("LoadingScreen - HideSplashsSreen()");
#endif

			singleton.StartCoroutine(singleton.WaitHideSplashScreen());
		}

		private IEnumerator WaitHideSplashScreen ()
		{
			while(m_splashScreenApparitionTime + ApplicationManager.config.application.splashScreenDuration > Time.time)
			{
				yield return null;
			}

			singleton.m_viewPortFront.DOFade(0f, 0.2f).SetEase(Ease.OutQuad);
			singleton.m_viewPortBack.DOFade(0f, 0.2f).SetEase(Ease.OutQuad).OnComplete(OnFadeComplete);

			yield return false;
		}

		private void OnFadeComplete ()
		{
			singleton.m_viewPortFront.gameObject.SetActive(false);
			singleton.m_viewPortBack.gameObject.SetActive(false);
			//m_loadingScreenBackground.enabled = true;
			m_loader.SetActive(true);
			singleton.gameObject.SetActive(false);

			singleton.m_splashScreenActivated = false;

		}


		public static void Show (string text = "")
		{
			#if  DEBUG
				Debug.Log("LoadingScreen - Show()");
			#endif
			
			singleton.gameObject.SetActive(true);
			singleton.m_loadingText.text = text;
		}

		public static void Hide ()
		{
			#if DEBUG
				Debug.Log("LoadingScreen - Hide()");
			#endif

			if (!singleton.m_splashScreenActivated)
				singleton.gameObject.SetActive(false);
		}

		private void OnEnable ()
		{
			//if (screensCount > 0) // Set screens To Show diaporama loading
			//{
			//	//m_viewPortBack.color = Color.white;
			//	m_viewPortFront.color = Color.white;
			//	this.currenFront = Random.Range(0, this.screensCount);
			//	m_viewPortFront.sprite = m_screens[this.currenFront];
			//	this.StartCoroutine(this.WaitForNextSwap());
			//}
		}

		private void OnDisable ()
		{
			this.StopAllCoroutines();
		}

		private void ShowNextScreen ()
		{
			//this.currenBack = this.currenFront;
			//while (this.currenBack == currenFront)
			//	this.currenBack = Random.Range(0, this.screensCount);
			//m_viewPortBack.sprite = m_splashScreen[this.currenBack];
			//this.StartCoroutine(this.Swap(m_viewPortFront, m_viewPortBack));
		}
		
		private IEnumerator WaitForNextSwap ()
		{
			yield return (this.waitDelay);
			this.ShowNextScreen();
		}

		private IEnumerator Swap ( Image A, Image B )
		{
			Color c = Color.white;
			while (this.isActiveAndEnabled && A.color.a > 0.01f)
			{
				c.a = Mathf.Lerp(A.color.a, 0f, Time.deltaTime * m_fadeSpeed);
				A.color = c;

				if (this.isActiveAndEnabled == false)
					break;
				yield return (this.waitEndFrame);
			}

			A.sprite = B.sprite;
			A.color = Color.white;
			this.currenFront = this.currenBack;
			this.StartCoroutine(this.WaitForNextSwap());
		}

		

	}

}