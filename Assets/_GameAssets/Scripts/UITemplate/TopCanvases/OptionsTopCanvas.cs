﻿using Pinpin.UI;
using UnityEngine;

namespace Pinpin
{
	public class OptionsTopCanvas : AUITopCanvas
	{
		[SerializeField] private GameObject m_noAdsButton;

		[SerializeField] private RectTransformPositionsLerp m_posLerp;
		public override bool Visible
		{
			get
			{
				return m_visible;
			}
			set
			{
				m_visible = value;
				m_posLerp.SetPos(m_visible ? 1 : 0,false);
			}
		}

		//TODO no ads Btn?
		//TODO option button?
	}
}
