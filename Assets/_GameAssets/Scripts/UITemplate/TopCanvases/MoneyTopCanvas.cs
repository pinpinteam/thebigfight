﻿using DG.Tweening;
using Pinpin.Helpers;
using Pinpin.UI;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Pinpin
{
	public class MoneyTopCanvas : AUITopCanvas
	{
		[SerializeField] private CurrencyCounter m_softCounter;
		//[SerializeField] private CurrencyCounter m_hardCounter;

		public override bool Visible
		{
			get
			{
				return m_visible;
			}
			set
			{
				m_visible = value;
				m_softCounter.SetCurrencyInstant(ApplicationManager.datas.softCurrency);
				m_softCounter.SetVisible(m_visible);
			}
		}

		private Sequence m_bounceSequence;

		private void Start ()
		{
			ApplicationManager.onSoftChange += UpdateSoftDisplay;
			//ApplicationManager.onHardChange += UpdateHardDisplay;
			//GameDatas.onLoadComplete += ShowCoinDisplay;
		}

		private void OnDestroy ()
		{
			ApplicationManager.onSoftChange -= UpdateSoftDisplay;
			//ApplicationManager.onHardChange -= UpdateHardDisplay;
			//GameDatas.onLoadComplete -= ShowCoinDisplay;
		}

		void UpdateSoftDisplay ( ulong oldCurrency, ulong currentCurrency )
		{
			m_softCounter.UpdateDisplay(oldCurrency, currentCurrency);
		}

		public void ShakeCoinDisplay ()
		{
			m_softCounter.ShakeFx();
		}
	}
}
