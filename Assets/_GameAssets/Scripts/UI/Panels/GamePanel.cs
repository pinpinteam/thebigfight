﻿using Pinpin.UI;
using UnityEngine;
using UnityEngine.UI;
using Pinpin.Helpers;
using DG.Tweening;
using System;
using System.Collections;
using TMPro;

namespace Pinpin.Scene.MainScene.UI
{
	public sealed class GamePanel : AUIPanel
	{
		public static Action onChestOpenAction;

		[SerializeField] private RectTransform m_topContainer;
		[SerializeField] private CurrencyCounter m_currentGainCounter;
		[SerializeField] private CurrencyCounter m_currentAmmoCounter;
		[SerializeField] private Text m_LevelCounterTxt;

		[Header("destroyGauge")]
		[SerializeField] private RectTransform m_destroyGaugeContainer;
		[SerializeField] private RectTransform m_gaugeFillrectTfm;

		[Header("RewardStep")]
		[SerializeField] private RectTransform[] m_gemStepRectTfmArray;
		[SerializeField] private ParticleSystem[] m_rewardStepPSArray;
		[SerializeField] private GameObject[] m_rewardStepGreyArray;
		[SerializeField] private Image m_gemWhiteImg;

		[Header("RewardText")]
		[SerializeField] private TextMeshProUGUI m_rewardStepTMP;
		[SerializeField] private CanvasGroup m_rewardStepCanvasGroup;
		[SerializeField] private RectTransform m_rewardStepCanvasGroupRectTfm;

		[Header("finisherCelebration")]
		[SerializeField] private GameObject m_GemTextAnimatorGO;
		[SerializeField] private TextMeshProUGUI m_bigGemRewardTMP;
		[SerializeField] private Text m_bigGemRewardTxt;

		[Header("finisherCelebration")]
		[SerializeField] private CanvasGroup m_finisherTutoCanvasGr;

		private int m_currentRewardIndex;
		private float m_topContainerYPos;
		private float m_destroyContainerYPos;
		private Vector2 m_currentSizeDelta;
		private Color m_chestAddColor;
		private Tweener m_tutoTween;
		private Tweener m_rewardStepPosTween;
		private Tweener m_rewardStepFadeTween;
		private Vector2 m_rewardStepTextEndPos;
		private bool m_isCastleDestroyed;
		private ulong m_currentFinisherAmmo;
		private ulong m_currentGemCollected;
		private ulong m_currentFinisherTotalReward;

		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}

		public override bool OnBackAction ()
		{
			return false;
		}

		protected override void Awake ()
		{
			base.Awake();
			GameManager.OnLevelEnd += OnLevelEnd;
			Level.onCollectibleCollectAction += OnCollectibleCollected;
			PlayerGunnerController.updateGameAmmo += UpdateGameAmmo;

			//Finisher.playerCrowdEnterEndLevelAction += OnStartFinisher;
			//Finisher.OnBlockDestroyedCountChange += OnFinisherUpdateValue;
			//Finisher.onFirstInputAction += HideTuto;
			//Finisher.onShootAction += UpdateFinisherAmmo;
			//Finisher.gemExplodeAction += GemExplodeAnimation;

			m_chestAddColor = m_gemWhiteImg.color;
			m_currentSizeDelta.y = m_gaugeFillrectTfm.sizeDelta.y;
			m_rewardStepTextEndPos = m_rewardStepCanvasGroupRectTfm.anchoredPosition;

			//PlayerCrowdController.onPlayerCrowdProgressAction += OnPlayerCrowdProgressAction;

			m_topContainerYPos = m_topContainer.anchoredPosition.y;
			m_destroyContainerYPos = m_destroyGaugeContainer.anchoredPosition.y;
			m_currentGainCounter.Init();
		}

		void OnEnable ()
		{
			m_isCastleDestroyed = false;

			m_currentRewardIndex = 0;
			m_currentFinisherTotalReward = 0;
			for (int i = 0; i < m_rewardStepGreyArray.Length; i++)
			{
				m_rewardStepGreyArray[i].SetActive(true);
				m_gemStepRectTfmArray[i].localScale = Vector3.one;
			}
			m_gemWhiteImg.rectTransform.localScale = Vector3.one;

			m_rewardStepPosTween.Pause();
			m_rewardStepPosTween.Kill();
			m_rewardStepFadeTween.Pause();
			m_rewardStepFadeTween.Kill();

			m_rewardStepCanvasGroupRectTfm.anchoredPosition = Vector2.zero;
			m_rewardStepCanvasGroup.alpha = 0f;

			m_topContainer.anchoredPosition = new Vector2(0, m_topContainerYPos + 200f);
			m_topContainer.DOAnchorPosY(m_topContainerYPos, 0.5f).SetEase(Ease.OutQuad);

			m_currentGainCounter.ResetToHiddenPos();
			m_currentGainCounter.SetCurrencyInstant(0);
			m_currentGainCounter.SetVisible(true);

			m_currentFinisherAmmo = GameManager.level.player.ammoCount;
			m_currentAmmoCounter.SetCurrencyInstant(m_currentFinisherAmmo);
			m_currentAmmoCounter.SetVisible(true);

			m_gemWhiteImg.gameObject.SetActive(false);
			m_chestAddColor.a = 0f;
			m_gemWhiteImg.color = m_chestAddColor;

			m_destroyGaugeContainer.anchoredPosition = new Vector3(0, m_destroyContainerYPos + 500f);

			m_LevelCounterTxt.text = "LEVEL " + (ApplicationManager.datas.totalLevelDoneCount + 1);
		}

		public void OnRetry ()
		{
			GameManager.Instance.GameState = GameManager.EGameState.InGame;
			OnEnable();
		}

		private void UpdateGameAmmo ( ulong oldCount, ulong currentCount )
		{
			m_currentAmmoCounter.UpdateDisplay(oldCount, currentCount);
		}

		//#region Finisher
		//void OnStartFinisher ( Transform tfm )
		//{
		//	m_currentSizeDelta.x = Finisher.destroyRatio * 100f;
		//	m_gaugeFillrectTfm.sizeDelta = m_currentSizeDelta;
		//	m_destroyGaugeContainer.DOAnchorPosY(m_destroyContainerYPos, 0.5f).SetEase(Ease.OutQuad);

		//	m_currentFinisherAmmo = GameManager.level.player.ammoCount;
		//	m_currentAmmoCounter.SetCurrencyInstant(m_currentFinisherAmmo);
		//	m_currentAmmoCounter.SetVisible(true);

		//	//Set Current index Upgrade
		//	m_currentSizeDelta.x = Finisher.destroyRatio * 100f;
		//	m_gaugeFillrectTfm.sizeDelta = m_currentSizeDelta;

		//	for (int i = 0; i < 3; i++)
		//	{
		//		if (Finisher.destroyRatio >= (i + 1) * 0.25f)
		//		{
		//			if (m_currentRewardIndex < 0 || m_currentRewardIndex >= m_rewardStepGreyArray.Length)
		//				break;
		//			m_rewardStepGreyArray[m_currentRewardIndex].SetActive(false);
		//			//m_currentFinisherTotalReward += ApplicationManager.config.game.finisherRewardStepArray[m_currentRewardIndex];
		//			m_currentRewardIndex++;
		//		}
		//	}

		//	//TUTO
		//	if (ApplicationManager.datas.totalLevelDoneCount <= 0)
		//	{
		//		m_finisherTutoCanvasGr.alpha = 0f;
		//		m_finisherTutoCanvasGr.gameObject.SetActive(true);
		//		m_tutoTween = m_finisherTutoCanvasGr.DOFade(1f, 0.3f).SetDelay(1f);
		//	}
		//}

		//private void UpdateFinisherAmmo ()
		//{
		//	//m_currentFinisherAmmo--;
		//	//m_currentAmmoCounter.UpdateDisplay(m_currentFinisherAmmo + 1, m_currentFinisherAmmo);
		//	m_currentAmmoCounter.UpdateDisplay(m_currentFinisherAmmo, GameManager.level.player.ammoCount);
		//	m_currentFinisherAmmo = GameManager.level.player.ammoCount;
		//}

		//void HideTuto ()
		//{
		//	m_tutoTween.Pause();
		//	m_tutoTween.Kill();
		//	m_tutoTween = m_finisherTutoCanvasGr.DOFade(0f, 0.3f);
		//	m_tutoTween.onComplete += OnTutoHidden;
		//}

		//void OnTutoHidden ()
		//{
		//	m_finisherTutoCanvasGr.gameObject.SetActive(false);
		//}

		//void OnFinisherUpdateValue ()
		//{
		//	if (m_isCastleDestroyed) return;

		//	m_currentSizeDelta.x = Finisher.destroyRatio * 100f;
		//	m_gaugeFillrectTfm.sizeDelta = m_currentSizeDelta;

		//	if (Finisher.destroyRatio >= (m_currentRewardIndex + 1) * 0.25f)
		//	{
		//		m_rewardStepPSArray[m_currentRewardIndex].Stop();
		//		m_rewardStepPSArray[m_currentRewardIndex].Play();
		//		m_rewardStepGreyArray[m_currentRewardIndex].SetActive(false);
		//		m_gemStepRectTfmArray[m_currentRewardIndex].DOPunchScale(Vector3.one * 0.5f, 0.8f, 1, 0.1f);

		//		if (m_currentRewardIndex <= 2)
		//		{
		//			m_currentFinisherTotalReward += ApplicationManager.config.game.finisherRewardStepArray[m_currentRewardIndex];

		//			m_rewardStepTMP.text = "+" + m_currentFinisherTotalReward.ToString();
		//			//ADD NEW GEM REWARD
		//			m_currentGemCollected += ApplicationManager.config.game.finisherRewardStepArray[m_currentRewardIndex];
		//			m_currentGainCounter.UpdateDisplay(m_currentGemCollected - ApplicationManager.config.game.finisherRewardStepArray[m_currentRewardIndex], m_currentGemCollected);

		//			m_rewardStepPosTween.Pause();
		//			m_rewardStepPosTween.Kill();
		//			m_rewardStepCanvasGroupRectTfm.anchoredPosition = Vector2.zero;
		//			m_rewardStepPosTween = m_rewardStepCanvasGroupRectTfm.DOAnchorPos(m_rewardStepTextEndPos, 0.3f).SetEase(Ease.OutQuad);

		//			m_rewardStepFadeTween.Pause();
		//			m_rewardStepFadeTween.Kill();
		//			m_rewardStepCanvasGroup.alpha = 1f;
		//			m_rewardStepFadeTween = m_rewardStepCanvasGroup.DOFade(0f, 0.5f).SetEase(Ease.Linear).SetDelay(2f);
		//		}

		//		m_currentRewardIndex++;

		//		//COMPLETE
		//		//if (Finisher.destroyRatio >= 1f)
		//		if (m_currentRewardIndex > 3)
		//		{
		//			m_isCastleDestroyed = true;

		//			m_chestAddColor.a = 1f;
		//			m_gemWhiteImg.color = m_chestAddColor;
		//			m_gemWhiteImg.gameObject.SetActive(true);

		//			m_gemWhiteImg.DOFade(0f, 1f).SetEase(Ease.Linear);
		//			m_gemWhiteImg.rectTransform.DOScale(2f, 1f).SetEase(Ease.OutQuad);
		//		}
		//	}
		//}

		//public void GemExplodeAnimation ( int gemCharacterCount )
		//{
		//	m_bigGemRewardTMP.text = $"+{gemCharacterCount + (int)ApplicationManager.config.game.finisherRewardStepArray[3]}";
		//	m_bigGemRewardTxt.text = m_bigGemRewardTMP.text;
		//	m_GemTextAnimatorGO.SetActive(true);

		//	m_currentGainCounter.UpdateDisplay(m_currentGemCollected, m_currentGemCollected + (ulong)gemCharacterCount + ApplicationManager.config.game.finisherRewardStepArray[3]);
		//	m_currentGemCollected += (ulong)gemCharacterCount + ApplicationManager.config.game.finisherRewardStepArray[3];
		//}

		//public void OnFinisherCelebrationCompletedAE ()
		//{
		//	m_GemTextAnimatorGO.SetActive(false);
		//	m_destroyGaugeContainer.DOAnchorPosY(m_destroyContainerYPos + 500, 0.5f).SetEase(Ease.OutQuad);
		//	m_topContainer.DOAnchorPosY(m_topContainerYPos + 300f, 0.5f).SetEase(Ease.OutQuad);
		//	m_currentGainCounter.SetVisible(false);
		//	m_currentAmmoCounter.SetVisible(false);
		//	UIManager.OpenPopup<WinPopup>().Init(m_currentGemCollected);
		//}

		//#endregion
		void OnLevelEnd ( bool win )
		{
			if (!m_isCastleDestroyed)
			{
				m_destroyGaugeContainer.DOAnchorPosY(m_destroyContainerYPos + 500, 0.5f).SetEase(Ease.OutQuad);
				m_currentGainCounter.SetVisible(false);
				m_currentAmmoCounter.SetVisible(false);
			}

			if (win)
			{
				if (!m_isCastleDestroyed)
				{
					m_topContainer.DOAnchorPosY(m_topContainerYPos + 300f, 0.5f).SetEase(Ease.OutQuad);
					UIManager.OpenPopup<WinPopup>().Init(m_currentGemCollected);
				}
			}
			else
			{
				m_topContainer.DOAnchorPosY(m_topContainerYPos + 300f, 0.5f).SetEase(Ease.OutQuad).SetDelay(1.5f);
				UIManager.OpenPopup<LosePopup>();
			}
		}

		/*private void OnPlayerCrowdProgressAction(float value)
		{
			m_progressionBar.fillAmount = value;
		}*/

		#region Game

		private void OnCollectibleCollected ( ulong oldValue, ulong newValue )
		{
			m_currentGainCounter.UpdateDisplay(oldValue, newValue);
			m_currentGemCollected = newValue;
		}

		string ConvertSecToTimerFormat ( float timer )
		{
			string minutes = Mathf.Floor(timer / 60).ToString("00");
			string seconds = (timer % 60).ToString("00");

			return string.Format("{0}:{1}", minutes, seconds);
		}
		#endregion
	}
}