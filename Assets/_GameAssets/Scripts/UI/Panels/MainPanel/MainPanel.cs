﻿using AdsManagement;
using Pinpin.UI;
using Pinpin.InputControllers;
using UnityEngine;
using UnityEngine.UI;
using Pinpin.Helpers;
using DG.Tweening;
using UVector3 = UnityEngine.Vector3;
using System;
using System.Collections;
using URandom = UnityEngine.Random;
using System.Collections.Generic;
using TMPro;

namespace Pinpin.Scene.MainScene.UI
{
	public sealed class MainPanel : AUIPanel
	{
		#region members
		[Header("Animation")]
		[SerializeField] Text m_startText;
		[SerializeField] private Image m_modalImg;

		[Header("Member")]
		[SerializeField] private RectTransform m_bottomContainerRectTfm;
		[SerializeField] private RectTransform m_rightBtnContainerRectTfm;
		//[SerializeField] private PushButton m_startButton;
		[SerializeField] private PushButton m_gemUpgradeBtn;
		[SerializeField] private PushButton m_gemUpgradeRVBtn;
		[SerializeField] private TextMeshProUGUI[] m_gemUpgradeTMPArray;
		[SerializeField] private PushButton m_bulletUpgradeBtn;
		[SerializeField] private PushButton m_bulletUpgradeRVBtn;
		[SerializeField] private TextMeshProUGUI[] m_bulletUpgradeTMPArray;
		[SerializeField] private AutoAnimateColor[] m_btnGradientBlinkArray;

		[SerializeField] private GameObject m_blockClickGO;

		public static Action MoveCrowdDuringSlideToStart;

		private Vector2 m_slideCount = Vector2.zero;
		private float m_distanceSlide;

		private bool CanClick
		{
			get
			{
				return !m_blockClickGO.activeInHierarchy;
			}
			set
			{
				m_blockClickGO.SetActive(!value);
			}
		}

		private Color m_modalColor;
		private Tweener m_textAlphaTween;

		private Tweener m_bottomContainerTween;
		private Tweener m_rightContainerTween;
		private Vector2 m_startBtnPos;
		private Vector2 m_rightContainerPos;

		private WaitForSeconds m_delayCanClick = new WaitForSeconds(0.2f);

		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}
		#endregion

		#region MonoBehavior
		protected override void Awake ()
		{
			base.Awake();

			//m_startButton.onClick += OnStartButtonClick;
			m_gemUpgradeBtn.onClick += OnClickUpgradeGem;
			m_gemUpgradeRVBtn.onClick += OnClickUpgradeGemRV;
			m_bulletUpgradeBtn.onClick += OnClickUpgradeBullet;
			m_bulletUpgradeRVBtn.onClick += OnClickUpgradeBulletRV;

			m_startBtnPos = m_bottomContainerRectTfm.anchoredPosition;
			m_rightContainerPos = m_rightBtnContainerRectTfm.anchoredPosition;
			m_modalColor = m_modalImg.color;
		}

		private void Update ()
		{
			sliderStartButton();
		}

		private void OnEnable ()
		{
			UpdateBtnStatus();

			GameManager.Instance.GameState = GameManager.EGameState.InMenu;

			UIManager.ShowTopCanvas<MoneyTopCanvas>();

			//show Animation
			m_textAlphaTween.Pause();
			m_textAlphaTween.Kill();
			m_startText.color = Color.white;
			m_textAlphaTween = m_startText.DOFade(0.5f, 1f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
			m_startText.rectTransform.localScale = Vector3.one;
			m_modalImg.color = m_modalColor;

			m_bottomContainerTween.Pause();
			m_bottomContainerTween.Kill();
			m_bottomContainerRectTfm.anchoredPosition = m_startBtnPos - Vector2.up * 500f;
			m_bottomContainerTween = m_bottomContainerRectTfm.DOAnchorPos(m_startBtnPos, 0.5f).SetEase(Ease.OutQuad);

			m_rightContainerTween.Pause();
			m_rightContainerTween.Kill();
			m_rightBtnContainerRectTfm.anchoredPosition = m_rightContainerPos + Vector2.right * 500f;
			m_rightContainerTween = m_rightBtnContainerRectTfm.DOAnchorPos(m_rightContainerPos, 0.7f).SetEase(Ease.OutQuad);

			StartCoroutine(DelayCanClickCR());

			m_rightBtnContainerRectTfm.gameObject.SetActive(ApplicationManager.datas.totalLevelDoneCount >= 1);
		}

		IEnumerator DelayCanClickCR ()
		{
			CanClick = false;
			yield return m_delayCanClick;
			CanClick = true;
		}

		protected override void OnDestroy ()
		{
			base.OnDestroy();
		}
		#endregion

		void UpdateBtnStatus ()
		{
			ulong price;
			string multiplierValue;
			//GEM 
			//if Maxed out
			multiplierValue = "x" + ApplicationManager.config.game.GetGemUpgradeValue(ApplicationManager.datas.gemUpgradeLevel).ToString();
			for (int i = 0; i < m_gemUpgradeTMPArray.Length; i++)
			{
				m_gemUpgradeTMPArray[i].text = multiplierValue;
			}

			if (ApplicationManager.datas.gemUpgradeLevel >= ApplicationManager.config.game.gemUpgradeMaxLevel)
			{
				m_gemUpgradeRVBtn.gameObject.SetActive(false);
				m_gemUpgradeBtn.gameObject.SetActive(true);
				m_gemUpgradeBtn.isInteractable = false;
				m_btnGradientBlinkArray[0].SetValue(0);
				m_btnGradientBlinkArray[0].enabled = false;
			}
			else
			{
				price = ApplicationManager.config.game.GetGemUpgradePrice(ApplicationManager.datas.gemUpgradeLevel);
				m_gemUpgradeBtn.text = price.ToString();


				m_gemUpgradeBtn.gameObject.SetActive(ApplicationManager.datas.softCurrency >= price);
				m_gemUpgradeRVBtn.gameObject.SetActive(ApplicationManager.datas.softCurrency < price);
				m_gemUpgradeBtn.isInteractable = true;
				m_btnGradientBlinkArray[0].enabled = true;
			}

			//BULLET
			multiplierValue = "x" + ApplicationManager.config.game.GetBulletUpgradeValue(ApplicationManager.datas.bulletUpgradeLevel).ToString();
			for (int i = 0; i < m_bulletUpgradeTMPArray.Length; i++)
			{
				m_bulletUpgradeTMPArray[i].text = multiplierValue;
			}

			if (ApplicationManager.datas.bulletUpgradeLevel >= ApplicationManager.config.game.bulletUpgradeMaxLevel)
			{
				m_bulletUpgradeRVBtn.gameObject.SetActive(false);
				m_bulletUpgradeBtn.gameObject.SetActive(true);
				m_bulletUpgradeBtn.isInteractable = false;
				m_btnGradientBlinkArray[1].SetValue(0);
				m_btnGradientBlinkArray[1].enabled = false;
			}
			else
			{
				price = ApplicationManager.config.game.GetBulletUpgradePrice(ApplicationManager.datas.bulletUpgradeLevel);
				m_bulletUpgradeBtn.text = price.ToString();

				m_bulletUpgradeBtn.gameObject.SetActive(ApplicationManager.datas.softCurrency >= price);
				m_bulletUpgradeRVBtn.gameObject.SetActive(ApplicationManager.datas.softCurrency < price);
				m_bulletUpgradeBtn.isInteractable = true;
				m_btnGradientBlinkArray[1].enabled = true;
			}
		}

		private void sliderStartButton ()
		{
			if (GameManager.Instance.GameState == GameManager.EGameState.InMenu)
			{
				if (InputManager.isTouching)
				{
					m_slideCount += InputManager.moveDeltaV2;
					m_distanceSlide = Vector2.Distance(m_slideCount, Vector2.zero);
					if (m_distanceSlide > ApplicationManager.config.game.distanceSlideToStart)
					{
						OnStartButtonClick();
						m_slideCount = Vector2.zero;
					}
					MoveCrowdDuringSlideToStart?.Invoke();
				}
				else
				{
					m_slideCount = Vector2.zero;
				}
			}
		}

		#region Buttons CallBacks
		private void OnStartButtonClick ()
		{
			if (!CanClick) return;

			CanClick = false;

			UIManager.HideTopCanvas<MoneyTopCanvas>();

			m_modalImg.DOFade(0f, 0.3f);
			m_textAlphaTween.Pause();
			m_textAlphaTween.Kill();
			m_startText.color = Color.white;
			m_startText.rectTransform.DOPunchScale(Vector3.one * 0.2f, 0.35f);
			m_textAlphaTween = m_startText.DOFade(0f, 0.4f).SetEase(Ease.InQuad);

			//Hide animation
			m_rightContainerTween.Pause();
			m_rightContainerTween.Kill();
			m_rightContainerTween = m_rightBtnContainerRectTfm.DOAnchorPos(m_rightContainerPos + Vector2.right * 500f, 0.3f);

			m_bottomContainerTween.Pause();
			m_bottomContainerTween.Kill();
			m_bottomContainerTween = m_bottomContainerRectTfm.DOAnchorPos(m_startBtnPos - Vector2.up * 500f, 0.45f);

			m_bottomContainerTween.onComplete += OnMainMenuHidden;
		}

		void OnMainMenuHidden ()
		{
			UIManager.OpenPanel<GamePanel>();//.Init();
			GameManager.Instance.StartGame();
		}

		void OnClickUpgradeGem ()
		{
			ApplicationManager.RemoveSoftCurrency(ApplicationManager.config.game.GetGemUpgradePrice(ApplicationManager.datas.gemUpgradeLevel));
			ApplicationManager.datas.gemUpgradeLevel++;
			UpdateBtnStatus();
		}

		void OnClickUpgradeBullet ()
		{
			ApplicationManager.RemoveSoftCurrency(ApplicationManager.config.game.GetBulletUpgradePrice(ApplicationManager.datas.bulletUpgradeLevel));
			ApplicationManager.datas.bulletUpgradeLevel++;
			UpdateBtnStatus();
		}

		void OnClickUpgradeGemRV ()
		{
#if !TAPNATION
			ApplicationManager.datas.gemUpgradeLevel++;
			UpdateBtnStatus();
#else
			UIManager.sceneManager.ShowRewardedVideo(OnUpgradeGemRV, "RV_GemUpgrade");
#endif
		}

		private void OnUpgradeGemRV ( AdsManagement.RewardedVideoEvent adEvent )
		{
			switch (adEvent)
			{
				case AdsManagement.RewardedVideoEvent.Rewarded:
					ApplicationManager.datas.gemUpgradeLevel++;
					UpdateBtnStatus();
					break;

				case AdsManagement.RewardedVideoEvent.Failed:
					//CanClick = true;
					break;
			}
		}

		void OnClickUpgradeBulletRV ()
		{
#if TAPNATION
			ApplicationManager.datas.bulletUpgradeLevel++;
			UpdateBtnStatus();
#else
			UIManager.sceneManager.ShowRewardedVideo(OnUpgradeBulletRV, "RV_BulletUpgrade");
#endif
		}

		private void OnUpgradeBulletRV ( AdsManagement.RewardedVideoEvent adEvent )
		{
			switch (adEvent)
			{
				case AdsManagement.RewardedVideoEvent.Rewarded:
					ApplicationManager.datas.bulletUpgradeLevel++;
					UpdateBtnStatus();
					break;

				case AdsManagement.RewardedVideoEvent.Failed:
					//CanClick = true;
					break;
			}
		}

		#endregion
	}
}