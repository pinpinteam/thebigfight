﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Pinpin.UI
{

	public class ShopButton : MonoBehaviour
	{

		public static Action<string> onClick;
		[SerializeField] private PushButton button;
		[SerializeField] private Text m_levelText;
		[SerializeField] private string id;

		private ulong m_cost;
		private int m_level;

		void Start ()
		{
			button.onClick += OnButtonClick;
			
		}

		private void OnDestroy ()
		{
			button.onClick -= OnButtonClick;
		}

		private void OnButtonClick ()
		{
			if (onClick != null)
				onClick.Invoke(id);
		}

	}

}