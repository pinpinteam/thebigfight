﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Pinpin
{
	public class FXManager : MonoBehaviour
	{
		public static FXManager Instance;

		public static Action onFadeInFinishedAction;
		public static Action onFadeOutFinishedAction;

		[SerializeField] private ParticleSystem m_moneyPS;
		[SerializeField] private ParticleSystem m_confettiPSPrefab;

		[Header("Fade")]
		[SerializeField] private Image m_fadeImg;
		[SerializeField] private Color[] m_fadeColors;

		[Header("Game")]
		[SerializeField] private ParticleSystem m_fxBombeFinisherPrefab;

		private void Awake ()
		{
			if (Instance == null)
				Instance = this;
			else
				Destroy(gameObject);

			m_fadeImg.color = m_fadeColors[0];
			m_fadeImg.gameObject.SetActive(false);
		}

		#region Fade
		public void FadeIn ()
		{
			m_fadeImg.gameObject.SetActive(true);
			m_fadeImg.DOColor(m_fadeColors[1], ApplicationManager.config.game.fadeTransitionDuration).onComplete += OnFadeInFinished;
		}

		void OnFadeInFinished ()
		{
			onFadeInFinishedAction?.Invoke();
			onFadeInFinishedAction = null;
		}

		public void FadeOut ()
		{
			m_fadeImg.DOColor(m_fadeColors[0], ApplicationManager.config.game.fadeTransitionDuration).onComplete += OnFadeOutFinished;
		}

		void OnFadeOutFinished ()
		{
			m_fadeImg.gameObject.SetActive(false);
			onFadeOutFinishedAction?.Invoke();
			onFadeOutFinishedAction = null;
		}
		#endregion

		public void PlayMoney ( bool isRewarded = false )
		{
			ParticleSystem.Burst[] bursts;

			if (isRewarded)
			{
				bursts = new ParticleSystem.Burst[2];
				bursts[1].time = 0.25f;
				bursts[1].count = 20;
				bursts[1].cycleCount = 8;
				bursts[1].repeatInterval = 0.05f;
				bursts[0].time = 0f;
				bursts[0].count = 20;
				bursts[0].cycleCount = 8;
				bursts[0].repeatInterval = 0.05f;
			}
			else
			{
				bursts = new ParticleSystem.Burst[1];
				bursts[0].time = 0f;
				bursts[0].count = 10;
				bursts[0].cycleCount = 8;
				bursts[0].repeatInterval = 0.05f;
			}
			m_moneyPS.emission.SetBursts(bursts);
			m_moneyPS.Play();
		}

		public void PlayConffeti ()
		{
			Instantiate(m_confettiPSPrefab, Camera.main.transform);
		}

		public void PlayBombExplosion(Vector3 positionFx)
		{
			ParticleSystem fxExplo = Instantiate(m_fxBombeFinisherPrefab, positionFx, m_fxBombeFinisherPrefab.transform.rotation, GameManager.level.transform);
			fxExplo.transform.localScale = new Vector3(2f, 2f, 2f);
			fxExplo.Play();
		}
	}
}