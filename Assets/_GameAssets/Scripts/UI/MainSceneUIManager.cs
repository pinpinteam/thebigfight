using UnityEngine;
using Pinpin.UI;

namespace Pinpin.Scene.MainScene.UI
{

	public sealed class MainSceneUIManager: AUIManager
	{

		public new MainSceneManager	sceneManager
		{
			get { return (base.sceneManager as MainSceneManager); }
		}

		protected override void Awake ()
		{
			base.Awake ();
		}

		protected override void Start ()
		{
			base.Start();
		}

		protected override void OnLeaveRootPanel ()
		{
#if DEBUG
			Debug.Log("MainMenuUIManager - Leave root panel");
#endif
			this.OpenPopup<LeaveGamePopup>();
		}

	}

}