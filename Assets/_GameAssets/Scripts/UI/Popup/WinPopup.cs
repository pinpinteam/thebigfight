﻿using UnityEngine;
using Pinpin.UI;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using PaperPlaneTools;
using System.Collections.Generic;
using System;
using Random = UnityEngine.Random;

namespace Pinpin.Scene.MainScene.UI
{
	public sealed class WinPopup : AClosablePopup
	{
		[SerializeField] private Animator m_animator;
		[SerializeField] private ParticleSystem[] m_burstPSArray;

		//TODO [SerializeField] private ItemUnlockPopup m_itemUnlockPopup;

		/*[Header("UNLOCK GAUGE")]
		[SerializeField] private RectTransform m_gaugeUnlockParent;
		[SerializeField] private RectTransform m_gaugeRect;
		[SerializeField] private Text m_gaugeText;
		[SerializeField] private int m_gaugeTextValue;
		[SerializeField] private ParticleSystem m_unlockCompletePS;
		[SerializeField] private RawImage m_gaugeGradientRawImg;
		[SerializeField] private GameObject[] m_gaugeTitleTexts;
		[SerializeField] private GameObject m_completeInnerGaugeGO;*/

		[Header("infoDisplay")]
		[SerializeField] private CurrencyCounter[] m_infoCounterArray;

		[Header("BTNs")]
		[SerializeField] private PushButton m_normalCollectBtn;
		[SerializeField] private PushButton m_RVCollectBtn;
		//[SerializeField] private PushButton m_VIPCollectBtn;
		//[SerializeField] private PushButton m_noThanksBtn;
		[Space()]
		[SerializeField] private RectTransform m_btnContainer;
		[SerializeField] private CanvasGroup m_btnContainerCanvasGr;

		[Header("CanClick")]
		[SerializeField] private GameObject m_blockClickGO;

		private ulong m_remainingFinisherAmmo;
		private ulong m_gemGain;

		private Tweener m_RVscaleTween;
		//private float m_targetRatio;
		//private int m_targetTextValue;
		private int m_gain = 100;

		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}

		private bool CanClick
		{
			get
			{
				return !m_blockClickGO.activeSelf;
			}
			set
			{
				m_blockClickGO.SetActive(!value);
			}
		}

		protected override void Awake ()
		{
			base.Awake();

			m_normalCollectBtn.onClick += OnClickCollect;
			m_RVCollectBtn.onClick += OnClickRVCollect;
			//m_VIPCollectBtn.onClick += OnClickVIPCollect;
			//m_noThanksBtn.onClick += OnClickNoThanks;
		}

		public void Init ( ulong p_gain )
		{
			m_gemGain = p_gain;
			CanClick = true;

			// -> (gem + (chara + ammo) * charaValue ) * gemUpgrade
			//m_gain = Mathf.RoundToInt((m_gemGain + (ulong)(GameManager.level.ammoCountOnFinisher * ApplicationManager.config.game.ammoValue)) * ApplicationManager.config.game.GetGemUpgradeValue(ApplicationManager.datas.gemUpgradeLevel));

			// -> (ammo * ammoValue) + (gem * gemValue * gemUpgrade)
			m_gain = Mathf.RoundToInt(((ulong)(GameManager.level.ammoCountOnFinisher * ApplicationManager.config.game.ammoValue)) + (m_gemGain *ApplicationManager.config.game.GetGemUpgradeValue(ApplicationManager.datas.gemUpgradeLevel)));

			for (int i = 0; i < m_infoCounterArray.Length; i++)
			{
				m_infoCounterArray[i].ResetToHiddenPos();
				m_infoCounterArray[i].SetCurrencyInstant(0);
			}

			UIManager.ShowTopCanvas<MoneyTopCanvas>();
			m_normalCollectBtn.text = m_gain.ToString();
			m_normalCollectBtn.gameObject.SetActive(false);
			m_RVscaleTween.Pause();
			m_RVscaleTween.Kill();
			m_RVCollectBtn.transform.localScale = Vector3.one * 0.75f;
			m_RVCollectBtn.text = (m_gain * 3).ToString();
			m_RVCollectBtn.gameObject.SetActive(false);

			//m_VIPCollectBtn.text = (m_gain * 3).ToString();
			//m_VIPCollectBtn.gameObject.SetActive(false);
			//m_noThanksBtn.gameObject.SetActive(false);

			m_btnContainer.anchoredPosition = Vector3.up * -150f;
			m_btnContainerCanvasGr.alpha = 0f;
			m_btnContainer.gameObject.SetActive(false);

			//Hide Unlock if in Bonus stage
			//if (!ApplicationManager.datas.HasRandomUnlockableContentRemaining() || GameManager.isInTournament)
			//{
			//TODO m_gaugeUnlockParent.gameObject.SetActive(false);
			/*}
			else
			{
				m_gaugeUnlockParent.gameObject.SetActive(true);

				m_gaugeGradientRawImg.color = m_gradientColor;
				m_completeInnerGaugeGO.SetActive(false);
				m_gaugeTitleTexts[0].SetActive(true);
				m_gaugeTitleTexts[1].SetActive(false);

				float initialRatio = ((ApplicationManager.datas.levelGiftProgression - 1) % 4) * 0.25f;
				m_targetRatio = initialRatio + 0.25f;

				m_gaugeTextValue = ((ApplicationManager.datas.levelGiftProgression - 1) % 4) * 25;
				m_targetTextValue = m_gaugeTextValue + 25;

				m_gaugeRect.sizeDelta = new Vector2(initialRatio * 100, m_gaugeRect.sizeDelta.y);
				m_gaugeText.text = m_gaugeTextValue + "%";
			}*/

			//if (GameManager.Instance.currentLevel.levelType != Level.LevelType.Tournament)
			//{
			//m_animator.SetTrigger("Show");
			/*}
			else
			{
				StartCoroutine(DelayStartForTournamentCR());
			}*/

			//GenerateFakeLB(25, 10);
		}

		/*IEnumerator DelayStartForTournamentCR ()
		{
			//TODO podium stuff GameManager.instance.CurrentLevel.podium
			Level currentLevel = GameManager.Instance.currentLevel;
			currentLevel.podium.podiumVirtualCamera.Priority = 15;

			yield return new WaitForSeconds(1.5f);

			int length = currentLevel.cars.Count;
			currentLevel.podium.RankCars(new CarController[] { currentLevel.cars[length - 1], currentLevel.cars[length - 2], currentLevel.cars[length - 3] });

			yield return new WaitForSeconds(4f);
			m_animator.SetTrigger("Show");
		}*/

		public void IntroAnimationFinishedAE ()
		{
			StartCoroutine(DisplayCounterCR());
		}

		IEnumerator DisplayCounterCR ()
		{
			int index = 0;
			WaitForSeconds delay = new WaitForSeconds(0.15f);

			while (index < m_infoCounterArray.Length)
			{
				m_infoCounterArray[index].SetVisible(true);

				if (index == 0) //character
					m_infoCounterArray[index].UpdateDisplay(0, (ulong)GameManager.level.ammoCountOnFinisher);
				else if (index == 1) //gem
					m_infoCounterArray[index].UpdateDisplay(0, m_gemGain);
				//else if (index == 2) //ammo
				//m_infoCounterArray[index].UpdateDisplay(0, m_remainingFinisherAmmo);
				else //gembonus
					m_infoCounterArray[index].UpdateDisplay("x", 0f, ApplicationManager.config.game.GetGemUpgradeValue(ApplicationManager.datas.gemUpgradeLevel), "n2");

				index++;
				yield return delay;
			}

			yield return new WaitForSeconds(1.5f);

			OnInfoDisplayFinished();
		}

		private void OnInfoDisplayFinished ()
		{
			/*if (m_gaugeUnlockParent.gameObject.activeSelf)
			{
				StartCoroutine(UnlockGaugeCoroutine());
			}
			else if (GameManager.Instance.currentLevel.levelType == Level.LevelType.Tournament)
			{
				UnlockableContentScriptable unlockableContent = ApplicationManager.assets.listNationInfos[ApplicationManager.datas.lastRndOpponentNationIndex].unlockableContent;
				bool isItemUnlocked = ApplicationManager.datas.IsContentUnlocked(unlockableContent);

				ApplicationManager.datas.lastRndOpponentNationIndex = -1;

				//tournament Btn
				if (!isItemUnlocked)
				{
					//popUp
					m_itemUnlockPopup.gameObject.SetActive(true);
					m_itemUnlockPopup.Init(ClaimType.Tournament, unlockableContent, UIManager);
				}

				StartCoroutine(DelayNormalCollectAppearanceCR());
				ShowCollectBtns(0.5f);

			}
			else
			{
				StartCoroutine(DelayNormalCollectAppearanceCR());
				ShowCollectBtns(0.5f);

				m_RVCollectBtn.gameObject.SetActive(!ApplicationManager.datas.vip);
				m_VIPCollectBtn.gameObject.SetActive(ApplicationManager.datas.vip);
			}*/

			m_RVCollectBtn.gameObject.SetActive(true);

			if (m_RVCollectBtn.isInteractable)
				m_RVscaleTween = m_RVCollectBtn.transform.DOScale(1f, 1f).SetLoops(-1, LoopType.Yoyo);

			StartCoroutine(DelayNormalCollectAppearanceCR());
			ShowCollectBtns(0.5f);
		}

		protected override void OnClose ()
		{
			base.OnClose();

			FXManager.onFadeInFinishedAction += OnFadeInLaunchNextLevel;
			FXManager.Instance.FadeIn();
		}

		void OnFadeInLaunchNextLevel ()
		{
			
#if TAPNATION
			if (!UIManager.sceneManager.ShowInterstitial() && ApplicationManager.datas.levelProgression > 4)
			{
#if UNITY_IOS
				RateBox.Instance.ShowIOSNativeRewiew();
#elif UNITY_ANDROID
				RateBox.Instance.Show();
#endif
			}
#endif
			FXManager.Instance.FadeOut();
			UIManager.OpenPanel<MainPanel>();
			UIManager.SetCurrentPanelAsRoot();

			GameManager.Instance.LoadNextLevel();
		}


		/*private IEnumerator UnlockGaugeCoroutine ()
		{
			float gaugeTimer = 1f;
			m_gaugeRect.DOSizeDelta(new Vector2(m_targetRatio * 100f, m_gaugeRect.sizeDelta.y), gaugeTimer);
			DOTween.To(() => m_gaugeTextValue, x => m_gaugeTextValue = x, m_targetTextValue, gaugeTimer).OnUpdate(
				() =>
				{
					m_gaugeText.text = m_gaugeTextValue + "%";
				});

			yield return new WaitForSeconds(gaugeTimer);

			m_gaugeGradientRawImg.DOFade(0f, 0.5f);

			//complete
			if (m_targetRatio >= 1f)
			{
				yield return new WaitForSeconds(0.2f);
				m_completeInnerGaugeGO.SetActive(true);
				m_gaugeTitleTexts[0].SetActive(false);
				m_gaugeTitleTexts[1].SetActive(true);
				m_gaugeUnlockParent.DOPunchScale(Vector3.one * 0.12f, 0.5f, 0, 0.3f);
				m_unlockCompletePS.Stop();
				m_unlockCompletePS.Play();
				yield return new WaitForSeconds(1f);

				//TODO m_itemUnlockPopup.gameObject.SetActive(true);
				//TODO m_itemUnlockPopup.Init(ClaimType.Shop, ApplicationManager.datas.SelectRandomUnlockableContent(), UIManager);
			}*/
		/*
		m_RVCollectBtn.gameObject.SetActive(!ApplicationManager.datas.vip);
		if (!ApplicationManager.datas.vip)
			StartCoroutine(DelayNormalCollectAppearanceCR());

		m_VIPCollectBtn.gameObject.SetActive(ApplicationManager.datas.vip);

		ShowCollectBtns(0.7f);*/
		//}

		void ShowCollectBtns ( float delay )
		{
			m_btnContainer.gameObject.SetActive(true);
			m_btnContainer.DOAnchorPosY(0f, 0.5f).SetDelay(delay);
			m_btnContainerCanvasGr.DOFade(1f, 0.5f).SetDelay(delay);
		}

		/*IEnumerator DelayNoThanksAppearanceCR ()
		{
			float t = ApplicationManager.config.game.loseItBtnDelay;

			while (t > 0f && gameObject.activeSelf)
			{
				if (CanClick)
					t -= Time.deltaTime;
				yield return null;
			}

			m_noThanksBtn.gameObject.SetActive(true);
		}*/

		IEnumerator DelayNormalCollectAppearanceCR ()
		{
			float t = ApplicationManager.config.game.loseItBtnDelay; //normalCollectBtnDelay

			while (t > 0f && gameObject.activeSelf)
			{
				if (CanClick)
					t -= Time.deltaTime;
				yield return null;
			}

			m_normalCollectBtn.gameObject.SetActive(true);
		}

		/*private List<(int rank, string name, float time)> GenerateFakeLB ( float playerTime, int playerCount = 10 )
		{
			List<(int rank, string name, float time)> LB = new List<(int rank, string name, float time)>();

			int pageNumber = Mathf.Max(0, 8 - (DateTime.UtcNow - ApplicationManager.ConvertTimeStampToDateTime(ApplicationManager.datas.firstApplicationLaunchTime)).Days);

			float playerRankProgression = 1 - (ApplicationManager.datas.selectedCarLevel / (float)ApplicationManager.config.game.carUpgradeCount);
			int playerRank = (int)((playerCount - 1) * playerRankProgression) + 1; //between 1 and playerCount

			for (int i = 1; i <= playerCount; i++)
			{
				(int rank, string name, float time) entry;
				entry.rank = pageNumber * 10 + i;

				if (i == playerRank)
				{
					entry.name = ApplicationManager.datas.playerName;
					entry.time = playerTime;
				}
				else
				{
					entry.name = NicknameGenerator.GetRandomNickname();
					entry.time = playerTime + Random.Range(-0.049f, 0.049f) + (i - playerRank) * 0.1f;
				}
				Debug.Log(entry.rank + " - " + entry.name + " - " + entry.time);
				LB.Add(entry);
			}

			return LB;
		}*/

		#region Btn Callback
		void OnClickCollect ()
		{
			if (!CanClick) return;
			StartCoroutine(CollectCoroutine(m_gain, m_normalCollectBtn));
		}

		void OnClickRVCollect ()
		{
			if (!CanClick) return;
#if !TAPNATION
			StartCoroutine(CollectCoroutine(m_gain * 3f, m_RVCollectBtn));
#else
			UIManager.sceneManager.ShowRewardedVideo(OnCollectGoldRV, "RV_WinPopUp_*3");
#endif
		}

		/*void OnClickVIPCollect ()
		{
			if (!CanClick)
			{
				return;
			}

			StartCoroutine(CollectCoroutine(m_gain * 3f, m_VIPCollectBtn, m_collectPSs[2]));
		}*/

		/*void OnClickRVTournamentCollect ()
		{
			if (!CanClick)
			{
				return;
			}

#if !TAPNATION
			StartCoroutine(CollectCoroutine(ApplicationManager.config.game.gemTournamentReward, m_RVTournamentCollectBtn, m_collectPSs[2]));
#else
			UIManager.sceneManager.ShowRewardedVideo(OnCollectGoldRV, "RV_WinPopUp_TournamentCollect");
#endif
		}*/

		private void OnCollectGoldRV ( AdsManagement.RewardedVideoEvent adEvent )
		{
			switch (adEvent)
			{
				case AdsManagement.RewardedVideoEvent.Rewarded:
					StartCoroutine(CollectCoroutine(m_gain * 3f, m_RVCollectBtn));
					break;

				case AdsManagement.RewardedVideoEvent.Failed:
					CanClick = true;
					break;
			}
		}

		IEnumerator CollectCoroutine ( float coin, PushButton btn )
		{
			CanClick = false;

			m_RVscaleTween.Pause();
			m_RVscaleTween.Kill();

			float t = 1f;
			float m_currentCoinDisplayed = coin;

			float startCoin = m_currentCoinDisplayed;

			ApplicationManager.AddSoftCurrency((ulong)m_currentCoinDisplayed);

			btn.linkedParticleSystem.Play();

			while (t > 0f)
			{
				t -= Time.deltaTime;
				m_currentCoinDisplayed = Mathf.Round(startCoin * t);
				if (m_currentCoinDisplayed >= 0f)
					btn.text = m_currentCoinDisplayed.ToString();
				else
					t = 0f;
				yield return null;
			}

			btn.text = "0";

			btn.linkedParticleSystem.Stop();

			yield return new WaitForSeconds(0.4f);
			Close();
		}

		void OnClickNoThanks ()
		{
			if (!CanClick) return;
			Close();
		}

		#endregion

		public void StartBurstAE ()
		{
			for (int i = 0; i < m_burstPSArray.Length; i++)
			{
				m_burstPSArray[i].Stop();
				m_burstPSArray[i].Play();
			}
		}
	}
}