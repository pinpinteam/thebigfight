﻿using UnityEngine;
using Pinpin.UI;
using System.Collections;
//using GameAnalyticsSDK.Setup;

namespace Pinpin.Scene.MainScene.UI
{
	public sealed class LosePopup : AClosablePopup
	{
		[SerializeField] private Animator m_animator;
		[SerializeField] private PushButton m_RetryRVBtn;
		[SerializeField] private PushButton m_RetryVIPBtn;
		[SerializeField] private PushButton m_noThanksBtn;
		[SerializeField] private GameObject m_blockClickGO;

		private bool m_retry = false;
		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}
		private bool CanClick
		{
			get
			{
				return !m_blockClickGO.activeSelf;
			}
			set
			{
				m_blockClickGO.SetActive(!value);
			}
		}

		protected override void Awake ()
		{
			//base.Awake();
			m_RetryRVBtn.onClick += OnClickRetryRV;
			m_RetryVIPBtn.onClick += OnClickRetryVIP;
			m_noThanksBtn.onClick += OnClickNoThanks;
		}

		protected override void OnDestroy ()
		{
			//base.OnDestroy();
			m_RetryRVBtn.onClick -= OnClickRetryRV;
			m_RetryVIPBtn.onClick -= OnClickRetryVIP;
			m_noThanksBtn.onClick -= OnClickNoThanks;
		}

		protected override void OnEnable ()
		{
			base.OnEnable();
			CanClick = true;

			if (ApplicationManager.datas.vipSubscription)
			{
				m_RetryRVBtn.gameObject.SetActive(false);
				m_RetryVIPBtn.gameObject.SetActive(true);
				m_noThanksBtn.gameObject.SetActive(false);
			}
			else
			{
				m_RetryRVBtn.gameObject.SetActive(true);
				m_RetryVIPBtn.gameObject.SetActive(false);
				m_noThanksBtn.gameObject.SetActive(false);
				StartCoroutine(DelayLoseItAppearanceCR());
			}
		}

		public override void Close ()
		{
			this.OnClose();
			//this.gameObject.SetActive(false);
		}

		IEnumerator DelayLoseItAppearanceCR ()
		{
			m_noThanksBtn.gameObject.SetActive(false);

			yield return new WaitForSeconds(1f);

			float t = ApplicationManager.config.game.loseItBtnDelay;

			while (t > 0f && gameObject.activeSelf)
			{
				t -= Time.deltaTime;
				yield return null;
			}

			m_noThanksBtn.gameObject.SetActive(true);
		}

		#region Callback
		void OnClickRetryRV ()
		{
			if (!CanClick) return;
#if !TAPNATION
			RetryLevel();
#else
			if (UIManager.sceneManager.ShowRewardedVideo(OnRetryRV, "RV_LosePopUp_Retry"))
				CanClick = false;
#endif
		}

		private void OnRetryRV ( AdsManagement.RewardedVideoEvent adEvent )
		{
			switch (adEvent)
			{
				case AdsManagement.RewardedVideoEvent.Rewarded:
					RetryLevel();
					break;

				case AdsManagement.RewardedVideoEvent.Failed:
					CanClick = true;
					break;
			}
		}

		void OnClickRetryVIP ()
		{
			if (!CanClick)
			{
				return;
			}

			CanClick = false;
			RetryLevel();
		}

		void RetryLevel ()
		{
			CanClick = false;
			m_retry = true;
			Close();
		}

		void OnClickNoThanks ()
		{
			if (!CanClick) return;
			StartCoroutine(DelayCloseCR());
		}

		IEnumerator DelayCloseCR ()
		{
			CanClick = false;
			yield return new WaitForSeconds(0.4f);
			Close();
		}
		#endregion
		void OnFadeOutFinishedRetry ()
		{
			UIManager.HideTopCanvas<MoneyTopCanvas>();
			//gameObject.SetActive(false);
			UIManager.OpenPanel<GamePanel>();

			//TODO REVIVE
			GameManager.Instance.Revive();
			//GameManager.Instance.StartCoroutine(GameManager.Instance.DelayCR(GameManager.Instance.Revive, 2));
			//GameManager.Instance.WaitForInput();
		}

		protected override void OnClose ()
		{
			base.OnClose();

			if (m_retry)
			{
				m_retry = false;
				FXManager.onFadeOutFinishedAction += OnFadeOutFinishedRetry;
				FXManager.Instance.FadeOut();
				gameObject.SetActive(false);
			}
			else
			{
				FXManager.onFadeInFinishedAction += OnFadeInFinishedGetBackToMainPanel;
				FXManager.Instance.FadeIn();
			}
		}


		void OnFadeInFinishedGetBackToMainPanel ()
		{
			gameObject.SetActive(false);

			UIManager.OpenPanel<MainPanel>();
			UIManager.SetCurrentPanelAsRoot();

			GameManager.Instance.LoadNextLevel();
			FXManager.Instance.FadeOut();
			//UIManager.sceneManager.ShowInterstitial();
		}

		public void OnHideAnimationFinishedAE ()
		{
			gameObject.SetActive(false);
		}
	}
}