﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Pinpin;

namespace Pinpin
{
	public class RisingGlowFx : MonoBehaviour
	{
		[SerializeField] private Renderer m_renderer;
		[SerializeField] private Color m_currentColor;
		[SerializeField] private Transform m_squareTfm;
		[SerializeField] private float m_alpha = 0.3f;

		[Header("Loop")]
		[SerializeField] private bool m_loop = true;
		[SerializeField] private AnimationCurve m_loopCurve;
		[SerializeField] private Vector3[] m_loopScalesArray;
		//[SerializeField] private float[] m_loopAlphaArray;
		[SerializeField] private float m_loopSpeed = 1f;

		[Header("Bounce")]
		[SerializeField] private AnimationCurve m_bounceCurve;
		[SerializeField] private Vector3 m_bounceScaleMax;
		//[SerializeField] private float m_bounceAlphaMax;
		[SerializeField] private float m_bounceSpeed = 1f;
		[SerializeField] private float m_initBounceDamp = 8f;


		private bool m_bounce = false;
		private float m_t;
		private Vector3 m_currentScale;
		private float m_currentAlpha;
		private float m_currentDamp = 0f;
		private MaterialPropertyBlock m_propBlock;

		private void Awake ()
		{
			m_propBlock = new MaterialPropertyBlock();
			m_renderer.GetPropertyBlock(m_propBlock);
			ApplyCurrentColor();
		}

		public void SetColor ( Color newColor )
		{
			m_currentColor = newColor;
			ApplyCurrentColor();
		}

		public void RestartLoop ()
		{
			StopAllCoroutines();
			m_loop = true;
			if (gameObject.activeInHierarchy)
				StartCoroutine(LoopCR());
		}

		void ApplyCurrentColor ()
		{
			m_currentColor.a = m_alpha;
			m_propBlock.SetColor("_Color", m_currentColor);
			m_renderer.SetPropertyBlock(m_propBlock);
		}

		private void OnEnable ()
		{
			if (m_loop)
				StartCoroutine(LoopCR());
		}

		private void Bounce ()
		{
			m_bounce = true;
			StartCoroutine(BounceCR());
		}

		IEnumerator LoopCR ()
		{
			while (!m_bounce && m_loop && gameObject.activeInHierarchy)
			{
				m_t += Time.deltaTime * m_loopSpeed;
				m_currentDamp = m_loopCurve.Evaluate(m_t);

				m_currentScale = Vector3.Lerp(m_loopScalesArray[0], m_loopScalesArray[1], m_currentDamp);
				m_squareTfm.localScale = m_currentScale;

				/*m_currentAlpha = Mathf.Lerp(m_loopAlphaArray[0], m_loopAlphaArray[1], m_currentDamp);
				m_currentColor.a = m_currentAlpha;
				ApplyCurrentColor();*/

				yield return null;
			}
		}

		IEnumerator BounceCR ()
		{
			m_t = 0f;

			//get quickly Back to smallest value
			while (gameObject.activeInHierarchy && m_currentDamp > 0f)
			{
				m_currentDamp = Mathf.MoveTowards(m_currentDamp, 0f, m_initBounceDamp * Time.deltaTime);

				m_currentScale = Vector3.Lerp(m_loopScalesArray[0], m_loopScalesArray[1], m_currentDamp);
				m_squareTfm.localScale = m_currentScale;

				/*m_currentAlpha = Mathf.Lerp(m_loopAlphaArray[0], m_loopAlphaArray[1], m_currentDamp);
				m_currentColor.a = m_currentAlpha;
				ApplyCurrentColor();*/

				yield return null;
			}

			//do the bounce
			while (gameObject.activeInHierarchy && m_t < 1f)
			{
				m_t += Time.deltaTime * m_bounceSpeed;
				m_currentDamp = m_bounceCurve.Evaluate(m_t);

				m_currentScale = Vector3.Lerp(m_loopScalesArray[0], m_bounceScaleMax, m_currentDamp);
				m_squareTfm.localScale = m_currentScale;

				/*m_currentAlpha = Mathf.Lerp(m_loopAlphaArray[0], m_bounceAlphaMax, m_currentDamp);
				m_currentColor.a = m_currentAlpha;
				ApplyCurrentColor();*/

				yield return null;
			}

			m_bounce = false;

			if (m_loop)
				StartCoroutine(LoopCR());
		}
	}
}