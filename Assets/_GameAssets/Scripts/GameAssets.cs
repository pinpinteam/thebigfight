using UnityEngine;
using UnityEngine.Audio;
using System.Collections.Generic;

namespace Pinpin
{
	[CreateAssetMenu(fileName = "GameAssets", menuName = "Game/GameAssets", order = 1)]
	public class GameAssets : ScriptableObject
	{
		public AudioMixer audioMixer;

		public Level baseLevel;

		[Header("Level Infos")]
		public LevelInfo[] levelInfos;
		public LevelInfo[] captureLevelInfos;


		[Header("GamePlayElement Prefabs")]
		public Wall wallPrefab;
		public BorderWall borderWallPrefab;
		public Slope slopePrefab;
		public MultiplicationPortal multiplicationPortalPrefab;
		public ColorPortal colorPortalPrefab;
		public CrowdArea colorCrowdAreaPrefab;
		public BulletStack bulletStackPrefab;
		public Collectible collectiblePrefab;
		public CollectibleBullet collectibleBulletPrefab;
		public SawTrap sawTrapPrefab;
		public PendulumTrap pendulumTrapPrefab;
		public RotatingCylinderTrap rotatingCylinderTrapPrefab;
		public SpikyCylinderTrap spikyCylinderTrapPrefab;
		public BarrelWall barrelWallTrapPrefab;
		public SwatterTrap swatterTrapPrefab;
		public BigCrowdArea bigColorCrowdAreaPrefab;
		public RotorTrap rotorTrapPrefab;
		public SpikeCarpetTrap spikeCarpetTrapPrefab;
		
		[Space]
		public RotatingPlatform rotatingPlatformPrefab;

		//[Header("Random Level Generator")]
		//public LevelPattern smallEmptyPattern;
		//public LevelPattern largeEmptyPattern;
		//public List<LevelPattern> colorChangePatterns;

		[Header("Prefabs")]
		public Character characterPrefab;
		public GameObject bulletPrefab;
		public GameObject scopePrefab;
		public GameObject BulletShootPrefab;
		public CharacterFinisher characterFinisher;
		public GameObject rocketPrefab;
		public GameObject gemPrefab;
	}
}