﻿using Pinpin.Scene.MainScene.UI;
using Pinpin.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

namespace Pinpin
{
	public class CheatMenu : Singleton<CheatMenu>
	{
		[Space()]
		[SerializeField] private GameObject m_menuGO;
		[Space()]
		[SerializeField] private PushButton m_skipLevelBtn;
		[SerializeField] private PushButton m_unlockAllBtn;
		[SerializeField] private PushButton m_hudBtn;
		[SerializeField] private Text m_currentLoadedLevelTxt;

		[Header("CHANGE SKYBOX")]
		[SerializeField] private PushButton m_skyboxLoadBtn;
		[SerializeField] private PushButton[] m_skyboxLRBtns;
		[SerializeField] private Text m_currentSkyboxText;
		[SerializeField] private Material[] m_skyboxes;
		int m_skyboxIndex = 0;

		[Header("CHANGE CAMERA")]

		[SerializeField] private PushButton m_cameraLoadBtn;
		[SerializeField] private PushButton[] m_cameraLRBtns;
		[SerializeField] private Text m_currentCameraText;

		[Header("CHANGE SPEED")]
		[SerializeField] private Slider m_sliderSpeed;
		public static Action ResetPlayerAction;

		[HideInInspector]
		public int m_cameraIndex = 0;

		[HideInInspector]
		public List<Color> colors = new List<Color>();


		[Space()]
		[SerializeField] private PushButton m_showBtn;
		[SerializeField] private PushButton m_quitBtn;

		[SerializeField] private MainSceneUIManager m_mainSceneUIManager;


		public bool isHUDVisible = true;
		public GameObject[] hudGO;

		public bool isOpen = false;

#if CAPTURE
		public override void Awake ()
		{
			base.Awake();

			m_skyboxes[0] = RenderSettings.skybox;

			m_hudBtn.onClick += OnClickHudBtn;
			m_skipLevelBtn.onClick += OnClickSkipBtn;
			m_unlockAllBtn.onClick += OnClickUnlockAllBtn;

			m_showBtn.onClick += OnClickShowBtn;
			m_quitBtn.onClick += OnClickQuit;

			m_skyboxLoadBtn.onClick += OnClickSkyboxLoadBtn;
			m_skyboxLRBtns[0].onClick += OnClickSkyboxLeftBtn;
			m_skyboxLRBtns[1].onClick += OnClickSkyboxRightBtn;

			m_cameraLoadBtn.onClick += OnClickCameraLoadBtn;
			m_cameraLRBtns[0].onClick += OnClickCameraLeftBtn;
			m_cameraLRBtns[1].onClick += OnClickCameraRightBtn;
			m_sliderSpeed.onValueChanged.AddListener(delegate { OnValueChangeSpeedCharacter(); });


			isOpen = m_menuGO.activeSelf;
		}

		private void OnEnable()
		{
			UpdateBtnStatus();
		}

		private void Start()
		{
			UpdateBtnStatus();
		}

		private void UpdateBtnStatus ()
		{
			m_hudBtn.text = isHUDVisible ? "HUD : Visible" : "HUD : Hidden";
			m_currentSkyboxText.text = m_skyboxes[m_skyboxIndex].name;
			m_currentCameraText.text = ApplicationManager.assets.baseLevel.captureLevelVCameras[m_cameraIndex].gameObject.name;

			if (gameObject.activeSelf)
				StartCoroutine(UpdateLevelNameCoroutine());
		}

		IEnumerator UpdateLevelNameCoroutine ()
		{
			//wait 2 frames cause the Load Level is a coroutine that wait 1 frame after removing the currentLevel
			yield return null;
			yield return null;
			m_currentLoadedLevelTxt.text = "CURRENT LEVEL : " + GameManager.level.currentLevelInfo.name;
		}

		void OnClickHudBtn()
		{
			isHUDVisible = !isHUDVisible;
			UpdateBtnStatus();

			foreach (GameObject GO in hudGO)
				GO.SetActive(isHUDVisible);
		}

		void ReloadLevel()
		{
			if (GameManager.Instance.levelIsLoading)
			{
				return;
			}
			if (GameManager.Instance.GameState != GameManager.EGameState.InMenu)
			{
				FXManager.Instance.FadeOut();
				m_mainSceneUIManager.CloseActivePopup();
				m_mainSceneUIManager.OpenPanel<MainPanel>();
				m_mainSceneUIManager.SetCurrentPanelAsRoot();
			}
			GameManager.Instance.LoadNextLevel();
			UpdateBtnStatus();
		}

		void OnClickSkipBtn ()
		{
			if (GameManager.Instance.levelIsLoading)
			{
				return;
			}

			if (GameManager.Instance.GameState != GameManager.EGameState.InMenu)
			{
				FXManager.Instance.FadeOut();
				m_mainSceneUIManager.CloseActivePopup();
				m_mainSceneUIManager.OpenPanel<MainPanel>();
				m_mainSceneUIManager.SetCurrentPanelAsRoot();
			}

			if (ApplicationManager.datas.levelProgression == GameManager.Instance.currentLevelIndex)
			{
				ApplicationManager.datas.levelProgression++;
				ApplicationManager.datas.postGameLevelDoneCount = 0;
			}
			else
			{
				ApplicationManager.datas.postGameLevelDoneCount++;
			}
			ApplicationManager.datas.totalLevelDoneCount++;

			GameManager.Instance.LoadNextLevel();
			UpdateBtnStatus();
		}

		public void OnValueChangeSpeedCharacter()
		{
			GameManager.level.player.speedMultiplier = m_sliderSpeed.value;
			ResetPlayerAction?.Invoke();
		}

		void OnClickUnlockAllBtn()
		{
			//ApplicationManager.datas.UnlockAllContent();
		}

		void OnClickSkyboxLoadBtn()
		{
			RenderSettings.skybox = m_skyboxes[m_skyboxIndex];
			//Load
			UpdateBtnStatus();
		}


		void OnClickSkyboxLeftBtn()
		{
			m_skyboxIndex = m_skyboxIndex == 0 ? m_skyboxes.Length - 1 : m_skyboxIndex - 1;
			UpdateBtnStatus();
		}

		void OnClickSkyboxRightBtn()
		{
			m_skyboxIndex = (m_skyboxIndex + 1) % m_skyboxes.Length;
			UpdateBtnStatus();
		}

		void OnClickCameraLoadBtn()
		{
			//RenderSettings.skybox = m_skyboxes[m_skyboxIndex];
			//Load
			ReloadLevel();
			UpdateBtnStatus();
		}


		void OnClickCameraLeftBtn()
		{
			m_cameraIndex = m_cameraIndex == 0 ? ApplicationManager.assets.baseLevel.captureLevelVCameras.Length - 1 : m_cameraIndex - 1;
			UpdateBtnStatus();
		}

		void OnClickCameraRightBtn()
		{
			m_cameraIndex = (m_cameraIndex + 1) % ApplicationManager.assets.baseLevel.captureLevelVCameras.Length;
			UpdateBtnStatus();
		}


		private void LateUpdate()
		{
			//For updating state at the end of the frame
			isOpen = m_menuGO.activeSelf;
			m_skipLevelBtn.isInteractable = !GameManager.Instance.levelIsLoading;
		}

		void OnClickShowBtn ()
		{
			isOpen = true;
			UpdateBtnStatus();
			m_menuGO.SetActive(true);
		}

		void OnClickQuit ()
		{
			m_menuGO.SetActive(false);
		}
#endif
	}

	[Serializable]
	public struct ColorSettings
	{
		public string colorName;
		public Color color;
	}
}
