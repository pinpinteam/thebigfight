using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Pinpin.UI.Editor
{
	//VERSION 06/07/2020
	[CustomEditor(typeof(PushButton), true), CanEditMultipleObjects]
	public class PushButtonEditor: UnityEditor.UI.ButtonEditor
	{
		private PushButton button;

		private SerializedProperty m_animationTransformProperty;
		private SerializedProperty m_fadeGroupProperty;

		private SerializedProperty m_blackVeilGameObjectProperty;
		private SerializedProperty m_selectedGameObjectProperty;
		private SerializedProperty m_clickParticlesProperty;
		private SerializedProperty m_linkedParticlesProperty;
		private SerializedProperty m_textsProperty;
		private SerializedProperty m_iconsProperty;

		private SerializedProperty m_isShiningProperty;
		private SerializedProperty m_shineImageProperty;
		private SerializedProperty m_shineStartDelayProperty;
		private SerializedProperty m_shinePauseDurationProperty;
		private SerializedProperty m_shineDurationProperty;

		private SerializedProperty m_isBouncingProperty;
		private SerializedProperty m_bounceStartDelayProperty;
		private SerializedProperty m_bouncePauseDuration;
		private SerializedProperty m_bounceDurationProperty;

		private SerializedProperty m_bounceOnClickProperty;

		private SerializedProperty m_repeatOnHoldProperty;
		private SerializedProperty m_holdFrequencyProperty;
		private SerializedProperty m_holdDelayProperty;

		private SerializedProperty m_haveNotificationProperty;
		private SerializedProperty m_notificationParentProperty;
		private SerializedProperty m_notificationTextProperty;

		private SerializedProperty m_adsButtonProperty;

		private bool m_isShining
		{
			get { return m_isShiningProperty.boolValue; }
			set
			{
				if(m_isShiningProperty.boolValue != value)
				{
					OnShiningValueChange(value);
				}
}
		}

		protected override void OnEnable ()
		{
			button = (PushButton)target;

			base.OnEnable();
			m_animationTransformProperty = serializedObject.FindProperty("m_animationTransform");
			m_fadeGroupProperty = serializedObject.FindProperty("m_fadeGroup");

			m_blackVeilGameObjectProperty = serializedObject.FindProperty("m_blackVeilGameObject");
			m_selectedGameObjectProperty = serializedObject.FindProperty("m_selectedGameObject");
			m_clickParticlesProperty = serializedObject.FindProperty("m_clickParticles");
			m_linkedParticlesProperty = serializedObject.FindProperty("m_linkedParticles");

			m_textsProperty = serializedObject.FindProperty("m_texts");
			m_iconsProperty = serializedObject.FindProperty("m_icons");

			m_shineImageProperty = serializedObject.FindProperty("m_shineImage");
			m_isShiningProperty = serializedObject.FindProperty("m_isShining");
			m_shineStartDelayProperty = serializedObject.FindProperty("m_shineStartDelay");
			m_shinePauseDurationProperty = serializedObject.FindProperty("m_shinePauseDuration");
			m_shineDurationProperty = serializedObject.FindProperty("m_shineDuration");

			m_isBouncingProperty = serializedObject.FindProperty("m_isBouncing");
			m_bounceStartDelayProperty = serializedObject.FindProperty("m_bounceStartDelay");
			m_bouncePauseDuration = serializedObject.FindProperty("m_bouncePauseDuration");
			m_bounceDurationProperty = serializedObject.FindProperty("m_bounceDuration");

			m_bounceOnClickProperty = serializedObject.FindProperty("m_bounceOnClick");

			m_repeatOnHoldProperty = serializedObject.FindProperty("m_repeatOnHold");
			m_holdFrequencyProperty = serializedObject.FindProperty("m_holdFrequency");
			m_holdDelayProperty = serializedObject.FindProperty("m_holdDelay");

			m_haveNotificationProperty = serializedObject.FindProperty("m_useNotification");
			m_notificationParentProperty = serializedObject.FindProperty("m_notificationParent");
			m_notificationTextProperty = serializedObject.FindProperty("m_notificationText");

			m_adsButtonProperty = serializedObject.FindProperty("m_adButton");
		}

		public override void OnInspectorGUI ()
		{
			base.OnInspectorGUI();

			serializedObject.Update();
			DisplayAnimationTransformProperty();
			DisplayFadeGroupProperty();

			DisplayDisableGameObjectProperty();
			DisplaySelectedGameObjectProperty();
			DisplayClickParticlesProperty();
			DisplayLinkedParticlesProperty();
			DisplayTextProperty();
			DisplayIconProperty();
			DisplayIsShiningProperty();
			if (m_isShiningProperty.boolValue)
			{
				m_isShining = m_isShiningProperty.boolValue;
				DisplayShineImageProperty();
				DisplayShineStartDelayProperty();
				DisplayShinePauseDurationProperty();
				DisplayShineDurationProperty();
			}
			DisplayIsBouncingProperty();
			if (m_isBouncingProperty.boolValue)
			{
				DisplayBounceStartDelayProperty();
				DisplayBouncePauseDurationProperty();
				DisplayBounceDurationProperty();
			}
			DisplayBounceOnClickProperty();

			DisplayRepeatOnHoldProperty();
			if (m_repeatOnHoldProperty.boolValue)
			{
				DisplayHoldDelayProperty();
				DisplayHoldFrequencyProperty();
			}

			DisplayHaveNotificationProperty();
			if (m_haveNotificationProperty.boolValue)
			{
				DisplayNotificationParentProperty();
				DisplayNotificationTextProperty();
			}

			DisplayAdProperty();

			serializedObject.ApplyModifiedProperties();

		}

		protected void DisplayAnimationTransformProperty ()
		{
			EditorGUILayout.PropertyField(m_animationTransformProperty, new GUIContent("Graphics Transform"));
		}

		protected void DisplayFadeGroupProperty ()
		{
			EditorGUILayout.PropertyField(m_fadeGroupProperty, new GUIContent("Fade Group"));
		}

		protected void DisplayDisableGameObjectProperty ()
		{
			EditorGUILayout.PropertyField(m_blackVeilGameObjectProperty, new GUIContent("BlackVeil when disabled"));
		}
		
		protected void DisplayClickParticlesProperty ()
		{
			EditorGUILayout.PropertyField(m_clickParticlesProperty, new GUIContent("Click Particles"));
		}
		protected void DisplayLinkedParticlesProperty ()
		{
			EditorGUILayout.PropertyField(m_linkedParticlesProperty, new GUIContent("Linked Particles"));
		}

		protected void DisplaySelectedGameObjectProperty ()
		{
			EditorGUILayout.PropertyField(m_selectedGameObjectProperty, new GUIContent("Selected Gameobject when selected"));
		}

		protected void DisplayTextProperty ()
		{
			EditorGUILayout.PropertyField(m_textsProperty, new GUIContent("Texts (optional)"));
		}

		protected void DisplayIconProperty ()
		{
			EditorGUILayout.PropertyField(m_iconsProperty, new GUIContent("Icons (optional)"));
		}

		protected void DisplayIsShiningProperty ()
		{
			EditorGUILayout.PropertyField(m_isShiningProperty, new GUIContent("Shining animation"));
		}

		protected void DisplayShineImageProperty ()
		{
			EditorGUILayout.PropertyField(m_shineImageProperty, new GUIContent("Shine Image"));
		}

		protected void DisplayShineStartDelayProperty ()
		{
			EditorGUILayout.PropertyField(m_shineStartDelayProperty, new GUIContent("Shine Start Delay"));
		}

		protected void DisplayShinePauseDurationProperty ()
		{
			EditorGUILayout.PropertyField(m_shinePauseDurationProperty, new GUIContent("Shine Pause Duration"));
		}

		protected void DisplayShineDurationProperty ()
		{
			EditorGUILayout.PropertyField(m_shineDurationProperty, new GUIContent("Shine Duration"));
		}

		protected void DisplayIsBouncingProperty ()
		{
			EditorGUILayout.PropertyField(m_isBouncingProperty, new GUIContent("Bouncing animation"));
		}

		protected void DisplayBounceStartDelayProperty ()
		{
			EditorGUILayout.PropertyField(m_bounceStartDelayProperty, new GUIContent("Bouncing Start Delay"));
		}

		protected void DisplayBouncePauseDurationProperty ()
		{
			EditorGUILayout.PropertyField(m_bouncePauseDuration, new GUIContent("Bouncing pause duration"));
		}

		protected void DisplayBounceDurationProperty ()
		{
			EditorGUILayout.PropertyField(m_bounceDurationProperty, new GUIContent("Bounce Duration"));
		}

		protected void DisplayBounceOnClickProperty()
		{
			EditorGUILayout.PropertyField(m_bounceOnClickProperty, new GUIContent("Bouncing on click"));
		}

		private void OnShiningValueChange (bool value)
		{
			if (value)
			{
				if (ReferenceEquals(button.gameObject.GetComponent<Mask>(), null))
				{
					button.gameObject.AddComponent<Mask>();
				}
			}
			else
			{
				if(!ReferenceEquals(button.gameObject.GetComponent<Mask>(), null))
				{
					Destroy(button.gameObject.GetComponent<Mask>());
				}
			}
		}

		protected void DisplayRepeatOnHoldProperty ()
		{
			EditorGUILayout.PropertyField(m_repeatOnHoldProperty, new GUIContent("Repeat On Hold"));
		}

		protected void DisplayHoldDelayProperty ()
		{
			EditorGUILayout.PropertyField(m_holdDelayProperty, new GUIContent("Hold Delay duration"));
		}

		protected void DisplayHoldFrequencyProperty ()
		{
			EditorGUILayout.PropertyField(m_holdFrequencyProperty, new GUIContent("Hold Frenquency Delay duration"));
		}

		protected void DisplayAdProperty ()
		{
			EditorGUILayout.PropertyField(m_adsButtonProperty, new GUIContent("Ads Button"));
		}



		protected void DisplayHaveNotificationProperty ()
		{
			EditorGUILayout.PropertyField(m_haveNotificationProperty, new GUIContent("Have Notification"));
		}

		protected void DisplayNotificationParentProperty ()
		{
			EditorGUILayout.PropertyField(m_notificationParentProperty, new GUIContent("Notification Parent"));
		}

		protected void DisplayNotificationTextProperty ()
		{
			EditorGUILayout.PropertyField(m_notificationTextProperty, new GUIContent("Notification Text"));
		}
	}

}