// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Multiplication"
{
	Properties
	{
		_Height("Height", Range( 0 , 1)) = 0.6000001
		_Color("Color", Color) = (1,0,0.9591947,0)
		_Whitensity("Whitensity", Range( 0 , 1)) = 0.2773955
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard alpha:fade keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _Color;
		uniform float _Whitensity;
		uniform float _Height;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			o.Emission = _Color.rgb;
			float smoothstepResult28 = smoothstep( -3.0 , 1.5 , _SinTime.w);
			float smoothstepResult13 = smoothstep( ( _Whitensity * smoothstepResult28 ) , (-1.8 + (_Height - 0.0) * (0.2 - -1.8) / (1.0 - 0.0)) , ( ( 1.0 - i.uv_texcoord.x ) * i.uv_texcoord.x ));
			o.Alpha = saturate( smoothstepResult13 );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18301
0;54;1920;965;2956.696;808.5834;1.679013;True;True
Node;AmplifyShaderEditor.TextureCoordinatesNode;1;-1812.695,-143.8439;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SinTimeNode;26;-1331.577,286.3144;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;11;-1476.695,-287.8438;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;17;-1267.695,143.1561;Inherit;False;Property;_Whitensity;Whitensity;2;0;Create;True;0;0;False;0;False;0.2773955;0.2773955;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;3;-895.3882,417.5868;Inherit;False;Property;_Height;Height;0;0;Create;True;0;0;False;0;False;0.6000001;0.6000001;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;28;-1157.116,336.1018;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;-3;False;2;FLOAT;1.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;12;-1220.695,-111.8439;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;24;-591.3882,417.5868;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-1.8;False;4;FLOAT;0.2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;27;-952.1163,169.1018;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;13;-628.6951,-15.84389;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;-0.88;False;2;FLOAT;0.25;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;14;-356.6947,-15.84389;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;8;-624,-416;Inherit;False;Property;_Color;Color;1;0;Create;True;0;0;False;0;False;1,0,0.9591947,0;1,0,0.9591947,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;-34.21048,-341.6864;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;Multiplication;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;False;0;False;Transparent;;Transparent;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;11;0;1;1
WireConnection;28;0;26;4
WireConnection;12;0;11;0
WireConnection;12;1;1;1
WireConnection;24;0;3;0
WireConnection;27;0;17;0
WireConnection;27;1;28;0
WireConnection;13;0;12;0
WireConnection;13;1;27;0
WireConnection;13;2;24;0
WireConnection;14;0;13;0
WireConnection;0;2;8;0
WireConnection;0;9;14;0
ASEEND*/
//CHKSM=0FBA247D4DF5E46F53DE21BFC1580CA26F3F851C