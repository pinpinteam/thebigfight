// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/WaterMobile"
{
	Properties
	{
		_FoamTexture("Foam Texture", 2D) = "white" {}
		[Header(Bigger Foam)]_BiggerFoamColorValue("BiggerFoamColorValue", Range( -1 , 1)) = 0.5
		_BigerFoamScale("Biger Foam  Scale", Range( 0 , 1)) = 3.27
		[Header(Smaller Noise)]_SmallerFoamColorValue("Smaller Foam Color Value", Range( -1 , 1)) = 0
		_SmallerFoamScale("Smaller Foam Scale", Range( 0 , 1)) = 0
		_SmmaerNoiseRotation("Smmaer Noise Rotation", Range( 0 , 360)) = 0
		[Header(Foam Wave)]_FoamSpeed("Foam Speed", Float) = 0.2
		_WaveFrequency("WaveFrequency", Float) = 1
		_WaveAmplitude("Wave Amplitude", Float) = 0
		[Header(Vertex Displacement)]_DisplacementSpeed("Displacement Speed", Range( 0 , 10)) = 0.365928
		_DisplacementScale("Displacement Scale", Float) = 5
		[Header(Depth)]_DepthDistance("Depth Distance", Range( 0 , 30)) = 1
		_DepthPower("Depth Power", Range( 0.001 , 10)) = 1
		_DepthColor("Depth Color", Color) = (0.007876454,0.1974638,0.5566038,0)
		[Header(Colorise)]_Min("Min", Range( 0 , 1)) = 0
		_Max("Max", Range( 0 , 1)) = 1
		[Header(Invincible)]_HueShiftSpeed("Hue Shift Speed", Range( 0 , 2)) = 1
		_MultiColorPower("MultiColor Power", Float) = 0
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha noshadow novertexlights nolightmap  nodynlightmap nodirlightmap nometa noforwardadd vertex:vertexDataFunc 
		struct Input
		{
			float3 worldPos;
			float4 screenPos;
		};

		uniform float _DisplacementSpeed;
		uniform float _DisplacementScale;
		uniform float4 MainColor;
		uniform float4 SecondaryColor;
		uniform float _Min;
		uniform float _Max;
		uniform float ColoriseDistance;
		uniform float3 ColoriseOrigin;
		uniform float _MultiColorPower;
		uniform float _HueShiftSpeed;
		uniform float Invincible;
		uniform sampler2D _FoamTexture;
		uniform float _FoamSpeed;
		uniform float _WaveFrequency;
		uniform float _WaveAmplitude;
		uniform float _BigerFoamScale;
		uniform float _BiggerFoamColorValue;
		uniform float _SmallerFoamColorValue;
		uniform float _SmallerFoamScale;
		uniform float _SmmaerNoiseRotation;
		UNITY_DECLARE_DEPTH_TEXTURE( _CameraDepthTexture );
		uniform float4 _CameraDepthTexture_TexelSize;
		uniform float _DepthDistance;
		uniform float _DepthPower;
		uniform float4 _DepthColor;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		float3 HSVToRGB( float3 c )
		{
			float4 K = float4( 1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0 );
			float3 p = abs( frac( c.xxx + K.xyz ) * 6.0 - K.www );
			return c.z * lerp( K.xxx, saturate( p - K.xxx ), c.y );
		}


		float3 RGBToHSV(float3 c)
		{
			float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
			float4 p = lerp( float4( c.bg, K.wz ), float4( c.gb, K.xy ), step( c.b, c.g ) );
			float4 q = lerp( float4( p.xyw, c.r ), float4( c.r, p.yzx ), step( p.x, c.r ) );
			float d = q.x - min( q.w, q.y );
			float e = 1.0e-10;
			return float3( abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
		}

		float2 UVDistort255( float2 UV , float2 sinAnim , float waveAmplitude )
		{
			half2 uvDistort = ((sin(0.9*sinAnim.xy) + sin(1.33*sinAnim.xy+3.14) + sin(2.4*sinAnim.xy+5.3))/3) * waveAmplitude;
			UV += uvDistort.xy;
			return UV;
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float4 appendResult5_g14 = (float4(_SinTime.w , _SinTime.z , 0.0 , 0.0));
			float WaterSpeed226 = _DisplacementSpeed;
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			float4 appendResult133 = (float4(ase_worldPos.x , ase_worldPos.z , 0.0 , 0.0));
			float4 WolrdPlannarUV136 = appendResult133;
			float2 panner7_g14 = ( 1.0 * _Time.y * ( appendResult5_g14 * ( WaterSpeed226 / 10000.0 ) ).xy + WolrdPlannarUV136.xy);
			float2 WaterMovement216 = panner7_g14;
			float simplePerlin2D75 = snoise( WaterMovement216*_DisplacementScale );
			simplePerlin2D75 = simplePerlin2D75*0.5 + 0.5;
			float Noise214 = simplePerlin2D75;
			float4 appendResult87 = (float4(0.0 , Noise214 , 0.0 , 0.0));
			float4 VertexOffset126 = appendResult87;
			v.vertex.xyz += VertexOffset126.xyz;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_worldPos = i.worldPos;
			float3 temp_output_250_0 = ( ColoriseOrigin - ase_worldPos );
			float dotResult249 = dot( temp_output_250_0 , temp_output_250_0 );
			float temp_output_237_0 = ( ColoriseDistance / dotResult249 );
			float smoothstepResult243 = smoothstep( _Min , _Max , temp_output_237_0);
			float4 lerpResult236 = lerp( MainColor , SecondaryColor , smoothstepResult243);
			float3 hsvTorgb3_g13 = HSVToRGB( float3(( ( ( ( ase_worldPos.x * 0.5 ) + ase_worldPos.z ) * _MultiColorPower ) - ( _Time.y * _HueShiftSpeed ) ),1.0,1.0) );
			float3 hsvTorgb310 = RGBToHSV( hsvTorgb3_g13 );
			float3 hsvTorgb312 = HSVToRGB( float3(hsvTorgb310.x,( hsvTorgb310.y * 0.8 ),( hsvTorgb310.z * 0.6 )) );
			float3 MultiColor300 = hsvTorgb312;
			float4 lerpResult297 = lerp( lerpResult236 , float4( MultiColor300 , 0.0 ) , Invincible);
			float4 Color97 = lerpResult297;
			float4 appendResult133 = (float4(ase_worldPos.x , ase_worldPos.z , 0.0 , 0.0));
			float4 WolrdPlannarUV136 = appendResult133;
			float2 UV255 = WolrdPlannarUV136.xy;
			float4 appendResult257 = (float4(ase_worldPos.x , ase_worldPos.y , 0.0 , 0.0));
			float4 appendResult259 = (float4(0.0 , ase_worldPos.y , ase_worldPos.z , 0.0));
			float2 sinAnim255 = ( ( _Time.y * _FoamSpeed ) + ( ( appendResult257 + appendResult259 ) * _WaveFrequency ) ).xy;
			float waveAmplitude255 = _WaveAmplitude;
			float2 localUVDistort255 = UVDistort255( UV255 , sinAnim255 , waveAmplitude255 );
			float2 UVDistortion277 = localUVDistort255;
			float4 tex2DNode251 = tex2D( _FoamTexture, ( UVDistortion277 * _BigerFoamScale ) );
			float4 BigNoise124 = ( ( 1.0 - ( tex2DNode251 * tex2DNode251.a ) ) * _BiggerFoamColorValue );
			float cos295 = cos( _SmmaerNoiseRotation );
			float sin295 = sin( _SmmaerNoiseRotation );
			float2 rotator295 = mul( ( _SmallerFoamScale * UVDistortion277 ) - float2( 0,0 ) , float2x2( cos295 , -sin295 , sin295 , cos295 )) + float2( 0,0 );
			float4 tex2DNode282 = tex2D( _FoamTexture, rotator295 );
			float4 SmallerNoise289 = ( _SmallerFoamColorValue * ( 1.0 - ( tex2DNode282 * tex2DNode282.a ) ) );
			float4 WaterColor171 = saturate( ( ( Color97 + BigNoise124 ) + SmallerNoise289 ) );
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float screenDepth180 = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE( _CameraDepthTexture, ase_screenPosNorm.xy ));
			float distanceDepth180 = saturate( abs( ( screenDepth180 - LinearEyeDepth( ase_screenPosNorm.z ) ) / ( _DepthDistance ) ) );
			float4 Depth183 = ( pow( ( 1.0 - distanceDepth180 ) , _DepthPower ) * _DepthColor );
			o.Albedo = saturate( ( WaterColor171 + Depth183 ) ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18301
376;117;846;627;2341.374;-843.031;1.898227;True;False
Node;AmplifyShaderEditor.CommentaryNode;278;-3817.573,138.0681;Inherit;False;2190.533;591.1674;Comment;16;264;277;132;267;64;265;133;259;257;136;258;260;261;266;262;255;World UV Distortion;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;316;-2406.263,-3873.442;Inherit;False;1967.95;718.3005;Comment;15;301;302;303;305;304;314;313;306;308;309;310;311;315;312;300;MultiColor;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldPosInputsNode;267;-3767.572,353.4805;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldPosInputsNode;301;-2356.263,-3405.351;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DynamicAppendNode;257;-3455.399,323.9435;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;259;-3454.96,457.5828;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;302;-2174.271,-3420.875;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;64;-3265.758,264.9114;Inherit;False;Property;_FoamSpeed;Foam Speed;6;0;Create;True;0;0;False;1;Header(Foam Wave);False;0.2;0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;260;-3457.156,602.0869;Inherit;False;Property;_WaveFrequency;WaveFrequency;7;0;Create;True;0;0;False;0;False;1;0.6;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;258;-3307.399,372.9433;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleTimeNode;264;-3227.186,188.0681;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;132;-2607.346,285.9927;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;261;-3100.306,475.0686;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleAddOpNode;305;-2037.644,-3391.02;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;304;-2322.205,-3533.387;Inherit;False;Property;_HueShiftSpeed;Hue Shift Speed;17;0;Create;True;0;0;False;1;Header(Invincible);False;1;0.5;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.TimeNode;303;-2321.164,-3702.24;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;133;-2429.348,330.9926;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;265;-2961.186,217.0679;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;314;-2156.295,-3271.141;Inherit;False;Property;_MultiColorPower;MultiColor Power;18;0;Create;True;0;0;False;0;False;0;0.01;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;136;-2303.572,393.9333;Inherit;False;WolrdPlannarUV;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;266;-2426.79,613.2354;Inherit;False;Property;_WaveAmplitude;Wave Amplitude;8;0;Create;True;0;0;False;0;False;0;0.3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;262;-2806.186,440.0674;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;313;-1876.275,-3416.752;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;306;-2004.855,-3610.056;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;247;-2208.304,-3001.118;Inherit;False;1709.125;859.209;Comment;17;240;231;237;238;244;246;230;243;1;236;97;232;249;250;297;298;299;Colorise Distance ;1,1,1,1;0;0
Node;AmplifyShaderEditor.CustomExpressionNode;255;-2127.275,519.1475;Inherit;False;half2 uvDistort = ((sin(0.9*sinAnim.xy) + sin(1.33*sinAnim.xy+3.14) + sin(2.4*sinAnim.xy+5.3))/3) * waveAmplitude@$$UV += uvDistort.xy@$return UV@;2;False;3;True;UV;FLOAT2;0,0;In;;Inherit;False;True;sinAnim;FLOAT2;0,0;In;;Inherit;False;True;waveAmplitude;FLOAT;1;In;;Inherit;False;UV Distort;True;False;0;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;308;-1786.307,-3605.256;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;309;-1610.881,-3704.233;Inherit;True;Simple HUE;-1;;13;32abb5f0db087604486c2db83a2e817a;0;1;1;FLOAT;0;False;4;FLOAT3;6;FLOAT;7;FLOAT;5;FLOAT;8
Node;AmplifyShaderEditor.CommentaryNode;268;-1374.849,76.84882;Inherit;False;1899.537;710.4795;Comment;20;279;124;114;115;166;254;251;253;66;281;282;283;284;285;286;287;288;289;295;296;Foam;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;277;-1851.037,536.597;Inherit;False;UVDistortion;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.Vector3Node;231;-2154.538,-2421.708;Inherit;False;Global;ColoriseOrigin;Colorise Origin;19;1;[HideInInspector];Create;True;0;0;False;0;False;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldPosInputsNode;240;-2158.304,-2593.871;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;284;-1183.98,359.2482;Inherit;False;Property;_SmallerFoamScale;Smaller Foam Scale;4;0;Create;True;0;0;False;0;False;0;0.25;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;279;-1169.54,459.8414;Inherit;False;277;UVDistortion;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;250;-1950.924,-2498.321;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RGBToHSVNode;310;-1371.242,-3712.499;Inherit;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;66;-1169.35,581.7239;Inherit;False;Property;_BigerFoamScale;Biger Foam  Scale;2;0;Create;True;0;0;False;0;False;3.27;0.125;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;311;-1129.99,-3823.442;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.8;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;315;-1089.484,-3539.889;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.6;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;283;-875.8416,360.3147;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;296;-894.1714,115.4555;Inherit;False;Property;_SmmaerNoiseRotation;Smmaer Noise Rotation;5;0;Create;True;0;0;False;0;False;0;90;0;360;0;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;281;-1244.031,142.17;Inherit;True;Property;_FoamTexture;Foam Texture;0;0;Create;True;0;0;False;0;False;None;1d2d37743b4a417489980511e11c0fd8;False;white;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;253;-788.3338,547.7252;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DotProductOpNode;249;-1813.279,-2488.488;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;232;-1865.058,-2625.435;Inherit;False;Global;ColoriseDistance;Colorise Distance;19;0;Create;True;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.HSVToRGBNode;312;-902.879,-3698.262;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleDivideOpNode;237;-1632.885,-2505.762;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RotatorNode;295;-716.0349,222.155;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;246;-1562.156,-2257.909;Inherit;False;Property;_Max;Max;16;0;Create;True;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;251;-572.6336,451.8936;Inherit;True;Property;_Tex;Tex;0;0;Create;True;0;0;False;0;False;-1;5930a2cf4b06d4747878f6bdaad8d95f;1d2d37743b4a417489980511e11c0fd8;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;244;-1559.156,-2327.909;Inherit;False;Property;_Min;Min;15;0;Create;True;0;0;False;1;Header(Colorise);False;0;0.4;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;282;-519.1116,170.1209;Inherit;True;Property;_TextureSample0;Texture Sample 0;14;0;Create;True;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;1;-1434.077,-2883.879;Inherit;False;Global;MainColor;MainColor;0;0;Create;True;0;0;False;0;False;0.3443396,0.8108984,1,1;0.28,0.58,0.645,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;300;-662.3138,-3685.956;Inherit;False;MultiColor;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SmoothstepOpNode;243;-1251.156,-2483.909;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;254;-223.0925,406.3514;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;230;-1508.95,-2682.874;Inherit;False;Global;SecondaryColor;SecondaryColor;18;0;Create;True;0;0;False;0;False;0,0,0,0;0.8490566,0.260324,0.260324,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;115;-189.3477,526.186;Inherit;False;Property;_BiggerFoamColorValue;BiggerFoamColorValue;1;0;Create;True;0;0;False;1;Header(Bigger Foam);False;0.5;-0.2;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;166;-76.73848,367.4402;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;298;-1139.329,-2361.52;Inherit;False;Global;Invincible;Invincible;17;0;Create;True;0;0;False;1;Header(Invincible);False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;299;-1043.734,-2473.034;Inherit;False;300;MultiColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;236;-1064.35,-2614.845;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;285;-207.1981,216.5037;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;114;159.2872,426.0929;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;287;-134.5854,117.8847;Inherit;False;Property;_SmallerFoamColorValue;Smaller Foam Color Value;3;0;Create;True;0;0;False;1;Header(Smaller Noise);False;0;0.25;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;128;-1864.898,1078.89;Inherit;False;1908.938;681.4824;Comment;10;88;75;87;126;85;135;214;216;226;223;Vertex Offset;1,1,1,1;0;0
Node;AmplifyShaderEditor.LerpOp;297;-819.3243,-2570.733;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;201;-1987.806,-688.6493;Inherit;False;1834.008;505.6926;Comment;8;182;180;181;189;188;192;193;183;Depth;1,1,1,1;0;0
Node;AmplifyShaderEditor.OneMinusNode;286;-56.19806,207.5037;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;85;-1844.144,1458.375;Inherit;False;Property;_DisplacementSpeed;Displacement Speed;9;0;Create;True;0;0;False;1;Header(Vertex Displacement);False;0.365928;3.23;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;288;154.0723,169.4324;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;172;-1211.669,-1860.297;Inherit;False;1559.394;526.1127;Comment;7;270;171;167;239;153;290;293;Water Color;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;182;-1937.806,-638.6494;Inherit;False;Property;_DepthDistance;Depth Distance;11;0;Create;True;0;0;False;1;Header(Depth);False;1;4.5;0;30;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;124;340.8236,408.1128;Inherit;False;BigNoise;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;97;-687.5786,-2396.723;Inherit;False;Color;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;289;305.0723,185.4324;Inherit;False;SmallerNoise;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;226;-1552.666,1440.897;Inherit;False;WaterSpeed;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;239;-867.9409,-1748.026;Inherit;False;97;Color;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;153;-915.889,-1603.984;Inherit;False;124;BigNoise;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;135;-1815.336,1566.36;Inherit;False;136;WolrdPlannarUV;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DepthFade;180;-1586.867,-624.8524;Inherit;False;True;True;True;2;1;FLOAT3;0,0,0;False;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;189;-1382.368,-499.3765;Inherit;False;Property;_DepthPower;Depth Power;12;0;Create;True;0;0;False;0;False;1;5;0.001;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;270;-617.0634,-1689.549;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;223;-1340.495,1583.632;Inherit;False;Water Pan;-1;;14;9b382295680ba6541acffd5b0c157bc3;0;2;2;FLOAT2;0,0;False;1;FLOAT;5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.OneMinusNode;181;-1267.424,-620.4898;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;290;-596.0001,-1566.542;Inherit;False;289;SmallerNoise;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;293;-365.9762,-1711.031;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;216;-997.2224,1234.847;Inherit;False;WaterMovement;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ColorNode;192;-1103.057,-394.9568;Inherit;False;Property;_DepthColor;Depth Color;13;0;Create;True;0;0;False;0;False;0.007876454,0.1974638,0.5566038,0;0.735849,0.735849,0.735849,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PowerNode;188;-1030.422,-561.4694;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;88;-1094.177,1401.652;Inherit;False;Property;_DisplacementScale;Displacement Scale;10;0;Create;True;0;0;False;0;False;5;0.05;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;193;-786.5875,-524.3;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;75;-801.33,1262.565;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;5.06;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;167;-88.29238,-1743.6;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;171;123.7261,-1762.555;Inherit;False;WaterColor;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;183;-377.7977,-540.3688;Inherit;False;Depth;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;214;-534.4202,1244.97;Inherit;False;Noise;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;185;-186.1526,-971.2132;Inherit;False;183;Depth;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;170;-195.9782,-1135.899;Inherit;False;171;WaterColor;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.DynamicAppendNode;87;-326.281,1270.625;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleAddOpNode;184;131.4032,-1097.44;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;126;-135.7604,1281.645;Inherit;False;VertexOffset;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ClampOpNode;238;-1476.234,-2490.601;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;212;-413.1212,-90.74922;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;276;-255.4151,-103.6456;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;215;-749.959,-126.0057;Inherit;False;214;Noise;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;127;495.0584,-722.6683;Inherit;False;126;VertexOffset;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SaturateNode;186;516.3317,-1048.183;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;213;-759.9858,-31.63273;Inherit;False;Property;_NoisePower;Noise Power;14;0;Create;True;0;0;False;1;Header(Noise);False;1;0.2;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;274;780.5098,-1032.953;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;Custom/WaterMobile;False;False;False;False;False;True;True;True;True;False;True;True;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;False;0;False;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;257;0;267;1
WireConnection;257;1;267;2
WireConnection;259;1;267;2
WireConnection;259;2;267;3
WireConnection;302;0;301;1
WireConnection;258;0;257;0
WireConnection;258;1;259;0
WireConnection;261;0;258;0
WireConnection;261;1;260;0
WireConnection;305;0;302;0
WireConnection;305;1;301;3
WireConnection;133;0;132;1
WireConnection;133;1;132;3
WireConnection;265;0;264;0
WireConnection;265;1;64;0
WireConnection;136;0;133;0
WireConnection;262;0;265;0
WireConnection;262;1;261;0
WireConnection;313;0;305;0
WireConnection;313;1;314;0
WireConnection;306;0;303;2
WireConnection;306;1;304;0
WireConnection;255;0;136;0
WireConnection;255;1;262;0
WireConnection;255;2;266;0
WireConnection;308;0;313;0
WireConnection;308;1;306;0
WireConnection;309;1;308;0
WireConnection;277;0;255;0
WireConnection;250;0;231;0
WireConnection;250;1;240;0
WireConnection;310;0;309;6
WireConnection;311;0;310;2
WireConnection;315;0;310;3
WireConnection;283;0;284;0
WireConnection;283;1;279;0
WireConnection;253;0;279;0
WireConnection;253;1;66;0
WireConnection;249;0;250;0
WireConnection;249;1;250;0
WireConnection;312;0;310;1
WireConnection;312;1;311;0
WireConnection;312;2;315;0
WireConnection;237;0;232;0
WireConnection;237;1;249;0
WireConnection;295;0;283;0
WireConnection;295;2;296;0
WireConnection;251;0;281;0
WireConnection;251;1;253;0
WireConnection;282;0;281;0
WireConnection;282;1;295;0
WireConnection;300;0;312;0
WireConnection;243;0;237;0
WireConnection;243;1;244;0
WireConnection;243;2;246;0
WireConnection;254;0;251;0
WireConnection;254;1;251;4
WireConnection;166;0;254;0
WireConnection;236;0;1;0
WireConnection;236;1;230;0
WireConnection;236;2;243;0
WireConnection;285;0;282;0
WireConnection;285;1;282;4
WireConnection;114;0;166;0
WireConnection;114;1;115;0
WireConnection;297;0;236;0
WireConnection;297;1;299;0
WireConnection;297;2;298;0
WireConnection;286;0;285;0
WireConnection;288;0;287;0
WireConnection;288;1;286;0
WireConnection;124;0;114;0
WireConnection;97;0;297;0
WireConnection;289;0;288;0
WireConnection;226;0;85;0
WireConnection;180;0;182;0
WireConnection;270;0;239;0
WireConnection;270;1;153;0
WireConnection;223;2;135;0
WireConnection;223;1;226;0
WireConnection;181;0;180;0
WireConnection;293;0;270;0
WireConnection;293;1;290;0
WireConnection;216;0;223;0
WireConnection;188;0;181;0
WireConnection;188;1;189;0
WireConnection;193;0;188;0
WireConnection;193;1;192;0
WireConnection;75;0;216;0
WireConnection;75;1;88;0
WireConnection;167;0;293;0
WireConnection;171;0;167;0
WireConnection;183;0;193;0
WireConnection;214;0;75;0
WireConnection;87;1;214;0
WireConnection;184;0;170;0
WireConnection;184;1;185;0
WireConnection;126;0;87;0
WireConnection;238;0;237;0
WireConnection;212;0;215;0
WireConnection;212;1;213;0
WireConnection;276;0;212;0
WireConnection;186;0;184;0
WireConnection;274;0;186;0
WireConnection;274;11;127;0
ASEEND*/
//CHKSM=A49802D76838C61BD338FD81F2CC33DED039760A