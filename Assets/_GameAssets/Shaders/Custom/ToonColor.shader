// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "URP_Custom/ToonColorUnlit"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,0)
		_MainTexture("Main Texture", 2D) = "white" {}
		_Ramp("Ramp", 2D) = "white" {}
		_ScaleandOffset("Scale and Offset", Float) = 0.5
		_RampStrength("Ramp Strength", Range( 0 , 1)) = 1
		_RimColor("Rim Color", Color) = (0,0.7845664,1,0)
		_RimOffset("Rim Offset", Float) = 1
		_RimPower("Rim Power", Range( 0 , 1)) = 0
		_Saturation("Saturation", Range( 0 , 3)) = 1
		_Brightness("Brightness", Range( 0 , 3)) = 1
		[Header(Highlight)][Toggle(_HIGHLIGHT_ON)] _Highlight("Highlight", Float) = 0
		_Amount("Amount", Range( 0 , 1)) = 0
		_HighlightColor("HighlightColor", Color) = (1,1,1,1)
		_Bias("Bias", Float) = 0
		_Scale("Scale", Float) = 0
		_Power("Power", Float) = 0
		[Header(Invincible)]_HueShiftSpeed("Hue Shift Speed", Range( 0 , 2)) = 1
		[Toggle(_CANINVINCIBLE_ON)] _canInvincible("can Invincible", Float) = 0
		_InvincibleWidth("InvincibleWidth", Range( 0 , 1)) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#pragma shader_feature_local _HIGHLIGHT_ON
		#pragma shader_feature_local _CANINVINCIBLE_ON
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
			float2 uv_texcoord;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform float _Bias;
		uniform float _Scale;
		uniform float _Power;
		uniform float _Amount;
		uniform sampler2D _Ramp;
		uniform float _ScaleandOffset;
		uniform float _RampStrength;
		uniform float4 _Color;
		uniform float _InvincibleWidth;
		uniform float _HueShiftSpeed;
		uniform float Invincible;
		uniform sampler2D _MainTexture;
		uniform float4 _MainTexture_ST;
		uniform float _RimOffset;
		uniform float _RimPower;
		uniform float4 _RimColor;
		uniform float _Saturation;
		uniform float _Brightness;
		uniform float4 _HighlightColor;


		float3 HSVToRGB( float3 c )
		{
			float4 K = float4( 1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0 );
			float3 p = abs( frac( c.xxx + K.xyz ) * 6.0 - K.www );
			return c.z * lerp( K.xxx, saturate( p - K.xxx ), c.y );
		}


		float3 RGBToHSV(float3 c)
		{
			float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
			float4 p = lerp( float4( c.bg, K.wz ), float4( c.gb, K.xy ), step( c.b, c.g ) );
			float4 q = lerp( float4( p.xyw, c.r ), float4( c.r, p.yzx ), step( p.x, c.r ) );
			float d = q.x - min( q.w, q.y );
			float e = 1.0e-10;
			return float3( abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
		}

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = Unity_SafeNormalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float3 ase_normWorldNormal = normalize( ase_worldNormal );
			float fresnelNdotV90 = dot( ase_normWorldNormal, ase_worldViewDir );
			float fresnelNode90 = ( _Bias + _Scale * pow( max( 1.0 - fresnelNdotV90 , 0.0001 ), _Power ) );
			#ifdef _HIGHLIGHT_ON
				float staticSwitch104 = _Amount;
			#else
				float staticSwitch104 = 0.0;
			#endif
			float temp_output_109_0 = ( fresnelNode90 * staticSwitch104 );
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = Unity_SafeNormalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float dotResult8 = dot( ase_normWorldNormal , ase_worldlightDir );
			float world_lightDir9 = dotResult8;
			float2 temp_cast_0 = ((world_lightDir9*_ScaleandOffset + _ScaleandOffset)).xx;
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float3 break165 = ( ase_vertex3Pos * float3( 1,1,1 ) );
			float saferPower161 = max( ( ( ( break165.x * 0.5 ) + break165.y ) * _InvincibleWidth ) , 0.0001 );
			float3 hsvTorgb3_g1 = HSVToRGB( float3(( pow( saferPower161 , 0.7 ) - ( _Time.y * _HueShiftSpeed ) ),1.0,1.0) );
			float3 hsvTorgb144 = RGBToHSV( hsvTorgb3_g1 );
			float3 hsvTorgb145 = HSVToRGB( float3(hsvTorgb144.x,( hsvTorgb144.y * 0.8 ),hsvTorgb144.z) );
			float3 MultiColor133 = hsvTorgb145;
			float4 lerpResult132 = lerp( _Color , float4( MultiColor133 , 0.0 ) , Invincible);
			#ifdef _CANINVINCIBLE_ON
				float4 staticSwitch153 = lerpResult132;
			#else
				float4 staticSwitch153 = _Color;
			#endif
			float2 uv_MainTexture = i.uv_texcoord * _MainTexture_ST.xy + _MainTexture_ST.zw;
			float4 Color22 = ( staticSwitch153 * tex2D( _MainTexture, uv_MainTexture ) );
			float4 Shadow14 = ( saturate( ( tex2D( _Ramp, temp_cast_0 ) + (1.0 + (_RampStrength - 0.0) * (0.0 - 1.0) / (1.0 - 0.0)) ) ) * Color22 );
			UnityGI gi29 = gi;
			float3 diffNorm29 = ase_worldNormal;
			gi29 = UnityGI_Base( data, 1, diffNorm29 );
			float3 indirectDiffuse29 = gi29.indirect.diffuse + diffNorm29 * 0.0001;
			float4 Lighting28 = ( ase_lightColor * ( Shadow14 * float4( ( indirectDiffuse29 + ase_lightAtten ) , 0.0 ) ) );
			float dotResult18 = dot( ase_normWorldNormal , ase_worldViewDir );
			float world_viewDir16 = dotResult18;
			float4 RimLight42 = ( saturate( ( pow( ( 1.0 - saturate( ( world_viewDir16 + _RimOffset ) ) ) , (1.0 + (_RimPower - 0.0) * (0.0 - 1.0) / (1.0 - 0.0)) ) * ( world_lightDir9 * ase_lightAtten ) ) ) * ( ase_lightColor * _RimColor ) );
			float3 hsvTorgb71 = RGBToHSV( ( Lighting28 + RimLight42 ).rgb );
			float3 hsvTorgb74 = HSVToRGB( float3(hsvTorgb71.x,( _Saturation * hsvTorgb71.y ),( hsvTorgb71.z * _Brightness )) );
			float3 Saturation75 = hsvTorgb74;
			float clampResult176 = clamp( temp_output_109_0 , 0.0 , 1.0 );
			float4 Highlight92 = ( _HighlightColor * clampResult176 );
			float4 Main105 = saturate( ( float4( saturate( ( ( 1.0 - temp_output_109_0 ) * Saturation75 ) ) , 0.0 ) + Highlight92 ) );
			c.rgb = Main105.rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows noambient nolightmap  nodynlightmap nodirlightmap nometa noforwardadd 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18301
471;75;846;627;-356.5123;1465.952;1.51794;True;False
Node;AmplifyShaderEditor.PosVertexDataNode;160;-4363.717,-1287.011;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;163;-4132.817,-1190.919;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;1,1,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;148;-3987.53,-1753.498;Inherit;False;1961.636;554.1476;Comment;15;141;138;143;136;144;146;145;133;149;150;151;152;154;155;161;Multi Color;1,1,1,1;0;0
Node;AmplifyShaderEditor.BreakToComponentsNode;165;-3993.718,-1166.22;Inherit;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;150;-3758.538,-1411.874;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;149;-3629.911,-1383.019;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;155;-3735.056,-1232.358;Inherit;False;Property;_InvincibleWidth;InvincibleWidth;18;0;Create;True;0;0;False;0;False;1;0.509;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TimeNode;138;-3905.432,-1693.239;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;152;-3843.901,-1511.872;Inherit;False;Property;_HueShiftSpeed;Hue Shift Speed;16;0;Create;True;0;0;False;1;Header(Invincible);False;1;0.75;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;154;-3470.058,-1351.857;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;151;-3589.122,-1601.055;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;161;-3328.608,-1399.113;Inherit;False;True;2;0;FLOAT;0;False;1;FLOAT;0.7;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;143;-3338.35,-1619.002;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;136;-3202.133,-1689.217;Inherit;True;Simple HUE;-1;;1;32abb5f0db087604486c2db83a2e817a;0;1;1;FLOAT;0;False;4;FLOAT3;6;FLOAT;7;FLOAT;5;FLOAT;8
Node;AmplifyShaderEditor.RGBToHSVNode;144;-2910.509,-1697.498;Inherit;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.CommentaryNode;5;-2011.504,-430.861;Inherit;False;692.8512;373.0349;Comment;4;9;8;7;6;World Light Dir;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;146;-2687.873,-1507.041;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.8;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;6;-1961.504,-233.1913;Inherit;False;True;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldNormalVector;7;-1955.911,-390.4044;Inherit;False;True;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.HSVToRGBNode;145;-2487.146,-1689.261;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RegisterLocalVarNode;133;-2249.892,-1687.31;Inherit;False;MultiColor;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DotProductOpNode;8;-1684.977,-263.8696;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;135;-2479.201,-1099.201;Inherit;False;1254.082;585.2682;Comment;8;131;132;17;134;22;57;56;153;Albedo;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;15;-1960.334,90.91255;Inherit;False;656.3043;408.9857;Comment;4;21;20;18;16;World View Dir;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldNormalVector;20;-1910.334,140.9126;Inherit;False;True;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ColorNode;17;-2394.36,-1049.201;Inherit;False;Property;_Color;Color;0;0;Create;True;0;0;False;0;False;1,1,1,0;0.2132001,0.9056604,0.1153435,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;131;-2429.201,-681.1842;Inherit;False;Global;Invincible;Invincible;17;0;Create;True;0;0;False;1;Header(Invincible);False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;21;-1894.134,311.898;Inherit;False;World;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.CommentaryNode;34;-1013.486,-989.7547;Inherit;False;1633.961;748.2473;Shadow;11;24;23;14;13;11;12;10;60;61;63;64;Shadow;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;134;-2400.026,-866.4347;Inherit;False;133;MultiColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;9;-1542.651,-262.2924;Inherit;False;world_lightDir;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;10;-963.4856,-831.5247;Inherit;False;Property;_ScaleandOffset;Scale and Offset;3;0;Create;True;0;0;False;0;False;0.5;0.3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;132;-2066.536,-907.9554;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.DotProductOpNode;18;-1688.769,260.5621;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;11;-961.2826,-939.7547;Inherit;False;9;world_lightDir;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;153;-1871.2,-889.248;Inherit;False;Property;_canInvincible;can Invincible;17;0;Create;True;0;0;False;0;False;0;0;1;True;;Toggle;2;Key0;Key1;Create;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;56;-2074.182,-743.9331;Inherit;True;Property;_MainTexture;Main Texture;1;0;Create;True;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;16;-1528.03,255.0412;Inherit;False;world_viewDir;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;60;-619.0865,-594.4404;Inherit;False;Property;_RampStrength;Ramp Strength;4;0;Create;True;0;0;False;0;False;1;0.8;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;52;-1186.679,323.9273;Inherit;False;1958.025;697.4697;Comment;18;37;38;39;40;41;35;36;42;47;46;45;44;48;49;51;50;53;55;Rim Light;1,1,1,1;0;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;12;-722.495,-906.9991;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;13;-533.9486,-870.6877;Inherit;True;Property;_Ramp;Ramp;2;0;Create;True;0;0;False;0;False;-1;None;91559b72ba487d347a1a33f0bfae6a4d;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;35;-1136.679,373.9272;Inherit;False;16;world_viewDir;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;37;-1084.048,497.8252;Inherit;False;Property;_RimOffset;Rim Offset;6;0;Create;True;0;0;False;0;False;1;0.55;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;64;-302.9544,-620.525;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;1;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;57;-1580.363,-814.8438;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;61;-205.7759,-847.9419;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;36;-857.1724,382.9702;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;22;-1418.119,-810.4575;Inherit;False;Color;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;38;-693.2499,428.9253;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;24;6.182259,-669.1971;Inherit;False;22;Color;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;63;-48.77594,-842.9419;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;41;-1099.65,612.8254;Inherit;False;Property;_RimPower;Rim Power;7;0;Create;True;0;0;False;0;False;0;0.644;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;49;-510.9542,710.1827;Inherit;False;9;world_lightDir;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;39;-516.4499,432.8253;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightAttenuation;50;-518.2636,793.5104;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;55;-716.0234,579.5638;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;1;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;23;216.6276,-819.0717;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;33;-2387.069,628.6304;Inherit;False;1055.327;484.6177;Comment;8;26;25;28;30;29;27;31;32;Lighting;1,1,1,1;0;0
Node;AmplifyShaderEditor.PowerNode;40;-288.95,436.7252;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.IndirectDiffuseLighting;29;-2337.069,924.7053;Inherit;False;Tangent;1;0;FLOAT3;0,0,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LightAttenuation;30;-2311.854,1011.248;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;51;-309.2133,724.8016;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;14;392.0919,-805.8548;Inherit;False;Shadow;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;27;-2311,808.1245;Inherit;False;14;Shadow;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;45;-51.23454,809.3972;Inherit;False;Property;_RimColor;Rim Color;5;0;Create;True;0;0;False;0;False;0,0.7845664,1,0;0,0.7845664,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;48;-43.14816,435.3474;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightColorNode;44;-60.85447,643.761;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleAddOpNode;31;-2052.729,938.6913;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;32;-1914.942,851.3231;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;53;130.7237,443.9963;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;46;177.6299,667.4045;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LightColorNode;25;-1979.949,678.6304;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-1740.958,727.2884;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;47;326.0596,443.0984;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;85;-1879.997,-1634.665;Inherit;False;1357;458.2209;Comment;10;72;71;79;75;78;74;80;65;43;86;Saturation & Brightness;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;42;547.346,457.1174;Inherit;False;RimLight;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;28;-1555.742,719.2712;Inherit;False;Lighting;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;43;-1871.224,-1392.525;Inherit;False;42;RimLight;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;72;-1865.997,-1502.22;Inherit;False;28;Lighting;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;115;-25.63928,-1933.639;Inherit;False;2243.939;643.0818;Comment;12;110;104;99;109;100;90;98;95;97;92;173;176;Highlight;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;86;-1695.763,-1468.175;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;79;-1583.578,-1292.444;Inherit;False;Property;_Brightness;Brightness;9;0;Create;True;0;0;False;0;False;1;1;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;65;-1585.265,-1584.665;Inherit;False;Property;_Saturation;Saturation;8;0;Create;True;0;0;False;0;False;1;1;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.RGBToHSVNode;71;-1551.099,-1465.813;Inherit;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;97;63.95427,-1761.883;Inherit;False;Property;_Scale;Scale;14;0;Create;True;0;0;False;0;False;0;5.72;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;98;59.95427,-1691.883;Inherit;False;Property;_Power;Power;15;0;Create;True;0;0;False;0;False;0;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;110;24.36072,-1445.797;Inherit;False;Property;_Amount;Amount;11;0;Create;True;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;95;59.95427,-1837.883;Inherit;False;Property;_Bias;Bias;13;0;Create;True;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FresnelNode;90;297.5717,-1782.996;Inherit;False;Standard;WorldNormal;ViewDir;True;True;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;104;398.5403,-1573.432;Inherit;False;Property;_Highlight;Highlight;10;0;Create;True;0;0;False;1;Header(Highlight);False;0;0;1;True;;Toggle;2;Key0;Key1;Create;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;78;-1239.578,-1554.444;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;80;-1219.578,-1315.444;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;109;727.3675,-1661.746;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.HSVToRGBNode;74;-1058.997,-1471.22;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RegisterLocalVarNode;75;-746.9966,-1466.22;Inherit;False;Saturation;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ClampOpNode;176;1075.742,-1604.22;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;99;965.0427,-1883.639;Inherit;False;Property;_HighlightColor;HighlightColor;12;0;Create;True;0;0;False;0;False;1,1,1,1;0.535849,1,0.3915094,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;116;793.2205,-1041.566;Inherit;False;1249.829;506.3972;Comment;5;105;103;91;169;19;Main;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;19;850.9593,-955.6194;Inherit;True;75;Saturation;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.OneMinusNode;173;961.7068,-1428.352;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;100;1367.43,-1713.557;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;171;1069.869,-1158.388;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;92;2013.516,-1668.603;Inherit;False;Highlight;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;91;825.8668,-747.0875;Inherit;True;92;Highlight;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;178;1324.853,-1065.57;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;169;1329.005,-851.5406;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;103;1609.359,-843.1376;Inherit;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;105;1849.615,-828.597;Inherit;False;Main;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WorldPosInputsNode;141;-3980.531,-1425.35;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.GetLocalVarNode;106;1117.159,107.1067;Inherit;False;105;Main;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.ObjectScaleNode;162;-4358.871,-1066.266;Inherit;False;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;130;1368.887,-120.9908;Float;False;True;-1;2;ASEMaterialInspector;0;0;CustomLighting;URP_Custom/ToonColorUnlit;False;False;False;False;True;False;True;True;True;False;True;True;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Absolute;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;163;0;160;0
WireConnection;165;0;163;0
WireConnection;150;0;165;0
WireConnection;149;0;150;0
WireConnection;149;1;165;1
WireConnection;154;0;149;0
WireConnection;154;1;155;0
WireConnection;151;0;138;2
WireConnection;151;1;152;0
WireConnection;161;0;154;0
WireConnection;143;0;161;0
WireConnection;143;1;151;0
WireConnection;136;1;143;0
WireConnection;144;0;136;6
WireConnection;146;0;144;2
WireConnection;145;0;144;1
WireConnection;145;1;146;0
WireConnection;145;2;144;3
WireConnection;133;0;145;0
WireConnection;8;0;7;0
WireConnection;8;1;6;0
WireConnection;9;0;8;0
WireConnection;132;0;17;0
WireConnection;132;1;134;0
WireConnection;132;2;131;0
WireConnection;18;0;20;0
WireConnection;18;1;21;0
WireConnection;153;1;17;0
WireConnection;153;0;132;0
WireConnection;16;0;18;0
WireConnection;12;0;11;0
WireConnection;12;1;10;0
WireConnection;12;2;10;0
WireConnection;13;1;12;0
WireConnection;64;0;60;0
WireConnection;57;0;153;0
WireConnection;57;1;56;0
WireConnection;61;0;13;0
WireConnection;61;1;64;0
WireConnection;36;0;35;0
WireConnection;36;1;37;0
WireConnection;22;0;57;0
WireConnection;38;0;36;0
WireConnection;63;0;61;0
WireConnection;39;0;38;0
WireConnection;55;0;41;0
WireConnection;23;0;63;0
WireConnection;23;1;24;0
WireConnection;40;0;39;0
WireConnection;40;1;55;0
WireConnection;51;0;49;0
WireConnection;51;1;50;0
WireConnection;14;0;23;0
WireConnection;48;0;40;0
WireConnection;48;1;51;0
WireConnection;31;0;29;0
WireConnection;31;1;30;0
WireConnection;32;0;27;0
WireConnection;32;1;31;0
WireConnection;53;0;48;0
WireConnection;46;0;44;0
WireConnection;46;1;45;0
WireConnection;26;0;25;0
WireConnection;26;1;32;0
WireConnection;47;0;53;0
WireConnection;47;1;46;0
WireConnection;42;0;47;0
WireConnection;28;0;26;0
WireConnection;86;0;72;0
WireConnection;86;1;43;0
WireConnection;71;0;86;0
WireConnection;90;1;95;0
WireConnection;90;2;97;0
WireConnection;90;3;98;0
WireConnection;104;0;110;0
WireConnection;78;0;65;0
WireConnection;78;1;71;2
WireConnection;80;0;71;3
WireConnection;80;1;79;0
WireConnection;109;0;90;0
WireConnection;109;1;104;0
WireConnection;74;0;71;1
WireConnection;74;1;78;0
WireConnection;74;2;80;0
WireConnection;75;0;74;0
WireConnection;176;0;109;0
WireConnection;173;0;109;0
WireConnection;100;0;99;0
WireConnection;100;1;176;0
WireConnection;171;0;173;0
WireConnection;171;1;19;0
WireConnection;92;0;100;0
WireConnection;178;0;171;0
WireConnection;169;0;178;0
WireConnection;169;1;91;0
WireConnection;103;0;169;0
WireConnection;105;0;103;0
WireConnection;130;13;106;0
ASEEND*/
//CHKSM=AA2E91CA411923D157A3271A14FDBB6BB5EF6C19