// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Bulge"
{
	Properties
	{
		_Bulge_Width("Bulge_Width", Float) = 4
		_Smoothness("_Smoothness", Range( 0 , 1)) = 0
		_Lerp_Intensity("Lerp_Intensity", Float) = 8.5
		_MainColor_A("MainColor_A", Color) = (0,0.6313725,1,0)
		_Color_Lerp("Color_Lerp", Range( 0 , 1)) = 0.4664364
		_MainColor_B("MainColor_B", Color) = (1,0.7031918,0,0)
		_Panner_1("Panner_1", Range( 0 , 1)) = 0.4664364
		_1_Color("1_Color", Range( 0 , 1)) = 0
		_Panner_2("Panner_2", Range( 0 , 1)) = 0.4664364
		_2_Color("2_Color", Range( 0 , 1)) = 0
		_Panner_3("Panner_3", Range( 0 , 1)) = 0.4664364
		_3_Color("3_Color", Range( 0 , 1)) = 0
		_Panner_4("Panner_4", Range( 0 , 1)) = 0.4664364
		_4_Color("4_Color", Range( 0 , 1)) = 0
		_Panner_5("Panner_5", Range( 0 , 1)) = 0.4664364
		_5_Color("5_Color", Range( 0 , 1)) = 0
		_Panner_6("Panner_6", Range( 0 , 1)) = 0.4664364
		_6_Color("6_Color", Range( 0 , 1)) = 0
		_Stripes_Number("Stripes_Number", Float) = 10
		_Stripes_Color("Stripes_Color", Color) = (1,0.6132548,0,0)
		_Stripes_Wet("Stripes_Wet", Float) = 2
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 4.6
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float _Panner_1;
		uniform float _Bulge_Width;
		uniform float _Panner_2;
		uniform float _Panner_3;
		uniform float _Panner_4;
		uniform float _Panner_5;
		uniform float _Panner_6;
		uniform float4 _MainColor_A;
		uniform float4 _MainColor_B;
		uniform float _Color_Lerp;
		uniform float _Lerp_Intensity;
		uniform float _1_Color;
		uniform float _2_Color;
		uniform float _3_Color;
		uniform float _4_Color;
		uniform float _5_Color;
		uniform float _6_Color;
		uniform float _Stripes_Number;
		uniform float _Stripes_Wet;
		uniform float4 _Stripes_Color;
		uniform float _Smoothness;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float V32 = v.texcoord.xy.y;
			float temp_output_399_0 = ( 1.0 - V32 );
			float smoothstepResult407 = smoothstep( 0.0 , 0.19 , ( ( temp_output_399_0 * temp_output_399_0 ) - (-0.5 + (0.25 - 0.0) * (1.5 - -0.5) / (1.0 - 0.0)) ));
			float Fade_Out405 = smoothstepResult407;
			float Bulge_width36 = _Bulge_Width;
			float temp_output_4_0 = ( ( (-0.5 + (_Panner_1 - 0.0) * (1.5 - -0.5) / (1.0 - 0.0)) - V32 ) * Bulge_width36 );
			float temp_output_7_0 = ( 1.0 - ( temp_output_4_0 * temp_output_4_0 ) );
			float Power_428 = 0.35;
			float temp_output_427_0 = saturate( ( temp_output_7_0 * Power_428 ) );
			float Def_1125 = temp_output_427_0;
			float temp_output_42_0 = ( ( (-0.5 + (_Panner_2 - 0.0) * (1.5 - -0.5) / (1.0 - 0.0)) - V32 ) * Bulge_width36 );
			float temp_output_44_0 = ( 1.0 - ( temp_output_42_0 * temp_output_42_0 ) );
			float temp_output_431_0 = saturate( ( temp_output_44_0 * Power_428 ) );
			float Def_2129 = temp_output_431_0;
			float temp_output_51_0 = ( ( (-0.5 + (_Panner_3 - 0.0) * (1.5 - -0.5) / (1.0 - 0.0)) - V32 ) * Bulge_width36 );
			float temp_output_53_0 = ( 1.0 - ( temp_output_51_0 * temp_output_51_0 ) );
			float temp_output_434_0 = saturate( ( temp_output_53_0 * Power_428 ) );
			float Def_3130 = temp_output_434_0;
			float temp_output_67_0 = ( ( (-0.5 + (_Panner_4 - 0.0) * (1.5 - -0.5) / (1.0 - 0.0)) - V32 ) * Bulge_width36 );
			float temp_output_70_0 = ( 1.0 - ( temp_output_67_0 * temp_output_67_0 ) );
			float temp_output_437_0 = saturate( ( temp_output_70_0 * Power_428 ) );
			float Def_4131 = temp_output_437_0;
			float temp_output_78_0 = ( ( (-0.5 + (_Panner_5 - 0.0) * (1.5 - -0.5) / (1.0 - 0.0)) - V32 ) * Bulge_width36 );
			float temp_output_81_0 = ( 1.0 - ( temp_output_78_0 * temp_output_78_0 ) );
			float temp_output_440_0 = saturate( ( temp_output_81_0 * Power_428 ) );
			float Def_5132 = temp_output_440_0;
			float temp_output_89_0 = ( ( (-0.5 + (_Panner_6 - 0.0) * (1.5 - -0.5) / (1.0 - 0.0)) - V32 ) * Bulge_width36 );
			float temp_output_92_0 = ( 1.0 - ( temp_output_89_0 * temp_output_89_0 ) );
			float temp_output_443_0 = saturate( ( temp_output_92_0 * Power_428 ) );
			float Def_6133 = temp_output_443_0;
			float3 ase_vertexNormal = v.normal.xyz;
			v.vertex.xyz += ( ( Fade_Out405 * ( Def_1125 + Def_2129 + Def_3130 + Def_4131 + Def_5132 + Def_6133 ) ) * ase_vertexNormal );
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float V32 = i.uv_texcoord.y;
			float Bulge_width36 = _Bulge_Width;
			float temp_output_4_0 = ( ( (-0.5 + (_Panner_1 - 0.0) * (1.5 - -0.5) / (1.0 - 0.0)) - V32 ) * Bulge_width36 );
			float temp_output_7_0 = ( 1.0 - ( temp_output_4_0 * temp_output_4_0 ) );
			float Power_428 = 0.35;
			float temp_output_427_0 = saturate( ( temp_output_7_0 * Power_428 ) );
			float smoothstepResult263 = smoothstep( 0.0 , 0.25 , temp_output_427_0);
			float clampResult341 = clamp( ( ( (-0.5 + (_Color_Lerp - 0.0) * (2.0 - -0.5) / (1.0 - 0.0)) - V32 ) * _Lerp_Intensity ) , 0.0 , 1.0 );
			float4 lerpResult302 = lerp( _MainColor_A , _MainColor_B , clampResult341);
			float4 Main_Color116 = lerpResult302;
			float4 Final_1200 = ( smoothstepResult263 * ( Main_Color116 * _1_Color ) );
			float temp_output_42_0 = ( ( (-0.5 + (_Panner_2 - 0.0) * (1.5 - -0.5) / (1.0 - 0.0)) - V32 ) * Bulge_width36 );
			float temp_output_44_0 = ( 1.0 - ( temp_output_42_0 * temp_output_42_0 ) );
			float temp_output_431_0 = saturate( ( temp_output_44_0 * Power_428 ) );
			float smoothstepResult271 = smoothstep( 0.0 , 0.25 , temp_output_431_0);
			float4 Final_2203 = ( smoothstepResult271 * ( Main_Color116 * _2_Color ) );
			float temp_output_51_0 = ( ( (-0.5 + (_Panner_3 - 0.0) * (1.5 - -0.5) / (1.0 - 0.0)) - V32 ) * Bulge_width36 );
			float temp_output_53_0 = ( 1.0 - ( temp_output_51_0 * temp_output_51_0 ) );
			float temp_output_434_0 = saturate( ( temp_output_53_0 * Power_428 ) );
			float smoothstepResult278 = smoothstep( 0.0 , 0.25 , temp_output_434_0);
			float4 Final_3204 = ( smoothstepResult278 * ( Main_Color116 * _3_Color ) );
			float temp_output_67_0 = ( ( (-0.5 + (_Panner_4 - 0.0) * (1.5 - -0.5) / (1.0 - 0.0)) - V32 ) * Bulge_width36 );
			float temp_output_70_0 = ( 1.0 - ( temp_output_67_0 * temp_output_67_0 ) );
			float temp_output_437_0 = saturate( ( temp_output_70_0 * Power_428 ) );
			float smoothstepResult285 = smoothstep( 0.0 , 0.25 , temp_output_437_0);
			float4 Final_4205 = ( smoothstepResult285 * ( Main_Color116 * _4_Color ) );
			float temp_output_78_0 = ( ( (-0.5 + (_Panner_5 - 0.0) * (1.5 - -0.5) / (1.0 - 0.0)) - V32 ) * Bulge_width36 );
			float temp_output_81_0 = ( 1.0 - ( temp_output_78_0 * temp_output_78_0 ) );
			float temp_output_440_0 = saturate( ( temp_output_81_0 * Power_428 ) );
			float smoothstepResult292 = smoothstep( 0.0 , 0.25 , temp_output_440_0);
			float4 Final_5206 = ( smoothstepResult292 * ( Main_Color116 * _5_Color ) );
			float temp_output_89_0 = ( ( (-0.5 + (_Panner_6 - 0.0) * (1.5 - -0.5) / (1.0 - 0.0)) - V32 ) * Bulge_width36 );
			float temp_output_92_0 = ( 1.0 - ( temp_output_89_0 * temp_output_89_0 ) );
			float temp_output_443_0 = saturate( ( temp_output_92_0 * Power_428 ) );
			float smoothstepResult299 = smoothstep( 0.0 , 0.25 , temp_output_443_0);
			float4 Final_6207 = ( smoothstepResult299 * ( Main_Color116 * _6_Color ) );
			float4 color308 = IsGammaSpace() ? float4(1,1,1,0) : float4(1,1,1,0);
			float Sub_1309 = smoothstepResult263;
			float4 temp_cast_0 = (Sub_1309).xxxx;
			float Sub_2312 = smoothstepResult271;
			float4 temp_cast_1 = (Sub_2312).xxxx;
			float Sub_3313 = smoothstepResult278;
			float4 temp_cast_2 = (Sub_3313).xxxx;
			float Sub_4314 = smoothstepResult285;
			float4 temp_cast_3 = (Sub_4314).xxxx;
			float Sub_5315 = smoothstepResult292;
			float4 temp_cast_4 = (Sub_5315).xxxx;
			float Sub_6316 = smoothstepResult299;
			float4 temp_cast_5 = (Sub_6316).xxxx;
			float4 Bulge_Sub328 = ( ( ( ( ( ( color308 - temp_cast_0 ) - temp_cast_1 ) - temp_cast_2 ) - temp_cast_3 ) - temp_cast_4 ) - temp_cast_5 );
			float2 uv_TexCoord344 = i.uv_texcoord * float2( 3,1 );
			float smoothstepResult365 = smoothstep( 0.0 , 0.2 , ( frac( ( -_Stripes_Number * uv_TexCoord344.y ) ) * frac( ( uv_TexCoord344.y * _Stripes_Number ) ) ));
			float clampResult384 = clamp( ( ( 1.0 - smoothstepResult365 ) * _Stripes_Wet ) , 0.0 , 1.0 );
			float Stripes377 = clampResult384;
			float4 temp_cast_6 = (Stripes377).xxxx;
			float4 Sub_Result379 = ( Bulge_Sub328 - temp_cast_6 );
			float4 Stripes_Colored371 = ( _Stripes_Color * clampResult384 );
			o.Albedo = ( ( Final_1200 + Final_2203 + Final_3204 + Final_4205 + Final_5206 + Final_6207 ) + ( Sub_Result379 * Main_Color116 ) + Stripes_Colored371 ).rgb;
			o.Smoothness = _Smoothness;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18301
0;0;1920;1019;2571.665;4937.725;3.153877;True;True
Node;AmplifyShaderEditor.TextureCoordinatesNode;30;1.702713,1773.277;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;194;-1984,512;Inherit;False;4138.476;857.0743;Deformer 1;24;262;200;259;261;263;105;125;12;8;7;6;4;37;2;10;33;3;309;413;414;429;426;427;447;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;32;243.3503,1821.277;Inherit;False;V;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;3;-1945,750;Inherit;False;Property;_Panner_1;Panner_1;6;0;Create;True;0;0;False;0;False;0.4664364;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;195;-2007.182,-466;Inherit;False;4177.28;813.6542;Deformer 2;24;203;270;268;267;266;312;271;129;46;45;44;43;42;48;41;47;40;16;416;417;432;430;431;448;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;5;1.702713,1933.277;Inherit;False;Property;_Bulge_Width;Bulge_Width;0;0;Create;True;0;0;False;0;False;4;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;10;-1648,752;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-0.5;False;4;FLOAT;1.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;36;209.7027,1933.277;Inherit;False;Bulge_width;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;33;-1648,1008;Inherit;False;32;V;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;196;-2000,-1488;Inherit;False;4178.694;841.3818;Deformer 3;24;130;204;55;54;53;52;51;50;57;56;49;17;278;272;274;275;273;313;424;425;433;434;435;449;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;16;-1920,-320;Inherit;False;Property;_Panner_2;Panner_2;8;0;Create;True;0;0;False;0;False;0.4664364;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;17;-1968,-1264;Inherit;False;Property;_Panner_3;Panner_3;10;0;Create;True;0;0;False;0;False;0.4664364;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;197;-1984,-2512;Inherit;False;4224.353;848.8818;Deformer 4;24;205;131;72;71;70;68;67;65;66;63;64;62;279;280;281;283;285;314;422;423;436;437;438;450;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;37;-1392,976;Inherit;False;36;Bulge_width;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;2;-1408,752;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;40;-1632,-320;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-0.5;False;4;FLOAT;1.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;47;-1648,-80;Inherit;False;32;V;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;198;-1972.903,-3460.502;Inherit;False;4151.59;844.2944;Deformer 5;22;292;290;288;287;286;206;132;83;82;81;79;78;77;76;75;74;73;315;420;421;441;451;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;62;-1936,-2288;Inherit;False;Property;_Panner_4;Panner_4;12;0;Create;True;0;0;False;0;False;0.4664364;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;56;-1696,-1008;Inherit;False;32;V;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;4;-1168,752;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;49;-1680,-1264;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-0.5;False;4;FLOAT;1.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;41;-1424,-336;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;48;-1392,-96;Inherit;False;36;Bulge_width;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;9;-1920,3344;Inherit;False;Constant;_Power_1;Power_1;8;0;Create;True;0;0;False;0;False;0.35;0.35;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;199;-1985.37,-4418;Inherit;False;4161.421;843.0915;Deformer 6;22;207;133;94;93;92;90;89;87;88;86;85;84;294;295;296;298;299;316;418;419;444;452;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;50;-1472,-1264;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;42;-1184,-336;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;63;-1648,-2288;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-0.5;False;4;FLOAT;1.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;428;-1600,3344;Inherit;False;Power_;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;73;-1930.739,-3234.055;Inherit;False;Property;_Panner_5;Panner_5;14;0;Create;True;0;0;False;0;False;0.4664364;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;57;-1440,-1024;Inherit;False;36;Bulge_width;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;64;-1648,-2048;Inherit;False;32;V;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;6;-912,752;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;74;-1632.845,-3247.924;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-0.5;False;4;FLOAT;1.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;84;-1935.37,-4173.446;Inherit;False;Property;_Panner_6;Panner_6;16;0;Create;True;0;0;False;0;False;0.4664364;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;75;-1648,-3008;Inherit;False;32;V;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;43;-928,-336;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;51;-1216,-1248;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;65;-1424,-2288;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;429;-273,813;Inherit;False;428;Power_;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;66;-1392,-2064;Inherit;False;36;Bulge_width;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;7;-656,752;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;67;-1168,-2288;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;76;-1408,-3248;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;426;-48,752;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;85;-1645.312,-4174.252;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-0.5;False;4;FLOAT;1.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;52;-960,-1248;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;432;-224,-144;Inherit;False;428;Power_;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;86;-1680,-3936;Inherit;False;32;V;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;44;-672,-336;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;77;-1376,-3024;Inherit;False;36;Bulge_width;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;372;-1936,2560;Inherit;False;2735.861;669.439;Stripes;19;382;346;371;370;387;377;384;381;369;388;365;364;350;359;345;355;368;344;352;Stripes;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;88;-1416.088,-3944.397;Inherit;False;36;Bulge_width;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;87;-1440,-4176;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;53;-704,-1248;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;68;-912,-2288;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;427;224,752;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;430;0,-336;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;78;-1152,-3248;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;435;-320,-1120;Inherit;False;428;Power_;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;431;272,-336;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;433;-80,-1264;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;438;-256,-2224;Inherit;False;428;Power_;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;352;-1568,2944;Inherit;False;Property;_Stripes_Number;Stripes_Number;19;0;Create;True;0;0;False;0;False;10;8;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;70;-656,-2288;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;79;-896,-3248;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;263;624,784;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.25;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;89;-1193.044,-4166.245;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;81;-640,-3248;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;344;-1888,2816;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;3,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;335;721.7026,1581.277;Inherit;False;1426;857;Subtract bulges;14;308;310;311;319;317;320;318;321;322;324;325;323;326;328;Subtract Bulges;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;441;-304,-3056;Inherit;False;428;Power_;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;436;-48,-2320;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;434;192,-1264;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;271;608,-304;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.25;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;90;-937.042,-4166.245;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;309;896,896;Inherit;False;Sub_1;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;368;-1408,2768;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;334;-1950.297,1565.277;Inherit;False;1837.515;836.3448;LERP;13;116;302;301;341;61;305;340;306;333;303;304;385;386;LERP;1,1,1,1;0;0
Node;AmplifyShaderEditor.SaturateNode;437;224,-2320;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;444;-296,-4099;Inherit;False;428;Power_;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;310;769.7026,1837.277;Inherit;False;309;Sub_1;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;304;-1900.953,2035.735;Inherit;False;Property;_Color_Lerp;Color_Lerp;4;0;Create;True;0;0;False;0;False;0.4664364;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;439;-64,-3232;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;312;848,-192;Inherit;False;Sub_2;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;355;-1216,2768;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;92;-681.0421,-4166.245;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;308;769.7026,1645.277;Inherit;False;Constant;_Color0;Color 0;17;0;Create;True;0;0;False;0;False;1,1,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SmoothstepOpNode;278;688,-1216;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.25;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;345;-1216,2896;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;442;-64,-4208;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;311;1057.703,1821.277;Inherit;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.TFHCRemapNode;306;-1571.919,2032.724;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-0.5;False;4;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;285;688,-2272;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.25;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;350;-1040,2896;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;359;-1040,2768;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;313;945.1047,-1095.768;Inherit;False;Sub_3;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;440;208,-3232;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;319;769.7026,1933.277;Inherit;False;312;Sub_2;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;303;-1566.366,2205.947;Inherit;False;32;V;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;320;769.7026,2029.277;Inherit;False;313;Sub_3;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;305;-1331.297,2013.277;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;317;1185.703,1917.277;Inherit;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;443;176,-4208;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;364;-816,2832;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;333;-1549.297,2288.277;Inherit;False;Property;_Lerp_Intensity;Lerp_Intensity;2;0;Create;True;0;0;False;0;False;8.5;8;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;314;992,-2080;Inherit;False;Sub_4;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;292;624,-3200;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.25;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;321;769.7026,2125.277;Inherit;False;314;Sub_4;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;315;928,-3024;Inherit;False;Sub_5;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;365;-592,2832;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;318;1345.703,2013.277;Inherit;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;340;-1109.964,2146.557;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;299;656,-4160;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.25;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;386;-1536,1648;Inherit;False;Property;_MainColor_A;MainColor_A;3;0;Create;True;0;0;False;0;False;0,0.6313725,1,0;0.5019608,0.5019608,0.5019608,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;369;-320,2832;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;324;1473.703,2109.277;Inherit;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;322;769.7026,2221.277;Inherit;False;315;Sub_5;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;341;-944.1249,2029.378;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;403;848,2832;Inherit;False;1324.326;398.2034;Comment;8;405;407;402;398;400;397;399;390;Bulge fade out;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;388;-304,3136;Inherit;False;Property;_Stripes_Wet;Stripes_Wet;21;0;Create;True;0;0;False;0;False;2;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;385;-1536,1824;Inherit;False;Property;_MainColor_B;MainColor_B;5;0;Create;True;0;0;False;0;False;1,0.7031918,0,0;0,0,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;316;944,-3952;Inherit;False;Sub_6;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;390;912,2880;Inherit;False;32;V;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;323;769.7026,2333.277;Inherit;False;316;Sub_6;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;381;-80,2912;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;10;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;302;-700.1453,1673.31;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;325;1601.703,2205.277;Inherit;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;397;912,3024;Inherit;False;Constant;_Cap;Cap;11;0;Create;True;0;0;False;0;False;0.25;0.539;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;116;-396.1448,1673.31;Inherit;False;Main_Color;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;384;144,2912;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;399;1104,2896;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;326;1729.703,2317.277;Inherit;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;272;968.6843,-941.4872;Inherit;False;116;Main_Color;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;400;1280,2880;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;105;896,1024;Inherit;False;116;Main_Color;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;267;928,32;Inherit;False;Property;_2_Color;2_Color;9;0;Create;True;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;286;928,-2800;Inherit;False;Property;_5_Color;5_Color;15;0;Create;True;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;266;928,-80;Inherit;False;116;Main_Color;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;377;576,3008;Inherit;False;Stripes;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;280;992,-1968;Inherit;False;116;Main_Color;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;298;960,-3712;Inherit;False;Property;_6_Color;6_Color;17;0;Create;True;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;328;1923.365,2317.277;Inherit;False;Bulge_Sub;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;294;944,-3824;Inherit;False;116;Main_Color;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;287;928,-2912;Inherit;False;116;Main_Color;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;273;979.8352,-837.1476;Inherit;False;Property;_3_Color;3_Color;11;0;Create;True;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;398;1248,2992;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-0.5;False;4;FLOAT;1.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;380;1344,2480;Inherit;False;803.1572;322.3499;FINAL SUB;4;376;375;378;379;Final Subtract;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;262;896,1136;Inherit;False;Property;_1_Color;1_Color;7;0;Create;True;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;279;992,-1872;Inherit;False;Property;_4_Color;4_Color;13;0;Create;True;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;402;1488,2896;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;261;1216,1072;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;375;1392,2528;Inherit;False;328;Bulge_Sub;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;295;1264,-3824;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;281;1312,-1920;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;274;1344,-935.2515;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;376;1408,2656;Inherit;False;377;Stripes;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;268;1248,-80;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;288;1248,-2912;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;132;368,-2928;Inherit;False;Def_5;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;133;416,-3936;Inherit;False;Def_6;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;275;1648,-1216;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;259;1680,784;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;131;384,-2032;Inherit;False;Def_4;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;129;416,0;Inherit;False;Def_2;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;283;1760,-2272;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;290;1680,-3200;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;130;368,-1024;Inherit;False;Def_3;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;270;1616,-304;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;378;1664,2544;Inherit;True;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;125;448,1056;Inherit;False;Def_1;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;407;1715.73,2914.986;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.19;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;387;-224,2640;Inherit;False;Property;_Stripes_Color;Stripes_Color;20;0;Create;True;0;0;False;0;False;1,0.6132548,0,0;1,0.5019608,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;296;1616,-4160;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;136;2786.743,1861.098;Inherit;False;129;Def_2;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;405;1951.381,2886.741;Inherit;False;Fade_Out;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;138;2786.743,2053.098;Inherit;False;131;Def_4;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;207;1872,-4176;Inherit;False;Final_6;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;379;1920,2576;Inherit;False;Sub_Result;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;203;1856,-304;Inherit;False;Final_2;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;206;1936,-3200;Inherit;False;Final_5;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;370;416,2784;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;205;2000,-2272;Inherit;False;Final_4;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;139;2786.743,2149.098;Inherit;False;132;Def_5;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;200;1920,800;Inherit;False;Final_1;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;135;2786.743,1765.098;Inherit;False;125;Def_1;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;204;1920,-1232;Inherit;False;Final_3;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;137;2786.743,1957.098;Inherit;False;130;Def_3;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;140;2786.743,2245.097;Inherit;False;133;Def_6;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;211;3168,544;Inherit;False;205;Final_4;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;134;3277.474,1909.722;Inherit;True;6;6;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;210;3168,448;Inherit;False;204;Final_3;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;406;3399.329,1798.661;Inherit;False;405;Fade_Out;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;212;3168,640;Inherit;False;206;Final_5;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;209;3168,352;Inherit;False;203;Final_2;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;208;3168,256;Inherit;False;200;Final_1;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;329;3456,784;Inherit;False;379;Sub_Result;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;213;3168,736;Inherit;False;207;Final_6;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;307;3457,1010;Inherit;False;116;Main_Color;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;371;608,2800;Inherit;False;Stripes_Colored;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;327;3472,432;Inherit;True;6;6;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.NormalVertexDataNode;11;3665.73,2430.359;Inherit;True;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;330;3760,848;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;374;3799.218,1139.605;Inherit;True;371;Stripes_Colored;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;389;3657.171,1845.161;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;410;-37.19736,3340.448;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;447;-287.3585,1272.567;Inherit;False;446;Bulge_Power;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;94;176,-3936;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;451;-320,-2704;Inherit;False;446;Bulge_Power;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;450;-304,-1776;Inherit;False;446;Bulge_Power;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;12;256,1056;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;46;224,0;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;72;192,-2032;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;83;176,-2928;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;423;-352,-2032;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;93;-64,-3936;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;71;-48,-2032;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;82;-64,-2928;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;45;-16,0;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;8;-16,1056;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;417;-272,0;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;425;-400,-1024;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;449;-322.2976,-795.4832;Inherit;False;446;Bulge_Power;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;414;-304,1056;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;54;-80,-1024;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;409;197.387,3338.075;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;2,2;False;1;FLOAT;4;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;448;-265.453,220.2656;Inherit;False;446;Bulge_Power;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;346;-560,2608;Inherit;False;Global;Stripes_Color;Stripes_Color;19;0;Create;True;0;0;False;0;False;1,0.6132548,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;419;-416,-3936;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;301;-1902.297,1821.277;Inherit;False;Global;MainColor_B;MainColor_B;7;0;Create;True;0;0;False;0;False;1,0.7031918,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;382;-304,3056;Inherit;False;Global;Stripes_Wet;Stripes_Wet;22;0;Create;True;0;0;False;0;False;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;61;-1902.297,1613.277;Inherit;False;Global;MainColor_A;MainColor_A;7;0;Create;True;0;0;False;0;False;0,0.6313725,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;3935.078,1963.676;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;331;4105.209,825.9552;Inherit;True;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;342;4303.76,1330.337;Inherit;False;Property;_Smoothness;_Smoothness;1;0;Create;True;0;0;False;0;False;0;0.5;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;412;720,3344;Inherit;False;Noise;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;55;160,-1024;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;421;-352,-2928;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;452;-384,-3696;Inherit;False;446;Bulge_Power;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;413;-528,1072;Inherit;False;412;Noise;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;446;-1600,3456;Inherit;False;Bulge_Power;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;418;-624,-3920;Inherit;False;412;Noise;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;424;-576,-1008;Inherit;False;412;Noise;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;420;-544,-2912;Inherit;False;412;Noise;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;416;-496,16;Inherit;False;412;Noise;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;422;-544,-2016;Inherit;False;412;Noise;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;453;-1920,3457;Inherit;False;Property;_Bulge_Power;Bulge_Power;18;0;Create;True;0;0;False;0;False;0;0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;415;480,3344;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;1.14;False;2;FLOAT;-0.53;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;4578.343,1241.041;Float;False;True;-1;6;ASEMaterialInspector;0;0;Standard;Bulge;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;1;3.7;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;32;0;30;2
WireConnection;10;0;3;0
WireConnection;36;0;5;0
WireConnection;2;0;10;0
WireConnection;2;1;33;0
WireConnection;40;0;16;0
WireConnection;4;0;2;0
WireConnection;4;1;37;0
WireConnection;49;0;17;0
WireConnection;41;0;40;0
WireConnection;41;1;47;0
WireConnection;50;0;49;0
WireConnection;50;1;56;0
WireConnection;42;0;41;0
WireConnection;42;1;48;0
WireConnection;63;0;62;0
WireConnection;428;0;9;0
WireConnection;6;0;4;0
WireConnection;6;1;4;0
WireConnection;74;0;73;0
WireConnection;43;0;42;0
WireConnection;43;1;42;0
WireConnection;51;0;50;0
WireConnection;51;1;57;0
WireConnection;65;0;63;0
WireConnection;65;1;64;0
WireConnection;7;0;6;0
WireConnection;67;0;65;0
WireConnection;67;1;66;0
WireConnection;76;0;74;0
WireConnection;76;1;75;0
WireConnection;426;0;7;0
WireConnection;426;1;429;0
WireConnection;85;0;84;0
WireConnection;52;0;51;0
WireConnection;52;1;51;0
WireConnection;44;0;43;0
WireConnection;87;0;85;0
WireConnection;87;1;86;0
WireConnection;53;0;52;0
WireConnection;68;0;67;0
WireConnection;68;1;67;0
WireConnection;427;0;426;0
WireConnection;430;0;44;0
WireConnection;430;1;432;0
WireConnection;78;0;76;0
WireConnection;78;1;77;0
WireConnection;431;0;430;0
WireConnection;433;0;53;0
WireConnection;433;1;435;0
WireConnection;70;0;68;0
WireConnection;79;0;78;0
WireConnection;79;1;78;0
WireConnection;263;0;427;0
WireConnection;89;0;87;0
WireConnection;89;1;88;0
WireConnection;81;0;79;0
WireConnection;436;0;70;0
WireConnection;436;1;438;0
WireConnection;434;0;433;0
WireConnection;271;0;431;0
WireConnection;90;0;89;0
WireConnection;90;1;89;0
WireConnection;309;0;263;0
WireConnection;368;0;352;0
WireConnection;437;0;436;0
WireConnection;439;0;81;0
WireConnection;439;1;441;0
WireConnection;312;0;271;0
WireConnection;355;0;368;0
WireConnection;355;1;344;2
WireConnection;92;0;90;0
WireConnection;278;0;434;0
WireConnection;345;0;344;2
WireConnection;345;1;352;0
WireConnection;442;0;92;0
WireConnection;442;1;444;0
WireConnection;311;0;308;0
WireConnection;311;1;310;0
WireConnection;306;0;304;0
WireConnection;285;0;437;0
WireConnection;350;0;345;0
WireConnection;359;0;355;0
WireConnection;313;0;278;0
WireConnection;440;0;439;0
WireConnection;305;0;306;0
WireConnection;305;1;303;0
WireConnection;317;0;311;0
WireConnection;317;1;319;0
WireConnection;443;0;442;0
WireConnection;364;0;359;0
WireConnection;364;1;350;0
WireConnection;314;0;285;0
WireConnection;292;0;440;0
WireConnection;315;0;292;0
WireConnection;365;0;364;0
WireConnection;318;0;317;0
WireConnection;318;1;320;0
WireConnection;340;0;305;0
WireConnection;340;1;333;0
WireConnection;299;0;443;0
WireConnection;369;0;365;0
WireConnection;324;0;318;0
WireConnection;324;1;321;0
WireConnection;341;0;340;0
WireConnection;316;0;299;0
WireConnection;381;0;369;0
WireConnection;381;1;388;0
WireConnection;302;0;386;0
WireConnection;302;1;385;0
WireConnection;302;2;341;0
WireConnection;325;0;324;0
WireConnection;325;1;322;0
WireConnection;116;0;302;0
WireConnection;384;0;381;0
WireConnection;399;0;390;0
WireConnection;326;0;325;0
WireConnection;326;1;323;0
WireConnection;400;0;399;0
WireConnection;400;1;399;0
WireConnection;377;0;384;0
WireConnection;328;0;326;0
WireConnection;398;0;397;0
WireConnection;402;0;400;0
WireConnection;402;1;398;0
WireConnection;261;0;105;0
WireConnection;261;1;262;0
WireConnection;295;0;294;0
WireConnection;295;1;298;0
WireConnection;281;0;280;0
WireConnection;281;1;279;0
WireConnection;274;0;272;0
WireConnection;274;1;273;0
WireConnection;268;0;266;0
WireConnection;268;1;267;0
WireConnection;288;0;287;0
WireConnection;288;1;286;0
WireConnection;132;0;440;0
WireConnection;133;0;443;0
WireConnection;275;0;278;0
WireConnection;275;1;274;0
WireConnection;259;0;263;0
WireConnection;259;1;261;0
WireConnection;131;0;437;0
WireConnection;129;0;431;0
WireConnection;283;0;285;0
WireConnection;283;1;281;0
WireConnection;290;0;292;0
WireConnection;290;1;288;0
WireConnection;130;0;434;0
WireConnection;270;0;271;0
WireConnection;270;1;268;0
WireConnection;378;0;375;0
WireConnection;378;1;376;0
WireConnection;125;0;427;0
WireConnection;407;0;402;0
WireConnection;296;0;299;0
WireConnection;296;1;295;0
WireConnection;405;0;407;0
WireConnection;207;0;296;0
WireConnection;379;0;378;0
WireConnection;203;0;270;0
WireConnection;206;0;290;0
WireConnection;370;0;387;0
WireConnection;370;1;384;0
WireConnection;205;0;283;0
WireConnection;200;0;259;0
WireConnection;204;0;275;0
WireConnection;134;0;135;0
WireConnection;134;1;136;0
WireConnection;134;2;137;0
WireConnection;134;3;138;0
WireConnection;134;4;139;0
WireConnection;134;5;140;0
WireConnection;371;0;370;0
WireConnection;327;0;208;0
WireConnection;327;1;209;0
WireConnection;327;2;210;0
WireConnection;327;3;211;0
WireConnection;327;4;212;0
WireConnection;327;5;213;0
WireConnection;330;0;329;0
WireConnection;330;1;307;0
WireConnection;389;0;406;0
WireConnection;389;1;134;0
WireConnection;94;0;93;0
WireConnection;12;0;8;0
WireConnection;46;0;45;0
WireConnection;72;0;71;0
WireConnection;83;0;82;0
WireConnection;423;0;70;0
WireConnection;423;1;422;0
WireConnection;93;0;419;0
WireConnection;93;1;452;0
WireConnection;71;0;423;0
WireConnection;71;1;450;0
WireConnection;82;0;421;0
WireConnection;82;1;451;0
WireConnection;45;0;417;0
WireConnection;45;1;448;0
WireConnection;8;0;414;0
WireConnection;8;1;447;0
WireConnection;417;0;44;0
WireConnection;417;1;416;0
WireConnection;425;0;53;0
WireConnection;425;1;424;0
WireConnection;414;0;7;0
WireConnection;414;1;413;0
WireConnection;54;0;425;0
WireConnection;54;1;449;0
WireConnection;409;0;410;0
WireConnection;419;0;92;0
WireConnection;419;1;418;0
WireConnection;13;0;389;0
WireConnection;13;1;11;0
WireConnection;331;0;327;0
WireConnection;331;1;330;0
WireConnection;331;2;374;0
WireConnection;412;0;415;0
WireConnection;55;0;54;0
WireConnection;421;0;81;0
WireConnection;421;1;420;0
WireConnection;446;0;453;0
WireConnection;415;0;409;0
WireConnection;0;0;331;0
WireConnection;0;4;342;0
WireConnection;0;11;13;0
ASEEND*/
//CHKSM=986DE068C131116A2D66194D13FFBFDE049AFFDA