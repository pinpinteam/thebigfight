// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/DepthFog"
{
	Properties
	{
		_Distance("Distance", Float) = 1
		_DepthPower("Depth Power", Range( 0.0001 , 10)) = 1
		_NoiseScale("Noise Scale", Float) = 0
		_NoisePower("Noise Power", Range( 0 , 1)) = 1
		_Speed("Speed", Float) = 1
		_Opacity("Opacity", Range( 0 , 1)) = 1
		[Header(Colorise)]_Min("Min", Range( 0 , 1)) = 0
		_Max("Max", Range( 0 , 1)) = 1
		[Header(Invincible)]_HueShiftSpeed("Hue Shift Speed", Range( 0 , 2)) = 1
		_MultiColorPower("MultiColor Power", Float) = 0
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#pragma target 3.0
		#pragma surface surf Standard alpha:fade keepalpha noshadow exclude_path:deferred novertexlights nolightmap  nodynlightmap nodirlightmap nometa noforwardadd 
		struct Input
		{
			float3 worldPos;
			float4 screenPos;
		};

		uniform float4 MainColor;
		uniform float4 SecondaryColor;
		uniform float _Min;
		uniform float _Max;
		uniform float ColoriseDistance;
		uniform float3 ColoriseOrigin;
		uniform float _MultiColorPower;
		uniform float _HueShiftSpeed;
		uniform float Invincible;
		uniform float _Speed;
		uniform float _NoiseScale;
		uniform float _NoisePower;
		UNITY_DECLARE_DEPTH_TEXTURE( _CameraDepthTexture );
		uniform float4 _CameraDepthTexture_TexelSize;
		uniform float _Distance;
		uniform float _DepthPower;
		uniform float _Opacity;


		float3 HSVToRGB( float3 c )
		{
			float4 K = float4( 1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0 );
			float3 p = abs( frac( c.xxx + K.xyz ) * 6.0 - K.www );
			return c.z * lerp( K.xxx, saturate( p - K.xxx ), c.y );
		}


		float3 RGBToHSV(float3 c)
		{
			float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
			float4 p = lerp( float4( c.bg, K.wz ), float4( c.gb, K.xy ), step( c.b, c.g ) );
			float4 q = lerp( float4( p.xyw, c.r ), float4( c.r, p.yzx ), step( p.x, c.r ) );
			float d = q.x - min( q.w, q.y );
			float e = 1.0e-10;
			return float3( abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
		}

		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_worldPos = i.worldPos;
			float3 temp_output_58_0 = ( ColoriseOrigin - ase_worldPos );
			float dotResult62 = dot( temp_output_58_0 , temp_output_58_0 );
			float temp_output_65_0 = ( ColoriseDistance / dotResult62 );
			float smoothstepResult70 = smoothstep( _Min , _Max , temp_output_65_0);
			float4 lerpResult74 = lerp( MainColor , SecondaryColor , smoothstepResult70);
			float3 hsvTorgb3_g13 = HSVToRGB( float3(( ( ( ( ase_worldPos.x * 0.5 ) + ase_worldPos.z ) * _MultiColorPower ) - ( _Time.y * _HueShiftSpeed ) ),1.0,1.0) );
			float3 hsvTorgb59 = RGBToHSV( hsvTorgb3_g13 );
			float3 hsvTorgb64 = HSVToRGB( float3(hsvTorgb59.x,( hsvTorgb59.y * 0.45 ),( hsvTorgb59.z * 0.9 )) );
			float3 MultiColor69 = hsvTorgb64;
			float4 lerpResult75 = lerp( lerpResult74 , float4( MultiColor69 , 0.0 ) , Invincible);
			float4 Color76 = lerpResult75;
			float4 appendResult5_g1 = (float4(_SinTime.w , _SinTime.z , 0.0 , 0.0));
			float4 appendResult28 = (float4(ase_worldPos.x , ase_worldPos.z , 0.0 , 0.0));
			float2 panner7_g1 = ( 1.0 * _Time.y * ( appendResult5_g1 * ( _Speed / 10000.0 ) ).xy + appendResult28.xy);
			float simplePerlin2D25 = snoise( panner7_g1*_NoiseScale );
			simplePerlin2D25 = simplePerlin2D25*0.5 + 0.5;
			float lerpResult38 = lerp( simplePerlin2D25 , 1.0 , _NoisePower);
			o.Albedo = ( Color76 * lerpResult38 ).rgb;
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float screenDepth1 = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE( _CameraDepthTexture, ase_screenPosNorm.xy ));
			float distanceDepth1 = saturate( abs( ( screenDepth1 - LinearEyeDepth( ase_screenPosNorm.z ) ) / ( _Distance ) ) );
			o.Alpha = ( saturate( pow( distanceDepth1 , _DepthPower ) ) * _Opacity );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18301
0;73;621;642;2333.587;2614.434;1;True;False
Node;AmplifyShaderEditor.CommentaryNode;44;-3206.451,-2515.829;Inherit;False;1967.95;718.3005;Comment;15;69;64;61;60;59;55;54;52;51;50;49;48;47;46;45;MultiColor;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldPosInputsNode;45;-3156.451,-2047.739;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;46;-2974.459,-2063.263;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;47;-2837.832,-2033.408;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TimeNode;49;-3121.352,-2344.628;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;50;-2956.483,-1913.53;Inherit;False;Property;_MultiColorPower;MultiColor Power;9;0;Create;True;0;0;False;0;False;0;0.01;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;48;-3122.393,-2175.776;Inherit;False;Property;_HueShiftSpeed;Hue Shift Speed;8;0;Create;True;0;0;False;1;Header(Invincible);False;1;0.5;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;51;-2676.462,-2059.14;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;52;-2805.043,-2252.445;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;54;-2586.494,-2247.645;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;53;-3193.761,-1646.363;Inherit;False;1709.125;859.209;Comment;17;77;76;75;74;73;72;71;70;68;67;66;65;63;62;58;57;56;Colorise Distance ;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldPosInputsNode;57;-3143.76,-1239.117;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.FunctionNode;55;-2411.069,-2346.621;Inherit;True;Simple HUE;-1;;13;32abb5f0db087604486c2db83a2e817a;0;1;1;FLOAT;0;False;4;FLOAT3;6;FLOAT;7;FLOAT;5;FLOAT;8
Node;AmplifyShaderEditor.Vector3Node;56;-3139.995,-1066.954;Inherit;False;Global;ColoriseOrigin;Colorise Origin;19;1;[HideInInspector];Create;True;0;0;False;0;False;0,0,0;0,0.0002271923,305;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RGBToHSVNode;59;-2171.43,-2354.886;Inherit;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleSubtractOpNode;58;-2936.38,-1143.567;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;61;-1889.672,-2182.278;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.9;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;60;-1930.178,-2465.829;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.45;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;63;-2850.514,-1270.681;Inherit;False;Global;ColoriseDistance;Colorise Distance;19;0;Create;True;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;62;-2798.735,-1133.734;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;66;-2547.613,-903.1548;Inherit;False;Property;_Max;Max;7;0;Create;True;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;67;-2544.613,-973.155;Inherit;False;Property;_Min;Min;6;0;Create;True;0;0;False;1;Header(Colorise);False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;65;-2618.342,-1151.008;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.HSVToRGBNode;64;-1703.067,-2340.651;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldPosInputsNode;27;-2103.176,-357.1041;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ColorNode;68;-2419.534,-1529.124;Inherit;False;Global;MainColor;MainColor;0;0;Create;True;0;0;False;0;False;0.3443396,0.8108984,1,1;0.521983,0.8553627,0.9622642,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;71;-2446.963,-1351.268;Inherit;False;Global;SecondaryColor;SecondaryColor;18;0;Create;True;0;0;False;0;False;0,0,0,0;1,0.2877358,0.2877358,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SmoothstepOpNode;70;-2236.613,-1129.155;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;69;-1462.502,-2328.344;Inherit;False;MultiColor;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;74;-2049.807,-1260.09;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;40;-1642.468,-203.2012;Inherit;False;Property;_Speed;Speed;4;0;Create;True;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;3;-1554.948,72.36313;Inherit;False;Property;_Distance;Distance;0;0;Create;True;0;0;False;0;False;1;10;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;72;-2124.786,-1006.766;Inherit;False;Global;Invincible;Invincible;17;0;Create;True;0;0;False;1;Header(Invincible);False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;73;-2029.191,-1118.28;Inherit;False;69;MultiColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;28;-1867.521,-333.4498;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;29;-1294.422,-123.5493;Inherit;False;Property;_NoiseScale;Noise Scale;2;0;Create;True;0;0;False;0;False;0;0.14;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;75;-1804.782,-1215.979;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;6;-1248.216,269.7046;Inherit;False;Property;_DepthPower;Depth Power;1;0;Create;True;0;0;False;0;False;1;1.5;0.0001;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.DepthFade;1;-1356.06,84.09662;Inherit;False;True;True;True;2;1;FLOAT3;0,0,0;False;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;39;-1425.488,-314.1803;Inherit;False;Water Pan;-1;;1;9b382295680ba6541acffd5b0c157bc3;0;2;2;FLOAT2;0,0;False;1;FLOAT;5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;37;-719.7316,-211.2872;Inherit;False;Property;_NoisePower;Noise Power;3;0;Create;True;0;0;False;0;False;1;0.872;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;76;-1673.036,-1041.969;Inherit;False;Color;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;5;-815.0317,-67.93155;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;25;-1107.397,-287.9218;Inherit;False;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;4.81;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;38;-350.0321,-343.601;Inherit;False;3;0;FLOAT;1;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;78;-331.8202,-529.0421;Inherit;False;76;Color;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;41;-350.0143,187.0681;Inherit;False;Property;_Opacity;Opacity;5;0;Create;True;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;43;-474.5257,-36.62752;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;77;-2461.69,-1135.847;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;42;148.106,-43.58354;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-62.39809,-508.2957;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;420.544,-411.2526;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;Custom/DepthFog;False;False;False;False;False;True;True;True;True;False;True;True;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;False;0;False;Transparent;;Transparent;ForwardOnly;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;2;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;46;0;45;1
WireConnection;47;0;46;0
WireConnection;47;1;45;3
WireConnection;51;0;47;0
WireConnection;51;1;50;0
WireConnection;52;0;49;2
WireConnection;52;1;48;0
WireConnection;54;0;51;0
WireConnection;54;1;52;0
WireConnection;55;1;54;0
WireConnection;59;0;55;6
WireConnection;58;0;56;0
WireConnection;58;1;57;0
WireConnection;61;0;59;3
WireConnection;60;0;59;2
WireConnection;62;0;58;0
WireConnection;62;1;58;0
WireConnection;65;0;63;0
WireConnection;65;1;62;0
WireConnection;64;0;59;1
WireConnection;64;1;60;0
WireConnection;64;2;61;0
WireConnection;70;0;65;0
WireConnection;70;1;67;0
WireConnection;70;2;66;0
WireConnection;69;0;64;0
WireConnection;74;0;68;0
WireConnection;74;1;71;0
WireConnection;74;2;70;0
WireConnection;28;0;27;1
WireConnection;28;1;27;3
WireConnection;75;0;74;0
WireConnection;75;1;73;0
WireConnection;75;2;72;0
WireConnection;1;0;3;0
WireConnection;39;2;28;0
WireConnection;39;1;40;0
WireConnection;76;0;75;0
WireConnection;5;0;1;0
WireConnection;5;1;6;0
WireConnection;25;0;39;0
WireConnection;25;1;29;0
WireConnection;38;0;25;0
WireConnection;38;2;37;0
WireConnection;43;0;5;0
WireConnection;77;0;65;0
WireConnection;42;0;43;0
WireConnection;42;1;41;0
WireConnection;26;0;78;0
WireConnection;26;1;38;0
WireConnection;0;0;26;0
WireConnection;0;9;42;0
ASEEND*/
//CHKSM=8FEFE1E30BD98FA01D907E1C29FDF09AB12BDEB6