﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/ToonOutline"
{
    Properties
    {
		_Color("Color", Color) = (1,1,1,1)

		_SpecularPower("Specular Power", Range(0,1)) = 0.2
		_SpecularDistance("Specular Distance", Range(-1,1)) = 0
		_Gloss("Gloss", Range(0, 5)) = 1

		_FresnelPower("Fresnel Power", Range(0, 5)) = 0
		_FresnelDistance("Fresnel Distance", Range(0, 3)) = 0
		_FresnelIntensity("Fresnel Intensity", Range(0, 1)) = 1

		_OutlineColor("Outline Color", Color) = (0,0,0,1)
		_OutlinePower("Outline Power", Range(0,1.0)) = 1
		_OutlineWidth("Outline Width", Range(1.0, 2.0)) = 1
		_OutlineWidthX("Outline Width X", Range(1, 2.0)) = 1.1
		_OutlineWidthY("Outline Width Y", Range(1.0, 2.0)) = 1.1
		_OutlineWidthZ("Outline Width Z", Range(1.0, 2.0)) = 1.1

    }
    SubShader
    {
		Tags { "Queue" = "Geometry+1" "RenderType" = "Transparent" }
       
        //LOD 100

		Pass
		{
			ZWrite Off
			ZTest Always
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag            
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			float _OutlineWidth;
			float _OutlineWidthX;
			float _OutlineWidthY;
			float _OutlineWidthZ;
			float4 _OutlineColor;
			half _OutlinePower;

			v2f vert(appdata v)
			{
				v.vertex.x *= _OutlineWidthX * _OutlineWidth;
				v.vertex.y *= _OutlineWidthY * _OutlineWidth;
				v.vertex.z *= _OutlineWidthZ * _OutlineWidth;

				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}


			half4 frag(v2f i) : COLOR
			{
				half4 col = _OutlineColor;
				col.a = _OutlinePower;
				return col;
			}
			ENDCG
		}

        Pass
        {
			ZWrite On
			ZTest Less

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
			#include "Lighting.cginc"

			float4 _Color;

			float _SpecularDistance;
			float _SpecularPower;
			float _Gloss;

			uniform float _FresnelDistance;
			uniform float _FresnelPower;
			uniform float _FresnelIntensity;

			struct appdata 
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

            struct v2f
            {			
				float3 normal : NORMAL;
                float4 vertex : SV_POSITION;
				float3 viewDir : DIR;
            };

			float specularLight(float3 normal, float3 lightDir)
			{
				float gloss = max(0, pow(dot(normal, lightDir) - _SpecularDistance, (1 / _SpecularPower)))  * _Gloss;
				return clamp(gloss, 0, 1);
			}

			float Fresnel(float3 n, float3 viewDir)
			{
				float result = pow(max(0, (dot(n, viewDir) + _FresnelDistance)) / 2, (1 / _FresnelPower));
				return result;
			}

			float3 simpleLambert(fixed3 normal, float3 viewDir, float3 col)
			{
				float3 lightDir = _WorldSpaceLightPos0.xyz; // Light direction				
				float3 lightCol = _LightColor0.rgb; // Light color

				float light = dot(normal, lightDir) * 0.5 + 0.5;

				float gloss = specularLight(normal, lightDir);

				float fresnel = Fresnel(normal, viewDir) * _FresnelIntensity;

				return col * lightCol * light + float3(1, 1, 1) * (gloss + fresnel);
			}

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.normal = mul((float4x4)unity_ObjectToWorld, v.normal);
				o.normal = normalize(o.normal);

				float3 viewDir = mul((float3x3)unity_CameraToWorld, float3(0, 0, 1));
				o.viewDir = viewDir;

                return o;
            }

			fixed4 frag(v2f i) : SV_Target
			{ 			
				return fixed4(simpleLambert(i.normal.xyz, i.viewDir, _Color.rgb).xyz, 1);
			}
            ENDCG
        }
    }
		Fallback "VertexLit"
}
