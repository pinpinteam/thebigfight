﻿using System;
using System.Configuration;
using System.IO;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor.iOS.Xcode;
using UnityEngine;

namespace MadBox.Tools.Unity.PostProcess
{
	public class IronSourcePostProcess : IPostprocessBuildWithReport
	{
		//TODO Add Admob id
		private static string AndroidAdmobID = "ca-app-pub-2610362712410072~1464960414";
		private static string IOSAdmobID = "ca-app-pub-2610362712410072~1464960414";

		public int callbackOrder
		{
			get { return 1; }
		}
		public void OnPostprocessBuild ( BuildReport report )
		{
			var pathToBuiltProject = report.summary.outputPath;
			Debug.Log("IS POSTPROCESS BEGIN");


			if (report.summary.platform == BuildTarget.iOS)
			{
				if (String.IsNullOrEmpty(IOSAdmobID)) throw new ConfigurationException("YOU DIDN'T PUT YOUR ADMOB ID");
				var plistPath = pathToBuiltProject + "/Info.plist";
				var plist = new PlistDocument();
				plist.ReadFromString(File.ReadAllText(plistPath));

				var rootDict = plist.root;

				string projPath = pathToBuiltProject + "/Unity-iPhone.xcodeproj/project.pbxproj";
				PBXProject proj = new PBXProject();
				proj.ReadFromString(File.ReadAllText(projPath));
				string targetGUID = proj.GetUnityMainTargetGuid();

				rootDict.SetString("NSCalendarsUsageDescription", "Advertising");
				var newDict = rootDict.CreateDict("NSAppTransportSecurity");
				newDict.SetBoolean("NSAllowsArbitraryLoads", true);

				proj.AddFrameworkToProject(targetGUID, "AdSupport.framework", false);

				rootDict.SetString("GADApplicationIdentifier", IOSAdmobID);

				var skAdNetworkArray = rootDict.CreateArray("SKAdNetworkItems");
				AddSKAdNetwork(skAdNetworkArray, "7ug5zh24hu.skadnetwork"); //Liftoff
				AddSKAdNetwork(skAdNetworkArray, "ludvb6z3bs.skadnetwork"); //Applovin
				AddSKAdNetwork(skAdNetworkArray, "4fzdc2evr5.skadnetwork"); //Aarki
				AddSKAdNetwork(skAdNetworkArray, "2u9pt9hc89.skadnetwork"); //Remerge
				AddSKAdNetwork(skAdNetworkArray, "wzmmz9fp6w.skadnetwork"); //InMobi
				AddSKAdNetwork(skAdNetworkArray, "t38b2kh725.skadnetwork"); //Lifestreet
				AddSKAdNetwork(skAdNetworkArray, "9t245vhmpl.skadnetwork"); //Moloco
				AddSKAdNetwork(skAdNetworkArray, "m8dbw4sv7c.skadnetwork"); //Dataseat
				AddSKAdNetwork(skAdNetworkArray, "mlmmfzh3r3.skadnetwork"); //Appreciate
				AddSKAdNetwork(skAdNetworkArray, "tl55sbb4fm.skadnetwork"); //PubNative
				AddSKAdNetwork(skAdNetworkArray, "kbd757ywx3.skadnetwork"); //Mintegral
				AddSKAdNetwork(skAdNetworkArray, "f38h382jlk.skadnetwork"); //Chartboost
				AddSKAdNetwork(skAdNetworkArray, "yclnxrl5pm.skadnetwork"); //Jampp
				AddSKAdNetwork(skAdNetworkArray, "hs6bdukanm.skadnetwork"); //Criteo
				AddSKAdNetwork(skAdNetworkArray, "av6w8kgt66.skadnetwork"); //Wildlife
				AddSKAdNetwork(skAdNetworkArray, "8s468mfl3y.skadnetwork"); //RTB House
				AddSKAdNetwork(skAdNetworkArray, "prcb7njmu6.skadnetwork"); //Crossinstall
				AddSKAdNetwork(skAdNetworkArray, "4468km3ulz.skadnetwork"); //Kayzen
				AddSKAdNetwork(skAdNetworkArray, "9rd848q2bz.skadnetwork"); //Mange
				AddSKAdNetwork(skAdNetworkArray, "lr83yxwka7.skadnetwork"); //Apptimus
				AddSKAdNetwork(skAdNetworkArray, "bvpn9ufa9b.skadnetwork"); //Unity Technologie
				AddSKAdNetwork(skAdNetworkArray, "488r3q3dtq.skadnetwork"); //ADTIMING
				AddSKAdNetwork(skAdNetworkArray, "v79kvwwj4g.skadnetwork"); //Kidoz
				AddSKAdNetwork(skAdNetworkArray, "24t9a8vw3c.skadnetwork"); //Cheetah Medialink
				AddSKAdNetwork(skAdNetworkArray, "zmvfpc5aq8.skadnetwork"); //Maiden Marketing
				AddSKAdNetwork(skAdNetworkArray, "44n7hlldy6.skadnetwork"); //Spyke Media
				AddSKAdNetwork(skAdNetworkArray, "3sh42y64q3.skadnetwork"); //Centro
				AddSKAdNetwork(skAdNetworkArray, "5a6flpkh64.skadnetwork"); //REVX TECHNOLOGY PRIVATE LIMITED
				AddSKAdNetwork(skAdNetworkArray, "22mmun2rn5.skadnetwork"); //Pangle (non china)
				AddSKAdNetwork(skAdNetworkArray, "238da6jt44.skadnetwork"); //Pangle (china)
				AddSKAdNetwork(skAdNetworkArray, "su67r6k2v3.skadnetwork"); //Ironsource
				AddSKAdNetwork(skAdNetworkArray, "4pfyvq9l8r.skadnetwork"); //Adcolony
				AddSKAdNetwork(skAdNetworkArray, "cstr6suwn9.skadnetwork"); //Admob
				AddSKAdNetwork(skAdNetworkArray, "4dzt52r2t5.skadnetwork"); //UnityAds
				AddSKAdNetwork(skAdNetworkArray, "ecpz2srf59.skadnetwork"); //Tapjoy
				AddSKAdNetwork(skAdNetworkArray, "5lm9lj6jb7.skadnetwork"); //Loopme
				AddSKAdNetwork(skAdNetworkArray, "578prtvx9j.skadnetwork"); //Unicorn
				AddSKAdNetwork(skAdNetworkArray, "n9x2a789qt.skadnetwork"); //MyTarget
				AddSKAdNetwork(skAdNetworkArray, "v9wttpbfk9.skadnetwork"); //Facebook
				AddSKAdNetwork(skAdNetworkArray, "n38lu8286q.skadnetwork"); //Facebook
				AddSKAdNetwork(skAdNetworkArray, "nu4557a4je.skadnetwork"); //HyperMX
				AddSKAdNetwork(skAdNetworkArray, "v4nxqhlyqp.skadnetwork"); //Maio
				AddSKAdNetwork(skAdNetworkArray, "gta9lk7p23.skadnetwork"); //Vungle
				AddSKAdNetwork(skAdNetworkArray, "ydx93a7ass.skadnetwork"); //Vungle
				AddSKAdNetwork(skAdNetworkArray, "3rd42ekr43.skadnetwork"); //Vungle
				AddSKAdNetwork(skAdNetworkArray, "v72qych5uu.skadnetwork"); //Appier Pte
				AddSKAdNetwork(skAdNetworkArray, "c6k4g5qg8m.skadnetwork"); //Beeswax
				AddSKAdNetwork(skAdNetworkArray, "252b5q8x7y.skadnetwork"); //Brave People
				AddSKAdNetwork(skAdNetworkArray, "klf5c3l5u5.skadnetwork"); //Sift Media
				AddSKAdNetwork(skAdNetworkArray, "dzg6xy7pwj.skadnetwork"); //Singular Labs
				AddSKAdNetwork(skAdNetworkArray, "y45688jllp.skadnetwork"); //Singular Labs
				AddSKAdNetwork(skAdNetworkArray, "hdw39hrw9y.skadnetwork"); //Smadex
				AddSKAdNetwork(skAdNetworkArray, "ppxm28t8ap.skadnetwork"); //Singular Labs
				AddSKAdNetwork(skAdNetworkArray, "f73kdq92p3.skadnetwork"); //Spotad
				AddSKAdNetwork(skAdNetworkArray, "5l3tpt7t6e.skadnetwork"); //StartApp
				AddSKAdNetwork(skAdNetworkArray, "uw77j35x4d.skadnetwork"); //Triapodi
				AddSKAdNetwork(skAdNetworkArray, "44jx6755aq.skadnetwork"); //Chartboost 2
				AddSKAdNetwork(skAdNetworkArray, "737z793b9f.skadnetwork"); //Chartboost 5
				AddSKAdNetwork(skAdNetworkArray, "cj5566h2ga.skadnetwork"); //Chartboost 12
				AddSKAdNetwork(skAdNetworkArray, "glqzh8vgby.skadnetwork"); //Chartboost 14
				AddSKAdNetwork(skAdNetworkArray, "w9q455wk68.skadnetwork"); //Chartboost 21
				AddSKAdNetwork(skAdNetworkArray, "wg4vff78zm.skadnetwork"); //Chartboost 22
				AddSKAdNetwork(skAdNetworkArray, "7rz58n8ntl.skadnetwork"); //Adcolony 22
				AddSKAdNetwork(skAdNetworkArray, "ejvt5qm6ak.skadnetwork"); //Adcolony 23
				AddSKAdNetwork(skAdNetworkArray, "mtkv5xtk9e.skadnetwork"); //Adcolony 26
				AddSKAdNetwork(skAdNetworkArray, "mls7yz5dvl.skadnetwork"); //Smaato Net
				AddSKAdNetwork(skAdNetworkArray, "8m87ys6875.skadnetwork"); //Smaato Net
				AddSKAdNetwork(skAdNetworkArray, "97r2b46745.skadnetwork"); //Smaato Net
				AddSKAdNetwork(skAdNetworkArray, "cg4yq2srnc.skadnetwork"); //Smaato Net
				AddSKAdNetwork(skAdNetworkArray, "6xzpu9s2p8.skadnetwork"); //Mintegral Net
				AddSKAdNetwork(skAdNetworkArray, "424m5254lk.skadnetwork"); //Snapchat

				plist.root = rootDict;
				plist.WriteToFile(plistPath);
				File.WriteAllText(projPath, proj.WriteToString());
			}
			/*else if(report.summary.platform == BuildTarget.Android)
			{
				if (String.IsNullOrEmpty(AndroidAdmobID)) throw new ConfigurationException("YOU DIDN'T PUT YOUR ADMOB ID");
				var rootFolders = Directory.GetDirectories(pathToBuiltProject);
				var currentPath = Directory.GetCurrentDirectory();
				if(rootFolders.Length == 0) throw new SystemException(GetType() + ": No Android project folder found");
            
				var projectFolderPath = rootFolders[0] + "/";
				var manifestFile = projectFolderPath + "IronSource/AndroidManifest.xml";
            
				XmlDocument doc = new XmlDocument();
				doc.Load(manifestFile);

				var app = doc.LastChild.LastChild;
				var admobNode =  doc.CreateElement( "meta-data");

				admobNode.SetAttribute("name", "http://schemas.android.com/apk/res/android", "com.google.android.gms.ads.APPLICATION_ID");
				admobNode.SetAttribute("value", "http://schemas.android.com/apk/res/android", AndroidAdmobID);

				app.AppendChild(app.OwnerDocument.ImportNode(admobNode, true));

				doc.Save(manifestFile);
			}*/

			Debug.Log("IS POSTPROCESS DONE");
		}

		private static void AddSKAdNetwork ( PlistElementArray array, string id )
		{
			array.AddDict().SetString("SKAdNetworkIdentifier", id);
		}
	}

}